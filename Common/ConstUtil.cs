﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 系统常量定义
    /// </summary>
    public class ConstUtil
    {
        public const string  SALT = "!,@.#;$("; //加密盐值
        public const string LOGIN_SALT = "~~&D6>?/";


        //--------- 查询条件操作符 start ---------------------//
        public const string GT = ">";
        public const string LT = "<";
        public const string EQ = "=";
        public const string LIKE = "like";
        public const string LIKE_ST = "like";
        public const string LIKE_ED = "like";
        public const string EGT = ">=";
        public const string LGT = "<=";
        public const string NEQ = "<>";
        public const string DATESRT_EQ_DAY = "DATESRT_EQ_DAY";
        public const string DATESRT_EGT_DAY = "DATESRT_EGT_DAY";
        public const string DATESRT_LGT_DAY = "DATESRT_LGT_DAY";
        public const string STATIC_STR = "STATIC_STR";
        //--------- 查询条件操作符 end ---------------------//

        //--------- 流水账类型 start ---------------------//
        //引用DataDictionaryID
        public const int JOURNAL_LZ = 36;  //靈氣
        public const int JOURNAL_BTT = 37;  //BTT
        public const int JOURNAL_BTD = 38;  //BTD
        public const int JOURNAL_TJSY = 39;  //推荐收益

        public const int JOURNAL_TDJ = 86;  //团队奖
        public const int JOURNAL_CSJ = 87;  //城市奖
        public const int JOURNAL_FT = 88;  //复投
        public const int JOURNAL_BTZ = 89;  //络绎阁
        public const int JOURNAL_LYSY = 92;  //领养收益
        public const int JOURNAL_ZTJX = 93;  //直推加息比例
        public const int JOURNAL_SCSF = 94;  //锁仓释放
        public const int JOURNAL_SCSY = 95;  //锁仓收益

        //--------- 流水账类型 end ---------------------//

        //--------- 会员等级 start ---------------------//
        //引用DataDictionaryID
        public const int ULEVEL_PK = 3;        //普卡
        public const int ULEVEL_YK = 4;        //银卡
        public const int ULEVEL_JK = 5;        //金卡

        //--------- 会员等级 end ---------------------//

        //--------- 转账类型 start ---------------------//
        //引用DataDictionaryID
        public const int TRANSFER_SF_JJ_TO_DZ = 60;        //自身奖金币转电子币
        public const int TRANSFER_OT_LZ_TO_LZ = 61;        //靈氣转其他会员靈氣

        //--------- 转账类型 end ---------------------//

        //--------- 充值类型 start ---------------------//
        //引用DataDictionaryID
        public const int CHARGE_BANK = 63;        //银行汇款
        public const int CHARGE_YFB = 64;         //易付宝
        public const int CHARGE_ZFB = 65;         //支付宝
        public const int CHARGE_WX = 66;          //微信

        //--------- 充值类型 end ---------------------//

        //--------- 文章类型 start ---------------------//
        //引用DataDictionaryID
        public const int NEWS_GG = 63;           //公告
        public const int NEWS_ZCXY = 64;         //注册协议
        public const int NEWS_CFFA = 65;         //财富方案
        public const int NEWS_SJHT = 85;        //商家合同
        public const int NEWS_DBGZ = 100;        //夺宝规则

        //--------- 文章类型 end ---------------------//

        //超级管理员编码
        public const string SUPER_ADMIN = "su.admin";

        //PC前台报单中心管理菜单ID
        public const string MENU_SHOP = "10105";
        //手机前台报单中心管理菜单ID
        public const string WAP_MENU_SHOP = "10204";

        //--------- 短信通知code标识 start ---------------------//

        //根据此code 查找MobileNotice表中配置
        public const string MOBILE_NOTICE_OPEN_MEMBER = "openMember";   
        public const string MOBILE_NOTICE_TRANSFER = "transfer";         
        public const string MOBILE_NOTICE_TX_BEFORE = "tixianBefore";  
        public const string MOBILE_NOTICE_TX_AFTER = "tixianAfter";    
        public const string MOBILE_NOTICE_ADD_SHOP = "addShop";            
        public const string MOBILE_NOTICE_CHONG_ZHI = "chongzhi";  
        public const string MOBILE_NOTICE_ADMIN_EMAIL = "adminEmail";     
        public const string MOBILE_NOTICE_SEND_ORDER = "sendOrder";  

        //--------- 短信通知code标识 end ---------------------//

        //--------- 转账参数编码 start ---------------------//
        //根据code查找ParameterSet
        public const string TRANSFER_MIN = "transferMin";  //转账金额最小值
        public const string TRANSFER_BEI = "transferBei";  //转账金额必须是xx的倍数

        //--------- 转账参数编码 end ---------------------//

        //--------- 提现参数编码 start ---------------------//
        //根据code查找ParameterSet
        public const string TX_FEE = "txfee";  //提现手续费比例
        public const string TX_BEI = "txbei";  //提现金额必须是xx的倍数
        public const string TX_MIN = "txmin";  //提现最小金额

        //--------- 提现参数编码 end ---------------------//
    }
}
