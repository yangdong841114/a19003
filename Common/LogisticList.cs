﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 物流查询结果明细
    /// </summary>
    [Serializable]
    public class LogisticList : DtoData
    {
        //时间
        public DateTime? datetime { get; set; }

        //描述
        public string remark { get; set; }

        public string zone { get; set; }

    }
}
