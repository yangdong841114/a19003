﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 身份证实名认证结果
    /// </summary>
    [Serializable]
    public class idcardResult : DtoData
    {
        //真实姓名
        public string realname { get; set; }

        //身份证号
        public string idcard { get; set; }

        //匹配详情,1匹配,2不匹配
        public string res { get; set; }

    }
}
