﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 火币网市场行情
    /// </summary>
    [Serializable]
    public class Hbmarketdetail : DtoData
    {
        public string amount { get; set; }
        public string ts { get; set; }
        public string id { get; set; }
        public string price { get; set; }
        public string direction { get; set; }
    }
}
