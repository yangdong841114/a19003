﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// 添加查询参数工具类
    /// </summary>
    public class ParamUtil
    {
        private List<DbParameterItem> list = null;

        public ParamUtil()
        {
            list = new List<DbParameterItem>();
        }

        /// <summary>
        /// 添加一个DbParameterItem参数
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public ParamUtil Add(DbParameterItem db)
        {
            list.Add(db);
            return this;
        }

        /// <summary>
        /// 获取参数列表
        /// </summary>
        /// <returns></returns>
        public List<DbParameterItem> Result()
        {
            return list;
        }

        /// <summary>
        /// 获取当前对象
        /// </summary>
        /// <returns></returns>
        public static ParamUtil Get()
        {
            return new ParamUtil();
        }
    }
}
