﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html lang="zh">
<head runat="server">
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>会员登录</title>
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/css/style.css" rel="stylesheet" type="text/css" />
    <script src="/Content/js/jquery-3.2.1.min.js" id="main"></script>
    <script type="text/javascript" src="/Content/js/zui.min.js"></script>
    <script type="text/javascript" src="/Content/js/jquery.flexslider-min.js"></script>
        <script type="text/javascript">
            $(function () {
                $(".banner").flexslider({
                    slideshowSpeed: 3500,
                    directionNav: false,
                    pauseOnAction: false,
                    animation: "fade",
                    controlNav: false
                });
                //读取版权、网站信息
                $.ajax({
                    url: "/Common/GetSetMsg",
                    type: "GET",
                    success: function (result) {
                        if (result.status == "success") {
                            var dto = result.result;
                            $("#bqxx").html(dto.copyright);
                            //网站名称
                            $(document).attr("title", dto.sitename + "-会员登录");
                        }
                    }
                });

                //回车键绑定
                document.onkeydown = function (event) {
                    var e = event || window.event || arguments.callee.caller.arguments[0];
                    if (e && e.keyCode == 13) {
                        login();
                    }
                };
            });

            login = function () {
                if ($("#userId").val() == 0) {
                    $(".errImg").css("display", "block");
                    $(".errorword").html("用户名不能为空");
                } else if ($("#password").val() == 0) {
                    $(".errImg").css("display", "block");
                    $(".errorword").html("密码不能为空");
                } else {
                    $(".errImg").css("display", "none");
                    $(".errorword").html("");
                    $.ajax({
                        url: "/Common/MemberLogin",
                        type: "POST",
                        data: "userId=" + $("#userId").val() + "&password=" + $("#password").val(),
                        success: function (result) {
                            if (result.status == "fail") {
                                $(".errImg").css("display", "block");
                                $(".errorword").html("&nbsp;&nbsp;" + result.msg);
                            } else {
                                location.href = "/User/index.html";
                            }
                        }
                    });
                    //$("#loginForm").submit();
                }
            }
    </script>
</head>
<body>
    <div class="header">
        <img src="/Content/images/logo.png" />
    </div>
    <div class="banner">
        <ul class="slides">
            <li style="background: url(/Content/images/banner1.jpg) no-repeat center top;"></li>
            <li style="background: url(/Content/images/banner2.jpg) no-repeat center top;"></li>
        </ul>
        <div class="loginbox">
            <div class="logincont">
                <div class="loginlogo">
                    <img src="/Content/images/loginlogo.png" />
                </div>
                <div class="logintext">
                    <div class="logintextbox">
                        <div class="logininputtext col-lg-12">
                            <input type="text" class="form-control input-lg textboxlogin" id="userId" placeholder="请输入用户名">
                            <i class="icon icon-user"></i>
                        </div>
                        <div class="logininputtext col-lg-12">
                            <input type="password" class="form-control input-lg textboxlogin" id="password" placeholder="请输入密码">
                            <i class="icon icon-lock"></i>
                        </div>
                        <div class="clearfix"></div>
                        <div style="height:28px; text-align:center;vertical-align:middle;width:100%;padding-left:10px;">
                            <table>
                                <tr>
                                    <td>
                                        <img src="/Content/admin/images/ico_wrong.png" class="errImg" alt="" style="display: none;">
                                        
                                    </td>
                                    <td>&nbsp;&nbsp;<span class="errorword" style="color:red;"></span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="logininputtext loginbtn">
                            <div class="col-lg-6">
                                <button class="btn btn-lg btn-warning btn-block" onclick="login()" type="button">登 录</button>
                            </div>
                            <div class="col-lg-6">
                                <button class="btn btn-lg btnnoborder btn-block hl-primary" type="button" onclick="javascript:location.href='/ForgetPass/Index';">忘记密码</button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <%--<div class="">
                            <div class="col-lg-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox">
                                        记住我
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-6 ">
                                <div class="pull-right checkbox"><a href="#" class="text-gray">忘记密码？</a></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="copyright text-gray" id="bqxx">版权信息</div>
    </div>
     <div  style="display:none;">
 <script type="text/javascript">
     <%: @Html.Raw(ViewData["WebCode"])%>
    </script>
    </div>   
</body>
</html>