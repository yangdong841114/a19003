﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<%--<html lang="zh">--%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>登錄</title>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet">
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/css/zui.min.css" rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>

    <link type="text/css" rel="stylesheet" href="/Content/APP/User/img_ver/css/style.css">
    <script src="/Content/APP/User/img_ver/js/jquery-1.10.2.js"></script>
    <script src="/Content/APP/User/scniu/jq_slideImage.js" type="text/javascript" charset="utf-8"></script>
   
       
 


</head>
<body class="loginbg">
      <div class="loginenter" id="div_loginenter">
    <div class="loginlogo">
       <img src="/Content/APP/User/images/logo1.png" />
       <p>络绎阁</p>
    </div>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconuser.png" /></dt>
            <dd>
                <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" ></span>
                <input type="text" class="entertxt" id="phone" value="<%:ViewData["rem_phone"]%>"  placeholder="請輸入手機" />
            </dd>
        </dl>
        <dl>
            <dt>
                <img src="/Content/APP/User/images/iconpassword.png" /></dt>
            <dd> <span class="erase" style="display:none"><img src="/Content/APP/User/images/btnclose.png" ></span>
                <input type="password" class="entertxt" value="<%:ViewData["rem_pwd"]%>" id="password" placeholder="請登錄密碼" />
            </dd>
        </dl>
         <div class="loginset">
            <label>
                <input type="checkbox" value="remember" id="remember" />記住我</label>
            <a href="/ForgetPass/Index">忘記密碼？</a>
            <div class="clear"></div>
        </div>
        <div class="lbtnbox">
            <button class="bigbtn" onclick="showImgVer()">登錄</button>
            <div  style="text-align:center; margin-top:10px;">
            <p><a style="color:#ffc41f;" href="/Home/Reg/system">註冊新用戶</a></p>
        </div>
        </div>
        
    </div>

      <div class="mesbackbg"></div>
    <div id="slideImageWrap" class="loginverify"></div>





     <div  style="display:none;">
    <script type="text/javascript">
        <%-- <%: @Html.Raw(ViewData["WebCode"])%>--%>
        var cnzz_protocol = (("https:" == document.location.protocol) ? "https://" : "http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1277830778'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s23.cnzz.com/z_stat.php%3Fid%3D1277830778%26show%3Dpic' type='text/javascript'%3E%3C/script%3E"));
        cnzz_protocol = cnzz_protocol.replace(/&quot;/g, '"');
    </script>
          </div>
</body>
<script type="text/javascript">
    $(document).ready(function () {
        $(".erase").click(function () {
            $(this).next().val('');
            $(this).hide();
        });
        $("input[class='entertxt']").keyup(function () {
            //$(this).prev().show();
            if ($(this).val().length > 0) {
                $(this).prev().show();
            } else {
                $(this).prev().hide();
            }
        });
    });

    //显示错误消息
    showErrorMsg = function (msg) {
        var msgbox = new $.zui.Messager('提示消息：' + msg, {
            type: 'danger',
            icon: 'warning-sign',
            placement: 'center',
            parent: 'body',
            close: true
        });
        msgbox.show();
    }

    //登录校验
    login = function () {
        if ($("#phone").val() == 0) {
            showErrorMsg("用戶名不能為空");
        } else if ($("#password").val() == 0) {
            showErrorMsg("密碼不能為空");
        }
        else {
            var data = "phone=" + $("#phone").val() + "&password=" + $("#password").val();
            if (document.getElementById("remember").checked) {
                data += "&remenber=1";
            } else {
                data += "&remenber=0";
            }
            $.ajax({
                url: "/Common/MemberLoginByPhone",
                type: "POST",
                data: data,
                success: function (result) {
                    if (result.status == "fail") {
                       // showErrorMsg(result.msg);
                        alert(result.msg);
                        $(".mesbackbg").toggleClass("mesbackbgs");
                        $(".loginverify").toggleClass("loginverifys");
                        mySlideImage.resetSlide();
                    } else {
                        location.href = "/wap/User/index.html";
                    }
                }
            });
            //$("#loginForm").submit();
        }

        



    }

    //滑块验证
    showImgVer = function () {
        //document.getElementById("div_loginenter").style.display = "none";
        //document.getElementById("slideImageWrap").style.display = "block";
        $(".mesbackbg").toggleClass("mesbackbgs");
        $(".loginverify").toggleClass("loginverifys");
    }
    var mySlideImage = new SlideImageVerify('#slideImageWrap', {
        slideImage: ['/Content/APP/User/scniu/image/a1.png', '/Content/APP/User/scniu/image/a2.png', '/Content/APP/User/scniu/image/a3.png'],
        slideAreaNum: 5,
        refreshSlide: true,
        getSuccessState: function (res) {
            document.getElementById("div_loginenter").style.display = "block";
            //document.getElementById("slideImageWrap").style.display = "none";
          
            login();
        }
    })
    window.onresize = function (ev) {
        mySlideImage.resizeSlide();
    }
    mySlideImage.resizeSlide();
   

</script>

</html>
