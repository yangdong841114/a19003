
define(['text!MemberTj.html', 'jquery'], function (MemberTj, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("会员统计")
        var inputs = {};
        appView.html(MemberTj);

        var indexChecked = [0, 0];
        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
        }
        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
        }
        //省市区选择
        //var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
        //    selectProvince(data);
        //    indexChecked = indexArr;
        //}, initPosition);
        //var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
        //    selectProvince(data);
        //    indexChecked = indexArr;
        //}, initPosition);
        //查询按钮
        $("#searchBtn").on("click", function () {
            utils.AjaxPostNotLoadding("/Admin/BTZfb/MemberTj", { province: $("#province").val(), city: $("#city").val() }, function (result) {
                //初始化赋值
                var html_bb = result.result.imgUrl;
                $("#div_bb").html(html_bb);

            });

        })

       
        

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
          
        };
    };

    return controller;
});