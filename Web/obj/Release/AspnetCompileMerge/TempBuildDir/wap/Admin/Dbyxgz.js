
define(['text!Dbyxgz.html', 'jquery'], function (Dbyxgz, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("夺宝游戏规则");

        utils.AjaxPostNotLoadding("/Admin/Registerprotocol/InitViewDbyxgz", {}, function (result) {
            if (result.status == "success") {
                appView.html(Dbyxgz);

                var editor = new Quill("#editor", {
                    modules: {
                        toolbar: utils.getEditorToolbar()
                    },
                    theme: 'snow'
                });
                var cont = "";
                if (result.result) {
                    utils.setEditorHtml(editor, result.result.content)
                } 

                $("#saveBtn").on("click", function () {
                    if (editor.getText() == 0) {
                        utils.showErrMsg("请输入内容");
                    } else {
                        utils.AjaxPost("/Admin/Registerprotocol/SaveByUeditorDbyxgz", { content: utils.getEditorHtml(editor) }, function (result) {
                            if (result.status == "success") {
                                utils.showSuccessMsg("保存成功");
                            } else {
                                utils.showErrMsg(result.msg);
                            }
                        });
                    }
                })
            }
        });

        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});