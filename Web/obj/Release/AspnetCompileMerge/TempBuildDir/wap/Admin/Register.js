
define(['text!Register.html', 'jquery'], function (Register, $) {

    var controller = function (agm) {
        //设置标题
        $("#title").html("会员注册");
        appView.html(Register);
        var pmap = null;


        //初始化默认数据
        setDefaultData = function (map) {
            if (map.loginPass && map.loginPass != null) {
                $("#password").val(map.loginPass);
                $("#passwordRe").val(map.loginPass);
                $("#dfpass1").html(map.loginPass);
            }
            if (map.passOpen && map.passOpen != null) {
                $("#passOpen").val(map.passOpen);
                $("#passOpenRe").val(map.passOpen);
                $("#dfpass2").html(map.passOpen);
            }
           
            if (map.defaultReName && map.defaultReName != null) {
                $("#reName").val(map.defaultReName);
            }
            if (map.zcxy && map.zcxy != null) {
                $("#regContent").html(map.zcxy);
            }
           
            if (map.userIdPrefix && map.userIdPrefix != null) {
                $("#qzUserId").html(map.userIdPrefix);
            }
        }

       

        //第一步到第二步
        step1To2 = function () {
            if ($("#userId").val() == 0) {
                utils.showErrMsg("请输入会员编号");
                $("#userId").focus();
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("请输入登陆密码");
                $("#password").focus();
            } else if ($("#passOpen").val() == 0) {
                utils.showErrMsg("请输入安全密码");
                $("#passOpen").focus();
            } else if ($("#threepass").val() == 0) {
                utils.showErrMsg("请输入交易密码");
                $("#threepass").focus();
            } else if ($("#passwordRe").val() != $("#password").val()) {
                utils.showErrMsg("确认登录密码与登录密码不一致");
                $("#passwordRe").focus();
            } else if ($("#passOpenRe").val() != $("#passOpen").val()) {
                utils.showErrMsg("确认安全密码与安全密码不一致");
                $("#passOpenRe").focus();
            } else if ($("#threepassRe").val() != $("#threepass").val()) {
                utils.showErrMsg("确认交易密码与交易密码不一致");
                $("#threepassRe").focus();
            } else {
                $("#setpTab1").css("display", "none");
                $("#setpTab2").css("display", "block");
            }
        }

       

        //第二步到第三步
        step2To3 = function () {
            if ($("#fatherName").val() == 0) {
                utils.showErrMsg("请输入接点人编号");
                $("#fatherName").focus();
            } else if ($("#reName").val() == 0) {
                utils.showErrMsg("请输入推荐人编号");
                $("#reName").focus();
            } else if ($("#shopName").val() == 0) {
                utils.showErrMsg("请输入报单中心编号");
                $("#shopName").focus();
            } else {
                $("#setpTab2").css("display", "none");
                $("#setpTab3").css("display", "block");
            }
        }

        //打开注册协议
        openRegisterWin = function (index) {
            $("#zcxydiv").css("display", "block");
            $("#setpTab1").css("display", "none");
        }

        //同意并注册
        agreeRegister = function (ind) {
            $("#zcxydiv").css("display", "none");
            $("#setpTab1").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //选择左右区
        checkedTreePlace = function (index) {
            $("#treePlace1").removeClass("active");
            $("#treePlace2").removeClass("active");
            if (index == -1) {
                $("#treePlace1").addClass("active");
            } else {
                $("#treePlace2").addClass("active");
            }
        }



        //提交注册按钮
        $("#saveBtn").bind("click", function () {
            var fs = {};
            $("#setpTab1 input").each(function () {
                fs[this.id] = this.value;
            });
          
            if ($("#read")[0].checked) { fs["read"] = "read"; }
            fs["sourceMachine"] = "app";
                    fs["userId"] = $("#qzUserId").html() + "" + fs["userId"];
                    utils.AjaxPost("/Admin/Register/RegisterMember", fs, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            var mm = result.result;
                            regModel = mm;
                            location.href = "#regSuccess";
                        }
                    });
                
            
        });
       




       
        var data = { uid: 0 };
        //加载默认数据
        utils.AjaxPostNotLoadding("/Admin/Register/GetDefaultData", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                pmap = result.map;
                setDefaultData(pmap);


                ////初始化银行帐号下拉框
                //var bankLit = cacheList["UserBank"];
                //utils.InitMobileSelect('#bankName', '开户行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                //    $("#bankName").val(data[0].name);
                //});

                //省市区选择
                var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                //输入框取消按钮
                utils.CancelBtnBind(function (dom) {
                    var prev = dom.prev();
                    if (prev[0].id == "reName" || prev[0].id == "shopName" || prev[0].id == "fatherName") {
                        $("#" + prev[0].id + "" + prev.attr("checkflag")).empty();
                    }
                });
 

                //绑定获离开焦点事件
                $("input").each(function (index, ele) {
                    var dom = $(this);
                    if (dom.attr("checkflag")) {
                        var flag = dom.attr("checkflag");
                        var domId = dom[0].id;
                        dom.bind("blur", function (event) {
                            if (dom.val() && dom.val() != 0) {
                                var userId = dom.val();
                                utils.AjaxPostNotLoadding("/Admin/Register/CheckUserId", { userId: userId, flag: flag }, function (result) {
                                    if (result.status == "fail") {
                                        $("#" + domId + "" + flag).html(result.msg);
                                    } else {
                                        $("#" + domId + "" + flag).html(result.msg);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        controller.onRouteChange = function () {
            //销毁模态窗口
            $('#regContent').dialog("destroy");
        };
    };

    return controller;
});