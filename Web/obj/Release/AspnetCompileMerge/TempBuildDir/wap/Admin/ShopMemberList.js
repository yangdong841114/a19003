
define(['text!ShopMemberList.html', 'jquery'], function (ShopMemberList, $) {

    var controller = function (shopId) {
        //设置标题
        $("#title").html("会员列表")
        appView.html(ShopMemberList);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ShopMemberListdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ShopPassed/GetShopMemberList?shopId="+shopId, param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["passTime"] = utils.changeDateFormat(rows[i]["passTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            rows[i].empty = rows[i].empty == 0 ? "否" : "是";
                            rows[i].isLock = rows[i].isLock == 0 ? "否" : "是";
                            rows[i].isPay = rows[i].isPay == 0 ? "否" : "是";
                            var rr = cacheMap["rLevel"][rows[i].rLevel];
                            rows[i].rLevel = rr ? rr : "无";
                            rows[i].isFt = rows[i].isFt == 0 ? "否" : "是";

                            var dto = rows[i];
                            dto.passTime = dto.passTime ==null?"                   ":dto.passTime;
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.passTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '&nbsp;<span class="sum">' + dto.regMoney + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>推荐人编号</dt><dd>' + dto.reName + '</dd></dl>' +
                            '<dl><dt>会员级别</dt><dd>' + dto.uLevel + '</dd></dl><dl><dt>荣誉级别</dt><dd>' + dto.rLevel + '</dd></dl>' +
                            '<dl><dt>注册金额</dt><dd>' + dto.regMoney + '</dd></dl>' +
                            '<dl><dt>联系电话</dt><dd>' + dto.phone + '</dd></dl><dl><dt>是否复投</dt><dd>' + dto.isFt + '</dd></dl>' +
                            '<dl><dt>是否冻结</dt><dd>' + dto.isLock + '</dd></dl><dl><dt>是否空单</dt><dd>' + dto.empty + '</dd></dl>' +
                            '<dl><dt>操作人</dt><dd>' + dto.byopen + '</dd></dl>' +//<dl><dt>注册日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>开通日期</dt><dd>' + dto.passTime + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ShopMemberListitemList").append(html);
                    }, function () {
                        $("#ShopMemberListitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ShopMemberListitemList").empty();
            param["userId"] = $("#userId").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});