
define(['text!UCharge.html', 'jquery'], function (UCharge, $) {

    var controller = function (name) {

        var isInitTab2 = false;

        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期選擇框
                    utils.initCalendar(["startTime", "endTime"]);

                    //清空查詢條件按鈕
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })

                    //查詢按鈕
                    $("#searchBtn").bind("click", function () {
                        searchMethod();
                    })
                    isInitTab2 = true;
                }

                //加載數據
                searchMethod();
            }
        }

        LzTakecash = function () { utils.showErrMsg("正在籌建中，敬請期待"); };

        $("#title").html("賬戶充值");
        appView.html(UCharge);

        //復制功能
        copyaddress = function () { $("#copytext").focus(); $("#copytext").select(); if (document.execCommand('copy', false, null)) alert('復制成功') };
        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UChargeDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UCharge/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["status"] = rows[i].ispay == 1 ? "待審核" : "已通過";

                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">+' + dto.epoints + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>充值日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>充值金額</dt><dd>' + dto.epoints + '</dd></dl>' +
                                  '<dl><dt>訂單號</dt><dd>' + dto.bankTime + '</dd></dl><dl><dt>' +
                                  '匯款憑證</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="匯款憑證" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                  '<dl><dt>支付方式</dt><dd>' + dto.toBank + '</dd></dl><dl><dt>錢包地址</dt><dd>' + dto.bankCard + '</dd></dl>' +
                                  '<dl><dt>狀態</dt><dd>' + dto.status + '</dd></dl>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#UChargeItemList").append(html);

                        //初始化圖片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#UChargeItemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UChargeItemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //設置表單默認數據
        setDefaultValue = function () {
            $("#sysBankName").val("");
            $("#sysBankId").val("0");
            $("#toBankCard").empty();
            $("#toBankUser").empty();
            $("#fromBank").val("");
            $("#epoints").val("");
            $("#bankTime").val("");
            $("#file").val("");
        }

        //充值金額離開焦點
        $("#epoints").bind("blur", function () {
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            var dom = $(this);
            if (g.test(dom.val())) {
                var intVal = parseInt(dom.val());
                var val = intVal + parseFloat((Math.random()).toFixed(2));
                //dom.val(val);//去掉这个自动生成小数点
                //算出應該付的電子幣是多少
                if ($("#sysBankName").val() == "") {
                    utils.showErrMsg("請選擇支付方式");
                    $("#epoints").val("");
                }
                var epointsDzb = 0;
                if ($("#sysBankName").val() == "ETH")
                    epointsDzb = intVal / $("#ethRMB").val();
                if ($("#sysBankName").val() == "BTT" || $("#sysBankName").val() == "DOGE")
                    epointsDzb = intVal / $("#dogeRMB").val();
                var chargeLzzkje = $("#chargeLzzkje").val();
                if (intVal >= chargeLzzkje) {
                    if ($("#uLevel").val() == "1") epointsDzb = epointsDzb * $("#chargeLzkUlevel1").val();
                    if ($("#uLevel").val() == "2") epointsDzb = epointsDzb * $("#chargeLzkUlevel2").val();
                    if ($("#uLevel").val() == "3") epointsDzb = epointsDzb * $("#chargeLzkUlevel3").val();
                    if ($("#uLevel").val() == "4") epointsDzb = epointsDzb * $("#chargeLzkUlevel4").val();
                    if ($("#uLevel").val() == "5") epointsDzb = epointsDzb * $("#chargeLzkUlevel5").val();
                }
                $("#epointsDzb").html(epointsDzb.toFixed(2))

            }
        })

        utils.AjaxPostNotLoadding("/User/UserWeb/GetPara", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                $("#ethRMB").val(map.ethRMB);
                $("#bttRMB").val(map.bttRMB);
                $("#dogeRMB").val(map.dogeRMB);
                $("#uLevel").val(map.uLevel);
                $("#chargeLzzkje").val(map.chargeLzzkje);
                $("#chargeLzkUlevel1").val(map.chargeLzkUlevel1);
                $("#chargeLzkUlevel2").val(map.chargeLzkUlevel2);
                $("#chargeLzkUlevel3").val(map.chargeLzkUlevel3);
                $("#chargeLzkUlevel4").val(map.chargeLzkUlevel4);
                $("#chargeLzkUlevel5").val(map.chargeLzkUlevel5);

                $("#imgBTTfkmCode").attr("src", map.BTTfkmCode);
                $("#imgETHfkmCode").attr("src", map.ETHfkmCode);

            } else {
                utils.showErrMsg(result.msg);
            }
        });

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UCharge/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                var list = result.list;

                //初始化匯入銀行下拉框
                var bankData = [];
                if (list && list.length > 0) {
                    for (var i = 0; i < list.length; i++) {
                        bankData.push({ id: list[i].id + "|" + list[i].bankCard + "|" + list[i].bankUser, value: list[i].bankName });
                    }
                }
                //匯入銀行選擇框
                utils.InitMobileSelect('#sysBankName', '支付方式', bankData, null, [0], null, function (indexArr, data) {
                    var val = data[0].id;
                    if (val != 0) {
                        var v = val.split("|");
                        $("#toBankCard").html(v[1] + '<a href="javascript:copyaddress();">復制</a>');
                        $("#toBankUser").html(v[2]);
                        $("#copytext").val(v[1]);
                    } else {
                        $("#toBankCard").empty();
                        $("#toBankUser").empty();
                    }
                    $("#sysBankId").val(val);
                    $("#sysBankName").val(data[0].value);

                    document.getElementById("div_img_BTT").style.display = "none";
                    document.getElementById("div_img_ETH").style.display = "none";
                 
                    if ($("#sysBankName").val() == "DOGE") document.getElementById("div_img_BTT").style.display = "block";
                    if ($("#sysBankName").val() == "ETH") document.getElementById("div_img_ETH").style.display = "block";


                    var intVal = $("#epoints").val();
                    //dom.val(val);//去掉这个自动生成小数点
                    //算出應該付的電子幣是多少
                    if ($("#sysBankName").val() == "") {
                        utils.showErrMsg("請選擇支付方式");
                        $("#epoints").val("");
                    }
                    var epointsDzb = 0;
                    if ($("#sysBankName").val() == "ETH")
                        epointsDzb = intVal / $("#ethRMB").val();
                    if ($("#sysBankName").val() == "BTT" || $("#sysBankName").val() == "DOGE")
                        epointsDzb = intVal / $("#dogeRMB").val();
                    var chargeLzzkje = $("#chargeLzzkje").val();
                    if (intVal >= chargeLzzkje) {
                        if ($("#uLevel").val() == "1") epointsDzb = epointsDzb * $("#chargeLzkUlevel1").val();
                        if ($("#uLevel").val() == "2") epointsDzb = epointsDzb * $("#chargeLzkUlevel2").val();
                        if ($("#uLevel").val() == "3") epointsDzb = epointsDzb * $("#chargeLzkUlevel3").val();
                        if ($("#uLevel").val() == "4") epointsDzb = epointsDzb * $("#chargeLzkUlevel4").val();
                        if ($("#uLevel").val() == "5") epointsDzb = epointsDzb * $("#chargeLzkUlevel5").val();
                    }
                    $("#epointsDzb").html(epointsDzb.toFixed(2))
                    //清空
                    ("#epoints").val("");
                    $("#epointsDzb").html("");
                })

                //初始化匯款時間
                //utils.initCalendar(["bankTime"]);

                //初始默認值
                setDefaultValue();

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "sysBankName") {
                            $("#sysBankId").val("");
                            $("#toBankCard").empty();
                            $("#toBankUser").empty();
                        }

                        dom.prev().val("");
                    });
                });


                //*****************************************************銀行匯款 start **********************************************************//

                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();

                });

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#sysBankId").val();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    var isChecked = true;
                    if (val == 0) {
                        utils.showErrMsg("請選擇匯入銀行");
                    } else if (!g.test($("#epoints").val())) {
                        utils.showErrMsg("充值金額格式不正確");
                    } else if ($("#bankTime").val() == 0) {
                        utils.showErrMsg("請選擇訂單號");
                    } else if ($("#file").val() == 0) {
                        utils.showErrMsg("請上傳匯款憑證");
                    } else {
                        $("#czhryh").html($("#sysBankName").val());
                        $("#czkh").html($("#toBankCard").html());
                        $("#czkhm").html($("#toBankUser").html());
                        $("#czje").html($("#epoints").val());
                        utils.showOrHiddenPromp();
                    }
                });

                //確認保存
                $("#sureBtn").bind("click", function () {
                    var formdata = new FormData();
                    var bankId = $("#sysBankId").val().split("|")[0];
                    formdata.append("sysBankId", bankId);
                    formdata.append("fromBank", $("#fromBank").val());
                    formdata.append("epoints", $("#epoints").val());
                    formdata.append("passOpen", $("#passOpen").val());
                    formdata.append("bankTime", $("#bankTime").val());
                    formdata.append("epointsDzb", $("#epointsDzb").html());
                    formdata.append("accounttypeId", $("#accounttypeId").val());
                    formdata.append("img", $("#file")[0].files[0]);
                    utils.AjaxPostForFormData("/User/UCharge/SaveCharge", formdata, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                            //grid.datagrid("reload");
                        }
                    });
                });

                //*****************************************************銀行匯款 end **********************************************************//
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});