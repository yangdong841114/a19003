
define(['text!UsyzBTZ.html', 'jquery'], function (UsyzBTZ, $) {

    var controller = function (name) {
        appView.html(UsyzBTZ);
        var isInitTab2 = false;

        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn1").addClass("active")
            } else {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                if (!isInitTab2) {
                    //初始化日期選擇框
                    utils.initCalendar(["startTime", "endTime"]);
                    //綁定展開搜索更多
                    utils.bindSearchmoreClick();

                    //清空查詢條件按鈕
                    $("#clearQueryBtn").bind("click", function () {
                        utils.clearQueryParam();
                    })
                    //查詢條件轉賬類型選擇框
                    var transfer = [{ id: 39, name: "推薦收益" }, { id: 86, name: "團隊獎" }, { id: 87, name: "城市獎" }, { id: 88, name: "復投" }];
                    transfer.splice(0, 0, { id: 0, name: '全部' });
                    utils.InitMobileSelect('#accountIdName2', '轉賬類型', transfer, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                        $("#accountIdName2").val(data[0].name);
                        $("#accountId2").val(data[0].id);
                    })


                    isInitTab2 = true;
                }

                //加載數據
                searchMethod();
            }
        }

        //設置標題
        $("#title").html("收益轉比特豬");


        var dto = null;

        //轉賬明細
        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UMTransferdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPagesyzBTZ", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["accountId"] = cacheMap["JournalClass"][rows[i].accountId];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time><span class="sum">' + dto.accountId + '</span>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>品種</dt><dd>' + dto.BTZName + '</dd></dl>' +
                                  '<dl><dt>價值</dt><dd>' + dto.Cszz + '</dd></dl>' +
                                  '</div></li>';
                        }
                        $("#UMTransferitemList").append(html);
                    }, function () {
                        $("#UMTransferitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UMTransferitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["accountId"] = $("#accountId2").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //設置表單默認數據
        setDefaultValue = function (dto) {
            $("#agentTjsy").html(dto.account.agentTjsy);
            $("#agentTdj").html(dto.account.agentTdj);
            $("#agentCsj").html(dto.account.agentCsj);
            $("#agentFt").html(dto.account.agentFt);
        }
        //查詢按鈕
        $("#searchBtn").bind("click", function () {
            searchMethod();
        })

        //获取可转让各类型比特猪列表
        var btzlist_agentTjsy;
        var btzlist_agentTdj
        var btzlist_agentCsj;
        var btzlist_agentFt;

        utils.AjaxPostNotLoadding("/User/UMTransfer/GetBtzData", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                btzlist_agentTjsy = map.btzlist_agentTjsy;
                btzlist_agentTdj = map.btzlist_agentTdj;
                btzlist_agentCsj = map.btzlist_agentCsj;
                btzlist_agentFt = map.btzlist_agentFt;

                utils.InitMobileSelect('#BTZName_agentTjsy', '选择比特猪', btzlist_agentTjsy, { id: "id", value: "Gzzzxx" }, [0], null, function (indexArr, data) {
                    $("#BTZName_agentTjsy").val(data[0].Gzzzxx);
                    $("#Btzid").val(data[0].id);
                });
                utils.InitMobileSelect('#BTZName_agentTdj', '选择比特猪', btzlist_agentTdj, { id: "id", value: "Gzzzxx" }, [0], null, function (indexArr, data) {
                    $("#BTZName_agentTdj").val(data[0].Gzzzxx);
                    $("#Btzid").val(data[0].id);
                });
                utils.InitMobileSelect('#BTZName_agentCsj', '选择比特猪', btzlist_agentCsj, { id: "id", value: "Gzzzxx" }, [0], null, function (indexArr, data) {
                    $("#BTZName_agentCsj").val(data[0].Gzzzxx);
                    $("#Btzid").val(data[0].id);
                });
                utils.InitMobileSelect('#BTZName_agentFt', '选择比特猪', btzlist_agentFt, { id: "id", value: "Gzzzxx" }, [0], null, function (indexArr, data) {
                    $("#BTZName_agentFt").val(data[0].Gzzzxx);
                    $("#Btzid").val(data[0].id);
                });
            }
            else {
                utils.showErrMsg(result.msg);
            }
        });

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UMTransfer/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {

               


                InitBtzList = function () {
                    document.getElementById("BTZName_agentTjsy").style.display = "none";
                    document.getElementById("BTZName_agentTdj").style.display = "none";
                    document.getElementById("BTZName_agentCsj").style.display = "none";
                    document.getElementById("BTZName_agentFt").style.display = "none";
                  
                    //各类型帐户可转比特猪列表切换
                    if ($("#accountId").val() == "39") document.getElementById("BTZName_agentTjsy").style.display = "block";
                    if ($("#accountId").val() == "86") document.getElementById("BTZName_agentTdj").style.display = "block";
                    if ($("#accountId").val() == "87") document.getElementById("BTZName_agentCsj").style.display = "block";
                    if ($("#accountId").val() == "88") document.getElementById("BTZName_agentFt").style.display = "block";
                }

                dto = result.result;
                var transfer = [{ id: 39, name: "推薦收益" }, { id: 86, name: "團隊獎" }, { id: 87, name: "城市獎" }, { id: 88, name: "復投" }];
                //轉賬類型選擇框
                utils.InitMobileSelect('#accountIdName', '轉賬類型', transfer, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#accountIdName").val(data[0].name);
                    $("#accountId").val(data[0].id);
                    InitBtzList();
                })
                InitBtzList();

              



                //初始默認值
                setDefaultValue(dto);



                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //保存
                $("#saveBtn").on('click', function () {
                    utils.showOrHiddenPromp();
                })

                //確認按鈕
                $("#sureBtn").on('click', function () {
                    var data = { accountId: $("#accountId").val(),BTZid: $("#Btzid").val() };
                    utils.AjaxPost("/User/Uzrmx/SavesyzBTZ", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            dto = result.result;
                            setDefaultValue(dto);
                        }
                    });
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});