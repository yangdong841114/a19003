
define(['text!MemberPassing.html', 'jquery', 'j_easyui','datetimepicker'], function (MemberPassing, $) {

    var controller = function (name) {
        //设置标题
        $("#center").panel("setTitle", "待开通会员");
        appView.html(MemberPassing);

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#MemberPassing", toUid: 0 }, function () { });

        //根节点类型
        var rootObj = {};

        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });



        //初始化表格
        var grid = utils.newGrid("dg", {
            frozenColumns: [[{ field: 'userId', title: '会员编号', width: '10%' }]],
            columns: [[
             { field: 'reName', title: '推荐人编号',width:'10%'},
             { field: 'shopName', title: '报单中心', width: '10%' },
             { field: 'userName', title: '会员名称', width: '10%' },
             { field: 'regMoney', title: '注册金额', width: '10%' },
             { field: 'uLevel', title: '会员级别', width: '10%' },
             { field: 'phone', title: '联系电话', width: '10%' },
             { field: 'addTime', title: '注册日期', width: '12%' },
             {
                 field: '_operate', title: '操作', width: '210', formatter: function (val, row, index) {
                     return '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildEdit" >编辑</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildOpen1" >开通空单</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildOpen0" >开通实单</a>&nbsp;&nbsp;' +
                     '<a href="javascript:void(0);" memberId="' + row.id + '" class="gridFildDelete" >删除</a>';
                }
             }
            ]],
            url: "MemberPassing/GetListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                    data.rows[i].uLevel = cacheMap["ulevel"][data.rows[i].uLevel];
                }
            }
            return data;
        }, function () {
            //行编辑按钮
            $(".gridFildEdit").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    location.href = "#MemberInfo/" + memberId;
                    return false;
                }
            });
            //行开通空单按钮
            $(".gridFildOpen1").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("该会员确定要开通空单吗？", function () {
                        utils.AjaxPost("MemberPassing/OpenMember", { id: memberId, flag: 1 }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
            //行开通实单
            $(".gridFildOpen0").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("该会员确定要开通实单吗？", function () {
                        utils.AjaxPost("MemberPassing/OpenMember", { id: memberId, flag: 0 }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
            //行删除按钮
            $(".gridFildDelete").each(function (i, dom) {
                dom.onclick = function () {
                    var memberId = $(dom).attr("memberId");
                    utils.confirm("确定要删除该会员吗？", function () {
                        utils.AjaxPost("MemberPassing/Delete", { id: memberId }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg(result.msg);
                                //重刷grid
                                queryGrid();
                            }
                        });
                    });
                    return false;
                }
            });
        })

        //查询grid
        queryGrid = function () {
            var objs = $("#MemberPassingQueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
        }

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "MemberPassing/ExportNotOpenExcel";
        })

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })


        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});