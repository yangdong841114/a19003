﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%:ViewData["sitename"]%></title>
    <style type="text/css">
        .contentbox {
            position: relative;
        }
    </style>
    <link type="text/css" href="/Content/APP/User/css/pstyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="/Content/APP/css/font-awesome.min.css">
    <link href="/Content/APP/css/zui_ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/Content/APP/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="/Content/js/zui.js"></script>
    <link type="text/css" href="/Content/APP/mobileSelect/mobileSelect.css" rel="stylesheet" />
    <script type="text/javascript" src="/Content/APP/mobileSelect/mobileSelect.js"></script>
    <link type="text/css" href="/Content/dropload/dropload.css" rel="stylesheet" />
    <script type="text/javascript" src="/Content/dropload/dropload.min.js"></script>
    <script type="text/javascript">

        //初始化select选择框
        InitMobileSelect = function (selector, title, data, map, position, transition, callbackFn, showback) {
            $(selector)[0].readOnly = true;
            var catSelect = new MobileSelect({
                trigger: selector,
                title: title,
                wheels: [
                            { data: data }
                ],
                keyMap: map,
                position: position, //初始化定位 打开时默认选中的哪个 如果不填默认为0
                transitionEnd: function (indexArr, data) {
                    if (transition) {
                        transition(indexArr, data);
                    }
                },
                callback: function (indexArr, data) {
                    if (callbackFn) {
                        callbackFn(indexArr, data);
                    }
                },
                onShow: function (e) {
                    if (showback) {
                        showback(e);
                    }
                }
            });
        }

        //显示错误信息
        showErrMsg = function (msg) {
            var msgbox = new $.zui.Messager('提示消息：' + msg, {
                type: 'danger',
                icon: 'warning-sign',
                placement: 'center',
                parent: 'body',
                close: true
            });
            msgbox.show();
        }

        //过滤特殊字符
        this.CheckSpecialCharacters = function (str) {
            var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>《》/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
            var rs = "";
            for (var i = 0; i < str.length; i++) {
                rs =
                rs + str.substr(i, 1).replace(pattern, '');
            }
            return rs;
        }

      


       
        //打开注册协议
        openRegisterWin = function (index) {
            $("#zcxydiv").css("display", "block");
            $("#setpTab1").css("display", "none");
        }

        //同意并注册
        agreeRegister = function (ind) {
            $("#zcxydiv").css("display", "none");
            $("#setpTab1").css("display", "block");
            if (ind == 1) {
                $("#read")[0].checked = true;
            }
        }

        var indexChecked = [0, 0, 0];

        ////打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        ////选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        

       
       


       
        $(document).ready(function () {

            if ($('#reName').val() != "") $('#reName').attr("readonly", "readonly");//推荐人号码不为空不让编辑
            var flag_appregAdd = null;
            clearInterval();
            var s_appregAdd = 120;
            show_appregAdd = function () {
                if (s_appregAdd >= 1) {
                    $("#jyfBtn_appregAdd").text("校驗碼已發-" + s_appregAdd + "-秒後可重發");
                } else {
                    clearInterval(flag_appregAdd);
                    s_appregAdd = 120;
                    $("#jyfBtn_appregAdd")[0].disabled = false;
                    $("#jyfBtn_appregAdd").text("獲取校驗碼");
                }
                s_appregAdd--;
            }
            //发短信
            $("#jyfBtn_appregAdd").bind("click", function () {

                $.ajax({
                    url: "/Home/SendPhoneCode",           //请求地址
                    type: "POST",       //POST提交
                    data: { "totalNum": "", "workType": "appregAdd", "picYzm": $("#picYzm").val(), "phoneNo": $("#phone").val() },         //数据参数
                    success: function (result) {
                        if (result.status == "success") {
                            flag_appregAdd = setInterval(show_appregAdd, 1000);
                            $("#jyfBtn_appregAdd")[0].disabled = true;
                        }
                        else
                            showErrMsg(result.msg);

                    }
                });

            });
            //图片验证码变化
            document.getElementById("imgPicYzm_appregAdd").src = "/Common/ManageAppCode?etc=" + (new Date()).getTime();


            $.ajax({
                url: "/Home/GetRegBank",           //请求地址
                type: "POST",       //POST提交
                data: {},         //数据参数
                success: function (result) {
                    $("#regContent").html(result.map["zcxy"]);
                    var areaMap = null;
                    var areaData = null;
                    //区域
                    areaMap = result.map["area"];
                    if (areaMap) {
                        areaData = areaMap[0];
                        for (var i = 0; i < areaData.length; i++) {
                            var dto = areaData[i];
                            if (areaMap[dto.id]) {
                                dto["childs"] = areaMap[dto.id];
                                for (var j = 0; j < dto["childs"].length; j++) {
                                    var child = dto["childs"][j];
                                    if (areaMap[child.id]) {
                                        child["childs"] = areaMap[child.id];
                                    }
                                }
                            }
                        }
                    }

                    //省市区选择
                    var proSet = InitMobileSelect('#province', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);
                    var citySet = InitMobileSelect('#city', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);
                    var areaSet = InitMobileSelect('#area', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                        selectProvince(data);
                        indexChecked = indexArr;
                    }, initPosition);

                    //输入框取消按钮
                    $(".erase").each(function () {
                        var dom = $(this);
                        dom.bind("click", function () {
                            var prev = dom.prev();
                            if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                                $("#province").val("");
                                $("#city").val("");
                                $("#area").val("");
                            } else if (prev[0].id == "reName" || prev[0].id == "shopName" || prev[0].id == "fatherName") {
                                $("#" + prev[0].id + "" + prev.attr("checkflag")).empty();
                            }
                            dom.prev().val("");
                        });
                    });

                    //绑定获离开焦点事件
                    $("input").each(function (index, ele) {
                        var dom = $(this);
                        if (dom.attr("checkflag")) {
                            var flag = dom.attr("checkflag");
                            var domId = dom[0].id;
                            dom.bind("blur", function (event) {
                                if (dom.val() && dom.val() != 0) {
                                    var userId = dom.val();
                                    $.ajax({
                                        url: "/Home/CheckUserId",           //请求地址
                                        type: "POST",       //POST提交
                                        data: { userId: userId, flag: flag },         //数据参数
                                        success: function (result) {
                                            if (result.status == "fail") {
                                                $("#" + domId + "" + flag).html(result.msg);
                                            } else {
                                                $("#" + domId + "" + flag).html(result.msg);
                                            }
                                        },
                                        error: function (event, xhr, options, exc) {
                                            showErrMsg("操作失敗！");
                                        }
                                    });
                                }
                            });
                        }
                    });
                },
                error: function (event, xhr, options, exc) {
                    showErrMsg("操作失敗！");
                }
            });


            $("#saveBtn").bind("click", function () {
                var ptext = /^1(3|4|5|6|7|8|9)\d{9}$/;
                var reg = /^[0-9a-zA-Z]+$/;

                if ($("#userId").val() != 0) {
                    var usercode = CheckSpecialCharacters($("#userId").val());
                    $("#userId").val(usercode);
                }

                if (!reg.test($("#userId").val())) {
                    showErrMsg("玩家編號，只能錄入英文、數字");
                    $("#userId").focus();
                } else if ($("#password").val() == 0) {
                    showErrMsg("請輸入登陸密碼");
                    $("#password").focus();
                } else if ($("#passwordRe").val() != $("#password").val()) {
                    showErrMsg("確認登錄密碼與登錄密碼不壹致");
                    $("#passwordRe").focus();
                } else if ($("#passOpen").val() == 0) {
                    showErrMsg("请输入安全密码");
                    $("#passOpen").focus();
                } else if ($("#passOpenRe").val() != $("#passOpen").val()) {
                    showErrMsg("确认安全密码与安全密码不一致");
                    $("#passOpenRe").focus();
                } else if (!ptext.test($("#phone").val())) {
                    showErrMsg("手機號碼格式錯誤");
                    $("#phone").focus();
                } else if ($("#reName").val() == 0) {
                    showErrMsg("请输入推荐人编号");
                    $("#reName").focus();
                } else if ($("#read")[0].checked == false) {
                    showErrMsg("請勾選註冊協議");
                } else {
                    var fs = {};
                    $("#setpTab1 input").each(function () {
                        fs[this.id] = this.value;
                    });
                    fs["userId"] = $("#qzUserId").html() + "" + fs["userId"];
                    fs["sourceMachine"] = "app";
                    $.ajax({
                        url: "/Home/RegisterMember",           //请求地址
                        type: "POST",       //POST提交
                        data: fs,         //数据参数
                        beforeSend: function () {
                            document.getElementById("saveBtn").disabled = true;
                        },
                        success: function (result) {
                            document.getElementById("saveBtn").disabled = false;
                            if (result.status == "fail") {
                                showErrMsg(result.msg);
                            } else {
                                alert("恭喜妳成功註冊，您的會員編號為：" + result.msg + " 點擊下載安卓版APP");
                                //location.href = "/Home/Index";
                                location.href = "http://www.gooq1839.com/GOOQ.APK";
                            }
                        },
                        error: function (event, xhr, options, exc) {
                            document.getElementById("saveBtn").disabled = false;
                            showErrMsg("操作失敗！");
                        }
                    });
                }
            });
            


        });

    </script>
</head>
<body>
    <header id="headerHtml">
        <div class="membercentre">
            <%--<a href="#main"><i class="homeicon"><img src="/Content/APP/User/images/homew.png" /></i></a>--%>
            <h2 id="title">&nbsp;&nbsp;<%:ViewData["sitename"]%></h2>
        </div>
        <div class="clear"></div>
    </header>
    <main id="center">
        <div class="entryinfo" id="setpTab1" >
            <div>点击右上角按钮，然后在弹出的菜单中，点击在浏览器中打开，即可下载安装</div>
    <input type="hidden" id="regMoney" />
    <input type="hidden" id="regNum" />
    <div class="userdltl" style="font-size: 1.6rem; color: #ecae33;">
        <i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;登錄資料
    </div>
    <dl>
        <dt>會員編號</dt>
        <dd>
            <table style="padding:0rem 1rem 0rem 10rem;">
                <tr>
                    <td style="font-size:1.4rem;" id="qzUserId"><%:ViewData["userIdPrefix"]%></td>
                    <td>
                        <input type="text" class="entrytxt" required="required" placeholder="請輸入會員編號" style="padding:0 0 0 5px;" id="userId" name="userId" emptymsg="用戶名不能為空" />
                        <span class="erase"><i class="fa fa-times-circle-o"></i></span>
                    </td>
                </tr>
            </table>
        </dd>
    </dl>
    <%--<dl>
        <dt>默认登录密码</dt>
        <dd>
            <p><span id="dfpass1"></span></p>
        </dd>
    </dl>--%>
     <dl>
        <dt>登錄密碼</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="請輸入字母+數字組合" id="password" name="password" placeholder="密碼" emptymsg="登錄密碼不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>確認密碼</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="請輸入字母+數字組合" id="passwordRe" name="passwordRe" placeholder="密碼" emptymsg="確認登錄密碼不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <%--<dl>
        <dt>默认安全密码</dt>
        <dd>
            <p><span id="dfpass2"></span></p>
        </dd>
    </dl>--%>
     <dl>
        <dt>安全密碼</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="請輸入字母+數字組合" id="passOpen" name="passOpen" placeholder="密碼" emptymsg="安全密碼不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl>
        <dt>確認安全密碼</dt>
        <dd>
            <input type="password" required="required" class="entrytxt" placeholder="請輸入字母+數字組合" id="passOpenRe" name="passOpenRe" placeholder="密碼" emptymsg="確認安全密碼不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl style="display:none;">
        <dt>推薦人編號</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="reuserId" name="reuserId" value="<%:ViewData["reuserId"]%>"     checkflag="3"   />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl >
        <dt>推薦人號碼</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="reName" name="reName" value="<%:ViewData["reuserId"]%>"     checkflag="3" emptymsg="推薦人號碼不能為空" />
            <span class="erase" style="display:none;"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>
    <dl style="display:none;">
        <dt>推薦人姓名</dt>
        <dd>
            <p><span id="reName3"><%:ViewData["reUserName"]%></span></p>
        </dd>
    </dl>

    <dl>
        <dt>省</dt>
        <dd>
            <input type="text" class="entrytxt area" id="province" name="province" placeholder="請選擇省" readonly emptymsg="請選擇省" />
        </dd>
    </dl>
    <dl>
        <dt>市</dt>
        <dd>
            <input type="text" class="entrytxt area" id="city" name="city" placeholder="請選擇市" readonly emptymsg="請選擇市" />
        </dd>
    </dl>
    <dl>
        <dt>區</dt>
        <dd>
            <input type="text" class="entrytxt area" id="area" name="area" />
        </dd>
    </dl>
            
    <dl>
        <dt>手機號碼</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" placeholder="請輸入手機號碼" id="phone" name="phone" emptymsg="手機號碼不能為空" />
            <span class="erase"><i class="fa fa-times-circle-o"></i></span>
        </dd>
    </dl>

              <dl>
        <dt>驗證碼</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="picYzm" name="picYzm" placeholder="驗證碼" style="width:60%" />
            <img id="imgPicYzm_appregAdd" src="/Common/ManageAppCode" onclick="javascript:this.src = '/Common/ManageAppCode?etc='+ (new Date()).getTime();" style="height:50px; vertical-align:middle;cursor:hand;" />
        </dd>
    </dl>
     <dl>
        <dt>短信驗證碼</dt>
        <dd>
            <input type="text" required="required" class="entrytxt" id="yzm" name="yzm" placeholder="短信驗證碼" style="width:50%" />
            <button id="jyfBtn_appregAdd" class="smallbtn" style="width:45%" value="獲取校驗碼">獲取校驗碼</button>
           
       </dd>
    </dl>

    <dl>
        <dt></dt>
        <dd style="text-align:center;padding-left:20%;padding-top:1.2rem;padding-bottom:3rem">
            <table>
                <tr>
                    <td><input type="checkbox" id="read" name="read" value="read" style="width:2rem;height:2rem;" />&nbsp;</td>
                    <td style="font-size:1.4rem;">我已閱讀並同意</label><a href="javascript:openRegisterWin(1)" style="color:white;">《註冊協議》</a>
                        

                    </td>
                </tr>
                <tr>
                    <td colspan="2"  style="font-size:1.6rem;">  <a href="http://www.gooq1839.com/GOOQ.APK" style="color:red;" target="_blank">下載安卓版APP</a></td>
                </tr>
            </table>
        </dd>
    </dl>
 
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga1">
            <li><button class="bigbtn" style="width:98%;" id="saveBtn">提交註冊</button></li>
        </ul>
        <div class="clear"></div>
    </div>
     
</div>



<style type="text/css">
    .cont {
        font-size: 1.4rem;
        max-width: 100%;
        width: 100%;
        padding: 8px 8px 8px 8px;
        /*overflow-x: hidden;*/
        box-sizing: border-box;
        overflow-y: auto;
        padding-top: 10px;
        color:black;
        background-color: #fff;
    }

        .cont img {
            max-width: 100%;
        }

    .h1 {
        font-size: 2rem;
        text-align: center;
        font-weight: normal;
        margin-bottom: 1rem;
        color: #333;
        padding-left: 8px;
        padding-right: 8px;
    }
</style>

<div id="zcxydiv" style="display:none;">
    <div id="regContent" class="cont" >
        
    </div>
    <div class="replybox" style="bottom:0rem;">
        <ul class="tga2">
             <li><button class="cancelbtn" onclick="agreeRegister(0)" style="width:90%;">關閉</button></li>
            <li><button class="bigbtn" style="width:90%;" onclick="agreeRegister(1)">同意，並註冊</button></li>
        </ul>
    </div>
</div>

    </main>

</body>
</html>

