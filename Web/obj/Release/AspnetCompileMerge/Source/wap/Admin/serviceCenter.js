
define(['text!serviceCenter.html', 'jquery'], function (serviceCenter, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("比特猪");
        appView.html(serviceCenter);
        var ss_count=0;
        utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetNotic", {}, function (result) {
            if (result.status == "success") {
                var map = result.map;
                var waitSsglList = map["BTZBuyss"];
                ss_count = waitSsglList.length;


                //原有菜单HTML
                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("Admin/", "");
                        if (node.isShow == 0) {
                            if (node.resourceName == "申诉管理")
                                banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '<span>' + ss_count + '</span></p></a></li>';
                            else
                                banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                        }
                    }
                    $("#serviceCenter").html(banHtml);
                }
                //原有菜单HTML

                } else {utils.showErrMsg(result.msg);}
         });


       

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});