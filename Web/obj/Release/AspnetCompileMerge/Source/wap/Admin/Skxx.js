
define(['text!Skxx.html', 'jquery'], function (Skxx, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("收款信息")
        appView.html(Skxx);

        var dto = null;
        var editList = {};

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        var isSkshList = [{ id: 100, name: "全部" }, { id: 0, name: "未审核" }, { id: 1, name: "已审核" }, { id: 2, name: "未通过" }];
        utils.InitMobileSelect('#isSkshName', '选择状态', isSkshList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#isSkshName").val(data[0].name);
            $("#isSksh").val(data[0].id);
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });
        //初始化日期选择框
        utils.initCalendar(["passStartTime", "passEndTime"]);

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

     
        //删除按钮
        shRecord = function (id) {
            $("#prompTitle").html("确定审核吗？");
            $("#sureBtn").unbind();
            $("#prompCont").html("");
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/isSksh", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("审核成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        bhRecord = function (id) {
            $("#prompTitle").html("确定驳回吗？");
            $("#sureBtn").unbind();
            var html = '<dl><dt>驳回原因</dt><dd><input type="text" class="entrytxt" id="refuseSksh" placeholder="请输入驳回原因" /></dd></dl>';
            $("#prompCont").html(html);
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/isSkshBh", { id: id, refuseSksh: $("#refuseSksh").val() }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("驳回成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

       
        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Memdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberPassed/GetListPageSkxx", param, me,
                    function (rows, footers) {
                        var lightboxArray = []; //需要初始化的图片查看ID
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            var dto = rows[i];
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var show_skIsbank= rows[i].skIsbank == 1 ? "是" : "否";
                            var show_skIszfb = rows[i].skIszfb == 1 ? "是" : "否";
                            var show_skIswx = rows[i].skIswx == 1 ? "是" : "否";
                            var show_skIsszhb = rows[i].skIsszhb == 1 ? "是" : "否";
                            var show_isSksh;
                            if (rows[i].isSksh == 0) show_isSksh = "未审";
                            if (rows[i].isSksh == 1) show_isSksh = "已审";
                            if (rows[i].isSksh == 2) show_isSksh = "未通过";
                            var btn_shRecord = '<li><button class="sdelbtn" onclick=\'shRecord(' + dto.id + ')\'>确认</button></li><li><button class="sdelbtn" onclick=\'bhRecord(' + dto.id + ')\'>驳回</button></li>';
                            if (show_isSksh == "已审" || show_isSksh == "未通过") btn_shRecord = "";

                            var font_color = '';
                            if (show_isSksh == "已审") font_color = 'style = "color:red;"';//已审显示红色

                            var fkfsHtml = "";
                            var lightboxId = "lightbox" + dto.id;
                            fkfsHtml+='<dl><dt>银行汇款</dt><dd>' + show_skIsbank + '</dd></dl><dl><dt>开户行名称</dt><dd>' + dto.bankName + '</dd></dl>' +
                              '<dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl><dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                                '<dl><dt>开户名支行</dt><dd>' + dto.bankAddress + '</dd></dl>';

                            lightboxId = "lightboxzfb" + dto.id;
                            fkfsHtml += '<dl><dt>支付宝</dt><dd>' + show_skIszfb + '</dd></dl>' +
                            '<dl><dt>支付宝图片</dt><dd><img data-toggle="lightbox"  id="' + lightboxId + '" src="' + dto.imgUrlzfb + '" data-image="' + dto.imgUrlzfb + '" class="img-thumbnail" alt="" width="100"></dd></dl>';
                            lightboxArray.push(lightboxId)

                            lightboxId = "lightboxwx" + dto.id;
                            fkfsHtml += '<dl><dt>微信</dt><dd>' + show_skIswx + '</dd></dl>' +
                            '<dl><dt>微信图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlwx + '" data-image="' + dto.imgUrlwx + '" class="img-thumbnail" alt="" width="100"></dd></dl>';
                            lightboxArray.push(lightboxId)
                          
                            lightboxId = "lightboxszhb" + dto.id;
                            fkfsHtml += '<dl><dt>数字货币</dt><dd>' + show_skIsszhb + '</dd></dl>' +
                             '<dl><dt>货币名称1</dt><dd>' + dto.szhbmc + '</dd></dl>' +
                              '<dl><dt>货币1地址</dt><dd>' + dto.BTTaddress + '</dd></dl>' +
                            '<dl><dt>货币1二维码</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb + '" data-image="' + dto.imgUrlszhb + '" class="img-thumbnail" alt="" width="100"></dd></dl>';
                            lightboxArray.push(lightboxId)

                            lightboxId = "lightboxszhb1" + dto.id;
                            fkfsHtml += '<dl><dt>货币名称2</dt><dd>' + dto.szhbmc1 + '</dd></dl>' +
                              '<dl><dt>货币2地址</dt><dd>' + dto.ETHaddress + '</dd></dl>' +
                            '<dl><dt>货币2二维码</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb1 + '" data-image="' + dto.imgUrlszhb1 + '" class="img-thumbnail" alt="" width="100"></dd></dl>';
                            lightboxArray.push(lightboxId)


                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.addTime + '</time>';
                            html += '&nbsp;<span class="sum" ' + font_color + '>' + dto.userId + '-' + show_isSksh + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>真实姓名</dt><dd>' + dto.userName + '</dd></dl><dl><dt>手机号码</dt><dd>' + dto.phone + '</dd></dl>' +
                             '<dl><dt>身份证号</dt><dd>' + dto.code + '</dd></dl>' +
                            fkfsHtml+
                               '<dl><dt>状态</dt><dd>' + show_isSksh + '</dd></dl>' +
                                '<div class="btnbox"><ul class="tga3">'+
                                btn_shRecord +
                                 '<li><button class="seditbtn" onclick="location.href=\'#MemberInfo/' + dto.id + '\'">编辑</button></li>'  +
                            '</ul></div>' +
                            '</div></li>';
                            
                        }

                        var html_totalepoints = " <p>已审：0 未审：0 </p>";
                        for (var i = 0; i < footers.length; i++) {
                            var dto = footers[i];
                            html_totalepoints = '<p>已审:' + dto.skxxYs + ' &nbsp;  未审:' + dto.skxxWs + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);

                        $("#MemitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#MemitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#MemitemList").empty();
            param["userId"] = $("#userId").val();
            param["phone"] = $("#phone").val();
            param["startTime"] = $("#passStartTime").val();
            param["endTime"] = $("#passEndTime").val();
            param["isSksh"] = $("#isSksh").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});