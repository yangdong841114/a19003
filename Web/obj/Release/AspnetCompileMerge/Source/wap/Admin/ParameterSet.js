
define(['text!ParameterSet.html', 'jquery'], function (ParameterSet, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("参数设置")
        var inputs = {};

        utils.AjaxPostNotLoadding("/Admin/ParameterSet/InitView", {}, function (result) {
            appView.html(ParameterSet);
            //check和input值转换
            $("#chk_HgKg").click(function () {
                if ($('#chk_HgKg').is(':checked'))
                    $("#HgKg").val("是");
                else
                    $("#HgKg").val("否");
            });



            utils.InitMobileSelect('#cfnew_BTZName', '选择', btzlist, { id: "id", value: "BTZName" }, [0], null, function (indexArr, data) {
                $("#cfnew_BTZName").val(data[0].BTZName);
                $("#cfnew_BTZid").val(data[0].id);
            });


            //初始化赋值
            var rt = result.map;
            $(".entryinfo input").each(function () {
                inputs[this.id] = "";
            })
            if (rt != null) {
                for (name in rt) {
                    if (inputs[name] != undefined) {
                        var obj = rt[name];
                        $("#" + name).val(obj.paramValue);
                    }
                }
            }


            //根据值设置checkbox
            if ($("#HgKg").val() == "是")
                $("#chk_HgKg").attr('checked', true);
            else
                $("#chk_HgKg").attr('checked', false);

            //保存
            $("#saveParamBtn").on('click', function () {
                var dt = {}
                var checked = true;
                var i = 0;
                $(".entryinfo input").each(function () {
                    var dom = $(this);
                    var str_id = this.id;
                    if (str_id.indexOf("chk_") == -1) {
                        dt["list[" + i + "].paramCode"] = this.id;
                        dt["list[" + i + "].paramValue"] = dom.val();
                        i++;
                    }
                    if (dom.val() == "" || $.trim(dom.val()).length == 0) {
                        if (dom.attr("name") != "userIdPrefix") {
                            utils.showErrMsg("请输入参数");
                            dom.focus();
                            checked = false;
                            return false;
                        }
                    }

                })
                //提交表单
                if (checked) {
                    utils.AjaxPost("/Admin/ParameterSet/Save", dt, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showSuccessMsg("保存成功！");
                        }
                    });

                } 

            })
        });

        

        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});