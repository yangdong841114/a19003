
define(['text!BTZjscity.html', 'jquery'], function (BTZjscity, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("禁售城市")
        appView.html(BTZjscity);

        var dto = null;
        var editList = {};
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        var indexChecked = [0, 0, 0];

        //打开时确认选中数据
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //选择确认
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //省市区选择
        var proSet = utils.InitMobileSelect('#province', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
            selectProvince(data);
            indexChecked = indexArr;
        }, initPosition);
        var citySet = utils.InitMobileSelect('#city', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
            selectProvince(data);
            indexChecked = indexArr;
        }, initPosition);
        var areaSet = utils.InitMobileSelect('#area', '选择省市区', areaData, null, indexChecked, null, function (indexArr, data) {
            selectProvince(data);
            indexChecked = indexArr;
        }, initPosition);

     
        //删除按钮
        deleteRecord = function (id) {
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZjscity/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

      


        //发布商品
        $("#deployProduct").bind("click", function () {
            dto = null;
            $("#id").val("");
            $("#province").val("");
            $("#city").val("");
            $("#area").val("");
          
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });

      

        //保存发布商品
        $("#saveProductBtn").bind("click", function () {
            //金额校验
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            //非空校验
            if ($("#city").val() == 0) {
                utils.showErrMsg("城市不能为空");
            }  else {
                var formdata = new FormData();
                if (dto) {
                    formdata.append("id", $("#id").val());
                }
                formdata.append("province", $("#province").val());
                formdata.append("city", $("#city").val());
                formdata.append("area", $("#area").val());

                

                utils.AjaxPostForFormData("/Admin/BTZjscity/SaveOrUpdate", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        utils.showSuccessMsg("保存成功！");
                        searchMethod();
                    }
                });
            }
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //编辑商品
        editRecord = function (id) {
            dto = editList[id];
            $("#id").val(id);
            $("#province").val(dto.province);
            $("#city").val(dto.city);
            $("#area").val(dto.area);
           
          
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");

        }

        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Productdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZjscity/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                          
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.province + '</time>';
                           
                            html += '&nbsp;<span class="sum">' + dto.city + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">';
                           
                            html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                                
                            '</ul></div>' +
                            '<dl><dt>省</dt><dd>' + dto.province + '</dd></dl><dl><dt>市</dt><dd>' + dto.city + '</dd></dl>' +
                          
                            '</div></li>';
                            editList[dto.id] = dto;

                        }
                        $("#ProductitemList").append(html);
                    }, function () {
                        $("#ProductitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});