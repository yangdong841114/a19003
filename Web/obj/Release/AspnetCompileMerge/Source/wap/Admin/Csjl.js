
define(['text!Csjl.html', 'jquery'], function (Csjl, $) {

    var controller = function (id) {
        //设置标题
        $("#title").html("出售记录")
        appView.html(Csjl);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })
        var saleId = 0;
        var buyId = 0;
        if (!id || id == 0) id = "0";
        if (id.indexOf('saleId') > -1) {
            saleId = id.replace('saleId', '');
            //$("#saleId").val(saleId);
        }
        if (id.indexOf('buyId') > -1) {
            buyId = id.replace('buyId', '');
            //$("#saleId").val(saleId);
        }
       
       
        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);
        var BuymemTypeList = [{ id: 0, name: "全部" }, { id: 1, name: "普通会员订单" }, { id: 2, name: "虚拟会员订单" }];
        utils.InitMobileSelect('#BuymemTypeName', '选择状态', BuymemTypeList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#BuymemTypeName").val(data[0].name);
            $("#BuymemType").val(data[0].id);
        });
        var SalememTypeList = [{ id: 0, name: "全部" }, { id: 1, name: "普通会员订单" }, { id: 2, name: "虚拟会员订单" }];
        utils.InitMobileSelect('#SalememTypeName', '选择状态', SalememTypeList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#SalememTypeName").val(data[0].name);
            $("#SalememType").val(data[0].id);
        });
        

        //var flagto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售" }
        var flagList = [{ id: 0, name: "全部" }, { id: 1, name: "已预约" }, { id: 2, name: "已申请领养" }, { id: 3, name: "領養成功" }, { id: 4, name: "匹配失败" }, { id: 5, name: "已付款" }, { id: 6, name: "已确认付款" }, { id: 7, name: "付款超时领养失败" }, { id: 8, name: "已出售" }, { id: 9, name: "解除交易" }];
        utils.InitMobileSelect('#flagName', '选择状态', flagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });
        //审核状态选择框
        var saleflagList = [{ id: 0, name: "全部" }, { id: 1, name: "待转让" }, { id: 2, name: "已转让" }, { id: 3, name: "交易完成" }];
        utils.InitMobileSelect('#saleflagName', '选择状态', saleflagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#saleflagName").val(data[0].name);
            $("#saleflag").val(data[0].id);
        });
        utils.InitMobileSelect('#BTZName', '选择状态', btzlist, { id: "id", value: "BTZName" }, [0], null, function (indexArr, data) {
            $("#BTZName").val(data[0].BTZName);
            $("#Btzid").val(data[0].id);
        });
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //付款
        surefk = function (id) {
            $("#prompTitle").html("您确定付款吗？");
            $("#sureBtn").html("确定付款")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuyfk", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("确定付款操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //收款
        suresk = function (id) {
            $("#prompTitle").html("您确定收款吗？");
            $("#sureBtn").html("确定收款")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuysk", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("确定收款操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
        //撤单
        surecd = function (id) {
            $("#prompTitle").html("您确定撤单吗？");
            $("#sureBtn").html("确定撤单")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuycd", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("确定撤单操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Chargedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPageCsjl?saleId=" + saleId + "&id=" + buyId, param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        var saleflagDto = { 1: "待转让", 2: "已转让", 3: "交易完成" }
                        var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售", 9: "解除交易" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Cssj"] = utils.changeDateFormat(rows[i]["Cssj"]);
                            rows[i]["Ppsj"] = utils.changeDateFormat(rows[i]["Ppsj"]);
                            rows[i]["payTime"] = utils.changeDateFormat(rows[i]["payTime"]);
                            rows[i]["confirmPayTime"] = utils.changeDateFormat(rows[i]["confirmPayTime"]);
                            rows[i]["saleflag"] = saleflagDto[rows[i].saleflag];
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;
                            lightboxArray.push(lightboxId)
                            var saleflag_color = "#f90";
                            if (dto.saleflag == "待转让" && dto.flag == "付款超时领养失败") saleflag_color = "red";
                            if (dto.saleflag == "交易完成") saleflag_color = "blue";
                            var fkfsHtml = "";
                            if (dto.skIsbank == "1")
                                fkfsHtml+= '<dl><dt>卖家收款方式</dt><dd>银行汇款</dd></dl>' +
                                     '<dl><dt>开户行</dt><dd>' + dto.bankName + '</dd></dl>' +
                                      '<dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl>' +
                                       '<dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                                        '<dl><dt>开户支行</dt><dd>' + dto.bankAddress + '</dd></dl>';
                            if (dto.skIszfb == "1") {
                                lightboxId = "lightboxzfb" + dto.id;
                                fkfsHtml+= '<dl><dt>卖家收款方式</dt><dd>支付宝扫码</dd></dl>' +
                                        '<dl><dt>图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlzfb + '" data-image="' + dto.imgUrlzfb + '" data-caption="扫码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId)
                            }
                            if (dto.skIswx == "1") {
                                lightboxId = "lightboxwx" + dto.id;
                                fkfsHtml+= '<dl><dt>卖家收款方式</dt><dd>微信扫码</dd></dl>' +
                                        '<dl><dt>图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlwx + '" data-image="' + dto.imgUrlwx + '" data-caption="扫码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId)
                            }
                            if (dto.skIsszhb == "1") {
                                lightboxId = "lightboxszhb" + dto.id;
                                fkfsHtml+= '<dl><dt>卖家收款方式</dt><dd>' + dto.szhbmc + '扫码</dd></dl>' +
                                        '<dl><dt>图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb + '" data-image="' + dto.imgUrlszhb + '" data-caption="扫码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId)
                                if (dto.szhbmc1 != "") {
                                    lightboxId = "lightboxszhb1" + dto.id;
                                    fkfsHtml+= '<dl><dt>賣家收款方式</dt><dd>' + dto.szhbmc1 + '掃碼</dd></dl>' +
                                            '<dl><dt>圖片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb1 + '" data-image="' + dto.imgUrlszhb1 + '" data-caption="掃碼" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';

                                    lightboxArray.push(lightboxId)
                                }
                            }

                            lightboxId = "lightbox" + dto.id;

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.BTZName + '</time><span class="sum" style="color:' + saleflag_color + '">' + dto.saleflag + '</span>';
                            html += '<time>' + dto.Ppsj + '</time>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga3">' +
                             //'<li><button class="seditbtn" onclick=\'surefk(' + dto.id + ')\'>确认付款</button></li>' +
                           '<li><button class="sdelbtn" onclick=\'suresk(' + dto.id + ')\'>确认收款</button></li>' +
                             '<li><button class="seditbtn" onclick=\'surecd(' + dto.id + ')\'>撤单</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>卖出单号</dt><dd>' + dto.SaleNo + '</dd></dl><dl><dt>卖家编号</dt><dd>' + dto.saleuserId + '</dd></dl>' +
                             '<dl><dt>卖家手机</dt><dd>' + dto.salephone + '</dd></dl>' +
                            '<dl><dt>卖家姓名</dt><dd>' + dto.saleuserName + '</dd></dl><dl><dt>出售价值</dt><dd>' + dto.priceOldzz + '</dd></dl>' +
                             '<dl><dt>匹配次数</dt><dd>' + dto.Ppcs + '</dd></dl><dl><dt>出售时间</dt><dd>' + dto.Cssj + '</dd></dl>' +

                            '<dl><dt>求购单号</dt><dd>' + dto.BuyNo + '</dd></dl><dl><dt>买家编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>买家姓名</dt><dd>' + dto.userName + '</dd></dl><dl><dt>买家电话</dt><dd>' + dto.phone + '</dd></dl>' +
                             '<dl><dt>匹配时间</dt><dd>' + dto.Ppsj + '</dd></dl>' +

                            '<dl><dt>买单状态</dt><dd>' + dto.flag + '</dd></dl>' +

                             fkfsHtml +
                                   '<dl><dt>汇款凭证</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                     '<dl><dt>汇款时间</dt><dd>' + dto.payTime + '</dd></dl>' +
                                       '<dl><dt>收款时间</dt><dd>' + dto.confirmPayTime + '</dd></dl>' +
                            '</div></li>';
                           
                        }
                        var html_totalepoints = " <p>出售人数：XX 出售记录总数：XX 领养人数：XX</p><p>手续费总额：XX 出售总价值：XX</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>出售人数:' + dto.totalCsrs + ' &nbsp;  出售记录总数:' + dto.totalJlzs + '&nbsp;  领养人数:' + dto.totalLyrs + '</p>'+
                                '<p>手续费总额:' + dto.totalSxf.toFixed(2) + ' &nbsp;  出售总价值:' + dto.totalZjz.toFixed(2) + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#ChargeitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#ChargeitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ChargeitemList").empty();
            param["saleuserId"] = $("#userId").val();
            param["BuymemType"] = $("#BuymemType").val();
            param["SalememType"] = $("#SalememType").val();
            param["flag"] = $("#flag").val();
            param["saleflag"] = $("#saleflag").val();
            param["BTZid"] = $("#Btzid").val();
            param["saleId"] = $("#saleId").val();
            param["startTimePpsj"] = $("#startTime").val();
            param["endTimePpsj"] = $("#endTime").val();
            param["saleuserName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }
       
     
        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});