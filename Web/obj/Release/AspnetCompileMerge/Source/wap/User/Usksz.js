
define(['text!Usksz.html', 'jquery'], function (Usksz, $) {

    var controller = function (memberId) {

        //設置標題
        $("#title").html("收款方式")
        var pmap = null;

        //設置表單默認數據
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);

            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                    $(this).val(dto[this.id]);
                }
            });
            if ($("#isSksh").val() == "1")//放开便于二次上传其他收款设置方式，已提交的信息不能修改，只能修改未设置收款方式的账户
            {
                //document.getElementById("saveBtn").style.display = "none";
                $("#isSkshName").html("已通过");
            }
            if ($("#isSksh").val() == "2")
            {
                $("#isSkshName").html("未通过");
            }

            //根據值設置checkbox
            if ($("#skIsbank").val() == "1")
                $("#chk_skIsbank").attr('checked', true);
            else
                $("#chk_skIsbank").attr('checked', false);

            if ($("#skIszfb").val() == "1")
                $("#chk_skIszfb").attr('checked', true);
            else
                $("#chk_skIszfb").attr('checked', false);

            if ($("#skIswx").val() == "1")
                $("#chk_skIswx").attr('checked', true);
            else
                $("#chk_skIswx").attr('checked', false);

            if ($("#skIsszhb").val() == "1")
                $("#chk_skIsszhb").attr('checked', true);
            else
                $("#chk_skIsszhb").attr('checked', false);


            if ($("#imgUrlzfb").val() != "")
                document.getElementById("showImgzfb").style.backgroundImage = 'url(' + $("#imgUrlzfb").val() + ')';
            if ($("#imgUrlwx").val() != "")
                document.getElementById("showImgwx").style.backgroundImage = 'url(' + $("#imgUrlwx").val() + ')';
            if ($("#imgUrlszhb").val() != "")
                document.getElementById("showImgszhb").style.backgroundImage = 'url(' + $("#imgUrlszhb").val() + ')';
            if ($("#imgUrlszhb1").val() != "")
                document.getElementById("showImgszhb1").style.backgroundImage = 'url(' + $("#imgUrlszhb1").val() + ')';

        }
        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;



        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(Usksz);

                //初始化銀行帳號下拉框
                var bankLit = cacheList["UserBank"];
                utils.InitMobileSelect('#bankName', '開戶行', bankLit, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
                    $("#bankName").val(data[0].name);
                });

                //var hbmcList = [{ id: 'ETH', value: "ETH" }, { id: 'DOGE', value: "DOGE" }];
                ////初始化下拉框
                //utils.InitMobileSelect('#szhbmc', '帐户类型', hbmcList, null, [0], null, function (indexArr, data) {
                //    $("#szhbmc").val(data[0].value);
                //    //$("#accounttypeId").val(data[0].id);
                //});

                $("#chk_skIsbank").click(function () {
                    if ($('#chk_skIsbank').is(':checked'))
                        $("#skIsbank").val("1");
                    else
                        $("#skIsbank").val("0");
                });
                $("#chk_skIszfb").click(function () {
                    if ($('#chk_skIszfb').is(':checked'))
                        $("#skIszfb").val("1");
                    else
                        $("#skIszfb").val("0");
                });
                $("#chk_skIswx").click(function () {
                    if ($('#chk_skIswx').is(':checked'))
                        $("#skIswx").val("1");
                    else
                        $("#skIswx").val("0");
                });
                $("#chk_skIsszhb").click(function () {
                    if ($('#chk_skIsszhb').is(':checked'))
                        $("#skIsszhb").val("1");
                    else
                        $("#skIsszhb").val("0");
                });
              
                var dto = result.result;
                //初始表單默認值
                setDefaultFormValue(dto);
                //收款设置的所有方式审核通过后都可以多次修改，修改提交后重新审核。
                //if ($("#bankCard").val() != ""&&$("#isSksh").val()!= "2"||1==2)
                //    $("#bankCard").attr("readonly", "readonly");
                //if ($("#bankUser").val() != "" && $("#isSksh").val() != "2" || 1 == 2)
                //    $("#bankUser").attr("readonly", "readonly");
                //if ($("#bankAddress").val() != "" && $("#isSksh").val() != "2" || 1 == 2)
                //    $("#bankAddress").attr("readonly", "readonly");
                //if ($("#szhbmc").val() != "" && $("#isSksh").val() != "2" || 1 == 2)
                //    $("#szhbmc").attr("readonly", "readonly");
                //if ($("#BTTaddress").val() != "" && $("#isSksh").val() != "2" || 1 == 2)
                //    $("#BTTaddress").attr("readonly", "readonly");
                //if ($("#szhbmc1").val() != "" && $("#isSksh").val() != "2" || 1 == 2)
                //    $("#szhbmc1").attr("readonly", "readonly");
                //if ($("#ETHaddress").val() != "" && $("#isSksh").val() != "2" || 1 == 2)
                //    $("#ETHaddress").attr("readonly", "readonly");

                //預覽圖片
                if ($("#imgUrlzfb").val() == "" || $("#isSksh").val() == "2" || 1 == 1)
                {
                $("#imgFilezfb").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgzfb").style.backgroundImage = 'url(' + url + ')';
                })
                }
                if ($("#imgUrlwx").val() == "" || $("#isSksh").val() == "2" || 1 == 1)
                {
                $("#imgFilewx").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgwx").style.backgroundImage = 'url(' + url + ')';
                })
                }


                if ($("#imgUrlszhb").val() == "" || $("#isSksh").val() == "2" || 1 == 1)
                {
                $("#imgFileszhb").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgszhb").style.backgroundImage = 'url(' + url + ')';
                })
                }

                if ($("#imgUrlszhb1").val() == "" || $("#isSksh").val() == "2" ||1 == 1)
                {
                $("#imgFileszhb1").bind("change", function () {
                    var url = URL.createObjectURL($(this)[0].files[0]);
                    document.getElementById("showImgszhb1").style.backgroundImage = 'url(' + url + ')';
                })
                }

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        dom.prev().val("");
                    });
                });


                //保存按鈕
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //數據校驗
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                        if (jdom.attr("id") == 'bankCard') {
                            var card = jdom.val();
                            var reg = /^(\d{16,19})$/g;
                            if (!reg.test(jdom.val())) {
                                utils.showErrMsg("銀行卡號位數必須為16-19位！");
                                jdom.focus();
                                checked = false;
                                return false;
                            }
                        }
                    });
                    if (checked) {
                        var formdata = new FormData();
                        formdata.append("id", $("#id").val());
                        formdata.append("skIsbank", $("#skIsbank").val());
                        formdata.append("skIszfb", $("#skIszfb").val());
                        formdata.append("skIswx", $("#skIswx").val());
                        formdata.append("skIsszhb", $("#skIsszhb").val());
                        formdata.append("bankName", $("#bankName").val());
                        formdata.append("bankCard", $("#bankCard").val());
                        formdata.append("bankUser", $("#bankUser").val());
                        formdata.append("bankAddress", $("#bankAddress").val());
                        formdata.append("BTTaddress", $("#BTTaddress").val());
                        formdata.append("ETHaddress", $("#ETHaddress").val());
                        formdata.append("szhbmc", $("#szhbmc").val());
                        formdata.append("szhbmc1", $("#szhbmc1").val());
                        formdata.append("imgFilezfb", $("#imgFilezfb")[0].files[0]);
                        formdata.append("imgFilewx", $("#imgFilewx")[0].files[0]);
                        formdata.append("imgFileszhb", $("#imgFileszhb")[0].files[0]);
                        formdata.append("imgFileszhb1", $("#imgFileszhb1")[0].files[0]);


                        utils.AjaxPostForFormData("/User/UMemberInfo/Sksz", formdata, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });


                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});