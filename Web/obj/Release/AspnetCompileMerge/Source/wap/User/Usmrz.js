
define(['text!Usmrz.html', 'jquery'], function (Usmrz, $) {

    var controller = function (memberId) {
       
        //设置标题
        $("#title").html("實名認證")
        var pmap = null;

        //设置表单默认数据
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);
            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                  $(this).val(dto[this.id]);
                }
            });
       
        }
        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;

       

        //加载会员信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(Usmrz);

                var dto = result.result;
                //初始表单默认值
                setDefaultFormValue(dto);
                if ($("#isSmrz").val() == "1")
                {
                    document.getElementById("liHaveSmrz").style.display = "block";
                    document.getElementById("saveBtn").style.display = "none";
                    document.getElementById("dl_sfz").style.display = "none";//认证通过后身份证号不要显示
                    document.getElementById("erase_name").style.display = "none";
                    var userName = $("#userName").val();
                    $('#userName').attr("readonly", "readonly");
                    $("#userName").val('*'+userName.substring(1))
                }
                else
                {
                    document.getElementById("liHaveSmrz").style.display = "none";
                    document.getElementById("saveBtn").style.display = "block";
                }
                //输入框取消按钮
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        dom.prev().val("");
                    });
                });


                //保存按钮
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //数据校验
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                    });
                    if (checked) {
                        var fs = {};
                        $("#lemeinfo input").each(function () {
                            fs[this.id] = $(this).val();
                        });
                        utils.AjaxPost("/User/UMemberInfo/Smrz", fs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});