
define(['text!myCenterMan.html', 'jquery'], function (myCenterMan, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("會員中心");


        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(myCenterMan);
                clearInterval(flag_btz);//全局变量
                var map = result.map;
                var menuItems = map.menuItems
                var account = map.account;
                $("#agentLz").html(account.agentLz.toFixed(2));        //电子币
                $("#agentBTT").html(account.agentBTT.toFixed(2));        //奖金币
                $("#agentBTD").html(account.agentBTD.toFixed(2));        //购物币
              

                var userInfo = map.userInfo;
                $("#ulevel").html(cacheMap["ulevel"][userInfo.uLevel]);
                $("#userName").html(userInfo.userName);
                $("#userId").html("會員賬號：" + userInfo.userId);

                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("User/", "");
                        if (node.isShow == 0) {
                            banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                        }
                    }
                    $("#itemCont").html(banHtml);
                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});