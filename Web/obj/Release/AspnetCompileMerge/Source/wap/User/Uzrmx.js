
define(['text!Uzrmx.html', 'jquery'], function (Uzrmx, $) {

    var controller = function (name) {


        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "none";
                document.getElementById("detailTab5").style.display = "none";
                document.getElementById("detailTab6").style.display = "none";
                document.getElementById("sslemeinfo").style.display = "none";
                $("#tabBtn1").addClass("active")
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").removeClass("active")
                $("#tabBtn5").removeClass("active")
                $("#tabBtn6").removeClass("active")
                searchMethodQg();
            }
            if (index == 2) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "none";
                document.getElementById("detailTab5").style.display = "none";
                document.getElementById("detailTab6").style.display = "none";
                document.getElementById("sslemeinfo").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").addClass("active");
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").removeClass("active")
                $("#tabBtn5").removeClass("active")
                $("#tabBtn6").removeClass("active")
                //加載數據
                searchMethodDcs();
            }
            if (index == 3) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "block";
                document.getElementById("detailTab4").style.display = "none";
                document.getElementById("detailTab5").style.display = "none";
                document.getElementById("detailTab6").style.display = "none";
                document.getElementById("sslemeinfo").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").addClass("active");
                $("#tabBtn4").removeClass("active")
                $("#tabBtn5").removeClass("active")
                $("#tabBtn6").removeClass("active")
                //加載數據
                searchMethodsale();
            }
            if (index == 4) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "block";
                document.getElementById("detailTab5").style.display = "none";
                document.getElementById("detailTab6").style.display = "none";
                document.getElementById("sslemeinfo").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").addClass("active");
                $("#tabBtn5").removeClass("active")
                $("#tabBtn6").removeClass("active")

                //加載數據
                searchMethodbuy();
            }
            if (index == 5) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "none";
                document.getElementById("detailTab5").style.display = "block";
                document.getElementById("detailTab6").style.display = "none";
                document.getElementById("sslemeinfo").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").removeClass("active")
                $("#tabBtn5").addClass("active");
                $("#tabBtn6").removeClass("active")
                //加載數據
                //searchMethodJqrzr();
            }
            if (index == 6) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "none";
                document.getElementById("detailTab5").style.display = "none";
                document.getElementById("detailTab6").style.display = "block";
                document.getElementById("sslemeinfo").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").removeClass("active")
                $("#tabBtn5").removeClass("active")
                $("#tabBtn6").addClass("active");
                //加載數據
                searchMethodZrz();
            }
        }

        //設置標題
        $("#title").html("轉讓明細");
        appView.html(Uzrmx);
       
        //隱藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });


        //秒转时分秒
        function formatSeconds2(a) {
            var hh = parseInt(a / 3600);
            if (hh < 10) hh = "0" + hh;
            var mm = parseInt((a - hh * 3600) / 60);
            if (mm < 10) mm = "0" + mm;
            var ss = parseInt((a - hh * 3600) % 60);
            if (ss < 10) ss = "0" + ss;
            var length = hh + ":" + mm + ":" + ss;
            if (a > 0) {
                return length;
            } else {
                return "暂无";
            }
        }

        var flagDto = { 1: "已預約", 2: "已申請領養", 3: "領養成功", 4: "領養失敗", 5: "待確認收款", 6: "已確認", 7: "付款超時領養失敗", 8: "已出售", 9: "解除交易" }
        var dto = null;
        var djsrRowCount = 0;
        //查詢參數
        this.paramQg = utils.getPageData();

        var droploadQg = $('#Qgdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPageQg", paramQg, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            djsrRowCount = rows.length;
                            rows[i]["Yysj"] = utils.changeDateFormat(rows[i]["Yysj"]);
                            rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                            rows[i]["Ppsj"] = utils.changeDateFormat(rows[i]["Ppsj"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];

                           
                           

                                var dto = rows[i];
                            
                            var btn_fk = '<button class="bigbtn" style="width:50%;" onclick=\'jymx(' + dto.id + ')\' >去付款</button>';
                            if (dto.flag != "領養成功") btn_fk = "";
                            var btn_ss = '<button class="bigbtn" style="width:30%;" onclick=\'ss(' + dto.id + ',' + dto.saleId + ',' + dto.uid + ',' + dto.saleUid + ',' + dto.priceOldzz + ',"' + dto.saleuserId + '","' + dto.saleuserName + '","' + dto.BuyNo + '","' + dto.SaleNo + '")\' >申訴</button>';
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BTZName + '</span><time>' + dto.flag + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>求購單號</dt><dd>' + dto.BuyNo + '</dd></dl>' +
                                   //'<dl><dt>區塊豬編號</dt><dd>' + dto.BTZCode + '</dd></dl>' +
                                   // '<dl><dt>區塊豬名稱</dt><dd>' + dto.BTZName + '</dd></dl>' +
                                      '<dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                                        '<dl><dt>智能合約收益</dt><dd>' + dto.Hyts + '天/' + dto.Hyts*dto.RsyBili + '%</dd></dl>' +
                                  //'<dl><dt>手續費</dt><dd>' + dto.sxf + '</dd></dl><dl><dt>預約時間</dt><dd>' + dto.Yysj + '</dd></dl>' +
                                  //'<dl><dt>搶養時間</dt><dd>' + dto.Qysj + '</dd></dl>' +
                                  //'<dl><dt>匹配時間</dt><dd>' + dto.Ppsj + '</dd></dl>' +
                                  //'<dl><dt>賣家編號</dt><dd>' + dto.saleuserId + '</dd></dl>' +
                                  //'<dl><dt>賣家姓名</dt><dd>' + dto.saleuserName + '</dd></dl>' +
                                   // '<dl><dt>賣家電話</dt><dd>' + dto.salephone + '</dd></dl>' +
                                  '<dl><dt>付款剩余時間</dt><dd style="color:red;" id="showsyfksj' + i + '"></dd></dl>' +
                                  '<span style="display:none" id="syfksj' + i + '">' + dto.syfksj + '</span>' +
                                    '<dl><dt>收款剩余時間</dt><dd style="color:red;" id="showsysksj_qg' + i + '"></dd></dl>' +
                                  '<span style="display:none" id="sysksj_qg' + i + '">' + dto.sysksj + '</span>' +
                                  // '<dl><dt>操作人</dt><dd>' + dto.Czr + '</dd></dl>' +
                                btn_fk +
                                btn_ss+
                                  '</div></li>';
                        }
                        $("#QgitemList").append(html);
                    }, function () {
                        $("#QgitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });


        //3秒一次不断检测
        jssecond_syfksj = 0;//全局变量
       
        clearInterval(flag_syfksj);//全局变量
        show_syfksj = function () {
            jssecond_syfksj = jssecond_syfksj + 2;
            for (var i = 0; i < djsrRowCount; i++) {
                var syfksj = $("#syfksj" + i).html();
                var syfksjdjs = syfksj - jssecond_syfksj;
                $("#showsyfksj" + i).html(formatSeconds2(syfksjdjs));

                var sysksj_qg = $("#sysksj_qg" + i).html();
                var syfskjdjs_qg = sysksj_qg - jssecond_syfksj;
                $("#showsysksj_qg" + i).html(formatSeconds2(syfskjdjs_qg));
            }
        }
        flag_syfksj = setInterval(show_syfksj, 2000);
        show_syfksj();//默认进来就跑一次
        //跳轉到交易明細界面
        jymx = function (id) {
            location.href = '#Ujymx/' + id;
        }

        //查詢方法
        searchMethodQg = function () {
            paramQg.page = 1;
            $("#QgitemList").empty();
            droploadQg.unlock();
            droploadQg.noData(false);
            droploadQg.resetload();
        }

        //查詢參數
        this.paramDcs = utils.getPageData();

        var droploadDcs = $('#Dcsdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPageDcs", paramDcs, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                            rows[i]["Jssj"] = utils.changeDateFormat(rows[i]["Jssj"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            if (rows[i]["isZcs"] == "0") rows[i]["ZcszjBili"] = "0";

                            var dto = rows[i];
                            var btn_xy = '<button class="bigbtn" style="width:50%;" onclick=\'xy(' + dto.id + ')\' >續養</button>';
                            if (dto.isXy == "1") btn_xy = '<button class="bigbtn" style="width:50%;" onclick=\'xy(' + dto.id + ')\' >續養成功</button>';
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BTZName + '</span><time>' + dto.Qysj + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>求購單號</dt><dd>' + dto.BuyNo + '</dd></dl>' +
                                  //'<dl><dt>比特豬ID</dt><dd>' + dto.BTZid + '</dd></dl>' +
                                  '<dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                                  '<dl><dt>日收益率</dt><dd>' + dto.RsyBili + '%</dd></dl>' +
                                  '<dl><dt>加息率</dt><dd>' + dto.ZcszjBili + '%</dd></dl>' +
                                   '<dl><dt>合約天數</dt><dd>' + dto.Hyts + '</dd></dl>' +
                                    '<dl><dt>結束時間</dt><dd>' + dto.Jssj + '</dd></dl>' +
                                     '<dl><dt>可挖DOGE量</dt><dd>' + dto.ZsBTT + '</dd></dl>' +
                                      '<dl><dt>可挖BTD量</dt><dd>' + dto.ZsBTD + '</dd></dl>' +
                                 btn_xy +
                                  '</div></li>';
                        }
                        $("#DcsitemList").append(html);
                    }, function () {
                        $("#DcsitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //跳轉到交易明細界面
        xy = function (id) {
            $("#prompTitle").html("您確定續養嗎？");
            $("#sureBtn").html("確定續養")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/Uzrmx/BTZBuyxy", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("續養操作成功");
                        searchMethodDcs();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查詢方法
        searchMethodDcs = function () {
            paramDcs.page = 1;
            $("#DcsitemList").empty();
            droploadDcs.unlock();
            droploadDcs.noData(false);
            droploadDcs.resetload();
        }

        var djsrRowCount_zrz = 0;
        //查詢參數
        this.paramZrz = utils.getPageData();

        var droploadZrz = $('#Zrzdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPageZrz", paramZrz, me,
                    function (rows, footers) {
                        //var html = "";
                        //for (var i = 0; i < rows.length; i++) {
                        //    rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                        //    rows[i]["Jssj"] = utils.changeDateFormat(rows[i]["Jssj"]);
                        //    rows[i]["Cfsj"] = utils.changeDateFormat(rows[i]["Cfsj"]);
                        //    rows[i]["flag"] = flagDto[rows[i].flag];
                        //    if (rows[i]["isZcs"] == "0") rows[i]["ZcszjBili"] = "0";

                        //    var dto = rows[i];
                        //    html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BTZName + '</span><time>' + dto.Qysj + '</time>' +
                        //          '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                        //          '<div class="allinfo allinfoheigth" style="display: block;">' +
                        //          '<dl><dt>求購單號</dt><dd>' + dto.BuyNo + '</dd></dl>' +
                        //          //'<dl><dt>比特豬ID</dt><dd>' + dto.BTZid + '</dd></dl>' +
                        //          '<dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                        //          '<dl><dt>日收益率</dt><dd>' + dto.RsyBili + '%</dd></dl>' +
                        //          '<dl><dt>加息率</dt><dd>' + dto.ZcszjBili + '%</dd></dl>' +
                        //           '<dl><dt>合約天數</dt><dd>' + dto.Hyts + '</dd></dl>' +
                        //            '<dl><dt>結束時間</dt><dd>' + dto.Jssj + '</dd></dl>' +
                        //             '<dl><dt>可挖DOGE量</dt><dd>' + dto.ZsBTT + '</dd></dl>' +
                        //              '<dl><dt>可挖BTD量</dt><dd>' + dto.ZsBTD + '</dd></dl>' +
                        //      '<button class="bigbtn" style="width:30%;" onclick=\'ss(' + dto.id + ',' + dto.saleId + ',' + dto.uid + ',' + dto.saleUid + ',' + dto.priceOldzz + ',"' + dto.saleuserId + '","' + dto.saleuserName + '","' + dto.BuyNo + '","' + dto.SaleNo + '")\' >申訴</button>' +
                        //          '</div></li>';
                        //}

                        var html = "";
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        var saleflagDto = { 1: "待匹配", 2: "已转让", 3: "交易完成" }
                        for (var i = 0; i < rows.length; i++) {
                            djsrRowCount_zrz = rows.length;
                            rows[i]["Cssj"] = utils.changeDateFormat(rows[i]["Cssj"]);
                            rows[i]["payTime"] = utils.changeDateFormat(rows[i]["payTime"]);
                            rows[i]["confirmPayTime"] = utils.changeDateFormat(rows[i]["confirmPayTime"]);
                            rows[i]["flag"] = saleflagDto[rows[i].flag];


                            var dto = rows[i];
                            var lightboxId = "";
                            //匹配次数>=2,匹配次数不在显示数值，统一修改为：幸运猪
                            var show_Ppcs = dto.Ppcs;
                            if (dto.Ppcs * 1 >= 2) show_Ppcs = "幸运猪";

                            lightboxId = "lightboxsale" + dto.id;
                            var htmlQrfk = '<button class="bigbtn" style="width:50%;" onclick=\'qrfk(' + dto.ppBuyid + ')\' >確認收款</button>';
                            if (dto.flag != "已转让") htmlQrfk = "";
                            //买家的付款时间倒计时结束后，才能对买家进行申诉
                            var htmlSs = '<button class="bigbtn" style="width:30%;" onclick=\'ss(' + dto.ppBuyid + ',' + dto.id + ',' + dto.uid + ',' + dto.buyuid + ',' + dto.Cszz + ',"' + dto.buyuserId + '","' + dto.buyuserName + '","' + dto.ppBuyNo + '","' + dto.SaleNo + '")\' >申訴</button>';
                            htmlSs += ' <button class="bigbtn" style="width:30%;" onclick=\'goUss()\' >申訴记录</button>';
                            if (dto.syfksj > 0) htmlSs = "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BTZName + '</span><time>' + dto.flag + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>賣出單號</dt><dd>' + dto.SaleNo + '</dd></dl>' +
                                  '<dl><dt>出售價值</dt><dd style="color:red;">' + dto.Cszz + '</dd></dl>' +
                                  '<dl><dt>匹配次數</dt><dd>' + show_Ppcs + '</dd></dl>' +
                                  '<dl><dt>出售時間</dt><dd>' + dto.Cssj + '</dd></dl><dl><dt>購買單號</dt><dd>' + dto.ppBuyNo + '</dd></dl>' +
                                 // '<dl><dt>買家姓名</dt><dd>' + dto.buyuserNameHide + '</dd></dl>' +
                                  //'<dl><dt>買家電話</dt><dd>' + dto.buyphone + '</dd></dl>' +
                                  // '<dl><dt>買家付款方式</dt><dd>' + dto.payType + '</dd></dl>' +
                                      '<dl><dt>匯款憑證</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="匯款憑證" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                     '<dl><dt>匯款時間</dt><dd>' + dto.payTime + '</dd></dl>' +
                                       '<dl><dt>收款時間</dt><dd>' + dto.confirmPayTime + '</dd></dl>' +
                                          '<dl><dt>付款剩余時間</dt><dd style="color:red;" id="showsyfksj_zrz' + i + '"></dd></dl>' +
                                  '<span style="display:none" id="syfksj_zrz' + i + '">' + dto.syfksj + '</span>' +
                                          '<dl><dt>收款剩余時間</dt><dd style="color:red;" id="showsysksj_zrz' + i + '"></dd></dl>' +
                                  '<span style="display:none" id="sysksj_zrz' + i + '">' + dto.sysksj + '</span>' +
                                   
                                   htmlQrfk +
                                   htmlSs +
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        //$("#ZrzitemList").append(html);
                        $("#ZrzitemList").html(html);
                        //初始化圖片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                      
                    }, function () {
                        $("#ZrzitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

       

        //查詢方法
        searchMethodZrz = function () {
            paramZrz.page = 1;
            $("#ZrzitemList").empty();
            $("#ZrzitemList").html('');
            droploadZrz.unlock();
            droploadZrz.noData(false);
            droploadZrz.resetload();
        }
      

        var djsrRowCount_sale = 0;

        this.paramsale = utils.getPageData();

        var droploadsale = $('#saledatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPagesale", paramsale, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        var saleflagDto = { 1: "待转让", 2: "已转让", 3: "交易完成" }
                        for (var i = 0; i < rows.length; i++) {
                            djsrRowCount_sale = rows.length;
                            rows[i]["Cssj"] = utils.changeDateFormat(rows[i]["Cssj"]);
                            rows[i]["payTime"] = utils.changeDateFormat(rows[i]["payTime"]);
                            rows[i]["confirmPayTime"] = utils.changeDateFormat(rows[i]["confirmPayTime"]);
                            rows[i]["flag"] = saleflagDto[rows[i].flag];
                         
                          
                            var dto = rows[i];
                            var lightboxId = "";
                            var djsDisplayNone = "";
                           
                            lightboxId = "lightboxsale" + dto.id;
                            var htmlQrfk = '<button class="bigbtn" style="width:50%;" onclick=\'qrfk(' + dto.ppBuyid + ')\' >確認收款</button>';
                            if (dto.flag != "已转让") {
                                htmlQrfk = "";
                                djsDisplayNone = 'style="display:none"';
                            }
                            //买家的付款时间倒计时结束后，才能对买家进行申诉
                            var htmlSs = '<button class="bigbtn" style="width:30%;" onclick=\'ss(' + dto.ppBuyid + ',' + dto.id + ',' + dto.uid + ',' + dto.buyuid + ',' + dto.Cszz + ',"' + dto.buyuserId + '","' + dto.buyuserName + '","' + dto.ppBuyNo + '","' + dto.SaleNo + '")\' >申訴</button>';
                            htmlSs += ' <button class="bigbtn" style="width:30%;" onclick=\'goUss()\' >申訴记录</button>';
                            if (dto.syfksj > 0) htmlSs = "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BTZName + '</span><time>' + dto.flag + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>賣出單號</dt><dd>' + dto.SaleNo + '</dd></dl>' +
                                  '<dl><dt>出售價值</dt><dd style="color:red;">' + dto.Cszz + '</dd></dl>' +
                                  '<dl><dt>匹配次數</dt><dd>' + dto.Ppcs + '</dd></dl>' +
                                  '<dl><dt>出售時間</dt><dd>' + dto.Cssj + '</dd></dl><dl><dt>購買單號</dt><dd>' + dto.ppBuyNo + '</dd></dl>' +
                                  //'<dl><dt>買家姓名</dt><dd>' + dto.buyuserNameHide + '</dd></dl>' +
                                  //'<dl><dt>買家電話</dt><dd>' + dto.buyphone + '</dd></dl>' +
                                  // '<dl><dt>買家付款方式</dt><dd>' + dto.payType + '</dd></dl>' +
                                      '<dl><dt>匯款憑證</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="匯款憑證" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                     '<dl><dt>匯款時間</dt><dd>' + dto.payTime + '</dd></dl>' +
                                       '<dl><dt>收款時間</dt><dd>' + dto.confirmPayTime + '</dd></dl>' +
                                         '<dl><dt>收款剩余時間</dt><dd ' + djsDisplayNone + ' id="showsysksj' + i + '"></dd></dl>' +
                                  '<span style="display:none" id="sysksj' + i + '">' + dto.sysksj + '</span>' +
                                   htmlQrfk +
                                   htmlSs +
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#saleitemList").append(html);
                        //初始化圖片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#saleitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });


        //3秒一次不断检测
        jssecond_sysksj = 0;//全局变量

        clearInterval(flag_sysksj);//全局变量
        show_sysksj = function () {
            jssecond_sysksj = jssecond_sysksj + 2;
            for (var i = 0; i < djsrRowCount_sale; i++) {
                var sysksj = $("#sysksj" + i).html();
                var syfskjdjs = sysksj - jssecond_sysksj;
                $("#showsysksj" + i).html(formatSeconds2(syfskjdjs));
            }
            //转让中
            for (var i = 0; i < djsrRowCount_zrz; i++) {
                var sysksj_zrz = $("#sysksj_zrz" + i).html();
                var syfskjdjs_zrz = sysksj_zrz - jssecond_sysksj;
                $("#showsysksj_zrz" + i).html(formatSeconds2(syfskjdjs_zrz));
                //剩余付款时间
                var syfksj_zrz = $("#syfksj_zrz" + i).html();
                var syfksjdjs_zrz = syfksj_zrz - jssecond_sysksj;
                $("#showsyfksj_zrz" + i).html(formatSeconds2(syfksjdjs_zrz));
            }
        }
        flag_sysksj = setInterval(show_sysksj, 2000);
        show_sysksj();//默认进来就跑一次

        //跳轉到交易明細界面
        qrfk = function (id) {
            $("#prompTitle").html("您確認收款嗎？");
            $("#sureBtn").html("確認收款")
            $("#sureBtn").unbind()
            document.getElementById("divpassOpen").style.display = "block";//显示二级密码
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/Uzrmx/BTZBuysk", { id: id,passOpen:$("#passOpen").val() }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("確認收款操作成功");
                        document.getElementById("divpassOpen").style.display = "none";//不显示二级密码
                        searchMethodsale();
                        searchMethodZrz();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查詢方法
        searchMethodsale = function () {
            paramsale.page = 1;
            $("#saleitemList").empty();
            droploadsale.unlock();
            droploadsale.noData(false);
            droploadsale.resetload();
        }

        //查詢參數
        this.parambuy = utils.getPageData();

        var droploadbuy = $('#buydatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPagebuy", parambuy, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                            rows[i]["Jssj"] = utils.changeDateFormat(rows[i]["Jssj"]);
                            rows[i]["Kssj"] = utils.changeDateFormat(rows[i]["Kssj"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            if (rows[i]["isZcs"] == "0") rows[i]["ZcszjBili"] = "0";

                            var dto = rows[i];
                            //卖家的确认收款时间倒计时结束后，才能对卖家进行申诉
                            var htmlSs = '<button class="bigbtn" style="width:30%;" onclick=\'ss(' + dto.id + ',' + dto.saleId + ',' + dto.uid + ',' + dto.saleUid + ',' + dto.priceOldzz + ',"' + dto.saleuserId + '","' + dto.saleuserName + '","' + dto.BuyNo + '","' + dto.SaleNo + '")\' >申訴</button>';
                            htmlSs += ' <button class="bigbtn" style="width:30%;" onclick=\'goUss()\' >申訴记录</button>';
                            if (dto.sysksj > 0) htmlSs = "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BTZName + '</span><time>' + dto.flag + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>買入單號</dt><dd>' + dto.BuyNo + '</dd></dl>' +
                                   '<dl><dt>手續費</dt><dd>' + dto.sxf + '</dd></dl>' +
                                  '<dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                                  '<dl><dt>日收益率</dt><dd>' + dto.RsyBili + '%</dd></dl>' +
                                  '<dl><dt>加息率</dt><dd>' + dto.ZcszjBili + '%</dd></dl>' +
                                    '<dl><dt>開始時間</dt><dd>' + dto.Kssj + '</dd></dl>' +
                                   '<dl><dt>合約天數</dt><dd>' + dto.Hyts + '</dd></dl>' +
                                    '<dl><dt>結束時間</dt><dd>' + dto.Jssj + '</dd></dl>' +
                                     '<dl><dt>可挖DOGE量</dt><dd>' + dto.ZsBTT + '</dd></dl>' +
                                      '<dl><dt>可挖BTD量</dt><dd>' + dto.ZsBTD + '</dd></dl>' +
                                   '<dl><dt>操作人</dt><dd>' + dto.Czr + '</dd></dl>' +
                                    htmlSs +
                                  '</div></li>';
                        }
                        $("#buyitemList").append(html);
                    }, function () {
                        $("#buyitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });



        //查詢方法
        searchMethodbuy = function () {
            parambuy.page = 1;
            $("#buyitemList").empty();
            droploadbuy.unlock();
            droploadbuy.noData(false);
            droploadbuy.resetload();
        }

        this.paramss = utils.getPageData();
        var droploadss = $('#ssdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPagemyss", paramss, me,
                    function (rows, footers) {
                        var html = "";
                        var flagDto = { 1: "已申诉", 2: "已取消", 3: "已回复" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];

                            var dto = rows[i];
                            var btn_qxss = '<button class="bigbtn" style="width:50%;" onclick=\'qxss(' + dto.id + ')\' >取消申訴</button>';
                            if (dto.flag == "已回复" || dto.flag == "已取消" || dto.isCanqx == "no") btn_qxss = "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.Buyid + '</span><time>' + dto.addTime + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>購買單號</dt><dd>' + dto.Buyid + '</dd></dl>' +
                                  '<dl><dt>出售單號</dt><dd>' + dto.saleId + '</dd></dl><dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                                  '<dl><dt>對方編號</dt><dd>' + dto.saleuserId + '</dd></dl>' +
                                  '<dl><dt>對方姓名</dt><dd>' + dto.saleuserName + '</dd></dl>' +

                                     '<dl><dt>理由</dt><dd>' + dto.cont + '</dd></dl>' +
                                       '<dl><dt>時間</dt><dd>' + dto.addTime + '</dd></dl>' +
                                         '<dl><dt>狀態</dt><dd>' + dto.flag + '</dd></dl>' +
                                           btn_qxss +
                                  '</div></li>';

                        }

                        $("#ssitemList").append(html);

                    }, function () {
                        $("#ssitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //跳轉到交易明細界面
        qxss = function (id) {
            $("#prompTitle").html("您確定取消申訴嗎？");
            $("#sureBtn").html("確定取消申訴")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/Uzrmx/BTZBuyssqx", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("取消申訴操作成功");
                        searchMethodss();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查詢方法
        searchMethodss = function () {
            paramss.page = 1;
            $("#ssitemList").empty();
            droploadss.unlock();
            droploadss.noData(false);
            droploadss.resetload();
        }


        //初始化編輯器
        var editor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        ss = function (id, saleId, uid, saleUid, priceOldzz, saleuserId, saleuserName, BuyNo, SaleNo) {

            document.getElementById("detailTab1").style.display = "none";
            document.getElementById("detailTab2").style.display = "none";
            document.getElementById("detailTab3").style.display = "none";
            document.getElementById("detailTab4").style.display = "none";
            document.getElementById("detailTab5").style.display = "none";
            document.getElementById("detailTab6").style.display = "none";
            document.getElementById("sslemeinfo").style.display = "block";
            $("#uid").val(uid);
            $("#saleUid").val(saleUid);
            $("#Buyid").val(id);
            $("#saleId").val(saleId);
            $("#priceOldzz").val(priceOldzz);
            $("#saleuserId").val(saleuserId);
            $("#saleuserName").val(saleuserName);
            $("#BuyNo").val(BuyNo);
            $("#SaleNo").val(SaleNo);
        }

        //跳轉到申诉记录界面
        goUss = function () {
            location.href = '#Uss';
        }

        //申訴返回按鈕
        $("#closeBtnss").bind("click", function () {
            tabClick(1);
            document.getElementById("sslemeinfo").style.display = "none";
        })

        //申訴保存按鈕
        $("#saveBtnss").bind("click", function () {
            var formdata = new FormData();
            formdata.append("uid", $("#uid").val());
            formdata.append("saleUid", $("#saleUid").val());
            formdata.append("Buyid", $("#Buyid").val());
            formdata.append("saleId", $("#saleId").val());
            formdata.append("priceOldzz", $("#priceOldzz").val());
            formdata.append("saleuserId", $("#saleuserId").val());
            formdata.append("saleuserName", $("#saleuserName").val());
            formdata.append("cont", utils.getEditorHtml(editor));
            utils.AjaxPostForFormData("/User/Uzrmx/BTZBuyss", formdata, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg("申訴成功！");
                    location.href = "#Uss";
                }
            });
        });



        controller.onRouteChange = function () {
            droploadZrz = null;
            delete droploadZrz;
        };
    };

    return controller;
});