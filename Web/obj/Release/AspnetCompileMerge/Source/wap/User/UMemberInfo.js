
define(['text!UMemberInfo.html', 'jquery'], function (UMemberInfo, $) {

    var controller = function (memberId) {

        //設置標題
        $("#title").html("會員資料")
        var pmap = null;

        //設置表單默認數據
        setDefaultFormValue = function (dto) {
            $("#id").val(dto.id);
            $("span").each(function (index, ele) {
                if (dto[this.id]) {
                    $(this).html(dto[this.id]);
                }
            });
            $("p").each(function (index, ele) {
                if (dto[this.id]) {
                    if (this.id == "uLevel") {
                        $(this).html(cacheMap["ulevel"][dto[this.id + ""]]);
                    }
                    else {
                        $(this).html(dto[this.id]);
                    }

                }
            });
            $("input").each(function (index, ele) {
                if (dto[this.id]) {
                    $(this).val(dto[this.id]);
                }
            });
            if ($("#isModifyInfo").val() == "1") document.getElementById("saveBtn").style.display = "none";
            

        }
        var data = {};
        if (memberId && memberId > 0) { data["memberId"] = memberId; }

        var inputs = undefined;

        var indexChecked = [0, 0, 0];

        //打開時確認選中數據
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            if (indexChecked.length > 2) {
                e.locatePosition(2, indexChecked[2]);
            }
        }

        //選擇確認
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            if (data.length > 2) {
                $("#area").val(data[2].value);
            }
        }

        //加載會員信息
        utils.AjaxPostNotLoadding("/User/UMemberInfo/GetModel", data, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UMemberInfo);



                //省市區選擇
                var proSet = utils.InitMobileSelect('#province', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '選擇省市區', areaData, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);

                var dto = result.result;
                //初始表單默認值
                setDefaultFormValue(dto);

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        if (prev[0].id == "province" || prev[0].id == "city" || prev[0].id == "area") {
                            $("#province").val("");
                            $("#city").val("");
                            $("#area").val("");
                        }
                        dom.prev().val("");
                    });
                });


                //保存按鈕
                $("#saveBtn").bind("click", function () {
                    var checked = true;
                    //數據校驗
                    $("input").each(function (index, ele) {
                        var jdom = $(this);
                        if (jdom.attr("emptymsg") && jdom.val() == 0) {
                            utils.showErrMsg(jdom.attr("emptymsg"));
                            jdom.focus();
                            checked = false;
                            return false;
                        }
                        if (jdom.attr("id") == 'bankCard') {
                            var card = jdom.val();
                            var reg = /^(\d{16,19})$/g;
                            if (!reg.test(jdom.val())) {
                                utils.showErrMsg("銀行卡號位數必須為16-19位！");
                                jdom.focus();
                                checked = false;
                                return false;
                            }
                        }
                    });
                    if (checked) {
                        var fs = {};
                        $("#lemeinfo input").each(function () {
                            fs[this.id] = $(this).val();
                        });
                        utils.AjaxPost("/User/UMemberInfo/UpdateMember", fs, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showSuccessMsg("保存成功！");
                            }
                        });
                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});