
define(['text!serviceMan.html', 'jquery'], function (serviceMan, $) {

    var controller = function (parentId) {
        //設置標題
        $("#title").html("愛心奪寶箱");
        appView.html(serviceMan);
        clearInterval(flag_btz);//全局變量
        //隱藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

      


        //秒转时分秒
        function formatSeconds2(a) {
            var hh = parseInt(a / 3600);
            if (hh < 10) hh = "0" + hh;
            var mm = parseInt((a - hh * 3600) / 60);
            if (mm < 10) mm = "0" + mm;
            var ss = parseInt((a - hh * 3600) % 60);
            if (ss < 10) ss = "0" + ss;
            var length = hh + ":" + mm + ":" + ss;
            if (a > 0) {
                return length;
            } else {
                return "NaN";
            }
        }


        show_info = function () {
            utils.AjaxPostNotLoadding("/User/UserWeb/GetdbPara", {}, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    var account = map.account;
                    $("#agentLz").html(account.agentLz.toFixed(2));
                    $("#dbdjs").html(parseInt(map.dbdjs));
                    $("#olddbdjs").html(parseInt(map.dbdjs));
                    $("#axdbAccount").html(map.axdbAccount.toFixed(2));
                    $("#Axdbxmclz").html(map.Axdbxmclz);
                    $("#regContent").html(map.dbgz);
                    $("#dbxqs").html(map.dbxqs);
                    $("#winlist").html(map.winlist);
                    $("#isMusic").val(map.isMusic);
                    if ($("#isMusic").val() != "1") $('#music_dbx_load_Html').html('');
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }
        show_info();
        jsseconddb = 0;//全局變量
        clearInterval(flag_db);//全局變量
        show_dbbtn = function () {
            jsseconddb = jsseconddb + 1;
            var dbdjs = $("#dbdjs").html();
            var olddbdjs = $("#olddbdjs").html();
            if (dbdjs < 0)
                $("#dbdjs").html("0");
            dbdjs = olddbdjs - jsseconddb;
            if (dbdjs > 0)
            {
            $("#dbdjs").html(dbdjs);
            //$("#dbdjsFormat").html(formatSeconds2(dbdjs));
            $("#dbdjsFormat").html(dbdjs);
            }
        }
        flag_db = setInterval(show_dbbtn, 1000);



        //跳轉到交易明細界面
        iWantdb = function () {
            $("#prompTitle").html("您確定奪寶嗎？");
            $("#sureBtn").html("確定奪寶")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/UserWeb/iWantdb", { uid: 0 }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("申請奪寶成功");
                        show_info();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }




        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件監聽
        };
    };

    return controller;
});