
define(['text!UregSuccess.html', 'jquery'], function (UregSuccess, $) {

    var controller = function (name) {

        $("#title").html("注册成功");
        appView.html(UregSuccess);

        $("#userId").html("<span>会员编号：</span>" + regSuccess.userId);
        $("#regmoney").html("<span>注册金额：</span>￥" + regSuccess.regMoney);
        $("#ulevel").html("<span>注册级别：</span>" + cacheMap["ulevel"][regSuccess.uLevel]);

        controller.onRouteChange = function () {
        };
    };

    return controller;
});