
define(['text!NoPermission.html', 'jquery'], function (NoPermission, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("无权访问");
        appView.html(NoPermission);
        

        controller.onRouteChange = function () {
        };
    };

    return controller;
});