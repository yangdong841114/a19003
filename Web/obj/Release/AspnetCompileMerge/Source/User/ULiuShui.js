
define(['text!ULiuShui.html', 'jquery', 'j_easyui', 'zui','datetimepicker'], function (ULiuShui, $) {

    var controller = function (name) {

        appView.html(ULiuShui);

        //初始化查询区select
        $("#accountId").empty();
        $("#accountId").append("<option value='0'>--全部--</option>");
        var lit = cacheList["JournalClass"];
        if (lit && lit.length > 0) {
            for (var i = 0; i < lit.length; i++) {
                $("#accountId").append("<option value='" + lit[i].id + "'>" + lit[i].name + "</option>");
            }
        }

        //初始化日期选择框
        $(".form-date").datetimepicker(
        {
            language: "zh-CN",
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0,
            format: "yyyy-mm-dd"
        });

        //查询grid
        queryGrid = function () {
            var objs = $("#QueryForm").serializeObject();
            grid.datagrid("options").queryParams = objs;
            grid.datagrid("reload");
            
        }

        //查询按钮
        $("#submit").on("click", function () {
            queryGrid();
        })

        //初始化表格
        var grid = utils.newGrid("dg", {
            title:'流水帐明细',
            showFooter: true,
            columns: [[
             { field: 'abst', title: '业务摘要', width: '35%' },
             { field: 'accountId', title: '账户类型', width: '10%' },
             { field: 'optype', title: '收支', width: '10%' },
             { field: 'epotins', title: '金额', width: '15%' },
             { field: 'last', title: '余额', width: '15%' },
             { field: 'addtime', title: '日期', width: '15%' },
            ]],
            url: "ULiuShui/GetLiushuiDetailListPage"
        }, null, function (data) {
            if (data && data.rows) {
                for (var i = 0; i < data.rows.length; i++) {
                    data.rows[i]["addtime"] = utils.changeDateFormat(data.rows[i]["addtime"]);
                    data.rows[i]["epotins"] = data.rows[i]["epotins"].toFixed(2);
                    data.rows[i]["last"] = data.rows[i]["last"].toFixed(2);
                    data.rows[i].accountId = cacheMap["JournalClass"][data.rows[i].accountId];
                }
            }
            Total();
            return data;
        })


        //查询计算收入和支出总和
        function Total() {
            utils.AjaxPostNotLoadding("/User/ULiuShui/GetTotalMoney", { accountId: $("#accountId").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#srMoney").html(result.msg);
                    $("#zcMoney").html(result.other);
                }
            });
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});