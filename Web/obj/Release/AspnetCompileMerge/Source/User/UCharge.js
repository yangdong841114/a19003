
define(['text!UCharge.html', 'jquery', 'j_easyui', 'zui', 'datetimepicker'], function (UCharge, $) {

    var controller = function (name) {

        //设置表单默认数据
        setDefaultValue = function () {
            $("#sysBankId").val("0");
            $("#toBankCard").html("");
            $("#toBankUser").html("");
            $("#fromBank").val("");
            $("#epoints").val("");
            $("#bankTime").val("");
            $("#file").val("");
        }

        //加载会员信息
        utils.AjaxPostNotLoadding("UCharge/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UCharge);
                var list = result.list;

                //初始化汇入银行下拉框
                $("#sysBankId").empty();
                $("#sysBankId").append("<option value='0'>--请选择--</option>");
                if (list && list.length > 0) {
                    for (var i = 0; i < list.length; i++) {
                        $("#sysBankId").append("<option value='" + list[i].id + "|" + list[i].bankCard + "|" + list[i].bankUser + "'>" + list[i].bankName + "</option>");
                    }
                }

                //汇入银行绑定选项改变事件
                $("#sysBankId").on("change", function () {
                    var val = $("#sysBankId").val();
                    if (val != 0) {
                        var v = val.split("|");
                        $("#toBankCard").html(v[1]);
                        $("#toBankUser").html(v[2]);
                    } else {
                        $("#toBankCard").html("");
                        $("#toBankUser").html("");
                    }
                })

                //初始化日期选择框
                $(".form-date").datetimepicker(
                {
                    language: "zh-CN",
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    minView: 2,
                    forceParse: 0,
                    format: "yyyy-mm-dd"
                });

                $(".form-datetime").datetimepicker(
                {
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0,
                    showMeridian: 1,
                    format: "yyyy-mm-dd hh:ii"
                });

                //初始默认值
                setDefaultValue();


                //*****************************************************银行汇款 start **********************************************************//
                //绑定获取焦点事件
                $(".form-control").each(function (index, ele) {
                    var current = $(this);
                    var parent = current.parent();
                    current.on("focus", function (event) {
                        parent.removeClass("has-error");
                        utils.destoryPopover(current);
                    })
                })

                //保存
                $("#saveBtn").on('click', function () {
                    var val = $("#sysBankId").val();
                    var current = $("#sysBankId");
                    var parent = current.parent();
                    var g = /^\d+(\.{0,1}\d+){0,1}$/;
                    var isChecked = true;
                    if (val == 0) {
                        parent.addClass("has-error");
                        utils.showPopover(current, "请选择汇入银行", "popover-danger");
                        isChecked = false;
                    }
                    if ($("#fromBank").val() == 0) {
                        var ct = $("#fromBank");
                        var pp = ct.parent();
                        pp.addClass("has-error");
                        utils.showPopover(ct, "请录入汇出银行", "popover-danger");
                        isChecked = false;
                    }
                    if (!g.test($("#epoints").val())) {
                        var ct = $("#epoints");
                        var pp = ct.parent();
                        pp.addClass("has-error");
                        utils.showPopover(ct, "充值金额格式不正确", "popover-danger");
                        isChecked = false;
                    }
                    if ($("#bankTime").val() == 0) {
                        var ct = $("#bankTime");
                        var pp = ct.parent();
                        pp.addClass("has-error");
                        utils.showPopover(ct, "请选择汇款时间", "popover-danger");
                        isChecked = false;
                    }
                    if ($("#file").val() == 0) {
                        var ct = $("#file");
                        var pp = ct.parent();
                        pp.addClass("has-error");
                        utils.showPopover(ct, "请上传汇款凭证", "popover-danger");
                        isChecked = false;
                    }

                    if (isChecked) {
                        utils.confirm("确定要充值吗？", function () {
                            var formdata = new FormData();
                            var bankId = $("#sysBankId").val().split("|")[0];
                            formdata.append("sysBankId", bankId);
                            formdata.append("fromBank", $("#fromBank").val());
                            formdata.append("epoints", $("#epoints").val());
                            formdata.append("bankTime", $("#bankTime").val());
                            formdata.append("img", $("#file")[0].files[0]);

                            utils.AjaxPostForFormData("UCharge/SaveCharge", formdata, function (result) {
                                if (result.status == "fail") {
                                    utils.showErrMsg(result.msg);
                                } else {
                                    utils.showSuccessMsg("操作成功！");
                                    dto = result.result;
                                    setDefaultValue(dto);
                                    grid.datagrid("reload");
                                }
                            });
                        })
                    }

                })

                //*****************************************************银行汇款 end **********************************************************//

                //*****************************************************支付宝付款 start **********************************************************//
                var g = /^\d+(\.{0,1}\d+){0,1}$/;

                $("#epoints2").on("focus", function () {
                    $("#epoints2").removeClass("inputError");
                });

                $("#zfbBtn").on("click", function () {
                    if (!g.test($("#epoints2").val())) {
                        var ct = $("#epoints2");
                        ct.addClass("inputError");
                    } else {
                        utils.AjaxPost("UCharge/CreateZfbQcode", { epoints: $("#epoints2").val() }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                $("#zfbImg").attr("src", result.msg);
                            }

                        });
                    }
                });

                

                //*****************************************************支付宝付款 end **********************************************************//

                //*****************************************************微信付款 start **********************************************************//
                $("#epoints3").on("focus", function () {
                    $("#epoints3").removeClass("inputError");
                });

                $("#wxBtn").on("click", function () {
                    if (!g.test($("#epoints3").val())) {
                        var ct = $("#epoints3");
                        ct.addClass("inputError");
                    } else {
                        utils.AjaxPost("UCharge/CreateWxQcode", { epoints: $("#epoints3").val() }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                $("#wxbImg").attr("src", result.msg); zfbImg
                            }

                        });
                    }
                });
                //*****************************************************微信付款 end **********************************************************//
                

                //初始化表格
                var grid = utils.newGrid("dg", {
                    title: "充值记录",
                    columns: [[
                     { field: 'fromBank', title: '汇出银行', width: '100' },
                     { field: 'epoints', title: '充值金额', width: '80' },
                     { field: 'bankTime', title: '汇款时间', width: '130' },
                     {
                         field: '_img', title: '汇款凭证', width: '100', formatter: function (val, row, index) {
                             return '<img data-toggle="lightbox" src="' + row.imgUrl + '" data-image="' + row.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" width="100">';
                         }
                     },
                     { field: 'toBank', title: '汇入银行', width: '100' },
                     { field: 'bankCard', title: '银行卡号/支付订单号', width: '160' },
                     { field: 'bankUser', title: '开户名/支付帐号', width: '140' },
                     { field: 'addTime', title: '充值日期', width: '130' },
                     { field: 'status', title: '状态', width: '130' },
                    ]],
                    url: "UCharge/GetListPage"
                }, null, function (data) {
                    if (data && data.rows) {
                        for (var i = 0; i < data.rows.length; i++) {
                            data.rows[i]["addTime"] = utils.changeDateFormat(data.rows[i]["addTime"]);
                            //data.rows[i]["bankTime"] = utils.changeDateFormat(data.rows[i]["bankTime"]);
                            data.rows[i]["status"] = data.rows[i].ispay == 1 ? "待审核" : "已通过";
                        }
                    }
                    return data;
                }, function () {
                    $(".img-thumbnail").each(function (index, ele) {
                        $(this).lightbox();
                    })
                })
                // datagrid


                //查询grid
                queryGrid = function () {
                    var objs = $("#QueryForm").serializeObject();
                    if (objs) {
                        for (name in objs) {
                            if (name == "typeId2") {
                                objs["typeId"] = objs[name];
                            }
                        }
                    }
                    grid.datagrid("options").queryParams = objs;
                    grid.datagrid("reload");
                }

                //查询按钮
                $("#mgQueryBtn").on("click", function () {
                    queryGrid();
                })

            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});