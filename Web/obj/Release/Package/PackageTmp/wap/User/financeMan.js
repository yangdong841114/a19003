
define(['text!financeMan.html', 'jquery'], function (financeMan, $) {

    var controller = function (parentId) {
        //设置标题
        $("#title").html("財務管理");


        ShowTakeCash = function () {
            if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "") {
                document.getElementById("waitThing_Txdz").style.display = "block";
                utils.showOrHiddenPromp();
            }
        }

        ShowUsyzBTZ = function () {
            if (parseFloat($("#currentHour").val())==-1) {
                utils.showErrMsg($("#currentHourMsg").val());
            } else {
                location.href = "#UsyzBTZ";
            }
        }

        utils.AjaxPostNotLoadding("/User/UserWeb/GetUserInfoMessage", {}, function (result) {
            if (result.status == "success") {
                appView.html(financeMan);
                clearInterval(flag_btz);//全局变量
                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });


                var map = result.map;
                var menuItems = map.menuItems
                var account = map.account;
                $("#ZJZ").html(map.ZJZ.toFixed(2));
                $("#Jqrsl").html(map.Jqrsl.toFixed(2));
                $("#Btzsl").html(map.Btzsl.toFixed(2));
                $("#zrmx_account").html(map.zrmx_account);
                $("#ZZC").html(map.ZZC.toFixed(2));
                $("#LJSY").html(map.LJSY.toFixed(2));
                $("#currentHour").val(map.currentHour);
                $("#currentHourMsg").val(map.currentHourMsg);
                

                $("#agentLz").html(account.agentLz.toFixed(2));        //电子币
                $("#agentBTD").html(account.agentBTD.toFixed(2));        //奖金币
                $("#agentBTT").html(account.agentBTT.toFixed(2));        //购物币
                $("#agentFt").html(account.agentFt.toFixed(2));        //复投币
                $("#agentTjsy").html(account.agentTjsy.toFixed(2));
                $("#agentTdj").html(account.agentTdj.toFixed(2));
                $("#agentCsj").html(account.agentCsj.toFixed(2));
                //$("#agentLysy").html(account.agentLysy.toFixed(2));



                $("#bankName").val(map.userInfo.bankName);
                $("#bankCard").val(map.userInfo.bankCard);
                $("#bankUser").val(map.userInfo.bankUser);
                $("#bankAddress").val(map.userInfo.bankAddress);
                var items = menuItem[parentId];
                if (items && items.length > 0) {
                    var banHtml = "";
                    var epHtml = "";
                    for (var i = 0; i < items.length; i++) {
                        var node = items[i];
                        var url = (node.curl + "").replace("User/", "");
                        if (node.isShow == 0) {
                          
                                if (node.id == "1020302") {
                                    //提现
                                    var txHtml = '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                                    if ($("#bankName").val() == "" || $("#bankCard").val() == "" || $("#bankUser").val() == "" || $("#bankAddress").val() == "")
                                        txHtml = '<li><div  onclick="ShowTakeCash()"><i  class="fa ' + node.imgUrl + '"></i><p style="color:#333;">' + node.resourceName + '</p></div></li>';
                                    else
                                        txHtml = '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                                    banHtml += txHtml;
                                }
                                else
                                    banHtml += '<li><a href="#' + url + '"><i class="fa ' + node.imgUrl + '"></i><p>' + node.resourceName + '</p></a></li>';
                            
                        }
                    }

                    //$("#itemCont").html(banHtml);
                   
                }
            } else {
                utils.showErrMsg(result.msg);
            }
        });

        controller.onRouteChange = function () {
            $('button').off('click');   //解除所有click事件监听
        };
    };

    return controller;
});