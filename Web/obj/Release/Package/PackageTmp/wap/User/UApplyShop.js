
define(['text!UApplyShop.html', 'jquery'], function (UApplyShop, $) {

    var controller = function (name) {

        $("#title").html("申請城市合夥人");
        var dto = undefined;

        //設置顯示內容
        setShowContent = function (dto) {
            $("#agentForm").css("display", "none");
            $("#agent0").css("display", "none");
            $("#agent1").css("display", "none");
            $("#agent2").css("display", "none");
            if (dto.isAgent == 0) {
                $("#shopName").html(dto.userId);
                $("#Bdzxzflz").html(dto.spare0);
                $("#Bdzxztrs").html(dto.spare1);
                $("#Bdzxxxrs").html(dto.spare2);
                $("#agentForm").css("display", "block");
                $("#agent0").css("display", "block");
            } else if (dto.isAgent == 1) {
                $("#shopName").html(dto.agentName);
                $("#mmy").html(dto.regAgentmoney);
                $("#province").val(dto.provinceAgent);
                $("#city").val(dto.cityAgent);
                $("#Bdzxzflz").html(dto.spare0);
                $("#Bdzxztrs").html(dto.spare1);
                $("#Bdzxxxrs").html(dto.spare2);
                $("#agentForm").css("display", "block");
                $("#agent1").css("display", "block");
            } else {
                $("#shopName2").html(dto.agentName);
                $("#provinceAgent").html(dto.provinceAgent);
                $("#cityAgent").html(dto.cityAgent);
                $("#agent2").css("display", "block");;
            }
        }

        //var indexChecked = [0, 0, 0];
        var indexChecked = [0,0];
        //打開時確認選中數據
        initPosition = function (e) {
            e.locatePosition(0, indexChecked[0]);
            e.locatePosition(1, indexChecked[1]);
            //if (indexChecked.length > 2) {
            //    e.locatePosition(2, indexChecked[2]);
            //}
        }

        //選擇確認
        selectProvince = function (data) {
            $("#province").val(data[0].value);
            $("#city").val(data[1].value);
            //if (data.length > 2) {
            //    $("#area").val(data[2].value);
            //}
        }

        //加載默認密碼
        utils.AjaxPostNotLoadding("/User/UApplyShop/GetDefaultMsg", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(UApplyShop);
                dto = result.result;

                setShowContent(dto);


                //省市區選擇
                var proSet = utils.InitMobileSelect('#province', '選擇省市區', areaDatahhr, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var citySet = utils.InitMobileSelect('#city', '選擇省市區', areaDatahhr, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);
                var areaSet = utils.InitMobileSelect('#area', '選擇省市區', areaDatahhr, null, indexChecked, null, function (indexArr, data) {
                    selectProvince(data);
                    indexChecked = indexArr;
                }, initPosition);


                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //提交申請按鈕
                $("#saveBtn").on('click', function () {
                    if ($("#read")[0].checked == false) {
                        utils.showErrMsg("请勾选协议");
                    }
                    else
                    utils.showOrHiddenPromp();
                });

                //確認提交按鈕
                $("#sureBtn").on('click', function () {

                    
                    if (dto && dto.isAgent == 0) {
                        utils.AjaxPost("/User/UApplyShop/ApplyShop", { provinceAgent: $("#province").val(), cityAgent: $("#city").val() }, function (result) {
                            if (result.status == "fail") {
                                utils.showErrMsg(result.msg);
                            } else {
                                utils.showOrHiddenPromp();
                                dto = result.result;
                                setShowContent(dto)
                                utils.showSuccessMsg("申請成功，請等待管理員審核！");
                            }
                        });
                    } else {
                        utils.showErrMsg("申請失敗，無法確認當前登錄用戶");
                    }
                });
            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});