
define(['text!Udbjl.html', 'jquery'], function (Udbjl, $) {

    var controller = function (name) {

        //設置標題
        $("#title").html("奪寶記錄");
        appView.html(Udbjl);

        //初始化日期選擇框
        utils.initCalendar(["startTime", "endTime"]);
        var dto = null;

        //轉賬明細
        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UMTransferdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UserWeb/GetListPageDbjl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var flagDto = { 1: "搶奪中", 2: "錯失寶藏", 3: "奪得寶藏" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addTime + '</time>' + dto.dbxqs + '<span class="sum">' + dto.flag + '</span>' + dto.userId +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>奪寶時間</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>奪寶賬號</dt><dd>' + dto.userId + '</dd></dl>' +
                                  '<dl><dt>投入藍鉆</dt><dd>' + dto.trLz + '</dd></dl><dl><dt>狀態</dt><dd>' + dto.flag + '</dd></dl>' +
                                  '<dl><dt>奪得藍鉆</dt><dd>' + dto.getLz + '</dd></dl>' +
                                  '<dl><dt>期數</dt><dd>' + dto.dbxqs + '</dd></dl>' +
                                  '</div></li>';
                        }
                        $("#UMTransferitemList").append(html);
                    }, function () {
                        $("#UMTransferitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UMTransferitemList").empty();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查詢按鈕
        $("#searchBtn").bind("click", function () {
            searchMethod();
        })



        controller.onRouteChange = function () {
        };
    };

    return controller;
});