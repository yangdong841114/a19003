
define(['text!Ujymx.html', 'jquery'], function (Ujymx, $) {

    var controller = function (Buyid) {
       
        //设置标题
        $("#title").html("交易明细")
        appView.html(Ujymx);
        //选项卡切换
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("dlbankName").style.display = "block";
                document.getElementById("dlbankCard").style.display = "block";
                document.getElementById("dlbankUser").style.display = "block";
                document.getElementById("dlbankAddress").style.display = "block";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "none";
                $("#payType").val("银行汇款");
                $("#tabBtn1").addClass("active")
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").removeClass("active")
            }
            if (index == 2) {
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "block";
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "none";
                $("#payType").val("支付宝扫码");
                $("#tabBtn1").removeClass("active")
                $("#tabBtn2").addClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").removeClass("active")
            }
            if (index == 3) {
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgwx").style.display = "block";
                document.getElementById("dlImgszhb").style.display = "none";
                $("#payType").val("微信扫码");
                $("#tabBtn1").removeClass("active")
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").addClass("active")
                $("#tabBtn4").removeClass("active")
                
            }
            if (index == 4) {
                document.getElementById("dlbankName").style.display = "none";
                document.getElementById("dlbankCard").style.display = "none";
                document.getElementById("dlbankUser").style.display = "none";
                document.getElementById("dlbankAddress").style.display = "none";
                document.getElementById("dlImgzfb").style.display = "none";
                document.getElementById("dlImgwx").style.display = "none";
                document.getElementById("dlImgszhb").style.display = "block";
                $("#payType").val("数字货币扫码");
                $("#tabBtn1").removeClass("active")
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn4").addClass("active")

            }
           
        }

        this.paramss = utils.getPageData();
        var droploadss = $('#ssdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPagess", paramss, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BuyNo + '</span><time>' + dto.addTime + '</time>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                  '<dl><dt>購買單號</dt><dd>' + dto.BuyNo + '</dd></dl>' +
                                  '<dl><dt>出售單號</dt><dd>' + dto.SaleNo + '</dd></dl><dl><dt>價值</dt><dd>' + dto.priceOldzz + '</dd></dl>' +
                                  '<dl><dt>對方編號</dt><dd>' + dto.saleuserId + '</dd></dl>' +
                                  '<dl><dt>對方姓名</dt><dd>' + dto.saleuserName + '</dd></dl>' +

                                     '<dl><dt>理由</dt><dd>' + dto.cont + '</dd></dl>' +
                                       '<dl><dt>時間</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '</div></li>';

                        }

                        $("#ssitemList").append(html);

                    }, function () {
                        $("#ssitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethodss = function () {
            paramss.page = 1;
            $("#ssitemList").empty();
            param["Buyid"] = $("#ssBuyid").val();
            droploadss.unlock();
            droploadss.noData(false);
            droploadss.resetload();
        }




        var flagDto = { 1: "已預約", 2: "已申請領養", 3: "領養成功", 4: "領養失敗", 5: "已付款", 6: "已確認付款", 7: "付款超時領養失敗", 8: "已出售" }
        //查詢參數
        this.paramJymx = utils.getPageData();

        var droploadJymx = $('#Jymxdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPageJymx?id=" + Buyid, paramJymx, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的圖片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Ppsj"] = utils.changeDateFormat(rows[i]["Ppsj"]);
                            rows[i]["payTime"] = utils.changeDateFormat(rows[i]["payTime"]);
                            rows[i]["confirmPayTime"] = utils.changeDateFormat(rows[i]["confirmPayTime"]);
                            var flagValue = rows[i]["flag"];
                            rows[i]["flag"] = flagDto[rows[i].flag];

                            var fk_div_display = "";
                            if (flagValue == "3") fk_div_display = "block";
                            else fk_div_display = "none";

                            var fk_btn_display_none = "display:none";
                            if (flagValue == "3") fk_btn_display_none = "";//能付款的就显示这按钮

                            var dto = rows[i];
                            var lightboxId = "";
                            var fkfsHtml = "";
                            var fkInfo = '<dl><dt>匯款憑證</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="匯款憑證" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                     '<dl><dt>匯款時間</dt><dd>' + dto.payTime + '</dd></dl>' +
                                       '<dl><dt>收款時間</dt><dd>' + dto.confirmPayTime + '</dd></dl>';
                            if (flagValue == "3") fkInfo = "";//付款时不显示已付款的信息

                            $("#payType").val("银行汇款");
                            if (dto.skIsbank == "1")
                                fkfsHtml = '<dl><dt>賣家收款方式</dt><dd>銀行匯款</dd></dl>' +
                                     '<dl><dt>開戶行</dt><dd>' + dto.bankName + '</dd></dl>' +
                                      '<dl><dt>銀行卡號</dt><dd>' + dto.bankCard + '</dd></dl>' +
                                       '<dl><dt>開戶名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                                        '<dl><dt>開戶支行</dt><dd>' + dto.bankAddress + '</dd></dl>';
                            if (dto.skIszfb == "1") {
                                lightboxId = "lightboxzfb" + dto.id;
                                fkfsHtml += '<dl><dt>賣家收款方式</dt><dd>支付寶掃碼</dd></dl>' +
                                        '<dl><dt>圖片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlzfb + '" data-image="' + dto.imgUrlzfb + '" data-caption="掃碼" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId);
                                $("#payType").val("支付宝扫码");
                            }
                            if (dto.skIswx == "1") {
                                lightboxId = "lightboxwx" + dto.id;
                                fkfsHtml += '<dl><dt>賣家收款方式</dt><dd>微信掃碼</dd></dl>' +
                                        '<dl><dt>圖片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlwx + '" data-image="' + dto.imgUrlwx + '" data-caption="掃碼" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId);
                                $("#payType").val("微信扫码");
                            }
                            if (dto.skIsszhb == "1") {
                                lightboxId = "lightboxszhb" + dto.id;
                                fkfsHtml += '<dl><dt>賣家收款方式</dt><dd>' + dto.szhbmc + '掃碼</dd></dl>' +
                                        '<dl><dt>圖片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb + '" data-image="' + dto.imgUrlszhb + '" data-caption="掃碼" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                             
                                lightboxArray.push(lightboxId)
                                if (dto.szhbmc1!="")
                                {
                                    lightboxId = "lightboxszhb1" + dto.id;
                                    fkfsHtml += '<dl><dt>賣家收款方式</dt><dd>' + dto.szhbmc1 + '掃碼</dd></dl>' +
                                            '<dl><dt>圖片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb1 + '" data-image="' + dto.imgUrlszhb1 + '" data-caption="掃碼" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';

                                    lightboxArray.push(lightboxId)
                                }
                                $("#payType").val("数字货币扫码");
                            }

                            lightboxId = "lightboxhkpz" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.saleuserId + '</span><time>' + dto.flag + '</time>' +
                                  '<i class="fa fa-angle-right  fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>賣出單號</dt><dd>' + dto.SaleNo + '</dd></dl>' +
                                      //'<dl><dt>區塊豬編號</dt><dd>' + dto.BTZCode + '</dd></dl>' +
                                    //'<dl><dt>區塊豬名稱</dt><dd>' + dto.BTZName + '</dd></dl>' +
                                      '<dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                                        '<dl><dt>智能合約收益</dt><dd>' + dto.Hyts + '天/' + dto.Hyts * dto.RsyBili + '%</dd></dl>' +
                                  '<dl><dt>賣方編號</dt><dd>' + dto.saleuserId + '</dd></dl>'+//<dl><dt>賣方電話</dt><dd>' + dto.salephone + '</dd></dl>' +
                                  //'<dl><dt>價值</dt><dd style="color:red;">' + dto.priceOldzz + '</dd></dl>' +
                                  '<dl><dt>領養時間</dt><dd>' + dto.Ppsj + '</dd></dl>' +
                                   fkfsHtml +
                                  fkInfo +


                                       '<div  class="entryinfo contentbox" style="display:' + fk_div_display + '"><dl><dt>匯款圖片</dt>' +
                            '<dd>' +
                            '<div class="uploaddiv">' +
                            '<div class="uploadimg">' +
                            '<span class="uploadimgfile" style="background-image:url(-testimg/testd1.jpg);" id="showImg"></span>' +
                            '<div class="uploadicon"><i class="fa fa-image"></i><br />上傳匯款圖片</div>' +
                            '<label class="uploadplug"><input class="fileinput" type="file" id="imgFile" /></label>' +
                            '</div>' +
                            '<div class="clear"></div>' +
                            ' </div>' +
                            '</dd>' +
                            '</dl>' +
                            '<dl>' +
                            '<dt>二級密碼</dt>' +
                            '<dd>' +
                            '<input type="password" required="required" class="entrytxt" style="width:80%;float:right;" placeholder="二級密碼" id="passOpen" />' +
                            ' </dd>' +
                            '</dl></div>' +
                                 '<button class="bigbtn" style="width:30%;' + fk_btn_display_none + '" onclick=\'fk(' + flagValue + ',' + dto.id + ',"' + dto.szhbmc + '","' + dto.szhbmc1 + '","' + dto.bankName + '","' + dto.bankCard + '","' + dto.bankUser + '","' + dto.bankAddress + '","' + dto.imgUrlzfb + '","' + dto.imgUrlwx + '","' + dto.imgUrlszhb + '","' + dto.imgUrlszhb1 + '")\' >確認匯款</button>' +
                                 '<button class="bigbtn" style="width:30%;" onclick=\'ss(' + dto.id + ',' + dto.saleId + ',' + dto.uid + ',' + dto.saleUid + ',' + dto.priceOldzz + ',"' + dto.saleuserId + '","' + dto.saleuserName + '","' + dto.BuyNo + '","' + dto.SaleNo + '")\' >申訴</button>' +
                                  '<button class="bigbtn" style="width:30%;" onclick=\'ssxq(' + dto.id + ')\' >申訴詳情</button>' +
                                  '</div></li>';
                            lightboxArray.push(lightboxId);
                           

                        }

                        $("#JymxitemList").append(html);
                        //初始化圖片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                        //預覽圖片
                        $("#imgFile").bind("change", function () {
                            var url = URL.createObjectURL($(this)[0].files[0]);
                           
                            document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
                        })
                    }, function () {
                        $("#JymxitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });


        //查詢方法
        searchMethodJymx = function () {
            paramJymx.page = 1;
            $("#JymxitemList").empty();
            droploadJymx.unlock();
            droploadJymx.noData(false);
            droploadJymx.resetload();
        }

        fk = function (flag, id, szhbmc, szhbmc1, bankName, bankCard, bankUser, bankAddress, imgUrlzfb, imgUrlwx, imgUrlszhb, imgUrlszhb1) {
            if (flag != "3")
                utils.showErrMsg("領養成功的記錄才能付款");
            else {
                //document.getElementById("detailTab1").style.display = "none";
                //document.getElementById("maintag").style.display = "block";
                //document.getElementById("lemeinfo").style.display = "block";
                $("#id").val(id);
                //两个名称拼起来
                //var szhbmc_all = "";
                //if(szhbmc != "null")
                //    szhbmc_all = szhbmc;
                //if (szhbmc1 != "null")
                //    szhbmc_all = szhbmc_all + szhbmc1;
                //if(szhbmc_all != "")
                //    $('#tabA4').html(szhbmc_all + "掃碼");

                //$("#bankName").val(bankName);
                //$("#bankCard").val(bankCard);
                //$("#bankUser").val(bankUser);
                //$("#bankAddress").val(bankAddress);
                //$("#Imgzfb").attr("src", imgUrlzfb);
                //$("#Imgwx").attr("src", imgUrlwx);
                //$("#Imgszhb").attr("src", imgUrlszhb);
                //$("#Imgszhb1").attr("src", imgUrlszhb1);
                var formdata = new FormData();
                formdata.append("id", $("#id").val());
                formdata.append("payType", $("#payType").val());
                formdata.append("passOpen", $("#passOpen").val());
                formdata.append("imgFile", $("#imgFile")[0].files[0]);
                utils.AjaxPostForFormData("/User/Uzrmx/BTZBuyfk", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        //document.getElementById("detailTab1").style.display = "block";
                        //document.getElementById("maintag").style.display = "none";
                        //document.getElementById("lemeinfo").style.display = "none";
                        searchMethodJymx();
                        utils.showSuccessMsg("付款成功！");
                    }
                });


            }
        }

        ss = function (id, saleId, uid, saleUid, priceOldzz, saleuserId, saleuserName, BuyNo, SaleNo) {

            document.getElementById("detailTab1").style.display = "none";
            document.getElementById("maintag").style.display = "none";
            document.getElementById("lemeinfo").style.display = "none";
            document.getElementById("sslemeinfo").style.display = "block";
            $("#uid").val(uid);
            $("#saleUid").val(saleUid);
            $("#Buyid").val(id);
            $("#saleId").val(saleId);
            $("#priceOldzz").val(priceOldzz);
            $("#saleuserId").val(saleuserId);
            $("#saleuserName").val(saleuserName);
            $("#BuyNo").val(BuyNo);
            $("#SaleNo").val(SaleNo);
        }
        ssxq = function (id) {
            document.getElementById("detailTab1").style.display = "none";
            document.getElementById("maintag").style.display = "none";
            document.getElementById("lemeinfo").style.display = "none";
            document.getElementById("sslemeinfo").style.display = "none";
            document.getElementById("detailTabss").style.display = "block";
            $("#ssBuyid").val(id);
            searchMethodss();
        }
        //初始化編輯器
        var editor = new Quill("#editor", {
            modules: {
                toolbar: utils.getEditorToolbar()
            },
            theme: 'snow'
        });

        //申訴返回按鈕
        $("#closeBtnss").bind("click", function () {
            document.getElementById("detailTab1").style.display = "block";
            document.getElementById("maintag").style.display = "none";
            document.getElementById("lemeinfo").style.display = "none";
            document.getElementById("sslemeinfo").style.display = "none";
        })

        //申訴保存按鈕
        $("#saveBtnss").bind("click", function () {
            var formdata = new FormData();
            formdata.append("uid", $("#uid").val());
            formdata.append("saleUid", $("#saleUid").val());
            formdata.append("Buyid", $("#Buyid").val());
            formdata.append("saleId", $("#saleId").val());
            formdata.append("priceOldzz", $("#priceOldzz").val());
            formdata.append("saleuserId", $("#saleuserId").val());
            formdata.append("saleuserName", $("#saleuserName").val());
            formdata.append("cont", utils.getEditorHtml(editor));
            utils.AjaxPostForFormData("/User/Uzrmx/BTZBuyss", formdata, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    document.getElementById("detailTab1").style.display = "block";
                    document.getElementById("maintag").style.display = "none";
                    document.getElementById("lemeinfo").style.display = "none";
                    document.getElementById("sslemeinfo").style.display = "none";
                    searchMethodss();
                    utils.showSuccessMsg("申訴成功！");
                }
            });
        });

       

        //付款返回按鈕
        $("#closeBtn").bind("click", function () {
            document.getElementById("detailTab1").style.display = "block";
            document.getElementById("maintag").style.display = "none";
            document.getElementById("lemeinfo").style.display = "none";
        })
        //保存按鈕
        $("#saveBtn").bind("click", function () {
           
        });





        controller.onRouteChange = function () {
        };
    };

    return controller;
});