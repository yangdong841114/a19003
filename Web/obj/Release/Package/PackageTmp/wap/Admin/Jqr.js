
define(['text!Jqr.html', 'jquery'], function (Jqr, $) {

    var controller = function (name) {

        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }

        //设置标题
        $("#title").html("机器人")
        appView.html(Jqr);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })
        $("#clearQueryBtnzr").bind("click", function () {
            utils.clearQueryParam();
        })
        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);
        utils.initCalendar(["startTimezr", "endTimezr"]);
       
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#JqrDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Jqr/GetListPage", param, me,
                    function (rows, footers) {
            
          
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["gmTime"] = utils.changeDateFormat(rows[i]["gmTime"], 'minute');
                            rows[i]["dqTime"] = utils.changeDateFormat(rows[i]["dqTime"], 'minute');
                            rows[i]["Lzsl"] = rows[i]["Lzsl"].toFixed(2);
                          

                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.userId + '</span><time>' + dto.gmTime + '</time>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>用户名</dt><dd>' + dto.userName + '</dd></dl><dl><dt>机器人ID</dt><dd>' + dto.id + '</dd></dl>' +
                            '<dl><dt>蓝钻数量</dt><dd>' + dto.Lzsl + '</dd></dl><dl><dt>认购时间</dt><dd>' + dto.gmTime + '</dd></dl>' +
                            '<dl><dt>到期时间</dt><dd>' + dto.dqTime + '</dd></dl><dl><dt>来源</dt><dd>' + dto.ly + '</dd></dl>' +
                        
                            '</div></li>';
                        }
                        var html_totalepoints = " <p>累计出售机器人总数:XX 有效机器伙总数:XX</p> <p>累计出售机器人总价值:0.0</p>";
                        for (var i = 0; i < footers.length; i++) {
                            var dto = footers[i];
                            html_totalepoints = '<p>累计出售总数:' + dto.ljcszs + ' &nbsp;  有效机器总数:' + dto.yxzs + ' &nbsp;  累计出售总价值:' + dto.ljcszjz + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#JqrItemList").append(html);
                       
                    }, function () {
                      
                        $("#JqrItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#JqrItemList").empty();
            param["userId"] = $("#userId").val();
            param["userName"] = $("#userName").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })



        //查询参数
        this.paramzr = utils.getPageData();

        var droploadzr = $('#JqrzrDatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/Jqr/GetListPagezr", paramzr, me,
                    function (rows, footers) {


                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"], 'minute');
                           
                            var dto = rows[i];
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.fromUserId + '</span><time>' + dto.addTime + '</time>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>转出会员ID</dt><dd>' + dto.fromUserId + '</dd></dl><dl><dt>转出会员姓名</dt><dd>' + dto.fromUserName + '</dd></dl>' +
                            '<dl><dt>转入会员ID</dt><dd>' + dto.toUserId + '</dd></dl><dl><dt>转入会员姓名</dt><dd>' + dto.toUserName + '</dd></dl>' +
                           
                            '</div></li>';
                        }
                       
                        $("#JqrzrItemList").append(html);

                    }, function () {

                        $("#JqrzrItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethodzr = function () {
            paramzr.page = 1;
            $("#JqrzrItemList").empty();
            paramzr["fromUserId"] = $("#fromUserId").val();
            paramzr["fromUserName"] = $("#fromUserName").val();
            paramzr["startTime"] = $("#startTimezr").val();
            paramzr["endTime"] = $("#endTimezr").val();
            droploadzr.unlock();
            droploadzr.noData(false);
            droploadzr.resetload();
        }

        //查询按钮
        $("#searchBtnzr").on("click", function () {
            searchMethodzr();
        })




        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});