
define(['text!Dbx.html', 'jquery'], function (Dbx, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("夺宝箱")
        appView.html(Dbx);


        //夺宝箱总设置
        InitDbSet = function () {

            utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetDb", {}, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    var baseSet = map.baseSet;
                    $("#dbxqs").html(baseSet.dbxqs);
                    $("#dbdjs").html(parseInt(baseSet.dbdjs));
                    $("#bqtzrs").html(baseSet.bqtzrs);
                    $("#jclz").html(baseSet.jclz);
                    $("#sxf").html(baseSet.sxf);
                }
            })
        }
        InitDbSet();
      

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        
        //添加按钮
        $("#kqdbBtn").bind("click", function () {

            utils.AjaxPost("/Admin/ManageWeb/kqdb", { id: 0 }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("开启成功");
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        });

        //增加奖池
        $("#deployProduct").bind("click", function () {
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });
        //保存发布商品
        $("#saveProductBtn").bind("click", function () {

            $("#prompTitle").html("确定增加金额吗？");
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/ManageWeb/AddDbJc", { trLz: $("#trLz").val() }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("增加成功");
                        searchMethod();
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        InitDbSet();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        
         })
        //关闭增加奖池
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

      
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#AccountUpdatedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ManageWeb/GetListPageDbx", param, me,
                    function (rows, footers) {
                        var html = "";
                        var flagDto = { 1: "抢夺中", 2: "错失宝藏", 3: "夺得宝藏", 4: "后台注资" };
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                         
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.addTime + '</span>';
                            html += '<span class="sum">' + dto.dbxqs + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>夺宝时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                  '<dl><dt>投入蓝钻</dt><dd>' + dto.trLz + '</dd></dl><dl><dt>状态</dt><dd>' + dto.flag + '</dd></dl>' +
                                  '<dl><dt>夺得蓝钻</dt><dd>' + dto.getLz + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#AccountUpdateitemList").append(html);
                    }, function () {
                        $("#AccountUpdateitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#AccountUpdateitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

       

        controller.onRouteChange = function () {
        };
    };

    return controller;
});