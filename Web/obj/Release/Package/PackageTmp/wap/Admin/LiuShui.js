
define(['text!LiuShui.html', 'jquery'], function (LiuShui, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("会员流水账")
        appView.html(LiuShui);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //进入明细界面
        toLiuShuiDetail = function (id) {
            location.href = '#LiuDetail/' + id;
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#LiuShuidatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/LiuShui/GetMemberAccountListPage", param, me,
                    function (rows, footers) {
                        var footer = footers[0];
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["agentLysy"] = rows[i]["agentLysy"].toFixed(2);
                            rows[i]["agentTjsy"] = rows[i]["agentTjsy"].toFixed(2);
                            rows[i]["agentCsj"] = rows[i]["agentCsj"].toFixed(2);
                            rows[i]["agentFt"] = rows[i]["agentFt"].toFixed(2);
                            rows[i]["agentTdj"] = rows[i]["agentTdj"].toFixed(2);
                            rows[i]["agentLz"] = rows[i]["agentLz"].toFixed(2);
                            rows[i]["agentBTT"] = rows[i]["agentBTT"].toFixed(2);
                            rows[i]["agentBTD"] = rows[i]["agentBTD"].toFixed(2);

                            var dto = rows[i];
                            dto.remark = (dto.remark && dto.remark != "null") ? dto.remark : "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.userName + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga1">' +
                            '<li><button class="sdelbtn" onclick=\'toLiuShuiDetail(' + dto.id + ')\'>流水明细</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>领养收益</dt><dd>' + dto.agentLysy + '</dd></dl><dl><dt>推荐收益</dt><dd>' + dto.agentTjsy + '</dd></dl>' +
                            '<dl><dt>城市奖</dt><dd>' + dto.agentCsj + '</dd></dl><dl><dt>团队奖</dt><dd>' + dto.agentTdj + '</dd></dl>' +
                             '<dl><dt>蓝钻</dt><dd>' + dto.agentLz + '</dd></dl><dl><dt>复投</dt><dd>' + dto.agentFt + '</dd></dl>' +
                                '<dl><dt>BTT</dt><dd>' + dto.agentBTT + '</dd></dl><dl><dt>BTD</dt><dd>' + dto.agentBTD + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#totalLysy").html(footer.agentLysy);
                        $("#totalTjsy").html(footer.agentTjsy);
                        $("#totalCsj").html(footer.agentCsj);
                        $("#totalTdj").html(footer.agentTdj);
                        $("#totalLz").html(footer.agentLz);
                        $("#totalFt").html(footer.agentFt);
                        $("#totalBtd").html(footer.agentBTD);
                        $("#totalBtt").html(footer.agentBTT);
                        $("#LiuShuiitemList").append(html);
                        Total();
                    }, function () {
                        $("#LiuShuiitemList").append('<p class="dropload-noData">暂无数据</p>');
                        Total();
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#LiuShuiitemList").empty();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        function Total() {
            utils.AjaxPostNotLoadding("/Admin/LiuShui/GetTotalMoney", { accountId: $("#accountId").val() }, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    $("#srMoney").html(result.msg);
                    $("#zcMoney").html(result.other);
                }
            });
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/LiuShui/ExportLiuShuiExcel";
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});