
define(['text!ShopPassing.html', 'jquery', 'j_easyui'], function (ShopPassing, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("待开通合伙人");
        appView.html(ShopPassing);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#ShopPassing", toUid: 0 }, function () { });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //删除记录
        deleteMethod = function (memberId) {
            $("#prompTitle").html("确定删除吗？");
            $("#prompCont").empty();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定删除</button>')
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/ShopPassing/Delete", { memberId: memberId }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        cancelMethod = function (memberId) {
            $("#prompTitle").html("确定取消吗？");
            $("#prompCont").empty();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定取消</button>')
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/ShopPassing/Cancel", { memberId: memberId }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //开通报单中心
        openShop = function (memberId) {
            $("#prompTitle").html("确定开通合伙人吗？");
            $("#prompCont").empty();
            $("#propBtnbox").empty();
            $("#propBtnbox").html('<button class="bigbtn" id="sureBtn">确定开通</button>')
            $("#sureBtn").unbind();
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/ShopPassing/Save", { memberId: memberId }, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg(result.msg);
                        searchMethod();
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#ShopPassingDataList').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/ShopPassing/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["applyAgentTime"] = utils.changeDateFormat(rows[i]["applyAgentTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];

                            var dto = rows[i];
                            var btn_open = '<li><button class="seditbtn" onclick="openShop(\'' + dto.id + '\')">确认</button></li>' +
                            '<li><button class="sdelbtn" onclick="deleteMethod(\'' + dto.id + '\')">删除</button></li>';
                            var btn_cancel = '<li><button class="sdelbtn" onclick="cancelMethod(\'' + dto.id + '\')">取消</button></li>';
                            if (dto.isAgent != "2") btn_cancel = "";
                            if (dto.isAgent == "2") btn_open = "";
                            var show_isAgent = "";
                            if (dto.isAgent == "1") show_isAgent = "未审核";
                            if (dto.isAgent == "2") show_isAgent = "已通过";
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.applyAgentTime + '</time><span class="sum">' + dto.userId + '</span>';
                            html += '&nbsp;<span class="sum">' + dto.provinceAgent + ' ' + dto.cityAgent +'-'+ show_isAgent + '</span><i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo"><div class="btnbox"><ul class="tga2">' +
                            btn_open + btn_cancel+
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>会员级别</dt><dd>' + dto.uLevel + '</dd></dl><dl><dt>省份</dt><dd>' + dto.provinceAgent + '</dd></dl><dl><dt>城市</dt><dd>' + dto.cityAgent + '</dd></dl>' +
                            '<dl><dt>申请日期</dt><dd>' + dto.applyAgentTime + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#ShopPassingItemList").append(html);
                    }, function () {
                        $("#ShopPassingItemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ShopPassingItemList").empty();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});