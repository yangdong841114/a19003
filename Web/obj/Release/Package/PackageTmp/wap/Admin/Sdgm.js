
define(['text!Sdgm.html', 'jquery'], function (Sdgm, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("手动挂卖")
        appView.html(Sdgm);

        //切换选项卡
        changeTab = function (index) {
            $("#tabLi1").removeClass("active");
            $("#tabLi2").removeClass("active");
            $("#tabDiv1").css("display", "none");
            $("#tabDiv2").css("display", "none");
            $("#tabDiv" + index).css("display", "block");
            $("#tabLi" + index).addClass("active");
        }


        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

     
        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);
        //var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售" }买记录状态
        //审核状态选择框
        var saleflagList = [{ id: 0, name: "全部" }, { id: 1, name: "待转让" }, { id: 2, name: "已转让" }, { id: 3, name: "交易完成" }];
        utils.InitMobileSelect('#saleflagName', '选择状态', saleflagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#saleflagName").val(data[0].name);
            $("#saleflag").val(data[0].id);
        });
        utils.InitMobileSelect('#BTZName', '选择状态', btzlist, { id: "id", value: "BTZName" }, [0], null, function (indexArr, data) {
            $("#BTZName").val(data[0].BTZName);
            $("#Btzid").val(data[0].id);
        });
        utils.InitMobileSelect('#suserId', '选择虚拟会员', xnmemlist, { id: "userId", value: "userName" }, [0], null, function (indexArr, data) {
            $("#suserName").val(data[0].userName);
            $("#suserId").val(data[0].userId);
        });
        utils.InitMobileSelect('#sBTZName', '选择状态', btzlist, { id: "id", value: "BTZName", value1: "Gzzzxx", value2: "Gzzzsx" }, [0], null, function (indexArr, data) {
            $("#sBTZName").val(data[0].BTZName);
            $("#sBtzid").val(data[0].id);
            $("#Gzzzxx").val(data[0].Gzzzxx);
            $("#Gzzzsx").val(data[0].Gzzzsx);
        });

        
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       

       
       
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Sdgmdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPageSdgm", param, me,
                    function (rows, footers) {
                        var html = "";
                      
                        var saleflagDto = { 1: "待转让", 2: "已转让", 3: "交易完成" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Cssj"] = utils.changeDateFormat(rows[i]["Cssj"]);
                            rows[i]["flag"] = saleflagDto[rows[i].flag];
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.userId + '</span><time>' + dto.flag + '</time>' +
                                  '<i class="fa fa-angle-right"></i></div>' +
                                  '<div class="allinfo">' +
                                    '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="seditbtn" onclick=\'jymx(' + dto.id + ')\'>交易详情</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'del(' + dto.id + ')\'>删除</button></li>' +
                            '</ul></div>' +
                                  '<dl><dt>卖出单号</dt><dd>' + dto.SaleNo + '</dd></dl>' +
                                  '<dl><dt>比特猪名称</dt><dd>' + dto.BTZName + '</dd></dl>' +
                                  '<dl><dt>出售价值</dt><dd>' + dto.Cszz + '</dd></dl>' +
                                  '<dl><dt>已转让价值</dt><dd>' + dto.Ppzz + '</dd></dl>' +
                                  '<dl><dt>匹配次数</dt><dd>' + dto.Ppcs + '</dd></dl>' +
                                  '<dl><dt>出售时间</dt><dd>' + dto.Cssj + '</dd></dl>' +
                                  '<dl><dt>卖方姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                  '<dl><dt>卖家电话</dt><dd>' + dto.salephone + '</dd></dl>' +
                                   '<dl><dt>操作人</dt><dd>' + dto.Czr + '</dd></dl>' +
                            '</div></li>';
                        }
                        var html_totalepoints = " <p>出售人数：XX 出售记录总数： 出售总价值：XX</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>出售人数:' + dto.totalCsrs + ' &nbsp;  出售记录总数:' + dto.totalJlzs  + ' &nbsp;  出售总价值:' + dto.totalZjz.toFixed(2) + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#SdgmitemList").append(html);
                       
                    }, function () {
                        $("#SdgmitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //跳转到交易明细界面
        jymx = function (id) {
            location.href = '#Csjl/saleId' + id;
        }

        //删除
        del = function (id) {
            $("#prompTitle").html("您确定删除吗？");
            $("#sureBtn").html("确定删除")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/delBTZSale", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("确定删除操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#SdgmitemList").empty();
            param["userId"] = $("#userId").val();
            param["flag"] = $("#saleflag").val();
            param["BTZid"] = $("#Btzid").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            changeTab(1);
        })
        //保存发布商品
        $("#saveProductBtn").bind("click", function () {

            $("#prompTitle").html("您确定挂卖吗？");
            $("#sureBtn").html("确定挂卖")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/addBTZSale", { userId: $("#suserId").val(), Btzid: $("#sBtzid").val(), Cszz: $("#Cszz").val(), Fbsm: $("#Fbsm").val() }, function (result) {
                    if (result.status == "success") {
                        changeTab(1);
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("挂卖操作成功");
                        searchMethod();
                       
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});