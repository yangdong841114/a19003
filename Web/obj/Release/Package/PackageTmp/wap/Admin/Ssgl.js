
define(['text!Ssgl.html', 'jquery'], function (Ssgl, $) {

    var controller = function (id) {
        //设置标题
        $("#title").html("申诉管理")
        appView.html(Ssgl);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })
       
        var editor = null;
        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        var isSsstateList = [{ id: 100, name: "全部" }, { id: 0, name: "否" }, { id: 1, name: "是" }];
        utils.InitMobileSelect('#isSsstateName', '选择状态', isSsstateList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#isSsstateName").val(data[0].name);
            $("#isSsstate").val(data[0].id);
        });

        var isJcjyList = [{ id: 100, name: "全部" }, { id: 0, name: "否" }, { id: 1, name: "是" }];
        utils.InitMobileSelect('#isJcjyName', '选择状态', isJcjyList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#isJcjyName").val(data[0].name);
            $("#isJcjy").val(data[0].id);
        });

        var flagList = [{ id: 0, name: "全部" }, { id: 1, name: "已申诉" }, { id: 2, name: "已取消" }, { id: 3, name: "已回复" }];
        utils.InitMobileSelect('#flagName', '选择状态', flagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });
       
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       

     
      
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Chargedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPageSsgl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        var flagDto = { 1: "已申诉", 2: "已取消", 3: "已回复" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                         
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;


                            var btn_qxss = '<button class="seditbtn" style="width:25%;" onclick=\'qxss(' + dto.id + ')\' >取消申诉</button>';
                            var btn_hhss = '&nbsp;<button class="seditbtn" style="width:18%;" onclick=\'hhss(' + dto.id + ')\' >回复</button>';
                            var btn_jcss = '&nbsp;<button class="seditbtn" style="width:25%;" onclick=\'jcss(' + dto.id + ')\' >解除申诉</button>';
                            var btn_jcjy = '&nbsp;<button class="seditbtn" style="width:25%;" onclick=\'jcjy(' + dto.id + ')\' >解除交易</button>';
                            if (dto.flag == "已回复")
                            {
                                btn_qxss = "";
                                btn_hhss = "";
                            }
                            var show_isSsstate = "是";
                            if (dto.isSsstate == "0") {//状态是‘已解除申诉’，屏蔽解除交易按钮
                                btn_jcss = "";
                                btn_jcjy = "";
                                show_isSsstate = "否";
                            }
                            var show_isJcjy = "是";
                            if (dto.isJcjy == "1") btn_jcjy = "";
                            if (dto.isJcjy == "0") show_isJcjy = "否";

                            if (dto.flag == "已取消")//状态‘已取消’，屏蔽解除申诉，解除交易按钮
                            {
                                btn_jcss = "";
                                btn_jcjy = "";
                            }
                            //未处理的日期显示红色
                            var font_color = 'style="color:red;"';
                            if (dto.flag == "已回复" || dto.flag == "已取消") font_color = "";

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum" ' + font_color + ' >' + dto.addTime +' ' +dto.flag + '</span>';
                           
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                          
                           '<dl><dt>购买单号</dt><dd><a href="#Csjl/buyId' + dto.Buyid + '">' + dto.BuyNo + '</a></dd></dl>' +
                                  '<dl><dt>出售单号</dt><dd><a href="#Csjl/buyId' + dto.Buyid + '">' + dto.SaleNo + '</a></dd></dl><dl><dt>价值</dt><dd>' + dto.priceOldzz + '</dd></dl>' +
                                  '<dl><dt>对方编号</dt><dd>' + dto.saleuserId + '</dd></dl>' +
                                  '<dl><dt>对方姓名</dt><dd>' + dto.saleuserName + '</dd></dl>' +

                                     '<dl><dt>理由</dt><dd>' + dto.cont + '</dd></dl>' +
                                       '<dl><dt>时间</dt><dd>' + dto.addTime + '</dd></dl>' +
                                          '<dl><dt>状态</dt><dd>' + dto.flag + '</dd></dl>' +
                                          '<dl><dt>回复</dt><dd>' + dto.hhcont + '</dd></dl>' +
                                           '<dl><dt>投诉类型</dt><dd>' + dto.ssType + '</dd></dl>' +
                                            '<dl><dt>申诉状态</dt><dd>' + show_isSsstate + '</dd></dl>' +
                                             '<dl><dt>解除交易</dt><dd>' + show_isJcjy + '</dd></dl>' +
                              btn_qxss +
                              btn_hhss +
                              btn_jcss +
                              btn_jcjy+
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                      
                        $("#ChargeitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#ChargeitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //取消申诉
        qxss = function (id) {
            $("#prompTitle").html("您确定取消申诉吗？");
            $("#sureBtn").html("确定取消申诉")
            $("#sureBtn").unbind();
            $("#prompCont").html("");
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuyssqx", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("取消申诉操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
        //解除申诉
        jcss = function (id) {
            $("#prompTitle").html("您确定解除申诉吗？");
            $("#sureBtn").html("确定解除申诉")
            $("#sureBtn").unbind();
            $("#prompCont").html("");
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuyssjc", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("解除申诉操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
        //解除交易
        jcjy = function (id) {
            $("#prompTitle").html("您确定解除交易吗？");
            $("#sureBtn").html("确定解除交易")
            $("#sureBtn").unbind();
            $("#prompCont").html("");
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuyssjcjy", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("解除交易操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
        //回复申诉
        hhss = function (id) {
            $("#prompTitle").html("您确定回复申诉吗？");
            $("#sureBtn").html("回复申诉");
            $("#sureBtn").unbind();
            var contHtml = '<ul><li><dl><dt>回复内容</dt></dl><div style="width:98%;padding-left:1%;"><div id="editor" style="height:25rem;" ></div></div>' +
            '</li></ul><input type="hidden" id="id" value="' + id + '"/>';
            $("#prompCont").html(contHtml);
            editor = new Quill("#editor", {
                modules: {
                    toolbar: utils.getEditorToolbar()
                },
                theme: 'snow'
            });
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuysshh", { id: id, hhcont: utils.getEditorHtml(editor)}, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("回复申诉操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
       
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ChargeitemList").empty();
            param["userId"] = $("#userId").val();
            param["isSsstate"] = $("#isSsstate").val();
            param["flag"] = $("#flag").val();
            param["isJcjy"] = $("#isJcjy").val();

            param["BTZid"] = $("#Btzid").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            param["saleuserId"] = $("#saleuserId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }
       
     
        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});