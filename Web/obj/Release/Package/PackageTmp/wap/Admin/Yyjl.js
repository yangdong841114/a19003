
define(['text!Yyjl.html', 'jquery'], function (Yyjl, $) {

    var controller = function (id) {
        //设置标题
        $("#title").html("预约记录")
        appView.html(Yyjl);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })
       
       
        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);
        //var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售" }买记录状态
        //审核状态选择框
        var flagList = [{ id: 0, name: "全部" }, { id: 1, name: "已预约" }, { id: 2, name: "已获领养资格" }, { id: 3, name: "領養成功" }];
        utils.InitMobileSelect('#flagName', '选择状态', flagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });
        utils.InitMobileSelect('#BTZName', '选择状态', btzlist, { id: "id", value: "BTZName" }, [0], null, function (indexArr, data) {
            $("#BTZName").val(data[0].BTZName);
            $("#Btzid").val(data[0].id);
        });
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       

     
      
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Chargedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPageYyjl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        var saleflagDto = { 1: "待转让", 2: "已转让", 3: "交易完成" }
                        var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售", 9: "解除交易" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Yysj"] = utils.changeDateFormat(rows[i]["Yysj"]);
                            rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                            rows[i]["Ppsj"] = utils.changeDateFormat(rows[i]["Ppsj"]);
                            rows[i]["YyVipsj"] = utils.changeDateFormat(rows[i]["YyVipsj"]);
                            rows[i]["Jqrkqsj"] = utils.changeDateFormat(rows[i]["Jqrkqsj"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;


                           

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.userName + '</span>';
                           
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="seditbtn" onclick=\'jymx(' + dto.id + ')\'>交易详情</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>求购单号</dt><dd>' + dto.BuyNo + '</dd></dl><dl><dt>比特猪ID</dt><dd>' + dto.BTZid + '</dd></dl>' +
                            '<dl><dt>比特猪名称</dt><dd>' + dto.BTZName + '</dd></dl><dl><dt>手续费</dt><dd>' + dto.sxf + '</dd></dl>' +
                             '<dl><dt>预约时间</dt><dd>' + dto.Yysj + '</dd></dl><dl><dt>抢养时间</dt><dd>' + dto.Qysj + '</dd></dl>' +

                            '<dl><dt>匹配时间</dt><dd>' + dto.Ppsj + '</dd></dl><dl><dt>VIP开启时间</dt><dd>' + dto.YyVipsj + '</dd></dl>' +
                            '<dl><dt>机器人开启时间</dt><dd>' + dto.Jqrkqsj + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.Czr + '</dd></dl>' +
                            '<dl><dt>省</dt><dd>' + dto.province + '</dd></dl>' +
                            '<dl><dt>市</dt><dd>' + dto.city + '</dd></dl>' +

                            '<dl><dt>买单状态</dt><dd>' + dto.flag + '</dd></dl>' +

                          
                                 
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        var html_totalepoints = " <p>求购人数：XX 求购记录总数：XX 匹配人数：XX</p><p>手续费总额：XX 出售总价值：XX</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>求购人数:' + dto.totalLyrs + ' &nbsp;  求购记录总数:' + dto.totalJlzs + '&nbsp;  匹配人数:' + dto.totalPprs + '</p>' +
                                '<p>手续费总额:' + dto.totalSxf.toFixed(2) + ' &nbsp;  出售总价值:' + dto.totalZjz.toFixed(2) + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#ChargeitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#ChargeitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });
        //跳转到交易明细界面
        jymx = function (id) {
            location.href = '#Csjl/buyId' + id;
        }
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ChargeitemList").empty();
            param["userId"] = $("#userId").val();
            param["province"] = $("#province").val();
            param["city"] = $("#city").val();
            param["flag"] = $("#flag").val();
            param["BTZid"] = $("#Btzid").val();
            param["startTimeYysj"] = $("#startTime").val();
            param["endTimeYysj"] = $("#endTime").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }
       
     
        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});