
define(['text!BTZjs.html', 'jquery'], function (BTZjs, $) {

    var controller = function (name) {
        ////设置标题
        //$("#center").panel("setTitle","会员注册");

        appView.html(BTZjs);


        //截取字符串
        sub = function (str, n) {
            var r = /[^\x00-\xff]/g;
            if (str.replace(r, "mm").length <= n) { return str; }
            var m = Math.floor(n / 2);
            for (var i = m; i < str.length; i++) {
                if (str.substr(0, i).replace(r, "mm").length >= n) {
                    return str.substr(0, i) + "...";
                }
            }
            return str;
        }

        //初始化滚动图片
        InitHImgs = function () {
            $(".main_visual").hover(function () {
                $("#btn_prev,#btn_next").fadeIn()
            }, function () {
                $("#btn_prev,#btn_next").fadeOut()
            });

            $dragBln = false;

            $(".main_image").touchSlider({
                flexible: true,
                speed: 400,
                btn_prev: $("#btn_prev"),
                btn_next: $("#btn_next"),
                paging: $(".flicking_con a"),
                counter: function (e) {
                    $(".flicking_con a").removeClass("on").eq(e.current - 1).addClass("on");
                }
            });

            $(".main_image").bind("mousedown", function () {
                $dragBln = false;
            });

            $(".main_image").bind("dragstart", function () {
                $dragBln = true;
            });

            $(".main_image a").click(function () {
                if ($dragBln) {
                    return false;
                }
            });

            timer = setInterval(function () {
                $("#btn_next").click();
            }, 10000);

            $(".main_visual").hover(function () {
                clearInterval(timer);
            }, function () {
                timer = setInterval(function () {
                    $("#btn_next").click();
                }, 10000);
            });

            $(".main_image").bind("touchstart", function () {
                clearInterval(timer);
            }).bind("touchend", function () {
                timer = setInterval(function () {
                    $("#btn_next").click();
                }, 10000);
            });
        }

        utils.AjaxPostNotLoadding("/User/UserWeb/GetMainData", {}, function (result) {
            if (result.status == "success") {
              

                var btzCount = 0;
                //络绎阁显示
                this.param = utils.getPageData()

                var dropload = $('#UBTZdatalist').dropload({
                    scrollArea: window,
                    autoLoad: true,
                    domDown: { domNoData: '<p class="dropload-noData"></p>' },
                    loadDownFn: function (me) {
                        utils.LoadPageData("/User/UserWeb/GetListPageBTZ", param, me,
                            function (rows, footers) {
                                var html = "";
                                btzCount = rows.length;
                                for (var i = 0; i < rows.length; i++) {
                                    var dto = rows[i];
                                    html += '<li>' +
                                          '<p><img data-toggle="lightbox" src="' + dto.imgUrl + '"  class="img-thumbnail" width="200" height="200" alt=""></p>' +
                                          '<span>名称：' + dto.BTZName + '</span><br>'
                                          + '<span>领养时间：' + dto.KsqysjStr + '-' + dto.JsqysjStr + '</span><br>'
                                          + '预约倒计时：<span id="Yydjs' + i + '">' + parseInt(dto.Yydjs) + '</span><span style="display:none" id="oldYydjs' + i + '">' + parseInt(dto.Yydjs) + '</span>秒<br>'
                                          + '领养倒计时：<span id="Qydjs' + i + '">' + parseInt(dto.Qydjs) + '</span><span style="display:none" id="oldQydjs' + i + '">' + parseInt(dto.Qydjs) + '</span>秒<br>'
                                          + '领养结束倒计时：<span id="Qyjsdjs' + i + '">' + parseInt(dto.Qyjsdjs) + '</span><span style="display:none" id="oldQyjsdjs' + i + '">' + parseInt(dto.Qyjsdjs) + '</span>秒<br>'
                                          + '<span>价值：' + dto.Zszzxx + '-' + dto.Zszzsx + '</span><br>'
                                          + '<span>预约/即抢领养靈氣：' + dto.Yysxlz + '/' + dto.Jqsxlz + '</span><br>'
                                           + '<span>智能收益率：' + dto.RsyBili + '%/天</span><br>'
                                          + '<span>可挖DOGE：' + dto.ZsBTT + '</span><br>'
                                          + '<span>可挖BTD：' + dto.ZsBTD + '</span><br>'
                                          + '<button class="bigbtn" style="width:80%;" onclick=\'fzz(' + dto.id + ')\' id="Btnfzz' + i + '">繁殖中</button><br>'
                                          + '<button class="bigbtn" style="width:80%;" onclick=\'yy(' + dto.id + ')\' id="Btnyy' + i + '">预约</button><br>'
                                          + '<button class="bigbtn" style="width:80%;" onclick=\'yyvip(' + dto.id + ')\' id="Btnyyvip' + i + '">预约 VIP+1%</button><br>'
                                          + '<button class="bigbtn" style="width:80%;" onclick=\'ly(' + dto.id + ')\' id="Btnly' + i + '">领养</button><br>'
                                    '</li>';
                                }
                                $("#UBTZitemList").append(html);
                            }, function () {
                                $("#UBTZitemList").append('<p class="dropload-noData">暂无数据</p>');
                            });
                    }
                });
                //络绎阁显示

                //查询方法
                searchMethod = function () {
                    param.page = 1;
                    $("#UBTZitemList").empty();
                    dropload.unlock();
                    dropload.noData(false);
                    dropload.resetload();
                }

                //络绎阁显示按钮3秒一次不断检测
              
                jssecond = 0;
                clearInterval(flag_btz);

                show_btzbtn = function () {
                    jssecond = jssecond + 2;
                    //$("#userId").html(jssecond);
                    for (var i = 0; i < btzCount; i++) {
                        var Yydjs = $("#Yydjs" + i).html();
                        var Qydjs = $("#Qydjs" + i).html();
                        var Qyjsdjs = $("#Qyjsdjs" + i).html();
                        var oldYydjs = $("#oldYydjs" + i).html();
                        var oldQydjs = $("#oldQydjs" + i).html();
                        var oldQyjsdjs = $("#oldQyjsdjs" + i).html();
                        if (Yydjs < 0)
                            document.getElementById("Yydjs" + i).style.display = "none";
                        if (Qydjs < 0)
                            document.getElementById("Qydjs" + i).style.display = "none";
                        if (Qyjsdjs < 0)
                            document.getElementById("Qyjsdjs" + i).style.display = "none";

                        //繁殖按钮显示条件:时间没有落在-预约与结束时间之间-就不显示
                        if (Yydjs > 0 || Qyjsdjs < 0)
                            document.getElementById("Btnfzz" + i).style.display = "block";
                        else
                            document.getElementById("Btnfzz" + i).style.display = "none";
                        //预约按钮显示条件：时间必须落在预约与开始时间之间
                        if (Yydjs < 0 && Qydjs > 0) {
                            document.getElementById("Btnyy" + i).style.display = "block";
                            document.getElementById("Btnyyvip" + i).style.display = "block";
                        }
                        else {
                            document.getElementById("Btnyy" + i).style.display = "none";
                            document.getElementById("Btnyyvip" + i).style.display = "none";
                        }
                        //领养按钮显示条件：必须在开始与结束时间之间
                        if (Qydjs < 0 && Qyjsdjs > 0)
                            document.getElementById("Btnly" + i).style.display = "block";
                        else
                            document.getElementById("Btnly" + i).style.display = "none";


                        //倒计时按触发时间不断减少
                        Yydjs = oldYydjs - jssecond;
                        Qydjs = oldQydjs - jssecond;
                        Qyjsdjs = oldQyjsdjs - jssecond;
                        $("#Yydjs" + i).html(Yydjs);
                        $("#Qydjs" + i).html(Qydjs);
                        $("#Qyjsdjs" + i).html(Qyjsdjs);

                    }

                }
                flag_btz = setInterval(show_btzbtn, 2000);

                //隐藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //退出按钮
                $("#exitSystem").bind('click', function () {
                    document.getElementById("waitThing_Txdz").style.display = "none";
                    document.getElementById("btnbox").style.display = "block";
                    $("#prompTitle").html("您确定退出登录吗？");
                    $("#sureBtn").unbind();
                    $("#sureBtn").bind("click", function () {
                        location.href = "/Home/exitUserLogin";
                    });
                    utils.showOrHiddenPromp();
                });

                var map = result.map;
                if (map) {
                    var user = map.user;
                    var account = map.account;
                    //$("#agentDz").html(account.agentDz.toFixed(2));        //电子币
                    //$("#agentJj").html(account.agentJj.toFixed(2));        //奖金币
                    //$("#agentGw").html(account.agentGw.toFixed(2));        //购物币
                    //$("#agentFt").html(account.agentFt.toFixed(2));        //复投币
                    //$("#bankName").val(user.bankName);
                    //$("#bankCard").val(user.bankCard);
                    //$("#bankUser").val(user.bankUser);
                    //$("#bankAddress").val(user.bankAddress);
                    //$("#agentTotal").html(account.agentTotal.toFixed(2));  //累计奖金
                    $("#userId").html(user.userId);
                    //$("#userId2").html(user.userId);
                    //$("#uLevel").html(cacheMap["ulevel"][user.uLevel]);
                    //$("#rLevel").html(cacheMap["rLevel"][user.rLevel]);
                    var baseset = map.baseSet;
                    //广告图
                    var banners = map.banners;
                    if (banners && banners.length > 0) {
                        var banHtml = "";
                        for (var i = 0; i < banners.length; i++) {
                            banHtml += '<li><img src="' + banners[i].imgUrl + '" /></li>';
                        }
                        $("#slides").html(banHtml);
                    }


                    //文章
                    var newsList = map.newsList;
                    var hh = "";
                    if (newsList && newsList.length > 0) {
                        for (var i = 0; i < newsList.length; i++) {
                            var n = newsList[i];
                            hh += '<li><a href="#UArticDetail/' + n.id + '">' + sub(n.title, 43) + '</a><time>' + utils.changeDateFormat(n.addTime) + '</time></li>';
                        }
                        $("#newsItem").html(hh);

                        document.getElementById("waitThing_Txdz").style.display = "block";
                        document.getElementById("btnbox").style.display = "none";
                        $("#prompTitle").html("请查看最新公告");
                        utils.showOrHiddenPromp();
                    }



                }

                //滚动广告图
                InitHImgs();


            } else {
                utils.showErrMsg(result.msg);
            }
        });




        controller.onRouteChange = function () {
            console.log('change');      //可以做一些销毁工作，例如取消事件绑定
        };
    };

    return controller;
});