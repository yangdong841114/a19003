
define(['text!UChangePassword.html', 'jquery'], function (UChangePassword, $) {

    var controller = function (name) {

        var tab2Init = false;
        var tab3Init = false;

        //修改活動樣式
        changeClass = function (index) {
            $("#litab1").removeClass("active");
            $("#litab2").removeClass("active");
            $("#litab3").removeClass("active");
            document.getElementById("passDiv1").style.display = "none";
            document.getElementById("passDiv2").style.display = "none";
            document.getElementById("passDiv3").style.display = "none";
            document.getElementById("passDiv" + index).style.display = "block";
            $("#litab" + index).addClass("active");
        }

        //保存數據
        saveData = function (oldPass, newPass, flag, cls, yzm) {
            var data = { oldPass: oldPass, newPass: newPass, flag: flag, yzm: yzm };
            utils.AjaxPost("/User/UChangePassword/ChangePassword", data, function (result) {
                if (result.status == "fail") {
                    utils.showErrMsg(result.msg);
                } else {
                    utils.showSuccessMsg("修改成功！");
                    //清空表單
                    $("." + cls).each(function (index, ele) {
                        $(this).val("");
                    });
                }
            });
        }

        //選項卡切換
        changeTab = function (index) {
            changeClass(index);
            if (index == 1) {
                $("#title").html("修改登錄密碼");
                document.getElementById("imgPicYzm_pw1").src = "/Common/ManageAppCode?etc=" + (new Date()).getTime();
            } else if (index == 2) {
                $("#title").html("修改安全密碼");
                //图片验证码变化
                document.getElementById("imgPicYzm_pw2").src = "/Common/ManageAppCode?etc=" + (new Date()).getTime();
                if (!tab2Init) {
                    //綁定修改安全密碼按鈕
                    $("#changBtn2").on('click', function () {
                        if ($("#oldPassOpen").val() == 0) {
                            utils.showErrMsg("舊安全密碼不能為空");
                        } else if ($("#passOpen").val() == 0) {
                            utils.showErrMsg("新安全密碼不能為空");
                        } else if ($("#passOpen").val() != $("#passOpenRe").val()) {
                            utils.showErrMsg("確認新密碼與新安全密碼不壹致");
                        } else {
                            saveData($("#oldPassOpen").val(), $("#passOpen").val(), 2, "pss2", $("#yzm_pw2").val());
                        }
                    });
                    tab2Init = true;
                }
            } else {
                $("#title").html("修改交易密碼");
                if (!tab3Init) {
                    //綁定修改交易密碼按鈕
                    $("#changBtn3").on('click', function () {
                        if ($("#oldThreepass").val() == 0) {
                            utils.showErrMsg("舊交易密碼不能為空");
                        } else if ($("#threepass").val() == 0) {
                            utils.showErrMsg("新交易密碼不能為空");
                        } else if ($("#threepass").val() != $("#threepassRe").val()) {
                            utils.showErrMsg("確認新密碼與新交易密碼不壹致");
                        } else {
                            saveData($("#oldThreepass").val(), $("#threepass").val(), 3, "pss3");
                        }
                    });
                    tab3Init = true;
                }
            }
        }

        //設置標題
        $("#title").html("修改登錄密碼")
        appView.html(UChangePassword);


        utils.CancelBtnBind();

        var flag_pw1 = null;
        var s_pw1 = 120;
        show_pw1 = function () {
            if (s_pw1 >= 1) {
                $("#jyfBtn_pw1").text("校驗碼已發-" + s_pw1 + "-秒後可重發");
            } else {
                clearInterval(flag_pw1);
                s_pw1 = 120;
                $("#jyfBtn_pw1")[0].disabled = false;
                $("#jyfBtn_pw1").text("獲取校驗碼");
            }
            s_pw1--;
        }
        //图片验证码变化
        document.getElementById("imgPicYzm_pw1").src = "/Common/ManageAppCode?etc=" + (new Date()).getTime();

        //发短信
        $("#jyfBtn_pw1").bind("click", function () {
            var data = { "totalNum": "", "workType": "pw1", "picYzm": $("#picYzm_pw1").val() }
            utils.AjaxPost("/User/UserWeb/SendPhoneCode", data, function (result) {
                if (result.status == "success") {
                    flag_pw1 = setInterval(show_pw1, 1000);
                    $("#jyfBtn_pw1")[0].disabled = true;
                }
                else
                    utils.showErrMsg(result.msg);
            });
        });


        var flag_pw2 = null;
        var s_pw2 = 120;
        show_pw2 = function () {
            if (s_pw2 >= 1) {
                $("#jyfBtn_pw2").text("校驗碼已發-" + s_pw2 + "-秒後可重發");
            } else {
                clearInterval(flag_pw2);
                s_pw2 = 120;
                $("#jyfBtn_pw2")[0].disabled = false;
                $("#jyfBtn_pw2").text("獲取校驗碼");
            }
            s_pw2--;
        }


        //发短信
        $("#jyfBtn_pw2").bind("click", function () {
            var data = { "totalNum": "", "workType": "pw2", "picYzm": $("#picYzm_pw2").val() }
            utils.AjaxPost("/User/UserWeb/SendPhoneCode", data, function (result) {
                if (result.status == "success") {
                    flag_pw2 = setInterval(show_pw2, 1000);
                    $("#jyfBtn_pw2")[0].disabled = true;
                }
                else
                    utils.showErrMsg(result.msg);
            });
        });



        //修改登錄密碼
        $("#changBtn1").on('click', function () {
            if ($("#oldPassword").val() == 0) {
                utils.showErrMsg("舊登錄密碼不能為空");
            } else if ($("#password").val() == 0) {
                utils.showErrMsg("新登錄密碼不能為空");
            } else if ($("#password").val() != $("#passwordRe").val()) {
                utils.showErrMsg("確認新密碼與新登錄密碼不壹致");
            } else {
                saveData($("#oldPassword").val(), $("#password").val(), 1, "pss1", $("#yzm_pw1").val());
            }
        });





        controller.onRouteChange = function () {
        };
    };

    return controller;
});