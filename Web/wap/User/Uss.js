
define(['text!Uss.html', 'jquery'], function (Uss, $) {

    var controller = function (name) {



        //設置標題
        $("#title").html("申訴記錄");
        appView.html(Uss);
        //隱藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();

        });
       
        this.paramss = utils.getPageData();
        var droploadss = $('#ssdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Uzrmx/GetListPagemyss", paramss, me,
                    function (rows, footers) {
                        var html = "";
                        var flagDto = { 1: "已申诉", 2: "已取消", 3: "已回复" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];

                          
                            

                            var dto = rows[i];
                            var btn_qxss = '<button class="bigbtn" style="width:50%;" onclick=\'qxss(' + dto.id + ')\' >取消申訴</button>';
                            if (dto.flag == "已取消" || dto.flag == "已回复" || dto.isCanqx == "no") btn_qxss = "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum">' + dto.BuyNo + '</span><time>' + dto.addTime + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>購買單號</dt><dd>' + dto.BuyNo + '</dd></dl>' +
                                  '<dl><dt>出售單號</dt><dd>' + dto.SaleNo + '</dd></dl><dl><dt>價值</dt><dd>' + dto.priceOldzz + '</dd></dl>' +
                                    '<dl><dt>申訴人編號</dt><dd>' + dto.userId + '</dd></dl>' +
                                     '<dl><dt>申訴人姓名</dt><dd>' + dto.userName + '</dd></dl>' +
                                  '<dl><dt>對方編號</dt><dd>' + dto.saleuserId + '</dd></dl>' +
                                  '<dl><dt>對方姓名</dt><dd>' + dto.saleuserName + '</dd></dl>' +

                                     '<dl><dt>理由</dt><dd>' + dto.cont + '</dd></dl>' +
                                       '<dl><dt>時間</dt><dd>' + dto.addTime + '</dd></dl>' +
                                         '<dl><dt>狀態</dt><dd>' + dto.flag + '</dd></dl>' +
                                          '<dl><dt>投訴類型</dt><dd>' + dto.ssType+ '</dd></dl>' +
                                         
                                            '<dl><dt>回复</dt><dd>' + dto.hhcont + '</dd></dl>' +
                                              btn_qxss +
                                  '</div></li>';

                        }

                        $("#ssitemList").append(html);

                    }, function () {
                        $("#ssitemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //跳轉到交易明細界面
        qxss = function (id) {
            $("#prompTitle").html("您確定取消申訴嗎？");
            $("#sureBtn").html("確定取消申訴")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/User/Uzrmx/BTZBuyssqx", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("取消申訴操作成功");
                        searchMethodss();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查詢方法
        searchMethodss = function () {
            paramss.page = 1;
            $("#ssitemList").empty();
            droploadss.unlock();
            droploadss.noData(false);
            droploadss.resetload();
        }



        controller.onRouteChange = function () {
        };
    };

    return controller;
});