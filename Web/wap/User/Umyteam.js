
define(['text!Umyteam.html', 'jquery'], function (Umyteam, $) {

    var controller = function (name) {

        //設置標題
        $("#title").html("我的團隊")
        appView.html(Umyteam);

        //清空查詢條件按鈕
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //初始化日期選擇框
        utils.initCalendar(["passStartTime", "passEndTime"]);

        //綁定展開搜索更多
        utils.bindSearchmoreClick();

        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UMemberPasseddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/UMemberPassed/GetListPageMyteam", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["passTime"] = utils.changeDateFormat(rows[i]["passTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            rows[i].isLock = rows[i].isLock == 0 ? "否" : "是";
                            rows[i].isFt = rows[i].isFt == 0 ? "否" : "是";

                            var dto = rows[i];
                            html += '<li><div class="userliifo"><img src="/Content/APP/User/images/man.png"><div class="userliname"><h3>' + dto.userId + '</h3>'
					        +' <p>' + dto.phone + '</p></div>'
			                +'</div>   ' 
			                +'<div class="userlilevel">'
				            +'<span class="level1">' + dto.reLevel + '</span>'
			                +'</div>'
			                + '<div class="clear"></div>'                            + '</li>';
                        }
                        $("#UMemberPasseditemList").append(html);
                    }, function () {
                        $("#UMemberPasseditemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethod = function () {
            param.page = 1;
            $("#UMemberPasseditemList").empty();
            param["passStartTime"] = $("#passStartTime").val();
            param["passEndTime"] = $("#passEndTime").val();
            param["userId"] = $("#userId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }


        //查詢按鈕
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
        };
    };

    return controller;
});