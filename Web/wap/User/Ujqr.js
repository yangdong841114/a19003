
define(['text!Ujqr.html', 'jquery'], function (Ujqr, $) {

    var controller = function (name) {


        //選項卡切換
        tabClick = function (index) {
            if (index == 1) {
                document.getElementById("detailTab1").style.display = "block";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "none";
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").removeClass("active")
                $("#tabBtn1").addClass("active")
            }
            if (index == 2) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "block";
                document.getElementById("detailTab3").style.display = "none";
                document.getElementById("detailTab4").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn3").removeClass("active")
                $("#tabBtn2").addClass("active");

                //初始化日期選擇框
                utils.initCalendar(["startTimeJqr", "endTimeJqr"]);
                //查詢按鈕
                $("#searchBtnJqr").bind("click", function () {
                    searchMethodJqr();
                })

                //加載數據
                searchMethodJqr();
                $(".orderbriefly").click();
            }
            if (index == 3) {
                document.getElementById("detailTab1").style.display = "none";
                document.getElementById("detailTab2").style.display = "none";
                document.getElementById("detailTab3").style.display = "block";
                document.getElementById("detailTab4").style.display = "none";
                $("#tabBtn1").removeClass("active");
                $("#tabBtn2").removeClass("active")
                $("#tabBtn3").addClass("active");

                //初始化日期選擇框
                utils.initCalendar(["startTimeJqrzr", "endTimeJqrzr"]);
                //查詢按鈕
                $("#searchBtnJqrzr").bind("click", function () {
                    searchMethodJqrzr();
                })

                //加載數據
                searchMethodJqrzr();
            }
        }

        //設置標題
        $("#title").html("機器人");
        appView.html(Ujqr);


        var dto = null;
        //查詢參數
        this.param = utils.getPageData();

        var dropload = $('#UJqrdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Ujqr/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["gmTime"] = utils.changeDateFormat(rows[i]["gmTime"]);
                            rows[i]["dqTime"] = utils.changeDateFormat(rows[i]["dqTime"]);
                            rows[i]["isStop"] = rows[i].isStop == 1 ? "已失效" : "有效";

                            var dto = rows[i];
                            var htmlZrbtn = '<button class="bigbtn" style="width:50%;" onclick=\'zr(' + dto.id + ')\' >轉讓</button>';
                            if (dto.isStop == "已失效") htmlZrbtn = "";
                            html += '<li><div class="orderbriefly openit" onclick="utils.showGridMessage(this)"><span class="sum"></span><time>' + dto.gmTime + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>靈氣數量</dt><dd>' + dto.Lzsl + '</dd></dl>' +
                                  '<dl><dt>到期時間</dt><dd>' + dto.dqTime + '</dd></dl><dl><dt>來源</dt><dd>' + dto.ly + '</dd></dl>' +
                                  '<dl><dt>當前狀態</dt><dd>' + dto.isStop + '</dd></dl>' +
                                 htmlZrbtn +
                                  '</div></li>';
                        }
                        $("#UJqritemList").append(html);
                    }, function () {
                        $("#UJqritemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });


        zr = function (Jqrid) {
            $("#JqrID").val(Jqrid);
            document.getElementById("detailTab1").style.display = "none";
            document.getElementById("detailTab2").style.display = "none";
            document.getElementById("detailTab3").style.display = "none";
            document.getElementById("detailTab4").style.display = "block";
        }



        //查詢方法
        searchMethodJqr = function () {
            param.page = 1;
            $("#UJqritemList").empty();
            param["startTime"] = $("#startTimeJqr").val();
            param["endTime"] = $("#endTimeJqr").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
         
        }


        this.paramzr = utils.getPageData();

        var droploadzr = $('#UJqrzrdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/User/Ujqr/GetListPagezr", paramzr, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);

                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><span class="sum"></span><time>' + dto.addTime + '</time>' +
                                  '<i class="fa fa-angle-right fa-rotate-90"></i></div>' +
                                  '<div class="allinfo allinfoheigth" style="display: block;">' +
                                  '<dl><dt>轉出會員ID</dt><dd>' + dto.fromPhone + '</dd></dl>' +
                                  '<dl><dt>轉出會員編號</dt><dd>' + dto.fromUserId + '</dd></dl><dl><dt>轉入會員ID</dt><dd>' + dto.toPhone + '</dd></dl>' +
                                  '<dl><dt>轉入會員編號</dt><dd>' + dto.toUserId + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#UJqrzritemList").append(html);
                    }, function () {
                        $("#UJqrzritemList").append('<p class="dropload-noData">暫無數據</p>');
                    });
            }
        });

        //查詢方法
        searchMethodJqrzr = function () {
            paramzr.page = 1;
            $("#UJqrzritemList").empty();
            paramzr["startTime"] = $("#startTimeJqrzr").val();
            paramzr["endTime"] = $("#endTimeJqrzr").val();
            droploadzr.unlock();
            droploadzr.noData(false);
            droploadzr.resetload();
        }



        //加載會員信息
        utils.AjaxPostNotLoadding("/User/Ujqr/InitView", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                appView.html(Ujqr);
                var map = result.map;
                var user = map.user;
                $("#agentLz").val(user.account.agentLz.toFixed(2));
                $("#Jqrjg").html(map.Jqrlz + "/年");
                if (map.isHaveJqr == "no") document.getElementById("saveBtnJqr").style.display = "block";
                else document.getElementById("saveBtnJqr").style.display = "none";

                if (map.isCanqg == "yes") document.getElementById("saveBtnJqr").style.display = "block";
                else
                {
                    document.getElementById("saveBtnJqr").style.display = "none";
                    utils.showErrMsg(map.isCanqgError);
                }

                //輸入框取消按鈕
                $(".erase").each(function () {
                    var dom = $(this);
                    dom.bind("click", function () {
                        var prev = dom.prev();
                        dom.prev().val("");
                    });
                });

                //會員編號離開焦點事件
                $("#toUserId").on("blur", function () {
                    $("#toUserName").empty();
                    utils.AjaxPostNotLoadding("/User/Ujqr/GetUserNameByphone", { phone: $("#toUserId").val() }, function (result) {
                        if (result.status == "fail" || result.msg == "會員不存在") {
                            $("#toUserName").empty()
                        } else {
                            $("#toUserName").html(result.msg);
                        }
                    });
                })

                //隱藏提示框
                $(".hideprompt").click(function () {
                    utils.showOrHiddenPromp();
                });

                //保存
                $("#saveBtnJqr").on('click', function () {
                    document.getElementById("sureBtnJqr").style.display = "block";
                    document.getElementById("sureBtnJqrzr").style.display = "none";
                    utils.showOrHiddenPromp();
                })
                //保存
                $("#saveBtnJqrzr").on('click', function () {
                    document.getElementById("sureBtnJqr").style.display = "none";
                    document.getElementById("sureBtnJqrzr").style.display = "block";
                    utils.showOrHiddenPromp();
                })

                //確認購買按鈕
                $("#sureBtnJqr").on('click', function () {
                    var data = { id: 0,passOpen:$("#passOpen").val() };
                    utils.AjaxPost("/User/Ujqr/SaveJqr", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            searchMethodJqr();
                            dto = result.result;
                            $("#agentLz").val(dto.account.agentLz.toFixed(2));
                            document.getElementById("saveBtnJqr").style.display = "none";
                        }
                    });
                });

                //確認轉讓按鈕
                $("#sureBtnJqrzr").on('click', function () {
                    var data = { Jqrid: $("#JqrID").val(), toUserId: $("#toUserId").val(), passOpen: $("#passOpenzr").val() };
                    utils.AjaxPost("/User/Ujqr/SaveJqrzr", data, function (result) {
                        if (result.status == "fail") {
                            utils.showErrMsg(result.msg);
                        } else {
                            utils.showOrHiddenPromp();
                            utils.showSuccessMsg("操作成功！");
                            searchMethodJqrzr();
                            dto = result.result;
                        }
                    });
                });

             



            }
        });


        controller.onRouteChange = function () {
        };
    };

    return controller;
});