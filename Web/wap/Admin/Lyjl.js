
define(['text!Lyjl.html', 'jquery'], function (Lyjl, $) {

    var controller = function (id) {
        //设置标题
        $("#title").html("领养记录")
        appView.html(Lyjl);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })
       
       
        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);
        var BuymemTypeList = [{ id: 0, name: "全部" }, { id: 1, name: "普通会员订单" }, { id: 2, name: "虚拟会员订单" }];
        utils.InitMobileSelect('#BuymemTypeName', '选择', BuymemTypeList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#BuymemTypeName").val(data[0].name);
            $("#BuymemType").val(data[0].id);
        });
        //var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售" }买记录状态
        //审核状态选择框
        // var flagList = [{ id: 0, name: "全部" }, { id: 6, name: "增值中" }, { id: 8, name: "合约到期" }];
        var flagList = [{ id: 0, name: "全部" }, { id: 1, name: "已预约" }, { id: 2, name: "已申请领养" }, { id: 3, name: "領養成功" }, { id: 4, name: "匹配失败" }, { id: 5, name: "已付款" }, { id: 6, name: "已确认付款" }, { id: 7, name: "付款超时领养失败" }, { id: 8, name: "已出售" }, { id: 9, name: "解除交易" }];
        utils.InitMobileSelect('#flagName', '选择状态', flagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });
        var isXyList = [{ id: 0, name: "全部" }, { id: 1, name: "续养记录" }];
        utils.InitMobileSelect('#isXyName', '选择', isXyList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#isXyName").val(data[0].name);
            $("#isXy").val(data[0].id);
        });
        utils.InitMobileSelect('#BTZName', '选择', btzlist, { id: "id", value: "BTZName" }, [0], null, function (indexArr, data) {
            $("#BTZName").val(data[0].BTZName);
            $("#Btzid").val(data[0].id);
        });
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       

     
      
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Chargedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPageYyjl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        var saleflagDto = { 1: "待转让", 2: "已转让", 3: "交易完成" }
                        var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售", 9: "解除交易" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Yysj"] = utils.changeDateFormat(rows[i]["Yysj"]);
                            rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                            rows[i]["Cfsj"] = utils.changeDateFormat(rows[i]["Cfsj"]);
                            rows[i]["Kssj"] = utils.changeDateFormat(rows[i]["Kssj"]);
                            rows[i]["Jssj"] = utils.changeDateFormat(rows[i]["Jssj"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;


                           

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.userName + '</span>';
                            html += '<span class="sum">' + dto.flag + '</span>';
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="seditbtn" onclick=\'xy(' + dto.id + ')\'>续养</button></li>' +
                            '<li><button class="seditbtn" onclick=\'jymx(' + dto.id + ')\'>交易详情</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>求购单号</dt><dd>' + dto.BuyNo + '</dd></dl><dl><dt>络绎阁ID</dt><dd>' + dto.BTZid + '</dd></dl>' +
                            '<dl><dt>络绎阁名称</dt><dd>' + dto.BTZName + '</dd></dl><dl><dt>手续费</dt><dd>' + dto.sxf + '</dd></dl>' +
                             '<dl><dt>价值</dt><dd>' + dto.priceOldzz + '</dd></dl><dl><dt>合约天数</dt><dd>' + dto.Hyts + '</dd></dl>' +

                            '<dl><dt>日收益比例</dt><dd>' + dto.RsyBili + '%</dd></dl><dl><dt>赠送DOGE</dt><dd>' + dto.ZsBTT + '</dd></dl>' +
                            '<dl><dt>赠送BTD</dt><dd>' + dto.ZsBTD + '</dd></dl><dl><dt>领养时间</dt><dd>' + dto.Qysj + '</dd></dl>' +
                            '<dl><dt>拆分时间</dt><dd>' + dto.Cfsj + '</dd></dl>' +
                             '<dl><dt>结束时间</dt><dd>' + dto.Jssj + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.Czr + '</dd></dl>' +
                           '<dl><dt>省</dt><dd>' + dto.province + '</dd></dl>' +
                            '<dl><dt>市</dt><dd>' + dto.city + '</dd></dl>' +

                            '<dl><dt>买单状态</dt><dd>' + dto.flag + '</dd></dl>' +
                             '<dl><dt>匹配备注</dt><dd>' + dto.ppLog+ '</dd></dl>' +
                          
                                 
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        var html_totalepoints = " <p>领养人数：XX 领养记录总数：XX 预约人数：XX</p><p>手续费总额：XX 领养总价值：XX 到期总价值：XX</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>领养人数:' + dto.totalLyrs + ' &nbsp;  领养记录总数:' + dto.totalJlzs + '&nbsp;  预约人数:' + dto.totalLyrs + '</p>' +
                                '<p>手续费总额:' + dto.totalSxf.toFixed(2) + ' &nbsp;  领养总价值:' + dto.totalZjz.toFixed(2) + ' &nbsp;  到期总价值:' + dto.totaldqZjz.toFixed(2) + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#ChargeitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#ChargeitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });
        //跳转到交易明细界面
        xy = function (id) {
            $("#prompTitle").html("您确定续养吗？");
            $("#sureBtn").html("确定续养")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/BTZBuyxy", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("续养操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
        //跳转到交易明细界面
        jymx = function (id) {
            location.href = '#Csjl/buyId' + id;
        }
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ChargeitemList").empty();
            param["userId"] = $("#userId").val();
            param["province"] = $("#province").val();
            param["city"] = $("#city").val();
            param["flag"] = $("#flag").val();
            param["BuymemType"] = $("#BuymemType").val();
            param["isXy"] = $("#isXy").val();
            param["BTZid"] = $("#Btzid").val();
            param["startTimeQysj"] = $("#startTime").val();
            param["endTimeQysj"] = $("#endTime").val();
            param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }
       
     
        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});