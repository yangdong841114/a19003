
define(['text!Hgjl.html', 'jquery'], function (Hgjl, $) {

    var controller = function (id) {
        //设置标题
        $("#title").html("回购记录")
        appView.html(Hgjl);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })
       
       
        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);
        //var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "已确认付款", 7: "付款超时领养失败", 8: "已出售" }买记录状态
        //审核状态选择框
        var flagList = [{ id: 0, name: "全部" }, { id: 1, name: "已预约" }, { id: 2, name: "已申请领养" }, { id: 3, name: "領養成功" }, { id: 5, name: "已付款" }, { id: 6, name: "交易完成" }, { id: 7, name: "付款超时领养失败" }];
        utils.InitMobileSelect('#flagName', '选择状态', flagList, { id: "id", value: "name" }, [0], null, function (indexArr, data) {
            $("#flagName").val(data[0].name);
            $("#flag").val(data[0].id);
        });
        utils.InitMobileSelect('#BTZName', '选择状态', btzlist, { id: "id", value: "BTZName" }, [0], null, function (indexArr, data) {
            $("#BTZName").val(data[0].BTZName);
            $("#Btzid").val(data[0].id);
        });
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });
        //预览图片
        $("#imgFile").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
        });

       
      

        //付款
        surefk = function (id) {
            $("#prompTitle").html("您确定付款吗？");
            $("#sureBtn").html("确定付款");
            document.getElementById("showImg").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                //原无图片模式
                //utils.AjaxPost("/Admin/BTZfb/BTZBuyfk", { id: id }, function (result) {
                //    if (result.status == "success") {
                //        utils.showOrHiddenPromp();
                //        utils.showSuccessMsg("确定付款操作成功");
                //        searchMethod();
                //    } else {
                //        utils.showErrMsg(result.msg);
                //    }
                //});
                //有图片模式
                var formdata = new FormData();
                formdata.append("id", id);
                formdata.append("imgFile", $("#imgFile")[0].files[0]);
                utils.AjaxPostForFormData("/Admin/BTZfb/BTZBuyfkHaveFile", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        searchMethod();
                        utils.showSuccessMsg("付款成功！");
                    }
                });

            });
            utils.showOrHiddenPromp();
        }
     
      
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Chargedatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPageHgjl", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        var saleflagDto = { 1: "待转让", 2: "已转让", 3: "交易完成" }
                        var flagDto = { 1: "已预约", 2: "已申请领养", 3: "領養成功", 4: "匹配失败", 5: "已付款", 6: "交易完成", 7: "付款超时领养失败", 8: "已出售", 9: "解除交易" }
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["Yysj"] = utils.changeDateFormat(rows[i]["Yysj"]);
                            rows[i]["Qysj"] = utils.changeDateFormat(rows[i]["Qysj"]);
                            rows[i]["Cfsj"] = utils.changeDateFormat(rows[i]["Cfsj"]);
                            rows[i]["Kssj"] = utils.changeDateFormat(rows[i]["Kssj"]);
                            rows[i]["Jssj"] = utils.changeDateFormat(rows[i]["Jssj"]);
                            rows[i]["payTime"] = utils.changeDateFormat(rows[i]["payTime"]);
                            rows[i]["confirmPayTime"] = utils.changeDateFormat(rows[i]["confirmPayTime"]);
                            rows[i]["flag"] = flagDto[rows[i].flag];
                            var dto = rows[i];
                            var lightboxId = "lightbox" + dto.id;

                            var btnfk = '<div class="btnbox"><ul class="tga3">' +
                            '<li><button class="seditbtn" onclick=\'surefk(' + dto.id + ')\'>确认付款</button></li>' +
                            '</ul></div>';

                            var fkfsHtml = "";
                            if (dto.skIsbank == "1")
                                fkfsHtml += '<dl><dt>卖家收款方式</dt><dd>银行汇款</dd></dl>' +
                                     '<dl><dt>开户行</dt><dd>' + dto.bankName + '</dd></dl>' +
                                      '<dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl>' +
                                       '<dl><dt>开户名</dt><dd>' + dto.bankUser + '</dd></dl>' +
                                        '<dl><dt>开户支行</dt><dd>' + dto.bankAddress + '</dd></dl>';
                            if (dto.skIszfb == "1") {
                                lightboxId = "lightboxzfb" + dto.id;
                                fkfsHtml += '<dl><dt>卖家收款方式</dt><dd>支付宝扫码</dd></dl>' +
                                        '<dl><dt>图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlzfb + '" data-image="' + dto.imgUrlzfb + '" data-caption="扫码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId)
                            }
                            if (dto.skIswx == "1") {
                                lightboxId = "lightboxwx" + dto.id;
                                fkfsHtml += '<dl><dt>卖家收款方式</dt><dd>微信扫码</dd></dl>' +
                                        '<dl><dt>图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlwx + '" data-image="' + dto.imgUrlwx + '" data-caption="扫码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId)
                            }
                            if (dto.skIsszhb == "1") {
                                lightboxId = "lightboxszhb" + dto.id;
                                fkfsHtml += '<dl><dt>卖家收款方式</dt><dd>' + dto.szhbmc + '扫码</dd></dl>' +
                                        '<dl><dt>图片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb + '" data-image="' + dto.imgUrlszhb + '" data-caption="扫码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';
                                lightboxArray.push(lightboxId)
                                if (dto.szhbmc1 != "") {
                                    lightboxId = "lightboxszhb1" + dto.id;
                                    fkfsHtml += '<dl><dt>賣家收款方式</dt><dd>' + dto.szhbmc1 + '掃碼</dd></dl>' +
                                            '<dl><dt>圖片</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb1 + '" data-image="' + dto.imgUrlszhb1 + '" data-caption="掃碼" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';

                                    lightboxArray.push(lightboxId)
                                }
                            }
                           
                            lightboxId = "lightbox" + dto.id;
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.flag + '</span>';
                           
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            btnfk+
                            '<dl><dt>求购单号</dt><dd>' + dto.BuyNo + '</dd></dl><dl><dt>络绎阁ID</dt><dd>' + dto.BTZid + '</dd></dl>' +
                            '<dl><dt>买方姓名</dt><dd>' + dto.userName + '</dd></dl><dl><dt>买方手机</dt><dd>' + dto.phone + '</dd></dl>' +
                            '<dl><dt>卖单单号</dt><dd>' + dto.SaleNo + '</dd></dl><dl><dt>卖方姓名</dt><dd>' + dto.saleuserName + '</dd></dl><dl><dt>卖方手机</dt><dd>' + dto.salephone + '</dd></dl>' +
                            '<dl><dt>络绎阁名称</dt><dd>' + dto.BTZName + '</dd></dl><dl><dt>手续费</dt><dd>' + dto.sxf + '</dd></dl>' +
                             '<dl><dt>价值</dt><dd>' + dto.priceOldzz + '</dd></dl><dl><dt>合约天数</dt><dd>' + dto.Hyts + '</dd></dl>' +

                            '<dl><dt>日收益比例</dt><dd>' + dto.RsyBili + '%</dd></dl><dl><dt>赠送DOGE</dt><dd>' + dto.ZsBTT + '</dd></dl>' +
                            '<dl><dt>赠送BTD</dt><dd>' + dto.ZsBTD + '</dd></dl><dl><dt>回购时间</dt><dd>' + dto.Qysj + '</dd></dl>' +
                           
                            fkfsHtml +
                              '<dl><dt>汇款凭证</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" data-caption="汇款凭证" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>' +
                                     '<dl><dt>汇款时间</dt><dd>' + dto.payTime + '</dd></dl>' +
                                       '<dl><dt>收款时间</dt><dd>' + dto.confirmPayTime + '</dd></dl>' +
                            '<dl><dt>买单状态</dt><dd>' + dto.flag + '</dd></dl>' +

                          
                                 
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        var html_totalepoints = " <p>回购记录总数：XX   回购总价值：XX</p>";
                        for (var i = 0; i < footers.length; i++)
                        {
                            var dto = footers[i];
                            html_totalepoints = '<p>回购记录总数:' + dto.totalJlzs + '&nbsp;  回购总价值:' + dto.totalZjz.toFixed(2) + '</p>';
                        }
                        $("#totalepoints").html(html_totalepoints);
                        $("#ChargeitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#ChargeitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });
       
        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ChargeitemList").empty();
            param["userId"] = $("#userId").val();
            param["flag"] = $("#flag").val();
            param["BTZid"] = $("#Btzid").val();
            param["startTimeQysj"] = $("#startTime").val();
            param["endTimeQysj"] = $("#endTime").val();
            //param["userName"] = $("#userName").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }
       
     
        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});