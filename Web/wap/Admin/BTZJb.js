
define(['text!BTZJb.html', 'jquery'], function (BTZJb, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("级别设置")
        appView.html(BTZJb);

        var dto = null;
        var editList = {};
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       
        //删除按钮
        deleteRecord = function (id) {
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZjscity/DeleteBTZJb", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

      


        //发布商品
        $("#deployProduct").bind("click", function () {
            dto = null;
            $("#id").val("");
            $("#BTZLevel").val("");
        
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });

      

        //保存发布商品
        $("#saveProductBtn").bind("click", function () {
            //金额校验
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            //非空校验
            if ($("#BTZLevel").val() == 0) {
                utils.showErrMsg("级别不能为空");
            }  else {
                var formdata = new FormData();
                if (dto) {
                    formdata.append("id", $("#id").val());
                }
                formdata.append("BTZLevel", $("#BTZLevel").val());
             

                utils.AjaxPostForFormData("/Admin/BTZjscity/SaveOrUpdateBTZJb", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        utils.showSuccessMsg("保存成功！");
                        searchMethod();
                    }
                });
            }
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //编辑商品
        editRecord = function (id) {
            dto = editList[id];
            $("#id").val(id);
            $("#BTZLevel").val(dto.BTZLevel);
       
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");

        }

        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Productdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZjscity/GetListPageBTZJb", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                          
                            var dto = rows[i];
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.BTZLevel + '</time>';
                           
                            html += '&nbsp;<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">';
                           
                            html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                                
                            '</ul></div>' +
                            '<dl><dt>级别</dt><dd>' + dto.BTZLevel + '</dd></dl>' +
                          
                            '</div></li>';
                            editList[dto.id] = dto;

                        }
                        $("#ProductitemList").append(html);
                    }, function () {
                        $("#ProductitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});