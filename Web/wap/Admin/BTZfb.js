
define(['text!BTZfb.html', 'jquery'], function (BTZfb, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("络绎阁发布")
        appView.html(BTZfb);

        var dto = null;
        var editList = {};
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });



        //级别列表下拉
        utils.AjaxPostNotLoadding("/Admin/BTZjscity/GetBTZJb", {}, function (result) {
            if (result.status == "fail") {
                util.utils.showErrMsg("加载数据失败");
                return;
            }
            var BTZJb_list = result.map["BTZJb_list"];      //基础设置
            if (BTZJb_list && BTZJb_list.length > 0) {
                utils.InitMobileSelect('#jLevel', '选择级别', BTZJb_list, { id: 'id', value: 'BTZLevel' }, [0], null, function (indexArr, data) {
                    $("#jLevel").val(data[0].BTZLevel);
                    //$("#jLevel").val(data[0].id);
                    initBjselect($("#jLevel").val());
                });
            }
        })
        //班级列表下拉
        initBjselect = function (parentId) {
            utils.AjaxPostNotLoadding("/Admin/BTZjscity/GetBTZBj", { parentId: parentId }, function (result) {
                if (result.status == "fail") {
                    util.utils.showErrMsg("加载数据失败");
                    return;
                }
                var BTZBj_list = result.map["BTZBj_list"];      //基础设置
                if (BTZBj_list && BTZBj_list.length > 0) {
                    utils.InitMobileSelect('#bLevel', '选择班级', BTZBj_list, { id: 'id', value: 'BTZLevel' }, [0], null, function (indexArr, data) {
                        $("#bLevel").val(data[0].BTZLevel);
                        //$("#jLevel").val(data[0].id);
                    });
                }
            })
        }

        //上架商品
        shelveProduct = function (id) {
            utils.AjaxPost("/Admin/BTZfb/Shelve", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("上架成功");
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //下架商品
        cancelShelveProduct = function (id) {
            utils.AjaxPost("/Admin/BTZfb/CancelShelve", { id: id }, function (result) {
                if (result.status == "success") {
                    utils.showSuccessMsg("下架成功");
                    searchMethod();
                } else {
                    utils.showErrMsg(result.msg);
                }
            });
        }

        //删除按钮
        deleteRecord = function (id) {
            $("#prompTitle").html("确定删除");
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/Delete", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("删除成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }
        //开始匹配
        startPp = function (id) {
            $("#prompTitle").html("确定开始匹配");
            $("#sureBtn").unbind();
            //确认删除
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/BTZfb/startPp", { id: id }, function (result) {
                    utils.showOrHiddenPromp();
                    if (result.status == "success") {
                        utils.showSuccessMsg("开启匹配时间成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            })
            utils.showOrHiddenPromp();
        }

        //加载默认参数
        initPara = function () {
            utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetPara", {}, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    $("#KsqysjStr").val(map.BTZqysj);
                    $("#Qycxsj").val(map.BTZcxsj);
                    $("#YyjzsjStr").val(map.BTZyysj);
                    $("#Yysxlz").val(map.BTZyylz);
                    $("#Jqsxlz").val(map.BTZjqlz);
                    $("#Hyts").val(map.BTZhyts);
                    $("#RsyBili").val(map.BTZrsyBili);
                    $("#ZcszjBili").val(map.BTZzcszzBili);
                    $("#Rfxesx").val(map.BTZmrjzsx);
                    $("#Gzzzxx").val(map.BTZgzjzxx);
                    $("#Gzzzsx").val(map.BTZgzjzsx);
                    $("#Zszzxx").val(map.BTZzsjzxx);
                    $("#Zszzsx").val(map.BTZzsjzsx);
                    $("#Cffzbs").val(map.BTZcffzbs);
                    $("#Cffzfs").val(map.BTZcffzfs);
                    $("#Mtxyrs").val(map.BTZmtxyrs);
                    $("#ZdcsBili").val(map.BTZzdcsBili);
                    $("#bounsAmounts").html(map.bounsAmounts + "%");
                    $("#maxBounsAmounts").html(map.maxBounsAmounts + "%");
                }
            })
        }

        //预览图片
        $("#imgFile").bind("change", function () {
            var url = URL.createObjectURL($(this)[0].files[0]);
            document.getElementById("showImg").style.backgroundImage = 'url(' + url + ')';
        })

        //发布商品
        $("#deployProduct").bind("click", function () {
            dto = null;
            $("#id").val("");
            $("#imgUrl").val("");
            $("#BTZCode").val("");
            $("#BTZName").val("");
            $("#KsqysjStr").val("");
            $("#Qycxsj").val("");
            document.getElementById("showImg").style.backgroundImage = 'url(-testimg/testd1.jpg)';
            $("#YyjzsjStr").val("");
            $("#Yysxlz").val("");
            $("#Jqsxlz").val("");
            $("#Hyts").val("");
            $("#RsyBili").val("");
            $("#ZcszjBili").val("");
            $("#ZsBTT").val("");
            $("#ZsBTD").val("");
            $("#Rfxesx").val("");
            $("#Gzzzxx").val("");
            $("#Gzzzsx").val("");
            $("#Zszzxx").val("");
            $("#Zszzsx").val("");
            $("#Cffzbs").val("");
            $("#Cffzfs").val("");
            $("#Mtxyrs").val("");
            $("#ZdcsBili").val("");
            initPara();
            $("#KsqysjStr").removeAttr("readonly");//开始抢养 去除只读
            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");
        });



        //保存发布商品
        $("#saveProductBtn").bind("click", function () {
            //金额校验
            var g = /^\d+(\.{0,1}\d+){0,1}$/;
            //非空校验
            if ($("#BTZCode").val() == 0) {
                utils.showErrMsg("编码不能为空");
            } else if ($("#BTZName").val() == 0) {
                utils.showErrMsg("名称不能为空");
            } else if ($("#imgFile").val() == 0 && !dto) { //新增必须上传图片，编辑时可以不用上传
                utils.showErrMsg("请选择上传的图片");
            } else {
                var formdata = new FormData();
                if (dto) {
                    formdata.append("id", $("#id").val());
                    formdata.append("imgUrl", $("#imgUrl").val());
                }
                formdata.append("BTZCode", $("#BTZCode").val());
                formdata.append("BTZName", $("#BTZName").val());
                formdata.append("KsqysjStr", $("#KsqysjStr").val());

                formdata.append("Qycxsj", $("#Qycxsj").val());
                formdata.append("YyjzsjStr", $("#YyjzsjStr").val());
                formdata.append("Yysxlz", $("#Yysxlz").val());
                formdata.append("Jqsxlz", $("#Jqsxlz").val());
                formdata.append("Hyts", $("#Hyts").val());
                formdata.append("RsyBili", $("#RsyBili").val());
                formdata.append("ZcszjBili", $("#ZcszjBili").val());
                formdata.append("ZsBTT", $("#ZsBTT").val());
                formdata.append("ZsBTD", $("#ZsBTD").val());
                formdata.append("Rfxesx", $("#Rfxesx").val());
                formdata.append("Gzzzxx", $("#Gzzzxx").val());
                formdata.append("Gzzzsx", $("#Gzzzsx").val());
                formdata.append("Zszzxx", $("#Zszzxx").val());
                formdata.append("Zszzsx", $("#Zszzsx").val());
                formdata.append("Cffzbs", $("#Cffzbs").val());

                formdata.append("Cffzfs", $("#Cffzfs").val());
                formdata.append("Mtxyrs", $("#Mtxyrs").val());
                formdata.append("ZdcsBili", $("#ZdcsBili").val());
                formdata.append("jLevel", $("#jLevel").val());
                formdata.append("bLevel", $("#bLevel").val());
                formdata.append("imgFile", $("#imgFile")[0].files[0]);

                utils.AjaxPostForFormData("/Admin/BTZfb/SaveOrUpdate", formdata, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        $("#mainDiv").css("display", "block");
                        $("#deployDiv").css("display", "none");
                        utils.showSuccessMsg("保存成功！");
                        searchMethod();
                    }
                });
            }
        })

        //关闭发布商品
        $("#closeDeployBtn").bind("click", function () {
            $("#mainDiv").css("display", "block");
            $("#deployDiv").css("display", "none");
        })

        //编辑商品
        editRecord = function (id) {
            utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetPara", {}, function (result) {
                if (result.status == "success") {
                    var map = result.map;
                    $("#bounsAmounts").html(map.bounsAmounts + "%");
                    $("#maxBounsAmounts").html(map.maxBounsAmounts + "%");
                }
            })
            dto = editList[id];
            $("#id").val(id);
            $("#imgUrl").val(dto.imgUrl);
            $("#BTZCode").val(dto.BTZCode);
            $("#BTZName").val(dto.BTZName);
            $("#KsqysjStr").val(dto.KsqysjStr);
            $("#Qycxsj").val(dto.Qycxsj);

            $("#YyjzsjStr").val(dto.YyjzsjStr);
            $("#Yysxlz").val(dto.Yysxlz);
            $("#Jqsxlz").val(dto.Jqsxlz);
            $("#Hyts").val(dto.Hyts);
            $("#RsyBili").val(dto.RsyBili);
            $("#ZcszjBili").val(dto.ZcszjBili);
            $("#ZsBTT").val(dto.ZsBTT);
            $("#ZsBTD").val(dto.ZsBTD);
            $("#Rfxesx").val(dto.Rfxesx);
            $("#Gzzzxx").val(dto.Gzzzxx);
            $("#Gzzzsx").val(dto.Gzzzsx);
            $("#Zszzxx").val(dto.Zszzxx);
            $("#Zszzsx").val(dto.Zszzsx);
            $("#Cffzbs").val(dto.Cffzbs);
            $("#Cffzfs").val(dto.Cffzfs);
            $("#Mtxyrs").val(dto.Mtxyrs);
            $("#ZdcsBili").val(dto.ZdcsBili);
            $("#jLevel").val(dto.jLevel);
            initBjselect($("#jLevel").val());
            $("#bLevel").val(dto.bLevel);
            //$('#KsqysjStr').attr("readonly", "readonly");//开始抢养只读
            document.getElementById("showImg").style.backgroundImage = 'url(' + dto.imgUrl + ')';

            $("#mainDiv").css("display", "none");
            $("#deployDiv").css("display", "block");

        }

        utils.CancelBtnBind();

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#Productdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/BTZfb/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i]["Ppkssj"] = utils.changeDateFormat(rows[i]["Ppkssj"]);
                            rows[i]["Ppjssj"] = utils.changeDateFormat(rows[i]["Ppjssj"]);
                            rows[i]["status"] = rows[i].isShelve == 1 ? "已下架" : "已上架";

                            var dto = rows[i];

                            var btn_startPp = '<li><button class="smallbtn" onclick="startPp(\'' + dto.id + '\')">开始匹配</button></li>';
                            if (dto.Ppkssj == "2000-01-01 00:00:00") { dto.Ppkssj = "未产生"; dto.Ppjssj = "未产生"; }
                            else btn_startPp = "";

                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)" style=""><time>' + dto.BTZCode + '</time>';
                            if (dto.status == "已上架") {
                                html += '<span class="ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="noship">' + dto.status + '</span>';
                            }
                            html += '&nbsp;<span class="sum">' + dto.BTZName + ' 级别：' + dto.jLevel + '</span><i class="fa fa-angle-right"></i></div>' +
                                '<div class="allinfo">' +
                                '<div class="btnbox"><ul class="tga4">';
                            if (dto.isShelve == 1) {
                                html += '<li><button class="smallbtn" onclick="shelveProduct(\'' + dto.id + '\')">上架</button></li>';
                            } else {
                                html += '<li><button class="sdelbtn" onclick="cancelShelveProduct(\'' + dto.id + '\')">下架</button></li>';
                            }
                            html += btn_startPp;
                            html += '<li><button class="sdelbtn" onclick=\'deleteRecord(' + dto.id + ')\'>删除</button></li>' +
                                '<li><button class="sdelbtn" onclick=\'editRecord(' + dto.id + ')\'>编辑</button></li>' +
                                '</ul></div>' +
                                '<dl><dt>络绎阁编码</dt><dd>' + dto.BTZCode + '</dd></dl><dl><dt>络绎阁名称</dt><dd>' + dto.BTZName + '</dd></dl>' +
                                '<dl><dt>图片</dt><dd><img data-toggle="lightbox" src="' + dto.imgUrl + '" data-image="' + dto.imgUrl + '" class="img-thumbnail" alt="" width="100"></dd></dl>' +
                                '<dl><dt>开始抢养时间</dt><dd>' + dto.KsqysjStr + '</dd></dl><dl><dt>抢养持续时间</dt><dd>' + dto.Qycxsj + '</dd></dl>' +
                                '<dl><dt>开始匹配时间</dt><dd>' + dto.Ppkssj + '</dd></dl><dl><dt>结束匹配时间</dt><dd>' + dto.Ppjssj + '</dd></dl>' +
                                '<dl><dt>预约截止时间</dt><dd>' + dto.YyjzsjStr + '</dd></dl>' +
                                '<dl><dt>预约领养所需靈氣</dt><dd>' + dto.Yysxlz + '</dd></dl><dl><dt>即抢领养所需靈氣</dt><dd>' + dto.Jqsxlz + '</dd></dl>' +
                                '<dl><dt>合约天数</dt><dd>' + dto.Hyts + '</dd></dl><dl><dt>日收益比例</dt><dd>' + dto.RsyBili + '</dd></dl>' +
                                '<dl><dt>再出售增加利息比例</dt><dd>' + dto.ZcszjBili + '</dd></dl><dl><dt>赠送DOGE数量</dt><dd>' + dto.ZsBTT + '</dd></dl>' +
                                '<dl><dt>赠送BTD数量</dt><dd>' + dto.ZsBTD + '</dd></dl><dl><dt>日发行价值额度上限</dt><dd>' + dto.Rfxesx + '</dd></dl>' +
                                '<dl><dt>规则价值下限</dt><dd>' + dto.Gzzzxx + '</dd></dl><dl><dt>规则价值上限</dt><dd>' + dto.Gzzzsx + '</dd></dl>' +
                                '<dl><dt>展示价值下限</dt><dd>' + dto.Zszzxx + '</dd></dl><dl><dt>展示价值上限</dt><dd>' + dto.Zszzsx + '</dd></dl>' +
                                '<dl><dt>拆份阀值倍数</dt><dd>' + dto.Cffzbs + '</dd></dl><dl><dt>拆份阀值份数</dt><dd>' + dto.Cffzfs + '</dd></dl>' +
                                '<dl><dt>每天续养人数</dt><dd>' + dto.Mtxyrs + '</dd></dl><dl><dt>自动出售比例</dt><dd>' + dto.ZdcsBili + '</dd></dl>' +
                                '<dl><dt>级别</dt><dd>' + dto.jLevel + '</dd></dl>' +
                                '<dl><dt>班级</dt><dd>' + dto.bLevel + '</dd></dl>' +
                                '<dl><dt>是否上架</dt><dd>' + dto.status + '</dd></dl><dl><dt>发布日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                                '</div></li>';
                            editList[dto.id] = dto;

                        }
                        $("#ProductitemList").append(html);
                    }, function () {
                        $("#ProductitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#ProductitemList").empty();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })


        controller.onRouteChange = function () {

        };
    };

    return controller;
});