
define(['text!Jdhy.html', 'jquery'], function (Jdhy, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("接单会员")
        appView.html(Jdhy);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

       
        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#MemberPasseddatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/MemberPassed/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["JiedanRq"] = utils.changeDateFormat(rows[i]["JiedanRq"]);
                            rows[i]["addTime"] = utils.changeDateFormat(rows[i]["addTime"]);
                            rows[i].uLevel = cacheMap["ulevel"][rows[i].uLevel];
                            var rr = cacheMap["rLevel"][rows[i].rLevel];
                            rows[i].rLevel = rr ? rr : "无";
                            rows[i].empty = rows[i].empty == 0 ? "否" : "是";
                            rows[i].isLock = rows[i].isLock == 0 ? "否" : "是";
                            rows[i].isJiedan = rows[i].isJiedan == 0 ? "否" : "是";

                           
                            var dto = rows[i];
                            dto.regMoney = dto.regMoney ? dto.regMoney : 0;
                            var gsd = dto.province+dto.city+dto.area;
                            html += '<li>' +
                                    '<div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.userId + '</time><span class="sum">' + dto.isJiedan + '</span>';
                            html += '&nbsp;<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">'+
                            '<div class="btnbox"><ul class="tga2">' +
                            '<li><button class="sdelbtn" onclick=\'jdyes(' + dto.id + ')\'>设为接单会员</button></li>' +
                            '<li><button class="smallbtn" onclick="jdno(\'' + dto.id + '\')">取消接单资格</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                         
                            '<dl><dt>会员级别</dt><dd>' + dto.uLevel + '</dd></dl>' +
                            '<dl><dt>归属地</dt><dd>' + gsd + '</dd></dl>' +
                            '<dl><dt>联系电话</dt><dd>' + dto.phone + '</dd></dl>' +
                            '<dl><dt>身份证号</dt><dd>' + dto.code + '</dd></dl>' +
                           '<dl><dt>注册日期</dt><dd>' + dto.addTime + '</dd></dl>' +
                            '<dl><dt>是否接单</dt><dd>' + dto.isJiedan + '</dd></dl>' +
                            '<dl><dt>设置日期</dt><dd>' + dto.JiedanRq + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#MemberPasseditemList").append(html);
                    }, function () {
                        $("#MemberPasseditemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

      

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#MemberPasseditemList").empty();
            param["userId"] = $("#userId").val();
     
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //确认通过
        jdyes = function (id) {
            $("#prompTitle").html("确定设为接单会员吗？");
            $("#sureBtn").html("确定")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/jdyes", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("设为接单会员成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }
        //确认通过
        jdno = function (id) {
            $("#prompTitle").html("取消接单资格吗？");
            $("#sureBtn").html("确定")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/MemberPassed/jdno", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("取消接单资格成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        controller.onRouteChange = function () {
        };
    };

    return controller;
});