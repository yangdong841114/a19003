
define(['text!LiuDetail.html', 'jquery'], function (LiuDetail, $) {

    var controller = function (uid) {
        //设置标题
        $("#title").html("会员流水账明细")
        appView.html(LiuDetail);

        //初始化查询区select
        var journalClassList = $.extend(true, [], cacheList["JournalClass"]);
        journalClassList.splice(0, 0, { id: "", name: '全部' });
        utils.InitMobileSelect('#accountIdName', '选择账户类型', journalClassList, { id: 'id', value: 'name' }, [0], null, function (indexArr, data) {
            $("#accountIdName").val(data[0].name);
            $("#accountId").val(data[0].id);
        });

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#LiuDetaildatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/LiuShui/GetLiushuiDetailListPage?uid=" + uid, param, me,
                    function (rows, footers) {
                        var html = "";
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addtime"] = utils.changeDateFormat(rows[i]["addtime"]);
                            rows[i].accountId = cacheMap["JournalClass"][rows[i].accountId];
                            rows[i]["epotins"] = Math.abs(rows[i]["epotins"].toFixed(2));
                            rows[i]["last"] = rows[i]["last"].toFixed(2);

                            var dto = rows[i];
                            dto.remark = (dto.remark && dto.remark != "null") ? dto.remark : "";
                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addtime + '</time><span class="sum">' + dto.accountId + '</span>';
                            if (dto.epotins == 0) {
                                html += '<span class="sum">' + dto.epotins + '</span>';
                            } else {
                                if (dto.optype == "收入") {
                                    html += '<span class="ship">+' + dto.epotins + '</span>';
                                } else {
                                    html += '<span class="noship">-' + dto.epotins + '</span>';
                                }
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl>' +
                            '<dl><dt>业务摘要</dt><dd>' + dto.abst + '</dd></dl><dl><dt>账户类型</dt><dd>' + dto.accountId + '</dd></dl>' +
                            '<dl><dt>收支</dt><dd>' + dto.optype + '</dd></dl><dl><dt>金额</dt><dd>' + dto.epotins + '</dd></dl>' +
                            '<dl onclick=EditBalance(' + dto.id + ',' + dto.last + ')><dt>余额</dt><dd>' + dto.last + '</dd></dl><dl><dt>日期</dt><dd>' + dto.addtime + '</dd></dl>' +
                            '</div></li>';
                        }
                        $("#LiuDetailitemList").append(html);
                    }, function () {
                        $("#LiuDetailitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#LiuDetailitemList").empty();
            param["accountId"] = $("#accountId").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        var userid;
        //初始化加载数据
        utils.AjaxPostNotLoadding("/Admin/ManageWeb/GetBaseData", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                userid = result.map["id"];
            }

        });

        EditBalance = function (id, last) {
            if (userid == 2) {
                //alert(id);
                //alert(last);
                
                utils.showOrHiddenPromp();
                $("#editid").val(id);
                $("#Balance").val(last);
            }
        }
        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //确定提交按钮
        $("#sureBtn").bind("click", function () {
            if ($("#修改金额").val() == 0) {
                utils.showErrMsg("请输入修改金额");
            } else {
                var data = { id: $("#editid").val(), last: $("#Balance").val() };
                utils.AjaxPost("/Admin/LiuShui/ReviseBalance", data, function (result) {
                    if (result.status == "fail") {
                        utils.showErrMsg(result.msg);
                    } else {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("修改成功！");
                        searchMethod();
                    }
                });
            }
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/LiuShui/ExportUserLiuShuiExcel?uid=" + uid;
        })

        controller.onRouteChange = function () {

        };
    };

    return controller;
});