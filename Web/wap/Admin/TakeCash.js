
define(['text!TakeCash.html', 'jquery'], function (TakeCash, $) {

    var controller = function (name) {
        //设置标题
        $("#title").html("提现管理")
        appView.html(TakeCash);

        //清空查询条件按钮
        $("#clearQueryBtn").bind("click", function () {
            utils.clearQueryParam();
        })

        //绑定展开搜索更多
        utils.bindSearchmoreClick();

        //删除消息提醒
        utils.AjaxPostNotLoadding("/Common/DeleteMsg", { url: "#TakeCash", toUid: 0 }, function () { });

        utils.AjaxPostNotLoadding("/Admin/TakeCash/GetTotalMoney", {}, function (result) {
            if (result.status == "fail") {
                utils.showErrMsg(result.msg);
            } else {
                $("#auditMoney").html(result.msg);
                $("#noAuditMoney").html(result.other);
            }
        });

        //初始化日期选择框
        utils.initCalendar(["startTime", "endTime"]);

        //审核状态选择框
        var isPayList = [{ id: 0, name: "全部" }, { id: 1, name: "待审核" }, { id: 2, name: "已通过" }, { id: 3, name: "已取消" }];
        utils.InitMobileSelect('#isPayName', '选择审核状态', isPayList, {id:"id",value:"name"}, [0], null, function (indexArr, data) {
            $("#isPayName").val(data[0].name);
            $("#isPay").val(data[0].id);
        });

        //隐藏提示框
        $(".hideprompt").click(function () {
            utils.showOrHiddenPromp();
        });

        //删除
        deleteTakeCash = function (id) {
            $("#prompTitle").html("您确定取消吗？");
            $("#sureBtn").html("确定取消")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/TakeCash/CancelTakeCash", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("取消操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //确认通过
        sureTakeCash = function (id) {
            $("#prompTitle").html("确定审核通过吗？");
            $("#sureBtn").html("确定")
            $("#sureBtn").unbind()
            $("#sureBtn").bind("click", function () {
                utils.AjaxPost("/Admin/TakeCash/AuditTakeCash", { id: id }, function (result) {
                    if (result.status == "success") {
                        utils.showOrHiddenPromp();
                        utils.showSuccessMsg("审核操作成功");
                        searchMethod();
                    } else {
                        utils.showErrMsg(result.msg);
                    }
                });
            });
            utils.showOrHiddenPromp();
        }

        //查询参数
        this.param = utils.getPageData();

        var dropload = $('#TakeCashdatalist').dropload({
            scrollArea: window,
            domDown: { domNoData: '<p class="dropload-noData"></p>' },
            loadDownFn: function (me) {
                utils.LoadPageData("/Admin/TakeCash/GetListPage", param, me,
                    function (rows, footers) {
                        var html = "";
                        var lightboxArray = []; //需要初始化的图片查看ID
                        for (var i = 0; i < rows.length; i++) {
                            rows[i]["addtime"] = utils.changeDateFormat(rows[i]["addtime"]);
                            rows[i]["smoney"] = rows[i].epoints - rows[i].fee;
                           
                            if (rows[i].isPay == 1)
                                rows[i]["status"] = "待审核";
                            if (rows[i].isPay == 2)
                                rows[i]["status"] = "已通过";
                            if (rows[i].isPay == 3)
                                rows[i]["status"] = "已取消";
                            var dto = rows[i];
                            dto.auditUser = (dto.auditUser && dto.auditUser != "null") ? dto.auditUser : "";
                            var lightboxId = "lightbox" + dto.id;
                            var ewm = '<dl><dt>二维码</dt><dd><img data-toggle="lightbox" id="' + lightboxId + '" src="' + dto.imgUrlszhb + '" data-image="' + dto.imgUrlszhb + '" data-caption="二维码" class="img-thumbnail" alt="" style="width:100px;height:80px;"></dd></dl>';


                            html += '<li><div class="orderbriefly" onclick="utils.showGridMessage(this)"><time>' + dto.addtime + '</time><span class="sum">' + dto.userId + '</span>';
                            if (dto.status == "已通过") {
                                html += '<span class="ship">' + dto.status + '</span>';
                            } else {
                                html += '<span class="noship">' + dto.status + '</span>';
                            }
                            html += '<i class="fa fa-angle-right"></i></div>' +
                            '<div class="allinfo">' +
                            '<div class="btnbox"><ul class="tga2">' + 
                            '<li><button class="seditbtn" onclick=\'sureTakeCash(' + dto.id + ')\'>确认</button></li>' +
                            '<li><button class="sdelbtn" onclick=\'deleteTakeCash(' + dto.id + ')\'>删除</button></li>' +
                            '</ul></div>' +
                            '<dl><dt>会员编号</dt><dd>' + dto.userId + '</dd></dl><dl><dt>会员名称</dt><dd>' + dto.userName + '</dd></dl>' +
                            '<dl><dt>申请日期</dt><dd>' + dto.addtime + '</dd></dl><dl><dt>提现金额</dt><dd>' + dto.epoints + '</dd></dl>' +
                            '<dl><dt>手续费</dt><dd>' + dto.fee + '</dd></dl><dl><dt>实际金额</dt><dd>' + dto.smoney + '</dd></dl>' +
                            //'<dl><dt>开户行</dt><dd>' + dto.bankName + '</dd></dl><dl><dt>银行卡号</dt><dd>' + dto.bankCard + '</dd></dl>' +
                            '<dl><dt>DOGE地址</dt><dd>' + dto.bankAddress + '</dd></dl>' +
                            ewm+
                            '<dl><dt>状态</dt><dd>' + dto.status + '</dd></dl><dl><dt>操作人</dt><dd>' + dto.auditUser + '</dd></dl>' +
                            '</div></li>';
                            lightboxArray.push(lightboxId)
                        }
                        $("#TakeCashitemList").append(html);
                        //初始化图片查看插件
                        for (var i = 0; i < lightboxArray.length; i++) {
                            $("#" + lightboxArray[i]).lightbox();
                        }
                    }, function () {
                        $("#TakeCashitemList").append('<p class="dropload-noData">暂无数据</p>');
                    });
            }
        });

        //查询方法
        searchMethod = function () {
            param.page = 1;
            $("#TakeCashitemList").empty();
            param["userId"] = $("#userId").val();
            param["isPay"] = $("#isPay").val();
            param["startTime"] = $("#startTime").val();
            param["endTime"] = $("#endTime").val();
            dropload.unlock();
            dropload.noData(false);
            dropload.resetload();
        }

        //查询按钮
        $("#searchBtn").on("click", function () {
            searchMethod();
        })

        //导出excel
        $("#ExportExcel").on("click", function () {
            location.href = "/Admin/TakeCash/ExportTakeCashExcel";
        })

        controller.onRouteChange = function () {
            
        };
    };

    return controller;
});