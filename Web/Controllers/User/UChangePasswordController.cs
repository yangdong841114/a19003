﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 修改密码Controller
    /// </summary>
    public class UChangePasswordController : Controller
    {
        public IMemberBLL memberBLL { get; set; }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="oldPass">原密码</param>
        /// <param name="newPass"新密码></param>
        /// <param name="flag">密码类型：1：登录密码，2：安全密码，3：交易密码</param>
        /// <returns></returns>
        public JsonResult ChangePassword(string oldPass, string newPass, int flag, string yzm)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                 object o = Session["pw1_yzm_code"];
                if (flag == 2)o = Session["pw2_yzm_code"];
                if (flag == 3) o = Session["pw3_yzm_code"];
                if (o == null) 
                { response.msg = "未产生短信验证码"; }
                else
                {
                if (o.ToString() == yzm)
                {
            
                    //原操作
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                int c = memberBLL.UpdatePassword(mb.id.Value, oldPass, newPass, flag);
                response.status = "success";
                    //原操作
                }
                else
                    response.msg = "短信验证码不正确";
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作出错，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
