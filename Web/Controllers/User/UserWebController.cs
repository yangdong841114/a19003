﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.IO;
using ThoughtWorks.QRCode.Codec;


namespace Web.User.Controllers
{
    public class UserWebController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IBaseSetBLL setBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public INewsBLL newBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }
        public IParamSetBLL PsetBLL { get; set; }
        public IProductTypeBLL producttypeBLL { get; set; }
        public IBTZBLL btzBLL { get; set; }
        public IAxdbjlBLL dbBLL { get; set; }
        public IMTransferBLL transferBLL { get; set; }
        public ISystemBankBLL bankBLL { get; set; }
        public IMobileNoticeBLL noticeBLL { get; set; }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetUserInfoMessage()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                mm = memberBLL.GetModelById(mm.id.Value);
                Dictionary<string, object> result = new Dictionary<string, object>();
                MemberAccount acc = accountBLL.GetModel(mm.id.Value);
                acc.agentLockBouns = acc.agentLockBouns == null ? 0 : acc.agentLockBouns.Value;
                result.Add("account", acc);
                Member m = memberBLL.GetModelByIdNoPassWord(mm.id.Value);
                Member m2 = new Member();
                m2.userId = m.userId;
                m2.userName = m.userName;
                m2.uLevel = m.uLevel;
                m2.rLevel = m.rLevel;
                m2.bounsAmounts = m.bounsAmounts = m.bounsAmounts == null ? 0 : m.bounsAmounts.Value;
                result.Add("userInfo", m2);
                double ZJZ = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0) as ZJZ from BTZBuy where flag=6 and uid=" + mm.id.Value).Rows[0]["ZJZ"].ToString());//络绎阁总价值包含收益的
                //double ZJZ = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz),0) as ZJZ from BTZBuy where flag=6 and uid=" + mm.id.Value).Rows[0]["ZJZ"].ToString());//络绎阁总价值不包含收益的
                result.Add("ZJZ", ZJZ);
                int Jqrsl = Convert.ToInt32(btzBLL.GetTable("select COUNT(1) as Jqrsl from Jqr where isStop=0 and uid=" + mm.id.Value).Rows[0]["Jqrsl"].ToString());//机器人数量
                result.Add("Jqrsl", Jqrsl);
                int Btzsl = Convert.ToInt32(btzBLL.GetTable("select COUNT(1) as Btzsl from BTZBuy where flag=6 and uid=" + mm.id.Value).Rows[0]["Btzsl"].ToString());//络绎阁数量
                btzBLL.GetTable("update Member set BTZsl=" + Btzsl + " where id=" + mm.id.Value + ";select COUNT(1) as Btzsl from BTZBuy where flag=6 and uid=" + mm.id.Value);
                result.Add("Btzsl", Btzsl);
                //3領養成功 ,6已确认付款,8已出售,5已付款
                //BTZSale 3交易完成
                int sale_account = Convert.ToInt32(btzBLL.GetTable("select COUNT(1) as sale_account from BTZSale where flag in (3) and uid=" + mm.id.Value).Rows[0]["sale_account"].ToString());
                //抢到记录未付款的记录，和有确认收款的记录
                int zrmx_account = Convert.ToInt32(btzBLL.GetTable("select COUNT(1) as zrmx_account from BTZBuy where flag in (3,6) and uid=" + mm.id.Value).Rows[0]["zrmx_account"].ToString());
                //zrmx_account += sale_account;
                if (mm.zrmx_account_notice == null) mm.zrmx_account_notice = 0;
                //跟原有数量一样不显示。不一样则显示
                if (zrmx_account != mm.zrmx_account_notice.Value)
                {
                    Member updateZrmx = new Member();
                    updateZrmx.id = mm.id.Value;
                    updateZrmx.zrmx_account_notice = zrmx_account;
                    updateZrmx.zrmx_account_notice_rq = DateTime.Now;
                    updateZrmx.zrmx_account_isclick = 0;
                    memberBLL.Update(updateZrmx);
                }
                if (zrmx_account == mm.zrmx_account_notice.Value && mm.zrmx_account_isclick == 1) zrmx_account = 0;
                result.Add("zrmx_account", zrmx_account);
                double ZZC = ZJZ + acc.agentTjsy.Value + acc.agentCsj.Value + acc.agentTdj.Value + acc.agentFt.Value;//推荐奖+团队奖+城市将+复投+猪窝价值
                result.Add("ZZC", ZZC);
                //result.Add("agentLysy", acc.agentLysy);
                double LJSY = Convert.ToDouble(btzBLL.GetTable("SELECT isnull(SUM(epoints),0) as LJSY FROM Currency where cat in (45) and uid=" + mm.id.Value).Rows[0]["LJSY"].ToString());//累计收益-领养收益累积
                result.Add("LJSY", LJSY);

                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("ZhBTZHourFrom", "ZhBTZHourTo");
                int ZhBTZHourFrom = Convert.ToInt16(param["ZhBTZHourFrom"].paramValue);
                int ZhBTZHourTo = Convert.ToInt16(param["ZhBTZHourTo"].paramValue);
                int currentHour = DateTime.Now.Hour;
                if (DateTime.Now.Hour < ZhBTZHourFrom) currentHour = -1;
                if (DateTime.Now.Hour > ZhBTZHourTo) currentHour = -1;
                result.Add("currentHour", currentHour);
                result.Add("currentHourMsg", "收益转络绎阁时间为：每天" + ZhBTZHourFrom + "~" + ZhBTZHourTo + "点完成");
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetdbPara()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                mm = memberBLL.GetModelById(mm.id.Value);//刷新当前所有信息防止脏数据，如音乐开关
                Dictionary<string, object> result = new Dictionary<string, object>();
                MemberAccount acc = accountBLL.GetModel(mm.id.Value);
                result.Add("account", acc);
                result.Add("isMusic", mm.isMusic);
                BaseSet set = setBLL.GetModel();
                double dbdjs = (set.axdbJssz.Value - DateTime.Now).TotalSeconds;
                result.Add("dbdjs", dbdjs);
                result.Add("dbxqs", set.dbxqs);
                result.Add("axdbAccount", set.axdbAccount);
                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("Axdbxmclz", "Axdbxmcms");
                double Axdbxmclz = Convert.ToDouble(param["Axdbxmclz"].paramValue); //每参与一次压宝会员需支付1靈氣进入平台
                result.Add("Axdbxmclz", Axdbxmclz);
                News n = newBLL.GetModelByTypeId(ConstUtil.NEWS_DBGZ);
                string dbgz = "";
                if (n != null) dbgz += n.content;
                result.Add("dbgz", dbgz);
                string winlist = dbBLL.GetSqKjInfo(set.dbxqs.Value - 1);//上一期获奖信息
                result.Add("winlist", winlist);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetSjht()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                News n = newBLL.GetModelByTypeId(ConstUtil.NEWS_SJHT);
                di.Add("sjht", n.content);
                response.Success();
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 获取PC前台菜单信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenu()
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["MemberUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, "101");
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            if (mm.isAgent != 2)
            {
                List<Resource> lit = new List<Resource>();
                if (list != null && list.Count > 0)
                {
                    foreach (Resource r in list)
                    {
                        if (r.id != ConstUtil.MENU_SHOP && r.parentResourceId != ConstUtil.MENU_SHOP)
                        {
                            lit.Add(r);
                        }
                    }
                }
                response.list = lit;
            }
            else
            {
                response.list = list;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取APP前台一级菜单
        /// </summary>
        /// <returns></returns>
        public JsonResult GetWapMenu()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            //ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["MemberUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByPartentId(mm.id.Value, "102");
            Dictionary<string, object> di = new Dictionary<string, object>();
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            if (mm.isAgent != 2)
            {
                List<Resource> lit = new List<Resource>();
                if (list != null && list.Count > 0)
                {
                    foreach (Resource r in list)
                    {
                        if (r.id != ConstUtil.WAP_MENU_SHOP && r.parentResourceId != ConstUtil.WAP_MENU_SHOP || 1 == 1)
                        {
                            lit.Add(r);
                        }
                    }
                }
                di.Add("frist", lit);
            }
            else
            {
                di.Add("frist", list);
            }
            List<Resource> allList = resourceBLL.GetListByUserAndParent(mm.id.Value, "102");
            di.Add("all", allList);
            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取基础数据
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public JsonResult GetBaseData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                Dictionary<string, List<DataDictionary>> cahceData = dataDictionaryBLL.GetAllToDictionary();
                BaseSet set = setBLL.GetModel();
                Member mm = (Member)Session["MemberUser"]; //当前登陆用户
                List<SystemMsg> mlist = msgBLL.GetList(mm.id.Value); //通知提醒
                result.Add("notic", mlist);
                result.Add("LoginUser", mm.userId);
                result.Add("cahceData", cahceData); //数据字典缓存
                result.Add("baseSet", set);         //基础数据
                result.Add("area", areaBLL.GetTreeModelList()); //省市区数据
                result.Add("areahhr", areaBLL.GetTreeModelListCshhr()); //省市区数据
                result.Add("productType", producttypeBLL.GetTreeModelList()); //分类数据
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 获取主页数据
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMainData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                current = memberBLL.GetModelById(current.id.Value);//刷新当前所有信息防止脏数据，如音乐开关
                MemberAccount account = accountBLL.GetModel(current.id.Value);      //当前用户账户余额
                List<Banner> lit = setBLL.GetBannerList();                      //广告图
                List<Banner> banners = new List<Banner>();
                for (int i = 0; i < lit.Count; i++)
                {
                    Banner b = lit[i];
                    string path = Server.MapPath("~" + b.imgUrl);
                    if (System.IO.File.Exists(path))
                    {
                        banners.Add(b);
                    }
                }

                BaseSet set = setBLL.GetModel();                                    //基础设置
                //前7条文章
                News n = new News();
                n.page = 1;
                n.rows = 7;
                n.typeId = ConstUtil.NEWS_GG; //公告管理

                n.uid = current.id;
                PageResult<News> page = newBLL.GetListPageNoread(n, "id,addTime,title,content");
                if (page != null && page.rows != null)
                {
                    di.Add("newsList", page.rows);
                    if (page.rows.Count > 0 && Session["isReadNews"] == null && Session["loadTime"] != null)//第二次加载时再减去
                    {
                        //头条弹出后显示为已读
                        newBLL.HaveRead(page.rows[0].id.Value, current.id.Value);
                        current.isReadNews = 1;
                        Session["isReadNews"] = "1";
                        Session["MemberUser"] = current;
                    }
                }
                Session["loadTime"] = "1";//第几次加载本 方法
                di.Add("user", current);
                di.Add("userName", current.userName);
                di.Add("userId", current.userId);
                di.Add("account", account);
                di.Add("banners", banners);
                di.Add("baseSet", set);         //基础数据
                //推广链接
                string siteUrl = "http://" + Request.Url.Host + "/Home/Reg/" + current.userId;
                di.Add("siteUrl", siteUrl);

                //是否存在二维码
                string filename = current.userId + ".jpg";
                string filepath = "~/Upload/qrcode/" + filename;
                if (!System.IO.File.Exists(Server.MapPath(filepath)))
                {
                    CreateCode_Choose(current.userId, siteUrl, "Byte", "M", 8, 4);
                }
                di.Add("qrcode", filename);

                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("BTZgzxlsj");//响铃提前时间
                double BTZgzxlsj = Convert.ToDouble(param["BTZgzxlsj"].paramValue);
                di.Add("musicDjs", BTZgzxlsj * 60);
                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "加载数据失败，请联系管理员！" + ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }








        public JsonResult GetProjectTypeData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=1");

                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);

                list = producttypeBLL.GetList("select * from ProductType where grade=2");
                di.Add("mlistSm", list);

                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        public JsonResult GetProjectTypeSmData(string parentId)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                List<ProductType> list = producttypeBLL.GetList("select * from ProductType where grade=2 and parentId=" + parentId);

                Dictionary<string, object> di = new Dictionary<string, object>();
                di.Add("mlist", list);



                response.map = di;
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        // 生成二维码
        /// </summary>
        /// <param name="strData">要生成的文字或者数字，支持中文。如： "4408810820 深圳－广州" 或者：4444444444</param>
        /// <param name="qrEncoding">三种尺寸：BYTE ，ALPHA_NUMERIC，NUMERIC</param>
        /// <param name="level">大小：L M Q H</param>
        /// <param name="version">版本：如 8</param>
        /// <param name="scale">比例：如 4</param>
        /// <returns></returns>
        public String CreateCode_Choose(string userId, string strData, string qrEncoding, string level, int version, int scale)
        {
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            string encoding = qrEncoding;
            switch (encoding)
            {
                case "Byte":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                    break;
                case "AlphaNumeric":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.ALPHA_NUMERIC;
                    break;
                case "Numeric":
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.NUMERIC;
                    break;
                default:
                    qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                    break;
            }

            qrCodeEncoder.QRCodeScale = scale;
            qrCodeEncoder.QRCodeVersion = version;
            switch (level)
            {
                case "L":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;
                    break;
                case "M":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
                    break;
                case "Q":
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.Q;
                    break;
                default:
                    qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.H;
                    break;
            }
            //文字生成图片
            System.Drawing.Image image = qrCodeEncoder.Encode(strData);
            string filename = userId + ".jpg";
            string filepath = Server.MapPath(@"~\UpLoad\qrcode");
            //如果文件夹不存在，则创建
            if (!Directory.Exists(filepath))
                Directory.CreateDirectory(filepath);
            filepath = filepath + "\\" + filename;
            System.IO.FileStream fs = new System.IO.FileStream(filepath, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write);
            image.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
            fs.Close();
            image.Dispose();
            return @"/UpLoad/qrcode/" + filename;
        }

        /// <summary>
        /// 验证前台安全密码
        /// </summary>
        /// <param name="pass2"></param>
        /// <returns></returns>
        public JsonResult CheckUPass2(string pass2)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "安全密码错误";
                Member cur = (Member)Session["MemberUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass2 = DESEncrypt.EncryptDES(pass2, ConstUtil.SALT);
                    if (pass2 == m.passOpen)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 验证前台交易密码
        /// </summary>
        /// <param name="pass3"></param>
        /// <returns></returns>
        public JsonResult CheckUPass3(string pass3)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "交易密码错误";
                Member cur = (Member)Session["MemberUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass3 = DESEncrypt.EncryptDES(pass3, ConstUtil.SALT);
                    if (pass3 == m.threepass)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetBTZInfo(int BTZid)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            try
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                BTZ model = btzBLL.GetModel(BTZid);
                model.Ppdjs = (model.Ppkssj.Value - DateTime.Now).TotalSeconds;
                model.Ppjsdjs = model.Ppdjs + model.Qycxsj.Value;
                result.Add("Ppdjs", model.Ppdjs);
                result.Add("Ppjsdjs", model.Ppjsdjs);
                response.Success();
                response.map = result;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "加载失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageBTZ(BTZ model)
        {

            Member current = (Member)Session["Memberuser"];
            string BTZid_in = btzBLL.GetJqrBTZid(current);//有机器人开启的络绎阁ID
            if (model == null) { model = new BTZ(); }
            model.isShelve = 2;
            PageResult<BTZ> page = btzBLL.GetListPage(model, "m.*");
            List<BTZ> list = page.rows;
            for (int i = 0; i < list.Count; i++)
            {
                //机器人多个络绎阁比对
                string isHavejqr = "no";
                string[] sArray = BTZid_in.Split(',');
                foreach (string BTZid in sArray)
                {
                    if (BTZid == list[i].id.Value.ToString()) isHavejqr = "yes";
                }
                list[i].isHavejqr = isHavejqr;

                if (btzBLL.IsVidyy(list[i].id.Value, current) > 0) list[i].isVipyy = "yes";
                else list[i].isVipyy = "no";

                if (btzBLL.Isyy(list[i].id.Value, current) > 0) list[i].isyy = "yes";
                else list[i].isyy = "no";

                if (btzBLL.Isly(list[i].id.Value, current) > 0) list[i].isly = "yes";
                else list[i].isly = "no";


            }
            page.rows = list;
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult QD(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string temp = id.ToString();
                Member current = (Member)Session["Memberuser"]; //当前用户
                transferBLL.QD(current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BTZJqr(int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                int c = btzBLL.BTZJqr(BTZid, current);
                if (c == -1) response.msg = "成功关闭机器人";
                else response.msg = "成功开启机器人";
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BTZyy(int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                btzBLL.BTZyy(BTZid, current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BTZyyvip(int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                btzBLL.BTZyyvip(BTZid, current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ly(int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                btzBLL.ly(BTZid, current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //领养是否成功
        public JsonResult isCg(int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                response.msg = btzBLL.isCg(BTZid, current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //音效开关
        public JsonResult isMusic()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["Memberuser"]; //当前用户
                memberBLL.isMusic(current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult iWantdb(Axdbjl model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {

                Member current = (Member)Session["Memberuser"]; //当前用户
                model.uid = current.id;
                dbBLL.SaveProduct(model, current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageDbjl(Axdbjl model)
        {
            //修改为显示当期所有投注的记录
            Member mb = (Member)Session["MemberUser"];
            BaseSet set = setBLL.GetModel();
            //model.uid = mb.id;
            //model.dbxqs = set.dbxqs.Value;
            model.flagIn = "1,2,3";
            PageResult<Axdbjl> page = dbBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 获取校验码
        /// </summary>
        /// <returns></returns>
        public JsonResult SendPhoneCode(string totalNum, string workType, string picYzm, string phoneNo)
        {
            ResponseData response = new ResponseData("fail");
            try
            {
                string show_workType = "";
                if (workType == "shopping")
                    show_workType = "购物";
                if (workType == "uaicAdd")
                    show_workType = "增入资产";
                if (workType == "umemberInfo")
                    show_workType = "修改玩家资料";
                if (workType == "umTransfer")
                    show_workType = "帐户转帐";
                if (workType == "我要买入" || workType == "我要買入")
                {
                    show_workType = "增入资产";
                    workType = "uaicTransAdd";
                }
                if (workType == "卖给TA" || workType == "賣給TA")
                {
                    show_workType = "减出资产";
                    workType = "uaicTransSub";
                }
                if (workType == "appregAdd")
                    show_workType = "注册新帐户";
                if (workType == "pw1" || workType == "pw2" || workType == "pw3")
                    show_workType = "修改密码";
                object tAppManageLoginCode = Session["AppManageLoginCode"];
                Member cur = (Member)Session["MemberUser"];
                cur = memberBLL.GetModelById(cur.id.Value);
                string phone = "";
                //注册页面传手机，非注册页面读手机
                if (workType == "appregAdd")
                    phone = phoneNo;
                else
                    phone = cur.phone;



                object t = Session["sendcodeTimes"];
                DateTime now = DateTime.Now; //当前时间
                if (phone == null) { throw new ValidateException("请到个人资料填写手机号码"); }
                if (phone.Length > 10 && picYzm == tAppManageLoginCode.ToString())
                {
                    //发送间隔120秒
                    DateTime next = now.AddSeconds(120); //120秒后可在再次发送
                    Session["sendcodeTimes"] = next;
                    BaseSet set = setBLL.GetModel();
                    string code = GetRandom();
                    Session[workType + "_yzm_code"] = code;
                    string msg = "您正在" + show_workType;
                    if (totalNum != "")
                        msg += ",数量:" + totalNum;

                    msg += ",验证码:" + code + "，10分钟内有效";
                    noticeBLL.SendMessage(phone, msg);
                    response.Success();
                }

                if (phone.Length < 10)
                    response.msg = "请正确填写手机号码";
                if (picYzm != tAppManageLoginCode.ToString())
                    response.msg = "图片验证码不正确";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员" + ex.Message; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        private string GetRandom()
        {
            Random ro = new Random(10);
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
            int iResult = ran.Next(999999);
            string s = iResult.ToString().PadLeft(6, '0');
            return s;
        }


        public JsonResult GetPara()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户

                double ethRMB = 137 * 6.8;
                double bttRMB = 0.00079494 * 6.8;
                double dogeRMB = 0.003 * 6.8;
                if (Request.Url.Host.IndexOf("localhost") > -1 || 1 == 1)
                {
                    //有当天价格则取当天价格，避免多次读火币网太慢
                    System.Data.DataTable dt_huobiMarket_ethusdt = PsetBLL.GetDataTable("select * from huobiMarket where symbol='ethusdt' and  DateDiff(dd,addTime,getdate())=0");
                    if (dt_huobiMarket_ethusdt.Rows.Count > 0)
                        ethRMB = double.Parse(dt_huobiMarket_ethusdt.Rows[0]["priceRMB"].ToString());
                    else
                    {
                        string price = JhInterface.huobimarketPrice("ethusdt");
                        ethRMB = double.Parse(price) * 6.8;
                        PsetBLL.ExecSql("INSERT INTO huobiMarket(addTime,symbol,price,priceRMB) VALUES(getdate() ,'ethusdt' ,'" + price + "','" + ethRMB + "')");
                    }


                    System.Data.DataTable dt_huobiMarket_bttusdt = PsetBLL.GetDataTable("select * from huobiMarket where symbol='bttusdt' and  DateDiff(dd,addTime,getdate())=0");
                    if (dt_huobiMarket_bttusdt.Rows.Count > 0)
                        bttRMB = double.Parse(dt_huobiMarket_bttusdt.Rows[0]["priceRMB"].ToString());
                    else
                    {
                        //火币网不让连续两次连所以这里如果上次有连了这次不连
                        if (dt_huobiMarket_ethusdt.Rows.Count > 0)
                        {
                            string price = JhInterface.huobimarketPrice("bttusdt");
                            bttRMB = double.Parse(price) * 6.8;
                            PsetBLL.ExecSql("INSERT INTO huobiMarket(addTime,symbol,price,priceRMB) VALUES(getdate() ,'bttusdt' ,'" + price + "','" + bttRMB + "')");
                        }
                    }

                    System.Data.DataTable dt_huobiMarket_dogeusdt = PsetBLL.GetDataTable("select * from huobiMarket where symbol='dogeusdt' and  DateDiff(dd,addTime,getdate())=0");
                    if (dt_huobiMarket_dogeusdt.Rows.Count > 0)
                        dogeRMB = double.Parse(dt_huobiMarket_dogeusdt.Rows[0]["priceRMB"].ToString());
                    else
                    {
                        //火币网不让连续两次连所以这里如果上次有连了这次不连
                        if (dt_huobiMarket_ethusdt.Rows.Count > 0 && dt_huobiMarket_bttusdt.Rows.Count > 0)
                        {
                            string price = JhInterface.huobimarketPrice("dogeusdt");
                            dogeRMB = double.Parse(price) * 6.8;
                            PsetBLL.ExecSql("INSERT INTO huobiMarket(addTime,symbol,price,priceRMB) VALUES(getdate() ,'dogeusdt' ,'" + price + "','" + dogeRMB + "')");
                        }
                    }
                }

                di.Add("ethRMB", ethRMB);//ETH汇率
                //di.Add("bttRMB", bttRMB);//BTT汇率
                di.Add("bttRMB", dogeRMB);//BTT暂时不用。全用DOGE汇率
                di.Add("dogeRMB", dogeRMB);//DOGE汇率
                di.Add("uLevel", current.uLevel.Value);
                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("chargeLzzkje", "chargeLzkUlevel1", "chargeLzkUlevel2", "chargeLzkUlevel3", "chargeLzkUlevel4", "chargeLzkUlevel5");
                double chargeLzzkje = Convert.ToDouble(param["chargeLzzkje"].paramValue);
                double chargeLzkUlevel1 = Convert.ToDouble(param["chargeLzkUlevel1"].paramValue);
                double chargeLzkUlevel2 = Convert.ToDouble(param["chargeLzkUlevel2"].paramValue);
                double chargeLzkUlevel3 = Convert.ToDouble(param["chargeLzkUlevel3"].paramValue);
                double chargeLzkUlevel4 = Convert.ToDouble(param["chargeLzkUlevel4"].paramValue);
                double chargeLzkUlevel5 = Convert.ToDouble(param["chargeLzkUlevel5"].paramValue);
                di.Add("chargeLzzkje", chargeLzzkje);
                di.Add("chargeLzkUlevel1", chargeLzkUlevel1);
                di.Add("chargeLzkUlevel2", chargeLzkUlevel2);
                di.Add("chargeLzkUlevel3", chargeLzkUlevel3);
                di.Add("chargeLzkUlevel4", chargeLzkUlevel4);
                di.Add("chargeLzkUlevel5", chargeLzkUlevel5);

                SystemBank bank_ETH = bankBLL.GetModel(4);
                SystemBank bank_BTT = bankBLL.GetModel(5);
                di.Add("BTTfkmCode", bank_BTT.imgUrl);
                di.Add("ETHfkmCode", bank_ETH.imgUrl);
                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "加载数据失败，请联系管理员！" + ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }








    }
}
