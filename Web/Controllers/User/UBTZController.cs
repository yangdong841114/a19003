﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 络绎阁Controller
    /// </summary>
    public class UBTZController : Controller
    {
   
        public IBTZBLL btzBLL { get; set; }
        public IBTZBuyBLL btzbuyBLL { get; set; }
        
     
        /// <summary>
        /// 分页查询上架的商品
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(BTZ model)
        {
            if (model == null) { model = new BTZ(); }
            model.isShelve = 2;
            PageResult<BTZ> page = btzBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageAll(BTZ model)
        {
            if (model == null) { model = new BTZ(); }
            model.isShelve = 2;
            PageResult<BTZ> page = btzBLL.GetListPageAll(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }



    }
}
