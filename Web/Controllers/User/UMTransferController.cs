﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 转账Controller
    /// </summary>
    public class UMTransferController : Controller
    {
        public IMTransferBLL transferBLL { get; set; }
        public IBTZBLL btzBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }


        public JsonResult GetBtzData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");

            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                Member current = (Member)Session["MemberUser"]; //当前登录用户
                current = memberBLL.GetModelAndAccountNoPass(current.id.Value);//刷新当前所有信息防止脏数据，如音乐开关
                List<BTZ> btzlist_agentTjsy = btzBLL.GetList(" select * from BTZ where Gzzzxx<=" + current.account.agentTjsy);
                List<BTZ> btzlist_agentTdj = btzBLL.GetList(" select * from BTZ where Gzzzxx<=" + current.account.agentTdj);
                List<BTZ> btzlist_agentCsj = btzBLL.GetList(" select * from BTZ where Gzzzxx<=" + current.account.agentCsj);
                List<BTZ> btzlist_agentFt = btzBLL.GetList(" select * from BTZ where Gzzzxx<=" + current.account.agentFt);
                BTZ ifnull = new BTZ();
                ifnull.id = 0;
                ifnull.Gzzzxx = 0;
                if (btzlist_agentTjsy == null) { btzlist_agentTjsy = new List<BTZ>(); btzlist_agentTjsy.Add(ifnull); }
                if (btzlist_agentTdj == null) {btzlist_agentTdj = new List<BTZ>();btzlist_agentTdj.Add(ifnull);}
                if (btzlist_agentCsj == null) {btzlist_agentCsj = new List<BTZ>();btzlist_agentCsj.Add(ifnull);}
                if (btzlist_agentFt == null) { btzlist_agentFt = new List<BTZ>(); btzlist_agentFt.Add(ifnull); }
                di.Add("btzlist_agentTjsy", btzlist_agentTjsy);
                di.Add("btzlist_agentTdj", btzlist_agentTdj);
                di.Add("btzlist_agentCsj", btzlist_agentCsj);
                di.Add("btzlist_agentFt", btzlist_agentFt);
                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "加载数据失败，请联系管理员！" + ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult InitView()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                
                //获取用户最新信息
                Member user = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
                user.shopid = DateTime.Now.Hour;//当前小时数
                response.Success();
                response.result = user;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        } 

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(MTransfer model)
        {
            if (model == null) { model = new MTransfer(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.fromUid = mb.id.Value;
            PageResult<MTransfer> page = transferBLL.GetListPage(model);
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提交转账申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveMTransfer(MTransfer model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                if (mb.isSmrz != 1) { throw new ValidateException("请先通过实名认证"); }
                //验证二级密码
                if (DESEncrypt.EncryptDES(model.passOpen, ConstUtil.SALT) != mb.passOpen) { throw new ValidateException("二级密码不正确"); }
                model.passOpen = null;//用完清除

                int c = transferBLL.SaveMTransfer(model, mb);
                response.Success();
                response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 查询用户名称
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetUserName(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelByUserId(userId);
                if (m == null)
                {
                    response.msg = "会员不存在";
                }
                else
                {
                    response.msg = m.userName;
                }
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

         public JsonResult GetUserNameByphone(string phone)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelByphone(phone);
                if (m == null)
                {
                    response.msg = "会员不存在";
                }
                else
                {
                    response.msg = m.userId;
                }
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
            
        

    }
}
