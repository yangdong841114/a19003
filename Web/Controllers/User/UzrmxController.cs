﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 络绎阁Controller
    /// </summary>
    public class UzrmxController : Controller
    {
   
        public IBTZBLL btzBLL { get; set; }
        public IBTZBuyBLL btzbuyBLL { get; set; }
        public IBTZSaleBLL btzsaleBLL { get; set; }
        public IBTZBuyssBLL btzbuyssBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }
        public ISystemMsgBLL msgBLL { get; set; }
     
        /// <summary>
        /// 分页查询上架的商品
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPageQg(BTZBuy model)
        {
            //领养中：显示自身交易明细状态=待付款、待确认收款的记录；
            Member mb = (Member)Session["MemberUser"];
            //看过了。不显示通知数目
            Member updateZrmx = new Member();
            updateZrmx.id = mb.id.Value;
            updateZrmx.zrmx_account_isclick = 1;
            memberBLL.Update(updateZrmx);


            if (model == null) { model = new BTZBuy(); }
            model.uid = mb.id;
            model.flag = -35;//3匹配成功,5付款成功
            PageResult<BTZBuy> page = btzbuyBLL.GetListPage(model, "m.*,bsale.salephone");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageDcs(BTZBuy model)
        {
            //显示猪的结束时间=当天日期
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZBuy(); }
            model.uid = mb.id;
            model.flag = 6;//已确认付款
            //model.startTimeJssj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " 00:00:01");
            model.endTimeJssj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " 23:59:59");
            PageResult<BTZBuy> page = btzbuyBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageZrz(BTZSale model)
        {
            //已经卖出的络绎阁
            //Member mb = (Member)Session["MemberUser"];
            //if (model == null) { model = new BTZBuy(); }
            //model.uid = mb.id;
            //model.flag = 8;//已出售
            //PageResult<BTZBuy> page = btzbuyBLL.GetListPage(model, "m.*");
            //return Json(page, JsonRequestBehavior.AllowGet);
            //已经卖出未完成的单
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZSale(); }
            model.uid = mb.id;
           // model.BuyflagNotIn = "7";
            model.flag = -12;//2: "已转让", 3: "交易完成"
         
            model.startTime = DateTime.Now.AddDays(-300);//我的卖单”只需显示最近3天的记录
            PageResult<BTZSale> page = btzsaleBLL.GetListPage(model, "m.*,bby.BuyNo as ppBuyNo,bby.uid as buyuid,bby.userId as buyuserId,bby.userName as buyuserName,bby.phone as buyphone,bby.payType,bby.payTime,bby.imgUrl,bby.confirmPayTime,bby.Ppsj");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetListPagesale(BTZSale model)
        {
            //已经卖出交易已完成的单
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZSale(); }
            model.uid = mb.id;
            model.BuyflagNotIn = "7";
            //model.flag = -23;//2: "已转让", 3: "交易完成"
            model.flag = 3;//3: "交易完成"
            model.startTime = DateTime.Now.AddDays(-3);//我的卖单”只需显示最近3天的记录
            PageResult<BTZSale> page = btzsaleBLL.GetListPage(model, "m.*,bby.BuyNo as ppBuyNo,bby.uid as buyuid,bby.userId as buyuserId,bby.userName as buyuserName,bby.phone as buyphone,bby.payType,bby.payTime,bby.imgUrl,bby.confirmPayTime,bby.Ppsj");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPagesyzBTZ(BTZSale model)
        {
            
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZSale(); }
            model.uid = mb.id;
            model.issyzBTZ = 1;
            PageResult<BTZSale> page = btzsaleBLL.GetListPage(model, "m.*,bby.BuyNo as ppBuyNo,bby.userId as buyuserId,bby.userName as buyuserName,bby.phone as buyphone,bby.payType");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult GetListPagebuy(BTZBuy model)
        {
            //交易明细状态=已确认 的记录
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZBuy(); }
            model.uid = mb.id;
            model.flag = 6;//6已确认付款
          
            PageResult<BTZBuy> page = btzbuyBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        

        public JsonResult GetListPageJymx(BTZBuy model)
        {

            PageResult<BTZBuy> page = btzbuyBLL.GetListPageJymx(model, "m.*,mem.phone as salephone,mem.bankName,mem.bankCard,mem.bankAddress,mem.bankUser,mem.skIsbank,mem.skIszfb,mem.skIswx,mem.imgUrlzfb,mem.imgUrlwx,mem.skIsszhb,mem.szhbmc,mem.szhbmc1,mem.imgUrlszhb,mem.imgUrlszhb1,mem.BTTaddress,mem.ETHaddress");
            return Json(page, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetListPagess(BTZBuyss model)
        {
            PageResult<BTZBuyss> page = btzbuyssBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPagemyss(BTZBuyss model)
        {
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZBuyss(); }
            model.uid = mb.id;
            PageResult<BTZBuyss> page = btzbuyssBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SavesyzBTZ(int accountId,int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member mb = (Member)Session["MemberUser"];
                int c = btzsaleBLL.syzBTZ(accountId,BTZid, mb);
                response.Success();
                response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BTZBuyxy(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyBLL.xy(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BTZBuyssqx(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyssBLL.qx(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        

         [ValidateInput(false)]
        public JsonResult BTZBuyss(BTZBuyss model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member mb = (Member)Session["MemberUser"];
                BTZBuyss add= btzbuyssBLL.SaveProduct(model, mb);

                //消息提醒
                SystemMsg msg = new SystemMsg();
                msg.isRead = 0;
                msg.toUid = 0;
                msg.url = "#Charge";
                msg.msg = "您有新的申诉申请";
                msg.recordId = add.id;
                msg.recordTable = "BTZBuyss";
                msgBLL.Save(msg);

                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

         public JsonResult BTZBuysk(int id, string passOpen)
         {
             ResponseDtoData response = new ResponseDtoData("fail");
             try
             {
                 //当前登录用户        
                 Member mb = (Member)Session["MemberUser"];
                 //验证二级密码
                 if (DESEncrypt.EncryptDES(passOpen, ConstUtil.SALT) != mb.passOpen) { throw new ValidateException("二级密码不正确"); }
               

                 int c = btzbuyBLL.sk(id);
                 response.Success();
             }
             catch (ValidateException va) { response.msg = va.Message; }
             catch (Exception ex) { response.msg = "操作失败，请联系管理员！"+ex.Message; }

             return Json(response, JsonRequestBehavior.AllowGet);
         }

        

        [ValidateInput(false)]
        public JsonResult BTZBuyfk(BTZBuy model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (btzbuyBLL.GetModel(model.id.Value).flag.Value != 3) { throw new ValidateException("付款的买单必须为已匹配状态"); }
                //当前登录用户        
                Member mb = (Member)Session["MemberUser"];
                //验证二级密码
                if (DESEncrypt.EncryptDES(model.passOpen, ConstUtil.SALT) != mb.passOpen) { throw new ValidateException("二级密码不正确"); }
                model.passOpen = null;//用完清除
                if (imgFile != null)
                {
                    model.imgUrl = UploadImg(imgFile, "BTZBuy_" + model.id + "fk");
                }
                else
                { throw new ValidateException("请上传图片"); }
                model.flag=5;
                model.payTime = DateTime.Now;
                btzbuyBLL.fk(model);
                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img,string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl =frontName+ "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }
        
        

       



    }
}
