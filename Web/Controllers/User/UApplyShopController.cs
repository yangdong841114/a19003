﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 申请报单中心Controller
    /// </summary>
    public class UApplyShopController : Controller
    {
        //会员业务处理接口
        public IMemberBLL memberBLL { get; set; }

        public IParamSetBLL paramBLL { get; set; }

        public IShopSetBLL shopSetBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public JsonResult GetDefaultMsg()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["MemberUser"];         //当前登录用户

                Member user = memberBLL.GetModelByIdNoPassWord(current.id.Value); //获取当前用户最新信息

                if (user.isAgent == 0 || user.isAgent == 1)          //可申请状态
                {
                    List<string> codes = new List<string>();
                    codes.Add("shopPrice");
                    codes.Add("Bdzxzflz");
                    codes.Add("Bdzxztrs");
                    codes.Add("Bdzxxxrs");
                    Dictionary<string,ParameterSet> di = paramBLL.GetToDictionary(codes);
                    user.regAgentmoney = Convert.ToDouble(di["shopPrice"].paramValue);
                    user.spare0 = Convert.ToDouble(di["Bdzxzflz"].paramValue);
                    user.spare1 = Convert.ToDouble(di["Bdzxztrs"].paramValue);
                    user.spare2 = Convert.ToDouble(di["Bdzxxxrs"].paramValue);
                }
                response.result = user;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception)
            {
                response.msg = "查询失败，请联系管理员";
            }

            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 申请成为报单中心
        /// </summary>
        /// <returns></returns>
        public JsonResult ApplyShop(string provinceAgent, string cityAgent)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (provinceAgent == "" || cityAgent == "") throw new ValidateException("请填写申请城市");
                if (cityAgent.IndexOf("已被抢") != -1) throw new ValidateException("已有城市合伙人"); 
                Member current = (Member)Session["MemberUser"];         //当前登录用户
                shopSetBLL.InsertApplyShop(current.id.Value, provinceAgent,  cityAgent);
                Member user = memberBLL.GetModelByIdNoPassWord(current.id.Value); //获取当前用户最新信息
                //消息提醒
                SystemMsg msg = new SystemMsg();
                msg.isRead = 0;
                msg.toUid = 0;
                msg.url = "#ShopPassing";
                msg.msg = "您有新的报单中心申请";
                msg.recordId = user.id;
                msg.recordTable = "Shop";
                msgBLL.Save(msg);

                response.result = user;
                response.status = "success";
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception){ response.msg = "申请失败，请联系管理员";}
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
