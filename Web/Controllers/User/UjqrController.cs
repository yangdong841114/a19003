﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.User.Controllers
{
    /// <summary>
    /// 转账Controller
    /// </summary>
    public class UjqrController : Controller
    {
        public IJqrBLL jqrBLL { get; set; }
        public IJqrzrBLL jqrzrBLL { get; set; }
        public IParamSetBLL PsetBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }


        public JsonResult InitView()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                //获取用户最新信息
                Member user = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
                di.Add("user", user);
                string isHaveJqr = "no";
                if (jqrBLL.GetDataTable("select * from Jqr where  isStop=0 and uid=" + mb.id.Value).Rows.Count > 0) isHaveJqr = "yes";
                di.Add("isHaveJqr", isHaveJqr);
                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("Jqrlz", "Jqrcszs", "Jqrqgkssj", "Jqrqgjssj", "Jqrmtsl");
                string Jqrlz = param["Jqrlz"].paramValue;
                di.Add("Jqrlz", Jqrlz);
                string isCanqg="yes";
                string isCanqgError="";
                //平台出售机器人总数
                //if (jqrBLL.GetDataTable("select * from Jqr where  ly='认购'").Rows.Count >= int.Parse(param["Jqrcszs"].paramValue))
                //{
                //    isCanqg = "no"; isCanqgError = "已达-平台出售机器人总数";
                //}
                DateTime Jqrqgkssj = DateTime.Parse(DateTime.Now.ToString("d") + " " + param["Jqrqgkssj"].paramValue);
                DateTime Jqrqgjssj = DateTime.Parse(DateTime.Now.ToString("d") + " " + "23:59");
                try {
                Jqrqgjssj=DateTime.Parse(DateTime.Now.ToString("d") + " " + param["Jqrqgjssj"].paramValue);
                     }
                catch
                {

                }
                //if (DateTime.Now < Jqrqgkssj || DateTime.Now > Jqrqgjssj)
                //{
                //    isCanqg = "no"; isCanqgError = "已过抢购时间-今天已抢完，请明天再来";
                //}
                //if (jqrBLL.GetDataTable("select * from Jqr where  ly='认购' and DateDiff(dd,gmTime,getdate())=0").Rows.Count >= int.Parse(param["Jqrmtsl"].paramValue))
                //{
                //    isCanqg = "no"; isCanqgError = "已达-每天出售机器人数量";
                //}
                di.Add("isCanqg", isCanqg);
                di.Add("isCanqgError", isCanqgError);

                response.status = "success";
                response.map = di;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "加载失败，请联系管理员！"+ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        } 

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Jqr model)
        {
            if (model == null) { model = new Jqr(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.uid = mb.id.Value;
            PageResult<Jqr> page = jqrBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPagezr(Jqrzr model)
        {
            if (model == null) { model = new Jqrzr(); }
            //当前登录用户
            Member mb = (Member)Session["MemberUser"];
            model.uid = mb.id.Value;
            PageResult<Jqrzr> page = jqrzrBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提交转账申请
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveJqr(string passOpen)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string isCanqg = "yes";
                string isCanqgError = "";
                Jqr model = new Jqr();
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                //验证二级密码
                if (DESEncrypt.EncryptDES(passOpen, ConstUtil.SALT) != mb.passOpen) { throw new ValidateException("二级密码不正确"); }

                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("Jqrlz", "Jqryxq", "Jqrcszs", "Jqrqgkssj", "Jqrqgjssj", "Jqrmtsl");
                if (jqrBLL.GetDataTable("select * from Jqr where  ly='认购'").Rows.Count >= int.Parse(param["Jqrcszs"].paramValue))
                {
                    isCanqg = "no"; isCanqgError = "已售完"; //"已达-平台出售机器人总数";
                }
                DateTime Jqrqgkssj = DateTime.Parse(DateTime.Now.ToString("d") + " " + param["Jqrqgkssj"].paramValue);
                DateTime Jqrqgjssj = DateTime.Parse(DateTime.Now.ToString("d") + " " + param["Jqrqgjssj"].paramValue);
                if (DateTime.Now < Jqrqgkssj || DateTime.Now > Jqrqgjssj)
                {
                    isCanqg = "no"; isCanqgError = "已售完"; //isCanqgError = "已过抢购时间-今天已抢完，请明天再来";
                }
                if (jqrBLL.GetDataTable("select * from Jqr where  ly='认购' and DateDiff(dd,gmTime,getdate())=0").Rows.Count >= int.Parse(param["Jqrmtsl"].paramValue))
                {
                    isCanqg = "no"; isCanqgError = "已售完";// isCanqgError = "已达-每天出售机器人数量";
                }
                if (isCanqg == "no") throw new ValidateException(isCanqgError);


                double Jqrlz =  Convert.ToDouble(param["Jqrlz"].paramValue);
                int Jqryxq = Convert.ToInt32(param["Jqryxq"].paramValue);
                model.gmTime = DateTime.Now;
                model.dqTime = model.gmTime.Value.AddYears(Jqryxq);
                model.isStop = 0;
                model.ly = "认购";
                model.Lzsl = Jqrlz;
                model.uid = mb.id;
                model.userId = mb.userId;
                model.userName = mb.userName;
                model.yxq = Jqryxq;
                jqrBLL.Save(model, mb);
                response.Success();
                response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveJqrzr(Jqrzr model, string passOpen)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member mb = (Member)Session["MemberUser"];
                //验证二级密码
                if (DESEncrypt.EncryptDES(passOpen, ConstUtil.SALT) != mb.passOpen) { throw new ValidateException("二级密码不正确"); }
                jqrzrBLL.Save(model, mb);
                response.Success();
                response.result = memberBLL.GetModelAndAccountNoPass(mb.id.Value);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 查询用户名称
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult GetUserName(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelByUserId(userId);
                if (m == null)
                {
                    response.msg = "会员不存在";
                }
                else
                {
                    response.msg = m.userName;
                }
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserNameByphone(string phone)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member m = memberBLL.GetModelByphone(phone);
                if (m == null)
                {
                    response.msg = "会员不存在";
                }
                else
                {
                    response.msg = m.userId;
                }
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "查询失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}
