﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 商品管理Controller
    /// </summary>
    public class BTZjscityController : Controller
    {
        public IBTZjscityBLL jscityBLL { get; set; }
        public IProductTypeBLL producttypeBLL { get; set; }
        

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(BTZjscity model)
        {
            if (model == null) { model = new BTZjscity(); }
            //model.uid = 1;
            PageResult<BTZjscity> page = jscityBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageBTZJb(ProductType model)
        {
            if (model == null) { model = new ProductType(); }
            PageResult<ProductType> page = null;
            model.grade = 1;
            page = producttypeBLL.GetListPage(model, "id,name,grade,parentId,BTZLevel,remark,no");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageBTZBj(ProductType model)
        {
            if (model == null) { model = new ProductType(); }
            PageResult<ProductType> page = null;
            model.grade = 2;
            page = producttypeBLL.GetListPage(model, "id,name,grade,parentId,BTZLevel,remark,no");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveOrUpdate(BTZjscity model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                BTZjscity newPro =jscityBLL.Save(model,current) ;
               
                response.Success();
                response.result = newPro;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [ValidateInput(false)]
        public JsonResult SaveOrUpdateBTZJb(ProductType model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.grade = 1;//级别
                model.no = "BTZJb";//级别
                model.parentId = 0;
                Member current = (Member)Session["LoginUser"];
                if (model.id != null)
                {
                    producttypeBLL.UpdateProductType(model);
                }
                else
                {
                    producttypeBLL.SaveProductType(model);
                }

                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [ValidateInput(false)]
        public JsonResult SaveOrUpdateBTZBj(ProductType model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                model.grade = 2;//班级
                model.no = "BTZBj";//级别
               
                Member current = (Member)Session["LoginUser"];
                if (model.id != null)
                {
                    producttypeBLL.UpdateProductType(model);
                }
                else
                {
                    producttypeBLL.SaveProductType(model);
                }

                response.Success();
                response.result = model;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        
     

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = jscityBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteBTZJb(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                producttypeBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteBTZBj(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                producttypeBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        

        public JsonResult GetBTZJb()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();

                List<ProductType> list = producttypeBLL.GetList(" select * from ProductType where grade=1");
                response.Success();
                di.Add("BTZJb_list", list); //级别
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载基础数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBTZBj(string parentId)
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();

                List<ProductType> list = producttypeBLL.GetList(" select * from ProductType where parentId=" + parentId);
                response.Success();
                di.Add("BTZBj_list", list); //班级
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载基础数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}
