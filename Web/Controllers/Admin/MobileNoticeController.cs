﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class MobileNoticeController : Controller
    {
        public IMobileNoticeBLL mnBLL { get; set; }

        /// <summary>
        /// 初始化页面，读取已有参数
        /// </summary>
        /// <returns></returns>
        public JsonResult InitView()
        {
            ResponseDtoMap<string, MobileNotice> response = new ResponseDtoMap<string, MobileNotice>();
            response.map = mnBLL.GetToDictionary();
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存参数
        /// </summary>
        /// <param name="list">参数列表</param>
        /// <returns></returns>
        public JsonResult Save(List<MobileNotice> list)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                mnBLL.UpdateList(list);
                response.status = "success";
            }
            catch (ValidateException ex){response.msg = ex.Message;}
            catch (Exception) { response.msg = "操作失败！请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

    }
}
