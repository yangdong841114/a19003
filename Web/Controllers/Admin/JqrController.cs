﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;
using System.Data;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 机器人Controller
    /// </summary>
    public class JqrController : Controller
    {
        public IJqrBLL jqrBLL { get; set; }
        public IJqrzrBLL jqrzrBLL { get; set; }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(Jqr model)
        {
            PageResult<Jqr> page = jqrBLL.GetListPage(model,"m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPagezr(Jqrzr model)
        {
            PageResult<Jqrzr> page = jqrzrBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        
    }
}
