﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    /// <summary>
    /// 商品管理Controller
    /// </summary>
    public class BTZfbController : Controller
    {
        public IBTZBLL btzBLL { get; set; }
        public IBTZBuyBLL btzbuyBLL { get; set; }
        public IBTZSaleBLL btzsaleBLL { get; set; }
         public IBTZBuyssBLL btzbuyssBLL { get; set; }
        /// <summary>
        /// 查询商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetModel(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.Success();
                response.result = btzBLL.GetModel(id);
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BTZBb()
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
//                周、月均是按自然周、自然月计算

//1、BTT未提现总额汇总：所有会员BTT账户余额合计；

//2、BTT已提现总额汇总：所有会员BTT账户提现记录的提现金额合计（含待审、已审）；

//3、BTD未提现总额汇总：所有会员BTD账户余额合计；

//4、未到期的猪总份数：所有会员持有的状态=增值中 的 记录总条数；

//5、未到期的猪总价值：所有会员持有的状态=增值中 的 记录 价值 总合计；

//6、3天后到期的猪总份数：所有3天后到期会员持有的状态=增值中 的 记录总条数；

//7、未到期的猪总价值：所有3天后到期会员持有的状态=增值中 的 记录 价值 总合计

//8、未提现推荐收益：所有会员推荐收益账户余额合计；

//9、已提现推荐收益：所有会员 推荐收益 转成 络绎阁出售 的总金额合计；

//10、未提现团队奖：所有会员团队奖账户余额合计；

//11、已提现团队奖：所有会员 团队奖 转成 络绎阁出售 的总金额合计

//12、未提现城市奖：所有会员城市奖账户余额合计；

//13、已提现城市奖：所有会员 城市奖 转成 络绎阁出售 的总金额合计

                string Html_bb = @"<table class='table table-bordered table-auto' cellpadding='0' cellspacing='0' style='width:100%'>
                <thead>
                    <tr>
                        <th>类目名称</th>
                        <th>本日</th>
                        <th>本周</th>
                        <th>本月</th>
                    </tr>
                </thead>
                <tbody>";
                //靈氣充值总额..
                double Lzcz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(convert(int,epoints)),0) as Lzcz_Day from Charge where DateDiff(dd,addTime,getdate())=0").Rows[0]["Lzcz_Day"].ToString());
                double Lzcz_Week = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(convert(int,epoints)),0) as Lzcz_Week from Charge where DateDiff(week,addTime,getdate())=0").Rows[0]["Lzcz_Week"].ToString());
                double Lzcz_Month = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(convert(int,epoints)),0) as Lzcz_Month from Charge where DateDiff(month,addTime,getdate())=0").Rows[0]["Lzcz_Month"].ToString());
                Html_bb += " <tr><td valign='middle'>靈氣充值总额</td><td>" + Lzcz_Day + "</td> <td> " + Lzcz_Week + " </td><td>" + Lzcz_Month + "</td></tr>";
                //BTT未提现总额汇总
                double BTTwtx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(agentBTT),0) as BTTwtx_Day from MemberAccount where 1=1").Rows[0]["BTTwtx_Day"].ToString());
                double BTTwtx_Week = BTTwtx_Day;
                double BTTwtx_Month = BTTwtx_Day;
                Html_bb += " <tr><td valign='middle'>DOGE未提现总额汇总</td><td>" + BTTwtx_Day + "</td> <td> " + BTTwtx_Week + " </td><td>" + BTTwtx_Month + "</td></tr>";
                //BTT已提现总额汇总
                double BTTytx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(convert(int,epoints)),0) as BTTytx_Day from TakeCash where DateDiff(dd,addTime,getdate())=0").Rows[0]["BTTytx_Day"].ToString());
                double BTTytx_Week = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(convert(int,epoints)),0) as BTTytx_Week from TakeCash where DateDiff(week,addTime,getdate())=0").Rows[0]["BTTytx_Week"].ToString());
                double BTTytx_Month = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(convert(int,epoints)),0) as BTTytx_Month from TakeCash where DateDiff(month,addTime,getdate())=0").Rows[0]["BTTytx_Month"].ToString());
                Html_bb += " <tr><td valign='middle'>DOGE已提现总额汇总</td><td>" + BTTytx_Day + "</td> <td> " + BTTytx_Week + " </td><td>" + BTTytx_Month + "</td></tr>";
                //BTD未提现总额汇总
                double BTDwtx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(agentBTD),0) as BTDwtx_Day from MemberAccount where 1=1").Rows[0]["BTDwtx_Day"].ToString());
                double BTDwtx_Week = BTDwtx_Day;
                double BTDwtx_Month = BTDwtx_Day;
                Html_bb += " <tr><td valign='middle'>BTD未提现总额汇总</td><td>" + BTDwtx_Day + "</td> <td> " + BTDwtx_Week + " </td><td>" + BTDwtx_Month + "</td></tr>";
                //未到期的猪总份数
                double BTZwdq_Day = Convert.ToDouble(btzBLL.GetTable("select count(1) as BTZwdq_Day from BTZBuy where flag=6").Rows[0]["BTZwdq_Day"].ToString());
                double BTZwdq_Week = BTZwdq_Day;
                double BTZwdq_Month = BTZwdq_Day;
                Html_bb += " <tr><td valign='middle'>未到期的猪总份数</td><td>" + BTZwdq_Day + "</td> <td> " + BTZwdq_Week + " </td><td>" + BTZwdq_Month + "</td></tr>";
                //未到期的猪总价值
                double BTZwdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0) as BTZwdqzz_Day from BTZBuy where flag=6").Rows[0]["BTZwdqzz_Day"].ToString());
                double BTZwdqzz_Week = BTZwdqzz_Day;
                double BTZwdqzz_Month = BTZwdqzz_Day;
                Html_bb += " <tr><td valign='middle'>未到期的猪总价值</td><td>" + BTZwdqzz_Day + "</td> <td> " + BTZwdqzz_Week + " </td><td>" + BTZwdqzz_Month + "</td></tr>";
                //1天后到期的猪总份数
                double BTZ1tdq_Day = Convert.ToDouble(btzBLL.GetTable("select count(1) as BTZ1tdq_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-1 and flag=6").Rows[0]["BTZ1tdq_Day"].ToString());
                double BTZ1tdq_Week = BTZ1tdq_Day;
                double BTZ1tdq_Month = BTZ1tdq_Day;
                Html_bb += " <tr><td valign='middle'>1天后到期的猪总份数</td><td>" + BTZ1tdq_Day + "</td> <td> " + BTZ1tdq_Week + " </td><td>" + BTZ1tdq_Month + "</td></tr>";
                //1天后到期的猪总价值
                double BTZ1tdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as BTZ1tdqzz_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-1 and flag=6").Rows[0]["BTZ1tdqzz_Day"].ToString());
                double BTZ1tdqzz_Week = BTZ1tdqzz_Day;
                double BTZ1tdqzz_Month = BTZ1tdqzz_Day;
                Html_bb += " <tr><td valign='middle'>1天后到期的猪总价值</td><td>" + BTZ1tdqzz_Day + "</td> <td> " + BTZ1tdqzz_Week + " </td><td>" + BTZ1tdqzz_Month + "</td></tr>";
                //2天后到期的猪总份数
                double BTZ2tdq_Day = Convert.ToDouble(btzBLL.GetTable("select count(1) as BTZ2tdq_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-2 and flag=6").Rows[0]["BTZ2tdq_Day"].ToString());
                double BTZ2tdq_Week = BTZ2tdq_Day;
                double BTZ2tdq_Month = BTZ2tdq_Day;
                Html_bb += " <tr><td valign='middle'>2天后到期的猪总份数</td><td>" + BTZ2tdq_Day + "</td> <td> " + BTZ2tdq_Week + " </td><td>" + BTZ2tdq_Month + "</td></tr>";
                //2天后到期的猪总价值
                double BTZ2tdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as BTZ2tdqzz_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-2 and flag=6").Rows[0]["BTZ2tdqzz_Day"].ToString());
                double BTZ2tdqzz_Week = BTZ2tdqzz_Day;
                double BTZ2tdqzz_Month = BTZ2tdqzz_Day;
                Html_bb += " <tr><td valign='middle'>2天后到期的猪总价值</td><td>" + BTZ2tdqzz_Day + "</td> <td> " + BTZ2tdqzz_Week + " </td><td>" + BTZ2tdqzz_Month + "</td></tr>";
                //3天后到期的猪总份数
                double BTZ3tdq_Day = Convert.ToDouble(btzBLL.GetTable("select count(1) as BTZ3tdq_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-3 and flag=6").Rows[0]["BTZ3tdq_Day"].ToString());
                double BTZ3tdq_Week = BTZ3tdq_Day;
                double BTZ3tdq_Month = BTZ3tdq_Day;
                Html_bb += " <tr><td valign='middle'>3天后到期的猪总份数</td><td>" + BTZ3tdq_Day + "</td> <td> " + BTZ3tdq_Week + " </td><td>" + BTZ3tdq_Month + "</td></tr>";
                //3天后到期的猪总价值
                double BTZ3tdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as BTZ3tdqzz_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-3 and flag=6").Rows[0]["BTZ3tdqzz_Day"].ToString());
                double BTZ3tdqzz_Week = BTZ3tdqzz_Day;
                double BTZ3tdqzz_Month = BTZ3tdqzz_Day;
                Html_bb += " <tr><td valign='middle'>3天后到期的猪总价值</td><td>" + BTZ3tdqzz_Day + "</td> <td> " + BTZ3tdqzz_Week + " </td><td>" + BTZ3tdqzz_Month + "</td></tr>";
                //未提现：推荐收益
                double TJSYwtx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(agentTjsy),0) as TJSYwtx_Day from MemberAccount where 1=1").Rows[0]["TJSYwtx_Day"].ToString());
                double TJSYwtx_Week = TJSYwtx_Day;
                double TJSYwtx_Month = TJSYwtx_Day;
                Html_bb += " <tr><td valign='middle'>未提现：推荐收益</td><td>" + TJSYwtx_Day + "</td> <td> " + TJSYwtx_Week + " </td><td>" + TJSYwtx_Month + "</td></tr>";
                //已提现：推荐收益
                double TJSYytx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(outlay),0) as TJSYytx_Day from LiuShuiZhang where tableName='syzBTZ' and accountId=39").Rows[0]["TJSYytx_Day"].ToString());
                double TJSYytx_Week = TJSYytx_Day;
                double TJSYytx_Month = TJSYytx_Day;
                Html_bb += " <tr><td valign='middle'>已提现：推荐收益</td><td>" + TJSYytx_Day + "</td> <td> " + TJSYytx_Week + " </td><td>" + TJSYytx_Month + "</td></tr>";
                //未提现：团队奖
                double TDJwtx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(agentTdj),0) as TDJwtx_Day from MemberAccount where 1=1").Rows[0]["TDJwtx_Day"].ToString());
                double TDJwtx_Week = TDJwtx_Day;
                double TDJwtx_Month = TDJwtx_Day;
                Html_bb += " <tr><td valign='middle'>未提现：团队奖</td><td>" + TDJwtx_Day + "</td> <td> " + TDJwtx_Week + " </td><td>" + TDJwtx_Month + "</td></tr>";
                //已提现：团队奖
                double TDJytx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(outlay),0) as TDJytx_Day from LiuShuiZhang where tableName='syzBTZ' and accountId=47").Rows[0]["TDJytx_Day"].ToString());
                double TDJytx_Week = TDJytx_Day;
                double TDJytx_Month = TDJytx_Day;
                Html_bb += " <tr><td valign='middle'>已提现：团队奖</td><td>" + TDJytx_Day + "</td> <td> " + TDJytx_Week + " </td><td>" + TDJytx_Month + "</td></tr>";
                //未提现：城市奖
                double CSJwtx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(agentCsj),0) as CSJwtx_Day from MemberAccount where 1=1").Rows[0]["CSJwtx_Day"].ToString());
                double CSJwtx_Week = CSJwtx_Day;
                double CSJwtx_Month = CSJwtx_Day;
                Html_bb += " <tr><td valign='middle'>未提现：城市奖</td><td>" + CSJwtx_Day + "</td> <td> " + CSJwtx_Week + " </td><td>" + CSJwtx_Month + "</td></tr>";
                //已提现：城市奖
                double CSJytx_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(outlay),0) as CSJytx_Day from LiuShuiZhang where tableName='syzBTZ' and accountId=48").Rows[0]["CSJytx_Day"].ToString());
                double CSJytx_Week = CSJytx_Day;
                double CSJytx_Month = CSJytx_Day;
                Html_bb += " <tr><td valign='middle'>已提现：城市奖</td><td>" + CSJytx_Day + "</td> <td> " + CSJytx_Week + " </td><td>" + CSJytx_Month + "</td></tr>";

              
                Html_bb+=" </tbody> </table>";   
               
                response.Success();
                BTZ temp = new BTZ();
                temp.imgUrl = Html_bb;
                response.result = temp;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BTZOneBb(int BTZid)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //if (BTZid == null) BTZid = 0;
                BTZ model = btzBLL.GetModel(BTZid);
                string Html_bb = "";
                if(model!=null)
                {
                    Html_bb = @"<table class='table table-bordered table-auto' cellpadding='0' cellspacing='0' style='width:100%'>
                <thead>
                    <tr>
                        <th>类目名称</th>
                        <th>数值</th>
                    </tr>
                </thead>
                <tbody>";
                    Html_bb += " <tr><td valign='middle'>编号</td><td>" + model.BTZCode + "</td> </tr>";
                    Html_bb += " <tr><td valign='middle'>区块猪名称</td><td>" + model.BTZName + "</td> </tr>";
                  
                    //预约数
                    int YYS = Convert.ToInt32(btzBLL.GetTable("select count(1) as YYS from BTZBuy where  DateDiff(dd,Yysj,getdate())=0 and BuymemType=1 and Yysj is not null and BTZid=" + BTZid).Rows[0]["YYS"].ToString());
                    Html_bb += " <tr><td valign='middle'>预约数</td><td>" + YYS + "</td> </tr>";
                    //预约总价值
                    double YYZJZ = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as YYZJZ from BTZBuy where DateDiff(dd,Yysj,getdate())=0 and  Yysj is not null and BTZid=" + BTZid).Rows[0]["YYZJZ"].ToString());
                    Html_bb += " <tr><td valign='middle'>预约总价值</td><td>" + YYZJZ + "</td> </tr>";
                    //VIP预约数
                    int VIPYYS = Convert.ToInt32(btzBLL.GetTable("select count(1) as VIPYYS from BTZBuy where  DateDiff(dd,Yysj,getdate())=0 and YyVipsj is not null and BTZid=" + BTZid).Rows[0]["VIPYYS"].ToString());
                    Html_bb += " <tr><td valign='middle'>VIP预约数</td><td>" + VIPYYS + "</td> </tr>";
                    //VIP预约总价值
                    double VIPYYZJZ = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as VIPYYZJZ from BTZBuy where DateDiff(dd,Yysj,getdate())=0 and  YyVipsj is not null and BTZid=" + BTZid).Rows[0]["VIPYYZJZ"].ToString());
                    Html_bb += " <tr><td valign='middle'>VIP预约总价值</td><td>" + VIPYYZJZ + "</td> </tr>";

                    //已申请领养数
                    int SQLYS = Convert.ToInt32(btzBLL.GetTable("select count(1) as SQLYS from BTZBuy where  DateDiff(dd,Qysj,getdate())=0 and BuymemType=1  and BTZid=" + BTZid).Rows[0]["SQLYS"].ToString());
                    Html_bb += " <tr><td valign='middle'>已申请领养数</td><td>" + SQLYS + "</td> </tr>";

                    //已领养数
                    int YLYS = Convert.ToInt32(btzBLL.GetTable("select count(1) as YLYS from BTZBuy where DateDiff(dd,Ppsj,getdate())=0 and  flag=6 and BTZid=" + BTZid).Rows[0]["YLYS"].ToString());
                    Html_bb += " <tr><td valign='middle'>已领养数</td><td>" + YLYS + "</td> </tr>";
                    //已领养总价值
                    double YLYZJZ = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as YLYZJZ from BTZBuy where DateDiff(dd,Ppsj,getdate())=0 and   flag=6  and BTZid=" + BTZid).Rows[0]["YLYZJZ"].ToString());
                    Html_bb += " <tr><td valign='middle'>已领养总价值</td><td>" + YLYZJZ + "</td> </tr>";

                    //当日机器人开启数
                    int JQRKQS = Convert.ToInt32(btzBLL.GetTable("select count(1) as JQRKQS from BTZBuy where DateDiff(dd,Jqrkqsj,getdate())=0  and BTZid=" + BTZid).Rows[0]["JQRKQS"].ToString());
                    Html_bb += " <tr><td valign='middle'>当日机器人开启数</td><td>" + JQRKQS + "</td> </tr>";

                    //当日出售数-虚拟会员不计算在内
                    int DRCSS = Convert.ToInt32(btzBLL.GetTable("select count(1) as DRCSS from BTZBuy where DateDiff(dd,Jssj,getdate())=0 and flag=6 and BuymemType=1 and BTZid=" + BTZid).Rows[0]["DRCSS"].ToString());
                    Html_bb += " <tr><td valign='middle'>当日出售数</td><td>" + DRCSS + "</td> </tr>";
                    //当日出售总价值-虚拟会员不计算在内
                    double DRCSZJZ = Convert.ToDouble(btzBLL.GetTable("select  isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as DRCSZJZ from BTZBuy where  DateDiff(dd,Jssj,getdate())=0 and flag=6 and BuymemType=1  and BTZid=" + BTZid).Rows[0]["DRCSZJZ"].ToString());
                    Html_bb += " <tr><td valign='middle'>当日出售总价值</td><td>" + DRCSZJZ + "</td> </tr>";
                    //1天后到期的猪总份数
                    int BTZ1tdq_Day = Convert.ToInt32(btzBLL.GetTable("select count(1) as BTZ1tdq_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-1 and flag=6  and BTZid=" + BTZid).Rows[0]["BTZ1tdq_Day"].ToString());
                    Html_bb += " <tr><td valign='middle'>1天后出售数</td><td>" + BTZ1tdq_Day + "</td> </tr>";
                    //1天后到期的猪总价值
                    double BTZ1tdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as BTZ1tdqzz_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-1 and flag=6 and BTZid=" + BTZid).Rows[0]["BTZ1tdqzz_Day"].ToString());
                    Html_bb += " <tr><td valign='middle'>1天后出售总价值</td><td>" + BTZ1tdqzz_Day + "</td> </tr>";
                    //2天后到期的猪总份数
                    int BTZ2tdq_Day = Convert.ToInt32(btzBLL.GetTable("select count(1) as BTZ2tdq_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-2 and flag=6  and BTZid=" + BTZid).Rows[0]["BTZ2tdq_Day"].ToString());
                    Html_bb += " <tr><td valign='middle'>2天后出售数</td><td>" + BTZ2tdq_Day + "</td> </tr>";
                    //2天后到期的猪总价值
                    double BTZ2tdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as BTZ2tdqzz_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-2 and flag=6 and BTZid=" + BTZid).Rows[0]["BTZ2tdqzz_Day"].ToString());
                    Html_bb += " <tr><td valign='middle'>2天后出售总价值</td><td>" + BTZ2tdqzz_Day + "</td> </tr>";
                    //3天后到期的猪总份数
                    int BTZ3tdq_Day = Convert.ToInt32(btzBLL.GetTable("select count(1) as BTZ3tdq_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-3 and flag=6  and BTZid=" + BTZid).Rows[0]["BTZ3tdq_Day"].ToString());
                    Html_bb += " <tr><td valign='middle'>3天后出售数</td><td>" + BTZ3tdq_Day + "</td> </tr>";
                    //3天后到期的猪总价值
                    double BTZ3tdqzz_Day = Convert.ToDouble(btzBLL.GetTable("select isnull(SUM(priceOldzz*(1+(RsyBili+isZcs*ZcszjBili)*Hyts/100)),0)  as BTZ3tdqzz_Day from BTZBuy where DateDiff(dd,Jssj,getdate())=-3 and flag=6 and BTZid=" + BTZid).Rows[0]["BTZ3tdqzz_Day"].ToString());
                    Html_bb += " <tr><td valign='middle'>3天后出售总价值</td><td>" + BTZ3tdqzz_Day + "</td> </tr>";

                    Html_bb += " </tbody> </table>";   
                }
                response.Success();
                BTZ temp = new BTZ();
                temp.imgUrl = Html_bb;
                response.result = temp;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult MemberTj(string province, string city)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                string where = " 1=1";
                if (province != "" && province != null) where += " and province='" + province + "' ";
                if (city != "" && city != null) where += " and city='" + city + "' ";
                System.Data.DataTable dt_Member = btzBLL.GetTable("select * from (select distinct province,city,COUNT(1) as count_rs from Member where " + where + " group by province,city ) t where 1=1 order by count_rs desc");
                string Html_bb = "";

                Html_bb = @"<table class='table table-bordered table-auto' cellpadding='0' cellspacing='0' style='width:100%'>
                <thead>
                    <tr>
                        <th>省</th>
                        <th>市</th>
                        <th>人数</th>
                    </tr>
                </thead>
                <tbody>";
                for(int i=0;i<dt_Member.Rows.Count;i++)
                {
                    Html_bb += " <tr><td>" + dt_Member.Rows[i]["province"].ToString() + "</td><td>" + dt_Member.Rows[i]["city"].ToString() + "</td><td>" + dt_Member.Rows[i]["count_rs"].ToString() + "</td> </tr>";
                   
                  
                }
                Html_bb += " </tbody> </table>";   
                response.Success();
                BTZ temp = new BTZ();
                temp.imgUrl = Html_bb;
                response.result = temp;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CurrencyTj(string userId)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                System.Data.DataTable dt = btzBLL.GetTable(@"   select top 50 t.*,M.userName from 
   (
   select uid,userId,
	                   sum(case when cat = 45 then ePoints else 0 end) as cat1,
	                  sum(case when cat = 46 then ePoints else 0 end) as cat2,
	                     sum(case when cat = 47 then ePoints else 0 end) as cat3,
	                     sum(case when cat = 48 then ePoints else 0 end) as cat4,
	                     sum(case when cat = 49 then ePoints else 0 end) as cat5,
	                     sum(case when cat = 50 then ePoints else 0 end) as cat6,
	                     sum(case when cat = 51 then ePoints else 0 end) as cat7,
	                     SUM(ePoints) as catAll
	                      from Currency group by uid,userId 
	                      ) t,Member m where t.uid=m.id and t.userId like '%" + userId + "%' order by t.catAll desc");
                string Html_bb = "";

                Html_bb = @"<table class='table table-bordered table-auto' cellpadding='0' cellspacing='0' style='width:100%'>
                <thead>
                    <tr>
                        <th>会员编号</th>
                        <th>会员姓名</th>
                        <th>动态奖</th>
                        <th>静态奖</th>
                        <th>总收益</th>
                    </tr>
                </thead>
                <tbody>";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    double dtj = double.Parse(dt.Rows[i]["cat2"].ToString()) + double.Parse(dt.Rows[i]["cat3"].ToString()) + double.Parse(dt.Rows[i]["cat4"].ToString());//动态奖=推荐收益+团队奖+城市奖
                    double jtj = double.Parse(dt.Rows[i]["cat1"].ToString());//静态奖=推荐收益
                    Html_bb += " <tr><td>" + dt.Rows[i]["userId"].ToString() + "</td><td>" + dt.Rows[i]["userName"].ToString() + "</td><td>" + dtj.ToString("0.00") + "</td><td>" + jtj.ToString("0.00") + "</td><td>" + double.Parse(dt.Rows[i]["catAll"].ToString()).ToString("0.00") + "</td> </tr>";
                   
                  
                }
                Html_bb += " </tbody> </table>";   
                response.Success();
                BTZ temp = new BTZ();
                temp.imgUrl = Html_bb;
                response.result = temp;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "加载失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Member">查询条件对象</param>
        /// <returns></returns>
        public JsonResult GetListPage(BTZ model)
        {
            if (model == null) { model = new BTZ(); }
            //model.uid = 1;
            PageResult<BTZ> page = btzBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageCsjl(BTZBuy model)
        {
            model.flagNotin = "7";
            PageResult<BTZBuy> page = btzbuyBLL.GetListPageJymx(model, "m.*,bsale.Cssj,bsale.flag as saleflag,bsale.Ppcs,mem.phone as salephone,mem.bankName,mem.bankCard,mem.bankAddress,mem.bankUser,mem.skIsbank,mem.skIszfb,mem.skIswx,mem.imgUrlzfb,mem.imgUrlwx,mem.skIsszhb,mem.szhbmc,mem.szhbmc1,mem.imgUrlszhb,mem.imgUrlszhb1,mem.BTTaddress,mem.ETHaddress");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageSdgm(BTZSale model)
        {
            Member mb = (Member)Session["MemberUser"];
            if (model == null) { model = new BTZSale(); }
            model.isVirtual = 1;
            PageResult<BTZSale> page = btzsaleBLL.GetListPage(model, "m.*,bby.userId as buyuserId,bby.userName as buyuserName,bby.phone as buyphone,bby.payType");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetListPageYyjl(BTZBuy model)
        {
            PageResult<BTZBuy> page = btzbuyBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageHgjl(BTZBuy model)
        {
            model.isVirtual = 1;
            PageResult<BTZBuy> page = btzbuyBLL.GetListPage(model, "m.*,bsale.Cssj,bsale.flag as saleflag,bsale.Ppcs,mem.phone as salephone,mem.bankName,mem.bankCard,mem.bankAddress,mem.bankUser,mem.skIsbank,mem.skIszfb,mem.skIswx,mem.imgUrlzfb,mem.imgUrlwx,mem.skIsszhb,mem.szhbmc,mem.szhbmc1,mem.imgUrlszhb,mem.imgUrlszhb1,mem.BTTaddress,mem.ETHaddress");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageSsgl(BTZBuyss model)
        {
            PageResult<BTZBuyss> page = btzbuyssBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BTZBuyssqx(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyssBLL.qx(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BTZBuyssjc(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyssBLL.jc(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BTZBuyssjcjy(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyssBLL.jcjy(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        


       [ValidateInput(false)]
        public JsonResult BTZBuysshh(int id, string hhcont)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                btzbuyssBLL.hh(id, hhcont);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        

        public JsonResult BTZBuyfk(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyBLL.fk(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [ValidateInput(false)]
        public JsonResult BTZBuyfkHaveFile(BTZBuy model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (btzbuyBLL.GetModel(model.id.Value).flag != 3) { throw new ValidateException("状态必须为領養成功的才能付款"); }
                if (imgFile != null)
                {
                    model.imgUrl = UploadImgFk(imgFile, "BTZBuy_" + model.id + "fk");
                }
                else
                { throw new ValidateException("请上传图片"); }
                model.flag = 5;
                model.payTime = DateTime.Now;
                btzbuyBLL.Update(model);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImgFk(HttpPostedFileBase img, string frontName)
        {
            if (img == null) { throw new ValidateException("请上传图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = frontName + "_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }

        public JsonResult BTZBuysk(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyBLL.sk(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BTZBuycd(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyBLL.cd(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult delBTZSale(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzsaleBLL.delBTZSale(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult addBTZSale(BTZSale add, int Fbsm)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member current = (Member)Session["LoginUser"];
                for(int i=0;i<Fbsm;i++)
                btzsaleBLL.addBTZSaleVirtual(add, current);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员！"+ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BTZBuyxy(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzbuyBLL.xy(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        

        /// <summary>
        /// 保存或更新商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public JsonResult SaveOrUpdate(BTZ model, HttpPostedFileBase imgFile)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                //当前登录用户
                Member current = (Member)Session["LoginUser"];
                BTZ newPro = null;
              
                if (ValidateUtils.CheckIntZero(model.id))
                {
                    //新增商品必须上传图片
                    model.imgUrl = UploadImg(imgFile);
                    newPro = btzBLL.SaveProduct(model, current);
                }
                else
                {
                    //修改时，如有更新图片则重新上传
                    if (imgFile != null)
                    {
                        model.imgUrl = UploadImg(imgFile);
                    }
                    newPro = btzBLL.UpdateProduct(model, current);
                }
                response.Success();
                response.result = newPro;
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception ex) { response.msg = "操作失败，请联系管理员！"+ex.Message; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private string UploadImg(HttpPostedFileBase img)
        {
            if (img == null) { throw new ValidateException("请上传络绎阁图片"); }
            string oldFileName = img.FileName;
            int lastIndex = oldFileName.LastIndexOf(".");
            string suffix = oldFileName.Substring(lastIndex, oldFileName.Length - lastIndex); //扩展名
            if (!img.ContentType.StartsWith("image/"))
            {
                throw new ValidateException("只能上传图片");
            }
            if (img.ContentLength > (1024 * 1024 * 2)) { throw new ValidateException("图片大小不能超过2M"); }
            TimeSpan ts = DateTime.Now - DateTime.Parse("1970-01-01 00:00:00");
            string endUrl = "BTZ_" + ts.Ticks + suffix;
            string newFileName = Server.MapPath("~/UpLoad/product/") + endUrl;
            img.SaveAs(newFileName);
            return "/UpLoad/product/" + endUrl;
        }

        /// <summary>
        /// 上架商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Shelve(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzBLL.SaveShelve(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 下架商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult CancelShelve(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzBLL.SaveCancelShelve(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzBLL.Delete(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult startPp(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                int c = btzBLL.startPp(id);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
    }
}
