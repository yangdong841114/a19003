﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class ManageWebController : Controller
    {
        public IMemberBLL memberBLL { get; set; }
        public IResourceBLL resourceBLL { get; set; }

        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        public IBaseSetBLL setBLL { get; set; }

        public ISystemMsgBLL msgBLL { get; set; }
        public IParamSetBLL PsetBLL { get; set; }

        public IAreaBLL areaBLL { get; set; }
        public IBTZBLL btzBLL { get; set; }
        public IAxdbjlBLL dbBLL { get; set; }

        /// <summary>
        /// 获取菜单信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetMenu(string parentId)
        {
            ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByUserAndParent(mm.id.Value, parentId);
            response.list = list;

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        //获取通知信息
        public JsonResult GetNotic()
        {
            ResponseDtoMap<string, List<SystemMsg>> response = new ResponseDtoMap<string, List<SystemMsg>>();
            try
            {
                Dictionary<string, List<SystemMsg>> di = msgBLL.GetMainData();
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDb()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            BaseSet set = setBLL.GetModel(); // 基础设置
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            Dictionary<string, object> di = new Dictionary<string, object>();
            Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("Axdbxyebili");
            double Axdbxyebili = Convert.ToDouble(param["Axdbxyebili"].paramValue);
            set.dbdjs = (set.axdbJssz.Value - DateTime.Now).TotalSeconds;
            if (set.dbdjs < 0) set.dbdjs = 0;
            set.sxf = set.jclz * (100 - Axdbxyebili) / 100;
            di.Add("baseSet", set);

            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddDbJc(double trLz)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member mm = (Member)Session["LoginUser"]; //当前登陆用户
                BaseSet set = setBLL.GetModel(); // 基础设置
                set.jclz = set.jclz + trLz;
                set.axdbAccount = set.axdbAccount + trLz;
                setBLL.SaveModel(set);
                Axdbjl model = new Axdbjl();
                model.trLz = trLz;
                dbBLL.SaveProductAdmin(model, mm);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetListPageDbx(Axdbjl model)
        {
            Member mb = (Member)Session["MemberUser"];
            //model.uid = mb.id;
            PageResult<Axdbjl> page = dbBLL.GetListPage(model, "m.*");
            return Json(page, JsonRequestBehavior.AllowGet);
        }

        public JsonResult kqdb(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("Axdbxscsj");
                double Axdbxscsj = Convert.ToDouble(param["Axdbxscsj"].paramValue);
                BaseSet set = setBLL.GetModel(); // 基础设置
                if (set.dbkg == 1) { throw new ValidateException("夺宝已开启"); };
                set.dbkg = 1;
                set.axdbJssz = DateTime.Now.AddHours(Axdbxscsj);
                set.axdbAccount = 0;
                if (set.dbxqs == 0)
                    set.dbxqs = 1;
                else
                    set.dbxqs = set.dbxqs + 1;
                set.bqtzrs = 0;
                set.jclz = 0;
                setBLL.SaveModel(set);
                response.Success();
            }
            catch (ValidateException va) { response.msg = va.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员！"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPara()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            Dictionary<string, object> di = new Dictionary<string, object>();
            Dictionary<string, ParameterSet> param = PsetBLL.GetDictionaryByCodes("BTZqysj", "BTZcxsj", "BTZyysj", "BTZyylz", "BTZjqlz", "BTZhyts", "BTZrsyBili", "BTZzcszzBili", "BTZmrjzsx", "BTZgzjzxx", "BTZgzjzsx", "BTZzsjzxx", "BTZzsjzsx", "BTZscpprs", "BTZcffzbs", "BTZcffzfs", "BTZmtxyrs", "BTZzdcsBili", "bounsAmounts", "maxBounsAmounts");
            string BTZqysj = param["BTZqysj"].paramValue;
            string BTZcxsj = param["BTZcxsj"].paramValue;
            string BTZyysj = param["BTZyysj"].paramValue;
            string BTZyylz = param["BTZyylz"].paramValue;
            string BTZjqlz = param["BTZjqlz"].paramValue;
            string BTZhyts = param["BTZhyts"].paramValue;
            string BTZrsyBili = param["BTZrsyBili"].paramValue;
            string BTZzcszzBili = param["BTZzcszzBili"].paramValue;
            string BTZmrjzsx = param["BTZmrjzsx"].paramValue;
            string BTZgzjzxx = param["BTZgzjzxx"].paramValue;
            string BTZgzjzsx = param["BTZgzjzsx"].paramValue;
            string BTZzsjzxx = param["BTZzsjzxx"].paramValue;
            string BTZzsjzsx = param["BTZzsjzsx"].paramValue;
            string BTZscpprs = param["BTZscpprs"].paramValue;
            string BTZcffzbs = param["BTZcffzbs"].paramValue;
            string BTZcffzfs = param["BTZcffzfs"].paramValue;
            string BTZmtxyrs = param["BTZmtxyrs"].paramValue;
            string BTZzdcsBili = param["BTZzdcsBili"].paramValue;
            string bounsAmounts = param["bounsAmounts"].paramValue;
            string maxBounsAmounts = param["maxBounsAmounts"].paramValue;


            di.Add("BTZqysj", BTZqysj);
            di.Add("BTZcxsj", BTZcxsj);
            di.Add("BTZyysj", BTZyysj);
            di.Add("BTZyylz", BTZyylz);

            di.Add("BTZjqlz", BTZjqlz);
            di.Add("BTZhyts", BTZhyts);
            di.Add("BTZrsyBili", BTZrsyBili);
            di.Add("BTZzcszzBili", BTZzcszzBili);
            di.Add("BTZmrjzsx", BTZmrjzsx);
            di.Add("BTZgzjzxx", BTZgzjzxx);
            di.Add("BTZgzjzsx", BTZgzjzsx);
            di.Add("BTZzsjzxx", BTZzsjzxx);
            di.Add("BTZzsjzsx", BTZzsjzsx);
            di.Add("BTZscpprs", BTZscpprs);
            di.Add("BTZcffzbs", BTZcffzbs);
            di.Add("BTZcffzfs", BTZcffzfs);
            di.Add("BTZmtxyrs", BTZmtxyrs);
            di.Add("BTZzdcsBili", BTZzdcsBili);
            di.Add("bounsAmounts", bounsAmounts);
            di.Add("maxBounsAmounts", maxBounsAmounts);

            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取APP前台一级菜单
        /// </summary>
        /// <returns></returns>
        public JsonResult GetWapMenu()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>();
            //ResponseDtoList<Resource> response = new ResponseDtoList<Resource>();
            Member mm = (Member)Session["LoginUser"]; //当前登陆用户
            List<Resource> list = resourceBLL.GetListByPartentId(mm.id.Value, "103");
            Dictionary<string, object> di = new Dictionary<string, object>();
            //如果当前会员不是报单中心，则过滤掉报单中心管理菜单
            //if (mm.isAgent != 2)
            //{
            //    List<Resource> lit = new List<Resource>();
            //    if (list != null && list.Count > 0)
            //    {
            //        foreach (Resource r in list)
            //        {
            //            if (r.id != ConstUtil.WAP_MENU_SHOP && r.parentResourceId != ConstUtil.WAP_MENU_SHOP)
            //            {
            //                lit.Add(r);
            //            }
            //        }
            //    }
            //    di.Add("frist", lit);
            //}
            //else
            //{
            //    di.Add("frist", list);
            //}
            List<Resource> allList = resourceBLL.GetListByUserAndParent(mm.id.Value, "103");
            di.Add("frist", list);
            di.Add("all", allList);
            response.map = di;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取基础数据
        /// </summary>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public JsonResult GetBaseData()
        {
            ResponseDtoMap<string, object> response = new ResponseDtoMap<string, object>("fail");
            try
            {
                Dictionary<string, object> di = new Dictionary<string, object>();
                BaseSet set = setBLL.GetModel(); // 基础设置
                Dictionary<string, List<DataDictionary>> result = dataDictionaryBLL.GetAllToDictionary(); //数据字典
                //List<SystemMsg> mlist = msgBLL.GetList(0); //通知提醒
                List<BTZ> btzlist = btzBLL.GetList(" select * from BTZ");
                if (btzlist == null)
                {
                    btzlist = new List<BTZ>();
                }
                for (int i = 0; i < btzlist.Count; i++)
                    btzlist[i].BTZName = btzlist[i].BTZName + " 级别：" + btzlist[i].jLevel + " 班级：" + btzlist[i].bLevel;
                List<Member> xnmemlist = memberBLL.GetList(" select * from Member where isVirtual=1");
                response.Success();
                Member cur = (Member)Session["LoginUser"];
                //di.Add("notic", mlist);
                di.Add("id", cur.id);
                di.Add("userId", cur.userId);
                di.Add("baseSet", set);
                di.Add("cacheData", result);
                di.Add("btzlist", btzlist);
                di.Add("xnmemlist", xnmemlist);
                di.Add("area", areaBLL.GetTreeModelList()); //省市区数据
                response.map = di;
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "加载基础数据失败"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 验证后台安全密码
        /// </summary>
        /// <param name="pass2"></param>
        /// <returns></returns>
        public JsonResult CheckAPass2(string pass2)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                Member cur = (Member)Session["LoginUser"];
                response.msg = "安全密码错误";
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass2 = DESEncrypt.EncryptDES(pass2, ConstUtil.SALT);
                    if (pass2 == m.passOpen)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 验证后台交易密码
        /// </summary>
        /// <param name="pass3"></param>
        /// <returns></returns>
        public JsonResult CheckAPass3(string pass3)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                response.msg = "交易密码错误";
                Member cur = (Member)Session["LoginUser"];
                Member m = memberBLL.GetModelById(cur.id.Value);
                if (m != null)
                {
                    pass3 = DESEncrypt.EncryptDES(pass3, ConstUtil.SALT);
                    if (pass3 == m.threepass)
                    {
                        response.Success();
                    }
                }
            }
            catch (ValidateException ex) { response.msg = ex.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
