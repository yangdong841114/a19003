﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using Common;
using System.Text;
using System.Reflection;
using BLL;


namespace Web.Admin.Controllers
{
    public class DataDictionaryController : Controller
    {
        public IDataDictionaryBLL dataDictionaryBLL { get; set; }

        /// <summary>
        /// 查询根节点列表
        /// </summary>
        /// <returns></returns>
        public JsonResult GetRootList()
        {
            ResponseDtoList<DataDictionary> response = new ResponseDtoList<DataDictionary>();
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("parentId", ConstUtil.EQ, 0)).Result();
            List<DataDictionary> list = dataDictionaryBLL.GetList("select * from DataDictionary", param);
            response.list = list;
            return Json(response, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 根据父节点ID查询
        /// </summary>
        /// <returns></returns>
        public JsonResult GetListChildren(DataDictionary model)
        {
            //ResponseDtoPage<DataDictionary> response = new ResponseDtoPage<DataDictionary>("fail");
            PageResult<DataDictionary> result = dataDictionaryBLL.GetListPage(model);
            //response.status = "success";
            return Json(result, JsonRequestBehavior.AllowGet); ;
        }

        /// <summary>
        /// 保存或更新记录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult SaveOrUpdate(DataDictionary model)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (model == null) { throw new ValidateException("保存对象为空"); }
                if (ValidateUtils.CheckNull(model.name)) { throw new ValidateException("名称为空"); }

                //id为空时保存，不为空时更新
                if (ValidateUtils.CheckIntZero(model.id))
                {
                    dataDictionaryBLL.Save(model);
                    response.msg = "添加成功";
                }
                else
                {
                    dataDictionaryBLL.Update(model);
                    response.msg = "更新成功";
                }
                response.status = "success";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 删除记录
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public JsonResult Delete(int id)
        {
            ResponseDtoData response = new ResponseDtoData("fail");
            try
            {
                if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("删除的记录为空"); }

                dataDictionaryBLL.Delte("DataDictionary", id);
                response.status = "success";
                response.msg = "删除成功";
            }
            catch (ValidateException e) { response.msg = e.Message; }
            catch (Exception) { response.msg = "操作失败，请联系管理员"; }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
