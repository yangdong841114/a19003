﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Web;
using Newtonsoft.Json;
using System.Configuration;

namespace Web.Models
{
    public class CourierHelp
    {
        private static List<Courier> CourierList = new List<Courier>();
        public static readonly string Appkey = ConfigurationManager.AppSettings["Appkey"];
        public static List<Courier> GetCourierList()
        {
            if (CourierList.Count==0)
            {
                SetCourier();
            }
            return CourierList;
        }
        /// <summary>
        /// 取得快递公司信息
        /// </summary>
        /// <returns></returns>
        public static bool SetCourier()
        {
            string url2 = "http://v.juhe.cn/exp/com";
            var parameters2 = new Dictionary<string, string>();

            parameters2.Add("key", Appkey);//你申请的key
            string result2 = sendPost(url2, parameters2, "get");
            var m = JsonConvert.DeserializeObject<CourierResult<Courier>>(result2);
            if (m.error_code == 0)
            {
                CourierList.AddRange(m.result);
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 取得物流信息
        /// </summary>
        /// <param name="com">快递公司</param>
        /// <param name="no">订单号</param>
        public static List<CourierInfo> GetWuLiuInfo(string com,string no)
        {
            var list = new List<CourierInfo>();
            var c = CourierList.Find(e => e.com == com);
            if (CourierList.Count==0||c == null)
            {
                return list;
            }
            string url1 = "http://v.juhe.cn/exp/index";

            var parameters1 = new Dictionary<string, string>();

            parameters1.Add("com", c.no); //需要查询的快递公司编号
            parameters1.Add("no", no); //需要查询的订单号
            parameters1.Add("key", Appkey);//你申请的key
            parameters1.Add("dtype", "json"); //返回数据的格式,xml或json，默认json
            string result1 = sendPost(url1, parameters1, "get");
            var m = JsonConvert.DeserializeObject<CourierResult<CourierInfo>>(result1);

            if (m.error_code == 0)
            {
                return m.result;
            }
            else
            {
                return list;
            }
        }
        /// <summary>
        /// Http (GET/POST)
        /// </summary>
        /// <param name="url">请求URL</param>
        /// <param name="parameters">请求参数</param>
        /// <param name="method">请求方法</param>
        /// <returns>响应内容</returns>
        public static string sendPost(string url, IDictionary<string, string> parameters, string method)
        {
            if (method.ToLower() == "post")
            {
                HttpWebRequest req = null;
                HttpWebResponse rsp = null;
                System.IO.Stream reqStream = null;
                try
                {
                    req = (HttpWebRequest)WebRequest.Create(url);
                    req.Method = method;
                    req.KeepAlive = false;
                    req.ProtocolVersion = HttpVersion.Version10;
                    req.Timeout = 5000;
                    req.ContentType = "application/x-www-form-urlencoded;charset=utf-8";
                    byte[] postData = Encoding.UTF8.GetBytes(BuildQuery(parameters, "utf8"));
                    reqStream = req.GetRequestStream();
                    reqStream.Write(postData, 0, postData.Length);
                    rsp = (HttpWebResponse)req.GetResponse();
                    Encoding encoding = Encoding.GetEncoding(rsp.CharacterSet);
                    return GetResponseAsString(rsp, encoding);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    if (reqStream != null) reqStream.Close();
                    if (rsp != null) rsp.Close();
                }
            }
            else
            {
                //创建请求
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url + "?" + BuildQuery(parameters, "utf8"));

                //GET请求
                request.Method = "GET";
                request.ReadWriteTimeout = 5000;
                request.ContentType = "text/html;charset=UTF-8";
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));

                //返回内容
                string retString = myStreamReader.ReadToEnd();
                return retString;
            }
        }

        /// <summary>
        /// 组装普通文本请求参数。
        /// </summary>
        /// <param name="parameters">Key-Value形式请求参数字典</param>
        /// <returns>URL编码后的请求数据</returns>
        public static string BuildQuery(IDictionary<string, string> parameters, string encode)
        {
            StringBuilder postData = new StringBuilder();
            bool hasParam = false;
            IEnumerator<KeyValuePair<string, string>> dem = parameters.GetEnumerator();
            while (dem.MoveNext())
            {
                string name = dem.Current.Key;
                string value = dem.Current.Value;
                // 忽略参数名或参数值为空的参数
                if (!string.IsNullOrEmpty(name))//&& !string.IsNullOrEmpty(value)
                {
                    if (hasParam)
                    {
                        postData.Append("&");
                    }
                    postData.Append(name);
                    postData.Append("=");
                    if (encode == "gb2312")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.GetEncoding("gb2312")));
                    }
                    else if (encode == "utf8")
                    {
                        postData.Append(HttpUtility.UrlEncode(value, Encoding.UTF8));
                    }
                    else
                    {
                        postData.Append(value);
                    }
                    hasParam = true;
                }
            }
            return postData.ToString();
        }

        /// <summary>
        /// 把响应流转换为文本。
        /// </summary>
        /// <param name="rsp">响应流对象</param>
        /// <param name="encoding">编码方式</param>
        /// <returns>响应文本</returns>
        public static string GetResponseAsString(HttpWebResponse rsp, Encoding encoding)
        {
            System.IO.Stream stream = null;
            StreamReader reader = null;
            try
            {
                // 以字符流的方式读取HTTP响应
                stream = rsp.GetResponseStream();
                reader = new StreamReader(stream, encoding);
                return reader.ReadToEnd();
            }
            finally
            {
                // 释放资源
                if (reader != null) reader.Close();
                if (stream != null) stream.Close();
                if (rsp != null) rsp.Close();
            }
        }
    }
}