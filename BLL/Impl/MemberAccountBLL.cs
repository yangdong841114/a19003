﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class MemberAccountBLL : BaseBLL<MemberAccount>, IMemberAccountBLL
    {

        private System.Type type = typeof(MemberAccount);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liuShuiZhangBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new MemberAccount GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            MemberAccount mb = (MemberAccount)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new MemberAccount GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            MemberAccount mb = (MemberAccount)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<MemberAccount> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<MemberAccount> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MemberAccount>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<MemberAccount> GetList(string sql)
        {
            List<MemberAccount> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<MemberAccount>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public int UpdateAdd(MemberAccount account)
        {
            if (account != null)
            {
                if (ValidateUtils.CheckIntZero(account.id))
                {
                    throw new ValidateException("修改账户的会员不能为空");
                }
                string sql = "update MemberAccount set ";
                bool exists = false;
                if (!ValidateUtils.CheckDoubleZero(account.agentLz))
                {
                    sql += " agentLz=agentLz+" + account.agentLz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBTT))
                {
                    sql += " agentBTT=agentBTT+" + account.agentBTT + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBTD))
                {
                    sql += " agentBTD=agentBTD+" + account.agentBTD + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTjsy))
                {
                    sql += " agentTjsy=agentTjsy+" + account.agentTjsy + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentFt))
                {
                    sql += " agentFt=agentFt+" + account.agentFt + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTdj))
                {
                    sql += " agentTdj=agentTdj+" + account.agentTdj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentCsj))
                {
                    sql += " agentCsj=agentCsj+" + account.agentCsj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBtz))
                {
                    sql += " agentBtz=agentBtz+" + account.agentBtz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentLysy))
                {
                    sql += " agentLysy=agentLysy+" + account.agentLysy + ",";
                    exists = true;
                }

                if (exists)
                {
                    sql = sql.Substring(0, sql.Length - 1);
                    sql += " where id=" + account.id;
                    return dao.ExecuteBySql(sql);
                }
            }
            return 0;
        }

        public int UpdateSub(MemberAccount account)
        {
            if (account != null)
            {
                if (ValidateUtils.CheckIntZero(account.id))
                {
                    throw new ValidateException("修改账户的会员不能为空");
                }
                string sql = "update MemberAccount set ";
                bool exists = false;
                if (!ValidateUtils.CheckDoubleZero(account.agentLz))
                {
                    sql += " agentLz=agentLz-" + account.agentLz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBTT))
                {
                    sql += " agentBTT=agentBTT-" + account.agentBTT + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBTD))
                {
                    sql += " agentBTD=agentBTD-" + account.agentBTD + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTjsy))
                {
                    sql += " agentTjsy=agentTjsy-" + account.agentTjsy + ",";
                    exists = true;
                }

                if (!ValidateUtils.CheckDoubleZero(account.agentFt))
                {
                    sql += " agentFt=agentFt-" + account.agentFt + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentTdj))
                {
                    sql += " agentTdj=agentTdj-" + account.agentTdj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentCsj))
                {
                    sql += " agentCsj=agentCsj-" + account.agentCsj + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentBtz))
                {
                    sql += " agentBtz=agentBtz-" + account.agentBtz + ",";
                    exists = true;
                }
                if (!ValidateUtils.CheckDoubleZero(account.agentLysy))
                {
                    sql += " agentLysy=agentLysy-" + account.agentLysy + ",";
                    exists = true;
                }

                if (exists)
                {
                    sql = sql.Substring(0, sql.Length - 1);
                    sql += " where id=" + account.id;
                    return dao.ExecuteBySql(sql);
                }
            }
            return 0;
        }

        public PageResult<MemberAccount> GetListPage(MemberAccount model)
        {
            PageResult<MemberAccount> page = new PageResult<MemberAccount>();
            string sql = "select m.userId,m.userName,a.*,row_number() over(order by a.id desc) rownumber from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            string countSql = "select count(1) from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<MemberAccount> list = new List<MemberAccount>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((MemberAccount)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public MemberAccount GetTotalSummary(MemberAccount model)
        {
            string sql = "select '' userId,'汇总合计：' userName, sum(agentDz)agentDz,sum(agentJj)agentJj,sum(agentGw)agentGw,sum(agentFt)agentFt,sum(agentLz)agentLz,sum(agentBTT)agentBTT,sum(agentBTD)agentBTD,sum(agentTjsy)agentTjsy,sum(agentTdj)agentTdj,sum(agentCsj)agentCsj,sum(agentLysy)agentLysy  from MemberAccount " +
                        " a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
            }
            MemberAccount result = this.GetOne(sql, param);
            return result;
        }

        public MemberAccount GetModel(int id)
        {
            string sql = "select * from MemberAccount ";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, id)).Result();
            return this.GetOne(sql, param);
        }

        public void UpdateLockBouns(MemberAccount model, double lockBouns)
        {
            string sql = "select * from Member ";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("id", ConstUtil.EQ, model.id.Value)).Result();
            var m = this.GetOne(sql, param);
            LiuShuiZhang liuShuiZhang = new LiuShuiZhang();
            liuShuiZhang.uid = m.id;
            liuShuiZhang.userId = m.userId;
            liuShuiZhang.accountId = ConstUtil.JOURNAL_SCSY;
            liuShuiZhang.last = lockBouns;
            liuShuiZhang.addtime = DateTime.Now;
            liuShuiZhang.tableName = "MemberPassed";
            liuShuiZhang.addUid = 1;
            liuShuiZhang.addUser = "system";
            //如果账户余额是大于更新金额--减少
            if (model.agentLockBouns > lockBouns)
            {
                liuShuiZhang.income = 0;
                liuShuiZhang.outlay = model.agentLockBouns - lockBouns;
                liuShuiZhang.abst = "锁仓账户余额扣除";
            }
            else
            {
                liuShuiZhang.income = lockBouns - model.agentLockBouns;
                liuShuiZhang.outlay = 0;
                liuShuiZhang.abst = "锁仓账户余额增加";
            }
            model.agentLockBouns = lockBouns;
            dao.Update(model);
            liuShuiZhangBLL.Save(liuShuiZhang);
        }

        public DataTable GetLiuShuiExcel()
        {
            string sql = "  select m.userId,m.userName,a.agentDz,a.agentJj,a.agentGw,a.agentFt,a.agentLz,a.agentBTT,a.agentBTD,a.agentTjsy,a.agentTdj,a.agentCsj,a.agentLysy from MemberAccount a inner join Member m on a.id = m.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }
    }
}
