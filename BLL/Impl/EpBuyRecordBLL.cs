﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;
using System.Data.SqlClient;

namespace BLL.Impl
{
    public class EpBuyRecordBLL : BaseBLL<EpBuyRecord>, IEpBuyRecordBLL
    {

        private System.Type type = typeof(EpBuyRecord);

        private System.Type statusType = typeof(EpBuyStatus);

        public IBaseDao dao { get; set; }

        public IMemberAccountBLL accBLL { get; set; }

        public IMemberBLL mbBLL { get; set; }


        public override IBaseDao GetDao()
        {
            return dao;
        }

        public PageResult<EpBuyRecord> GetListPage(EpBuyRecord model)
        {
            PageResult<EpBuyRecord> page = new PageResult<EpBuyRecord>();
            string sql = "select m.*,s.phone,s.qq,s.number snumber,s.uid suid,s.userId suserId,s.addTime saddTime,b.bankName,b.bankCard,b.bankUser,b.bankAddress," +
                "row_number() over(order by m.id desc) rownumber from EpBuyRecord m inner join EpSaleRecord s on m.sid = s.id inner join Member b on s.uid=b.id where m.flag<3 ";
            string countSql = "select count(1) from EpBuyRecord m inner join EpSaleRecord s on m.sid = s.id inner join Member b on s.uid=b.id where m.flag<3 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.flag != null && model.flag >= 0)
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.sid != null)
                {
                    param.Add(new DbParameterItem("m.sid", ConstUtil.EQ, model.sid));
                }
                if (model.suid != null)
                {
                    param.Add(new DbParameterItem("s.uid", ConstUtil.EQ, model.suid));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("m.typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.batchNumber != null)
                {
                    param.Add(new DbParameterItem("m.batchNumber", ConstUtil.LIKE, model.batchNumber));
                }
                if (model.opUserId != null)
                {
                    param.Add(new DbParameterItem("m.opUserId", ConstUtil.LIKE, model.opUserId));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.snumber != null)
                {
                    param.Add(new DbParameterItem("s.number", ConstUtil.LIKE, model.snumber));
                }
                if (model.suserId != null)
                {
                    param.Add(new DbParameterItem("s.userId", ConstUtil.LIKE, model.suserId));
                }
                if (model.number != null)
                {
                    param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<EpBuyRecord> list = new List<EpBuyRecord>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((EpBuyRecord)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public DataTable GetExportList(EpBuyRecord model)
        {
            string sql = "select m.*,case when m.flag=0 then '待买家付款' when m.flag=1 then '待卖家收款' when m.flag=2 then '已完成' else '已取消' end status,batchNumber,opUserId, " +
                "s.phone,s.qq,s.number snumber,s.uid suid,s.userId suserId,s.addTime saddTime,b.bankName,b.bankCard,b.bankUser,b.bankAddress,case when typeId=2 then '批量购买' else '普通购买' end buytype" +
                " from EpBuyRecord m inner join EpSaleRecord s on m.sid = s.id inner join Member b on s.uid=b.id where m.flag<3 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.flag != null && model.flag >= 0)
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.sid != null)
                {
                    param.Add(new DbParameterItem("m.sid", ConstUtil.EQ, model.sid));
                }
                if (model.suid != null)
                {
                    param.Add(new DbParameterItem("s.uid", ConstUtil.EQ, model.suid));
                }
                if (!ValidateUtils.CheckIntZero(model.typeId))
                {
                    param.Add(new DbParameterItem("m.typeId", ConstUtil.EQ, model.typeId));
                }
                if (model.batchNumber != null)
                {
                    param.Add(new DbParameterItem("m.batchNumber", ConstUtil.LIKE, model.batchNumber));
                }
                if (model.opUserId != null)
                {
                    param.Add(new DbParameterItem("m.opUserId", ConstUtil.LIKE, model.opUserId));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.snumber != null)
                {
                    param.Add(new DbParameterItem("s.number", ConstUtil.LIKE, model.snumber));
                }
                if (model.suserId != null)
                {
                    param.Add(new DbParameterItem("s.userId", ConstUtil.LIKE, model.suserId));
                }
                if (model.number != null)
                {
                    param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            DataTable dt = dao.GetList(sql, param, true);
            return dt;
        }


        public PageResult<EpBuyStatus> GetSatusListPage(EpBuyStatus model)
        {
            PageResult<EpBuyStatus> page = new PageResult<EpBuyStatus>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from EpBuyStatus m  where 1=1 ";
            string countSql = "select count(1) from EpBuyStatus m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.bid != null)
                {
                    param.Add(new DbParameterItem("m.bid", ConstUtil.EQ, model.bid));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (model.number != null)
                {
                    param.Add(new DbParameterItem("m.number", ConstUtil.LIKE, model.number));
                }
                if (model.userId != null)
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<EpBuyStatus> list = new List<EpBuyStatus>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((EpBuyStatus)ReflectionUtil.GetModel(statusType, row));
                }
            }
            page.rows = list;
            return page;
        }


        public int SaveRecord(EpBuyRecord model, Member current)
        {
            if (model == null) { throw new ValidateException("操作对象为空"); }
            if (ValidateUtils.CheckDoubleZero(model.buyNum)) { throw new ValidateException("请录入购买数量"); }
            if (model.sid == null) { throw new ValidateException("购买的挂卖记录不能为空"); }
            if (model.uid == null) { throw new ValidateException("购买人不能为空"); }

            //调用存储过程
            string pro = "saveEpBuy";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@sid",SqlDbType.Int),
                  new SqlParameter("@buyNum",SqlDbType.Money),
                  new SqlParameter("@bnumber",SqlDbType.VarChar),
                  new SqlParameter("@uid",SqlDbType.Int),
                  new SqlParameter("@opId",SqlDbType.Int),
                  new SqlParameter("@opUserId",SqlDbType.VarChar),
                  new SqlParameter("@flag",SqlDbType.Int),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = model.sid;
            p[1].Value = model.buyNum;
            p[2].Value = OrderNumberUtils.GetRandomNumber("EB");
            p[3].Value = model.uid;
            p[4].Value = current.id;
            p[5].Value = current.userId;
            p[6].Value = 0;
            p[7].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public int SaveRecordList(List<EpBuyRecord> list, Member current)
        {
            if (list == null || list.Count == 0) { throw new ValidateException("保存内容为空"); }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                EpBuyRecord dto = list[i];
                sb.Append(dto.sid).Append(",");
            }
            //查询挂卖记录
            string ids = sb.ToString().Substring(0, sb.Length - 1);
            string sql = "select * from EpSaleRecord where id in (" + ids + ")";
            DataTable dt = dao.GetList(sql);
            Dictionary<int, DataRow> di = new Dictionary<int, DataRow>();
            if (dt != null || dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    di.Add(Convert.ToInt32(row["id"]), row);
                }
            }

            //DateTime now = DateTime.Now;
            //string batchNumber = OrderNumberUtils.GetRandomNumber("EBT"); //批量购买单号

            string sidStr = "";
            string numStr = "";
            //核对挂卖记录
            for (int i = 0; i < list.Count; i++)
            {
                EpBuyRecord dto = list[i];
                if (!di.ContainsKey(dto.sid.Value)) { throw new ValidateException("挂卖单号【" + dto.snumber + "】在系统中不存在"); }
                DataRow row = di[dto.sid.Value];
                int flag = Convert.ToInt32(row["flag"]);
                double waitNum = Convert.ToDouble(row["waitNum"]);
                if (flag != 0 && flag != 1) { throw new ValidateException("挂卖单号【" + dto.snumber + "】不是挂卖中、部分出售"); }
                if (dto.buyNum > waitNum) { throw new ValidateException("挂卖单号【" + dto.snumber + "】购买数量【" + dto.buyNum + "】大于待售数量【" + waitNum + "】"); }

                sidStr += dto.sid + ",";
                numStr += dto.buyNum + ",";

            }
            sidStr = sidStr.Substring(0, sidStr.Length - 1);
            numStr = numStr.Substring(0, numStr.Length - 1);
            //调用存储过程
            string pro = "saveBatchEpBuy";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@sidStr",SqlDbType.VarChar),
                  new SqlParameter("@numStr",SqlDbType.VarChar),
                  new SqlParameter("@uid",SqlDbType.Int),
                  new SqlParameter("@opId",SqlDbType.Int),
                  new SqlParameter("@opUserId",SqlDbType.VarChar),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = sidStr;
            p[1].Value = numStr;
            p[2].Value = current.id;
            p[3].Value = current.id;
            p[4].Value = current.userId;
            p[5].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public int SaveCancel(int id, Member current)
        {
            //调用存储过程
            string pro = "cancelEpBuy";
            IDataParameter[] p = new IDataParameter[]{
                  new SqlParameter("@id",SqlDbType.Int,8),
                  new SqlParameter("@opId",SqlDbType.Int,8),
                  new SqlParameter("@opUserId",SqlDbType.VarChar,50),
                  new SqlParameter("@flag",SqlDbType.Int,4),
                  new SqlParameter("@return",SqlDbType.VarChar,200)
            };
            p[0].Value = id;
            p[1].Value = current.id;
            p[2].Value = current.userId;
            p[3].Value = 0;
            p[4].Direction = ParameterDirection.Output;
            string result = dao.RunProceOnlyPara(pro, p, "return");
            if (result == "success")
            {
                return 1;
            }
            else
            {
                throw new ValidateException(result);
            }
        }

        public int SaveBatchPay(List<EpBuyRecord> list, Member current, bool isAdmin)
        {
            if (list == null || list.Count == 0) { throw new ValidateException("操作记录不存在"); }
            for (int i = 0; i < list.Count; i++)
            {
                EpBuyRecord dto = list[i];
                SavePay(dto, current, isAdmin);
            }
            return 1;
        }

        public int SavePay(EpBuyRecord model, Member current, bool isAdmin)
        {
            if (model == null || model.id == null) { throw new ValidateException("操作的记录不存在"); }
            //if (model.imgUrl == null && isAdmin) { throw new ValidateException("请上传汇款凭证"); }
            DataRow row = dao.GetOne("select * from EpBuyRecord where id=" + model.id);
            EpBuyRecord dto = (EpBuyRecord)ReflectionUtil.GetModel(type, row);
            if (dto == null) { throw new ValidateException("购买记录不存在"); }
            if (dto.flag != 0) { throw new ValidateException("购买记录【" + dto.number + "】状态不是【待买家付款】"); }
            //dto.imgUrl = model.imgUrl;
            dto.flag = 1;
            dao.Update(dto);

            //记录购买状态记录
            EpBuyStatus buystatus = new EpBuyStatus();
            buystatus.sid = dto.sid;
            buystatus.snumber = dto.snumber;
            buystatus.number = dto.number;
            buystatus.bid = dto.id;
            buystatus.flag = 1;
            buystatus.opStatus = "买家确认付款";
            buystatus.uid = current.id;
            buystatus.userId = current.userId;
            buystatus.addTime = DateTime.Now;
            dao.Save(buystatus);

            //后台默认已完成
            if (isAdmin)
            {
                SaveSurePay(model, current);
            }

            return 1;
        }

        public int SaveSurePay(EpBuyRecord model, Member current)
        {
            if (model == null || model.id == null) { throw new ValidateException("操作的记录不存在"); }
            DataRow row = dao.GetOne("select * from EpBuyRecord where id=" + model.id);
            EpBuyRecord dto = (EpBuyRecord)ReflectionUtil.GetModel(type, row);
            if (dto == null) { throw new ValidateException("购买记录不存在"); }
            dto.flag = 2;
            dao.Update(dto);

            DateTime now = DateTime.Now;

            //记录购买状态记录
            EpBuyStatus buystatus = new EpBuyStatus();
            buystatus.sid = dto.sid;
            buystatus.number = dto.number;
            buystatus.bid = dto.id;
            buystatus.flag = 2;
            buystatus.opStatus = "卖家确认收款";
            buystatus.uid = current.id;
            buystatus.userId = current.userId;
            buystatus.addTime = DateTime.Now;
            dao.Save(buystatus);

            //增加买家的申购分
            MemberAccount acc = accBLL.GetModel(dto.uid.Value);
            dao.ExecuteBySql("update MemberAccount set AgentGw = agentGw+" + dto.buyNum + " where id=" + dto.uid);

            //记录流水
            LiuShuiZhang liu = new LiuShuiZhang();
            liu.uid = dto.uid;
            liu.userId = dto.userId;
            liu.accountId = ConstUtil.JOURNAL_BTD;
            liu.abst = "EP挂卖，购买记录单号【" + dto.number + "】订单完成，收入" + dto.buyNum;
            liu.addtime = now;
            liu.sourceId = dto.id;
            liu.income = dto.buyNum;
            liu.outlay = 0;
            liu.last = acc.agentGw + liu.income;
            liu.tableName = "EpBuyRecord";
            liu.addUid = current.id;
            liu.addUser = current.userId;
            dao.Save(liu);

            //判断是否可以完成挂卖记录
            DataRow row2 = dao.GetOne("select * from EpSaleRecord where id=" + dto.sid);
            if (row2 == null) { throw new ValidateException("挂卖记录不存在"); }
            EpSaleRecord sale = (EpSaleRecord)ReflectionUtil.GetModel(typeof(EpSaleRecord), row2);
            int ct = dao.GetCount("select count(1) from EpBuyRecord where sid=" + dto.sid + " and flag not in (2,3)");

            //挂卖记录状态＝全部售出且所有购买记录是已完成或已取消
            if (sale.waitNum == 0 && sale.flag == 2 && ct == 0)
            {
                dao.ExecuteBySql("update EpSaleRecord set flag=3 where id=" + dto.sid);

                //记录挂卖记录状态
                EpSaleStatus status = new EpSaleStatus();
                status.sid = sale.id;
                status.number = sale.number;
                status.flag = 3;
                status.opStatus = "挂卖记录全部完成";
                status.uid = current.id;
                status.userId = current.userId;
                status.addTime = now;
                dao.Save(status);
            }

            return 1;

        }

    }
}