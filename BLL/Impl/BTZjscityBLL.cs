﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BTZjscityBLL : BaseBLL<BTZjscity>, IBTZjscityBLL
    {

        private System.Type type = typeof(BTZjscity);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new BTZjscity GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            BTZjscity mb = (BTZjscity)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new BTZjscity GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BTZjscity mb = (BTZjscity)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<BTZjscity> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<BTZjscity> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZjscity>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZjscity)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }


        public new List<BTZjscity> GetList(string sql)
        {
            List<BTZjscity> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZjscity>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZjscity model = (BTZjscity)ReflectionUtil.GetModel(type, row);
                   
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<BTZjscity> GetListPage(BTZjscity model, string fields)
        {
            PageResult<BTZjscity> page = new PageResult<BTZjscity>();
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from BTZjscity m where 1=1 ";
            string countSql = "select count(1) from BTZjscity m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (!ValidateUtils.CheckNull(model.city))
                {
                    param.Add(new DbParameterItem("m.city", ConstUtil.LIKE, model.city));
                }
               
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZjscity> list = new List<BTZjscity>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZjscity modelrow = (BTZjscity)ReflectionUtil.GetModel(type, row);
                    list.Add(modelrow);
                }
            }
            page.rows = list;
            return page;
        }

        public BTZjscity Save(BTZjscity mb, Member current)
        {
            
            //保存
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

           
            return mb;
        }

       

        public int Delete(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到删除的记录"); }
            string sql = "delete from BTZjscity where id=" + id;
            return dao.ExecuteBySql(sql);
        }

       
    }
}
