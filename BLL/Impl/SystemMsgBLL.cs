﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class SystemMsgBLL : ISystemMsgBLL
    {

        private System.Type type = typeof(SystemMsg);
        public IBaseDao dao { get; set; }

        public List<SystemMsg> GetList(int toUid)
        {
            string sql = "select toUid,url,msg from SystemMsg where toUid= " + toUid + " group by toUid,url,msg";

            DataTable dt = dao.GetList(sql);
            List<SystemMsg> list = new List<SystemMsg>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((SystemMsg)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public Dictionary<string, List<SystemMsg>> GetMainData()
        {
            string sql = "select top 3  m.userId,m.userName,m.addTime,m.regMoney as money from SystemMsg s inner join Member m on s.recordId = m.id where s.recordTable='Member'";
            Dictionary<string, List<SystemMsg>> di = new Dictionary<string, List<SystemMsg>>();
            di.Add("Member", GetList(sql));
            sql = "select top 3 t.userId,t.addtime addTime,t.epoints as money from SystemMsg s inner join TakeCash t on s.recordId = t.id where s.recordTable='TakeCash'";
            di.Add("TakeCash", GetList(sql));
            sql = "select top 3  m.userId,m.userName,m.applyAgentTime addTime,m.regAgentmoney as money from SystemMsg s inner join Member m on s.recordId = m.id where s.recordTable='Shop'";
            di.Add("Shop", GetList(sql));
            sql = "select top 3 t.userId,t.addTime,t.epoints as money from SystemMsg s inner join Charge t on s.recordId = t.id where s.recordTable='Charge'";
            di.Add("Charge", GetList(sql));
            sql = "select  t.userId,t.addTime from BTZBuyss t where t.flag=1 ";
            di.Add("BTZBuyss", GetList(sql));
            sql = "select top 3 t.userId,t.addTime from Member t where t.isSksh=0 and id>4 and (skIsbank=1 or skIszfb=1 or skIswx=1 or skIsszhb=1)";
            di.Add("Skxx", GetList(sql));
            sql = "select top 3 t.userId,t.addTime from BTZBuy t where flag=3 and  t.uid in (select id from Member where isVirtual=1)";
            di.Add("DfkHg", GetList(sql));
            sql = "select top 3 t.userId,t.addTime from BTZBuy t where flag=5 and  t.saleUid in (select id from Member where isVirtual=1)";
            di.Add("Dqrsk", GetList(sql));
            sql = "select COUNT(1) from SystemMsg s where s.url='#EmailBox' and toUid=0 and isRead=0";
            List<SystemMsg> lit = new List<SystemMsg>();
            SystemMsg sm = new SystemMsg();
            sm.count = dao.GetCount(sql);
            lit.Add(sm);
            di.Add("Email", lit);
            return di;
        }

        private List<SystemMsg> GetList(string sql)
        {
            DataTable dt = dao.GetList(sql);
            List<SystemMsg> list = new List<SystemMsg>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((SystemMsg)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public int Save(SystemMsg modle)
        {
            return dao.Save(modle);
        }

        public int Delete(string url, int toUid)
        {
            string sql = "delete from SystemMsg where toUid="+toUid+" and url=@url";
            List<DbParameterItem> param = ParamUtil.Get().Add(new DbParameterItem("url",null,url)).Result();
           return dao.ExecuteBySql(sql, param);
        }

    }
}
