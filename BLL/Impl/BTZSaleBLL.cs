﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BTZSaleBLL : BaseBLL<BTZSale>, IBTZSaleBLL
    {

        private System.Type type = typeof(BTZSale);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new BTZSale GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            BTZSale mb = (BTZSale)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new BTZSale GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BTZSale mb = (BTZSale)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<BTZSale> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<BTZSale> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZSale>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZSale)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<BTZSale> GetList(string sql)
        {
            List<BTZSale> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZSale>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZSale model = (BTZSale)ReflectionUtil.GetModel(type, row);
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<BTZSale> GetListPage(BTZSale model, string fields)
        {
            PageResult<BTZSale> page = new PageResult<BTZSale>();
            string sql = "select " + fields + @",row_number() over(order by m.id desc) rownumber from BTZSale m 
                         inner join Member mem
                         on m.uid=mem.id
                         left join BTZBuy bby on m.id=bby.saleId and bby.flag!=7 where 1=1 ";
            string countSql = @"select count(1) from BTZSale m   inner join Member mem
                         on m.uid=mem.id left join BTZBuy bby on m.id=bby.saleId where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    if(model.flag!=-23&&model.flag!=-12)
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                    else if(model.flag==-23)
                    {
                        
                        sql += " and m.flag in (2,3) ";
                        countSql += " and m.flag in (2,3) ";
                    }
                    else if(model.flag==-12)
                    {

                        sql += " and m.flag in (2,1) ";
                        countSql += " and m.flag in (2,1) ";
                    }

                }
                if (!ValidateUtils.CheckIntZero(model.BTZid))
                {
                    param.Add(new DbParameterItem("m.BTZid", ConstUtil.EQ, model.BTZid));
                }
                if (model.isVirtual!=null)
                {
                    param.Add(new DbParameterItem("mem.isVirtual", ConstUtil.EQ, model.isVirtual));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if(model.issyzBTZ!=null)
                {
                    if(model.issyzBTZ==1)
                    {
                        sql += " and m.accountId>0 ";
                        countSql += " and m.accountId>0 ";
                    }
                }
                if (!ValidateUtils.CheckIntZero(model.accountId))
                {
                    param.Add(new DbParameterItem("m.accountId", ConstUtil.EQ, model.accountId));
                }
                if(model.BuyflagNotIn!=null&&model.BuyflagNotIn!="")
                {
                    sql += " and bby.flag not in (" + model.BuyflagNotIn + ") ";
                    countSql += " and bby.flag not in (" + model.BuyflagNotIn + ") ";
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZSale> list = new List<BTZSale>();
            Dictionary<string, ParameterSet> param_system = paramBLL.GetDictionaryByCodes("BTZcffksj");//买家会员成功抢养后，须在1小时内给卖家进行
            double BTZcffksj = Convert.ToDouble(param_system["BTZcffksj"].paramValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZSale modelrow = (BTZSale)ReflectionUtil.GetModel(type, row);
                    if (modelrow.SaleNo == null || modelrow.SaleNo == "")
                    {
                        string Generate_No = modelrow.addTime.Value.ToString("yyyyMMddHHmm") + GetRandom() + (1000 + modelrow.id).ToString().Substring(0, 4);
                        //生成编号
                        dao.ExecuteBySql("update BTZSale set SaleNo='" + Generate_No + "' where id=" + modelrow.id.Value);
                        modelrow.SaleNo = Generate_No;
                    }
                    if (modelrow.payTime!=null)
                    {
                        //算出剩余付款时间
                        TimeSpan ts = (modelrow.payTime.Value.AddHours(BTZcffksj) - DateTime.Now);
                        modelrow.sysksj = ts.TotalSeconds.ToString();
                    }
                    if (modelrow.Ppsj != null && (modelrow.flag == 3 || modelrow.flag == 2))
                    {
                        //算出剩余付款时间
                        TimeSpan ts = (modelrow.Ppsj.Value.AddHours(BTZcffksj) - DateTime.Now);
                        modelrow.syfksj = ts.TotalSeconds.ToString();
                        //人家付款了，转让中的付款剩余时间就显示 暂无
                        if (modelrow.payTime != null) modelrow.syfksj = "0";
                    }
                    //买家姓名用*遮挡部分
                    if(modelrow.buyuserName!=null&&modelrow.buyuserName!="")
                    {
                        modelrow.buyuserNameHide = "**" + modelrow.buyuserName.Substring(modelrow.buyuserName.Length-1);
                    }
                   
                    list.Add(modelrow);
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double totalZjz = 0;
            int totalJlzs = 0;
            int totalCsrs = 0;
            string sale_uid_in = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZSale model_row = (BTZSale)ReflectionUtil.GetModel(type, row);
                    totalJlzs++;
                    totalZjz += model_row.Cszz.Value;
                    if (sale_uid_in.IndexOf("," + model_row.uid.ToString()) == -1)
                    {
                        totalCsrs++;
                        sale_uid_in += "," + model_row.uid.ToString();
                    }
                  
                }
            }
            BTZSale foot_model = new BTZSale();
            list = new List<BTZSale>();
            foot_model.totalJlzs = totalJlzs;
            foot_model.totalZjz = totalZjz;
            foot_model.totalCsrs = totalCsrs;
            list.Add(foot_model);
            page.footer = list;


            return page;
        }


        private string GetRandom()
        {
            Random ro = new Random(10);
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
            int iResult = ran.Next(999999);
            string s = iResult.ToString().PadLeft(4, '0');
            return s;
        }

        public BTZSale GetModel(int id)
        {
            return this.GetOne("select * from BTZSale where id = " + id);
        }

        public BTZSale SaveProduct(BTZSale mb, Member current)
        {

            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
           
         

            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }

        public int addBTZSaleVirtual(BTZSale model, Member current)
        {
            //非空校验
            if (model == null) { throw new ValidateException("保存内容为空"); }
            if (model.userId == null) { throw new ValidateException("先选择会员"); }
            if (model.BTZid == null) { throw new ValidateException("先选择络绎阁"); }
            if (model.Cszz == null) { throw new ValidateException("先填写出售价值"); }
            Member memSale = memberBLL.GetModelByUserId(model.userId);
            if (memSale == null) { throw new ValidateException("此会员不存在"); }
            BTZ btz = (BTZ)ReflectionUtil.GetModel(typeof(BTZ), dao.GetOne("select * from  BTZ where id=" + model.BTZid));
            if (btz == null) { throw new ValidateException("此络绎阁不存在"); }
            if (model.Cszz > btz.Gzzzsx || model.Cszz < btz.Gzzzxx) { throw new ValidateException("出售价值必须在上下限之间"); }
            model.addTime = DateTime.Now;
            model.uid = memSale.id;
            model.userName = memSale.userName;
            model.BTZCode = btz.BTZCode;
            model.BTZName = btz.BTZName;
            model.flag = 1;
            model.fromBuyid = 0;
            model.ppBuyid = 0;
            model.Cssj = DateTime.Now;
            model.Czr ="手动挂卖-"+current.userId;
            model.Ppcs = 0;
            model.Ppzz = 0;
            model.salephone = memSale.phone;
            model.Cszz = (int)model.Cszz;
            //保存商品
            model.id = null; 
            object o = dao.SaveByIdentity(model);
            int newId = Convert.ToInt32(o);
            model.id = newId;

            return 1;
        }
        public int Delete(int id)
        {
            string sql = "delete from BTZSale where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int delBTZSale(int id)
        {
           BTZSale btzs=this.GetOne("select * from BTZSale where id = " + id);
           if (btzs.ppBuyid.Value > 0 || btzs.flag.Value==2) { throw new ValidateException("已转让卖单不能删除"); }
           if (memberBLL.GetModelById(btzs.uid.Value).isVirtual != 1) { throw new ValidateException("非虚拟会员卖单不能删除"); }
           return Delete(id);
        }
        public int syzBTZ(int accountId,int BTZid, Member current)
        {
         
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("ZhBTZsxfBili", "ZhBTZyemin", "BTZzs", "ZhBTZHourFrom", "ZhBTZHourTo");
            double ZhBTZsxfBili = Convert.ToDouble(param["ZhBTZsxfBili"].paramValue);
            double ZhBTZyemin = Convert.ToDouble(param["ZhBTZyemin"].paramValue);
            double BTZzs = Convert.ToDouble(param["BTZzs"].paramValue);
            int ZhBTZHourFrom = Convert.ToInt16(param["ZhBTZHourFrom"].paramValue);
            int ZhBTZHourTo = Convert.ToInt16(param["ZhBTZHourTo"].paramValue);
            if (DateTime.Now.Hour < ZhBTZHourFrom) throw new ValidateException("收益转络绎阁时间为：每天" + ZhBTZHourFrom + "~" + ZhBTZHourTo + "点完成现在时间点为" + DateTime.Now.Hour);
            if (DateTime.Now.Hour > ZhBTZHourTo) throw new ValidateException("收益转络绎阁时间为：每天" + ZhBTZHourFrom + "~" + ZhBTZHourTo + "点完成现在时间点为" + DateTime.Now.Hour);
            //推荐收益、团队奖、城市奖账户余额须>=500，才能转化为络绎阁
            double agentAccount_min = 0;//余额
            MemberAccount act_min = accountBLL.GetModel(current.id.Value);
            if (accountId == ConstUtil.JOURNAL_TJSY) agentAccount_min = act_min.agentTjsy.Value;
            if (accountId == ConstUtil.JOURNAL_TDJ) agentAccount_min = act_min.agentTdj.Value;
            if (accountId == ConstUtil.JOURNAL_CSJ) agentAccount_min = act_min.agentCsj.Value;
            if (accountId == ConstUtil.JOURNAL_FT) agentAccount_min = act_min.agentFt.Value;
            if (agentAccount_min < ZhBTZyemin) throw new ValidateException("余额须>=" + ZhBTZyemin + "，才能转化为络绎阁");
            if (BTZid == null || BTZid == 0) throw new ValidateException("未指定转化的络绎阁类型");


            //触发器触发后有可能锁住帐户
            if (double.Parse(dao.GetList("select IsNull(sum(income),0)-IsNull(sum(outlay),0) as ls_total from LiuShuiZhang where accountId=" + ConstUtil.JOURNAL_TJSY + " and uid=" + current.id).Rows[0]["ls_total"].ToString()) < act_min.agentTjsy)
            {
                dao.ExecuteBySql("update Member set isLock=1 where id=" + current.id + @"; INSERT INTOLoginHistory
           (uid
           ,userId
           ,loginTime
           ,flag
           ,mulx)
     VALUES(" + current.id + "," + current.userId + ",getdate(),6,'交易人余额与流水总额不符已冻结')");
                throw new ValidateException("交易人余额与流水总额不符已冻结");
            }
            if (double.Parse(dao.GetList("select IsNull(sum(income),0)-IsNull(sum(outlay),0) as ls_total from LiuShuiZhang where accountId=" + ConstUtil.JOURNAL_TDJ + " and uid=" + current.id).Rows[0]["ls_total"].ToString()) < act_min.agentTdj)
            {
                dao.ExecuteBySql("update Member set isLock=1 where id=" + current.id + @"; INSERT INTOLoginHistory
           (uid
           ,userId
           ,loginTime
           ,flag
           ,mulx)
     VALUES(" + current.id + "," + current.userId + ",getdate(),6,'交易人余额与流水总额不符已冻结')");
                throw new ValidateException("交易人余额与流水总额不符已冻结");
            }
            if (double.Parse(dao.GetList("select IsNull(sum(income),0)-IsNull(sum(outlay),0) as ls_total from LiuShuiZhang where accountId=" + ConstUtil.JOURNAL_CSJ + " and uid=" + current.id).Rows[0]["ls_total"].ToString()) < act_min.agentCsj)
            {
                dao.ExecuteBySql("update Member set isLock=1 where id=" + current.id + @"; INSERT INTOLoginHistory
           (uid
           ,userId
           ,loginTime
           ,flag
           ,mulx)
     VALUES(" + current.id + "," + current.userId + ",getdate(),6,'交易人余额与流水总额不符已冻结')");
                throw new ValidateException("交易人余额与流水总额不符已冻结");
            }
            if (double.Parse(dao.GetList("select IsNull(sum(income),0)-IsNull(sum(outlay),0) as ls_total from LiuShuiZhang where accountId=" + ConstUtil.JOURNAL_FT + " and uid=" + current.id).Rows[0]["ls_total"].ToString()) < act_min.agentFt)
            {
                dao.ExecuteBySql("update Member set isLock=1 where id=" + current.id + @"; INSERT INTOLoginHistory
           (uid
           ,userId
           ,loginTime
           ,flag
           ,mulx)
     VALUES(" + current.id + "," + current.userId + ",getdate(),6,'交易人余额与流水总额不符已冻结')");
                throw new ValidateException("交易人余额与流水总额不符已冻结");
            }
            //同一会员每天只能进行一次
            DataTable dt_BTZSale = dao.GetList("select * from BTZSale where accountId>0 and DateDiff(dd,Cssj,getdate())=0 and uid=" + current.id);
            if (dt_BTZSale.Rows.Count > 0) throw new ValidateException("同一会员每天只能进行一次");
            bool is_while = true;//是否进行循环转化
            int zhcs = 0;//转化次数
            while(is_while)
            {
                double agentAccount = 0;//余额
                MemberAccount act = accountBLL.GetModel(current.id.Value);
                if(accountId == ConstUtil.JOURNAL_TJSY) agentAccount = act.agentTjsy.Value;
                if (accountId == ConstUtil.JOURNAL_TDJ) agentAccount = act.agentTdj.Value;
                if (accountId == ConstUtil.JOURNAL_CSJ) agentAccount = act.agentCsj.Value;
                if (accountId == ConstUtil.JOURNAL_FT) agentAccount = act.agentFt.Value;



                BTZ btz = (BTZ)ReflectionUtil.GetModel(typeof(BTZ), dao.GetOne("select top 1 * from  BTZ where id=" + BTZid + " and Gzzzxx<=" + agentAccount + " order by Gzzzsx desc"));
                string Errormsg = "";
                if (agentAccount < BTZzs) Errormsg = "余额小于最少出售量" + BTZzs;
                if (btz != null && agentAccount >= BTZzs && agentAccount >= btz.Gzzzxx)
                {
                    double Cszz = 0;
                    Cszz = btz.Gzzzxx.Value;//直接取下限做转化
                    //if (agentAccount >= btz.Gzzzsx) Cszz = btz.Gzzzsx.Value;
                    //if (agentAccount < btz.Gzzzsx && agentAccount > btz.Gzzzxx) Cszz = agentAccount;
                    double sxfLz = Cszz * ZhBTZsxfBili / 100;
                    if(act.agentLz>=sxfLz)
                    { 
                    //开始转换
                    BTZSale model = new BTZSale();
                    model.addTime = DateTime.Now;
                    model.uid = current.id;
                    model.userId = current.userId;
                    model.userName = current.userName;
                    model.BTZCode = btz.BTZCode;
                    model.BTZName = btz.BTZName;
                    model.BTZid = btz.id;
                    model.flag = 1;
                    model.fromBuyid = 0;
                    model.ppBuyid = 0;
                    model.Cssj = DateTime.Now;
                    model.Czr = current.userId;
                    model.Ppcs = 0;
                    model.Ppzz = 0;
                    model.salephone = current.phone;
                    model.Cszz = (int)Cszz;
                    model.accountId = accountId;
                    object o = dao.SaveByIdentity(model);
                    int newId = Convert.ToInt32(o);
                    model.id = newId;

                    MemberAccount sub = new MemberAccount();
                    sub.id = current.id;
                    if (accountId == ConstUtil.JOURNAL_TJSY) sub.agentTjsy = Cszz;
                    if (accountId == ConstUtil.JOURNAL_TDJ) sub.agentTdj = Cszz;
                    if (accountId == ConstUtil.JOURNAL_CSJ) sub.agentCsj = Cszz;
                    if (accountId == ConstUtil.JOURNAL_FT) sub.agentFt = Cszz; 
                    accountBLL.UpdateSub(sub);

                    //流水帐
                    LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
                    fromLs.income = 0;
                    fromLs.outlay = Cszz;
                    fromLs.addtime = DateTime.Now;
                    fromLs.uid = current.id;
                    fromLs.userId = current.userId;
                    fromLs.tableName = "syzBTZ";
                    fromLs.addUid = current.id;
                    fromLs.addUser = current.userId;
                    fromLs.accountId = accountId;
                    fromLs.abst = "收益转络绎阁扣除";
                    if (accountId == ConstUtil.JOURNAL_TJSY) fromLs.last = act.agentTjsy.Value - Cszz;
                    if (accountId == ConstUtil.JOURNAL_TDJ) fromLs.last = act.agentTdj.Value - Cszz;
                    if (accountId == ConstUtil.JOURNAL_CSJ) fromLs.last = act.agentCsj.Value - Cszz;
                    if (accountId == ConstUtil.JOURNAL_FT) fromLs.last = act.agentFt.Value - Cszz;
                   
                    liushuiBLL.Save(fromLs);
                    //扣手续费
                    sub = new MemberAccount();
                    sub.id = current.id;
                    sub.agentLz = sxfLz;
                    accountBLL.UpdateSub(sub);

                    //流水帐
                    fromLs = new LiuShuiZhang(); //转出流水
                    fromLs.income = 0;
                    fromLs.outlay = sxfLz;
                    fromLs.addtime = DateTime.Now;
                    fromLs.uid = current.id;
                    fromLs.userId = current.userId;
                    fromLs.tableName = "syzBTZ";
                    fromLs.addUid = current.id;
                    fromLs.addUser = current.userId;
                    fromLs.accountId = ConstUtil.JOURNAL_LZ;
                    fromLs.abst = "收益转络绎阁扣除手续费";
                    fromLs.last = act.agentLz.Value - sxfLz;

                    liushuiBLL.Save(fromLs);

                    zhcs++;
                    //同一会员每天只能进行一次
                    if (zhcs >= 1) is_while = false;
                    }
                    else
                   is_while = false;
                }
                else
                is_while = false;
            }
            if (zhcs == 0) { throw new ValidateException("余额不足以转化成络绎阁或靈氣手续费不足请检查余额"); }
            return 1;
        }

       
    }
}
