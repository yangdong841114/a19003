﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ReMemberEditBLL : BaseBLL<ReMemberEdit>, IReMemberEditBLL
    {

        private System.Type type = typeof(ReMemberEdit);
        public IBaseDao dao { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new ReMemberEdit GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            ReMemberEdit mb = (ReMemberEdit)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ReMemberEdit GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ReMemberEdit mb = (ReMemberEdit)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<ReMemberEdit> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<ReMemberEdit> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<ReMemberEdit>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ReMemberEdit)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<ReMemberEdit> GetList(string sql)
        {
            List<ReMemberEdit> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ReMemberEdit>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ReMemberEdit)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public ReMemberEdit SaveReMemberEdit(ReMemberEdit mb, Member current)
        {
            string err = null;
            //校验start----------------------------------
            //非空校验
            if (mb == null) { err = "保存内容为空"; }
            else if (ValidateUtils.CheckNull(mb.userId)) { err = "会员编码不能为空"; }
            else if (ValidateUtils.CheckNull(mb.newReName)) { err = "新推荐人不能为空"; }
            else if (mb.userId.Equals(mb.newReName)) { err = "新推荐人不能是自己"; }
            if (err != null) { throw new ValidateException(err); }

            //修改的会员
            Member m = memberBLL.GetModelByUserId(mb.userId);
            if (m == null) { throw new ValidateException("修改的会员不存在"); }
            
            //新推荐人
            Member newRe = memberBLL.GetModelByUserId(mb.newReName);
            if (newRe == null || newRe.isPay == 0) { throw new ValidateException("新的推荐人不可用或不存在"); }
            if (newRe.rePath.Contains("," + m.id + ",")) { throw new ValidateException("新推荐人不能是修改会员的下线"); }
            //校验end----------------------------------

            //保存修改推荐关系记录
            int oldReId = m.reId.Value;
            string oldPath = m.rePath + m.id + ","; //原下线的匹配路径
            mb.addTime = DateTime.Now;
            mb.createId = current.id;
            mb.createUser = current.userId;
            mb.uid = m.id;
            mb.userName = m.userName;
            mb.oldReName = m.reName;
            object o = this.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //保存操作日志
            OperateLog log = new OperateLog();
            log.recordId = newId;
            log.uid = current.id;
            log.userId = current.userId;
            log.ipAddress = current.ipAddress;
            log.mulx = mb.userId + "推荐人修改为：" + mb.newReName;
            log.tableName = "ReMemberEdit";
            log.recordName = "修改推荐关系";
            this.SaveOperateLog(log);

            //更换修改会员的新路径
            string newPath = newRe.rePath + newRe.id + ","; //修改会员的新路径
            m.rePath = newPath;
            m.reId = newRe.id;
            m.reName = newRe.userId;
            m.reLevel = newRe.reLevel + 1;
            memberBLL.Update(m);
            newPath = m.rePath + m.id + ","; //新下线的匹配路径
            //更新会员的下线会员代数和路径
            string sql = " update member set repath=replace(repath,'" + oldPath + "','" + newPath + "') where " +
                        " repath like '%,'+cast(" + m.id + " as varchar(100))+',%'";
            dao.ExecuteBySql(sql);

            sql = " update member set relevel=dbo.Get_StrArrayLength(repath,',')-2 where " +
                         " repath like '%,'+cast(" + m.id + " as varchar(100))+',%'";
            dao.ExecuteBySql(sql);

            //减少原推荐人的直推人数
            sql = "update member set recount=recount-1 where isPay=1 and id = " + oldReId;
            dao.ExecuteBySql(sql);
            //计算下线人数
            sql = @"declare @uid int=" + oldReId + @";
update Member set reTreeCount=(select COUNT(1) from Member  where uLevel>=3 and CHARINDEX(','+CAST(@uid  AS VARCHAR)+',',rePath)>0 )
where id=@uid";
            dao.ExecuteBySql(sql);
            //增加新推荐人的直推人数
            sql = "update member set recount=recount+1 where isPay =1 and id = " + newRe.id;
            dao.ExecuteBySql(sql);
            //计算下线人数
            sql = @"declare @uid int=" + newRe.id + @";
update Member set reTreeCount=(select COUNT(1) from Member  where uLevel>=3 and CHARINDEX(','+CAST(@uid  AS VARCHAR)+',',rePath)>0 )
where id=@uid";
            dao.ExecuteBySql(sql);

            if (newId <= 0)
            {
                throw new ValidateException("修改失败，请联系管理员");
            }

            return mb;
        }


        public PageResult<ReMemberEdit> GetListPage(ReMemberEdit model)
        {
            PageResult<ReMemberEdit> page = new PageResult<ReMemberEdit>();
            string sql = "select *,row_number() over(order by m.id desc) rownumber from ReMemberEdit m where 1=1 ";
            string countSql = "select count(1) from ReMemberEdit where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("userName", ConstUtil.LIKE, model.userName));
                }
            }
            
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<ReMemberEdit> list = new List<ReMemberEdit>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ReMemberEdit)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

    }
}
