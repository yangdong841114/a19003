﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BTZBuyssBLL : BaseBLL<BTZBuyss>, IBTZBuyssBLL
    {

        private System.Type type = typeof(BTZBuyss);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new BTZBuyss GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            BTZBuyss mb = (BTZBuyss)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new BTZBuyss GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BTZBuyss mb = (BTZBuyss)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<BTZBuyss> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<BTZBuyss> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZBuyss>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZBuyss)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<BTZBuyss> GetList(string sql)
        {
            List<BTZBuyss> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZBuyss>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuyss model = (BTZBuyss)ReflectionUtil.GetModel(type, row);
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<BTZBuyss> GetListPage(BTZBuyss model, string fields)
        {
            PageResult<BTZBuyss> page = new PageResult<BTZBuyss>();
            string sql = "select " + fields + ",row_number() over(order by m.flag asc,m.id desc) rownumber from BTZBuyss m where 1=1 ";
            string countSql = "select count(1) from BTZBuyss m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    //param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                    sql += " and (saleUid=" + model.uid + " or uid=" + model.uid + ") ";
                    countSql += " and (saleUid=" + model.uid + " or uid=" + model.uid + ") ";
                }
                if (!ValidateUtils.CheckIntZero(model.Buyid))
                {
                    param.Add(new DbParameterItem("m.Buyid", ConstUtil.EQ, model.Buyid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.saleuserId))
                {
                    param.Add(new DbParameterItem("m.saleuserId", ConstUtil.LIKE, model.saleuserId));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.isSsstate != null && model.isSsstate!=100)
                {
                    param.Add(new DbParameterItem("m.isSsstate", ConstUtil.EQ, model.isSsstate));
                }
                if (model.isJcjy != null && model.isJcjy != 100)
                {
                    param.Add(new DbParameterItem("m.isJcjy", ConstUtil.EQ, model.isJcjy));
                }
               
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZBuyss> list = new List<BTZBuyss>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuyss modelrow = (BTZBuyss)ReflectionUtil.GetModel(type, row);
                    //如果查询的用户ID为。则要分为是卖家还是买家。卖家不可点击取消按钮
                    if (!ValidateUtils.CheckIntZero(model.uid))
                    {
                        if(model.uid == modelrow.saleUid)//卖家不可操作取消按钮
                        modelrow.isCanqx = "no";
                        else
                        modelrow.isCanqx = "yes";
                    }

                    list.Add(modelrow);
                }
            }
            page.rows = list;
            return page;
        }




        public BTZBuyss GetModel(int id)
        {
            return this.GetOne("select * from BTZBuyss where id = " + id);
        }

        public BTZBuyss SaveProduct(BTZBuyss mb, Member current)
        {
            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            if (mb.cont.Length > 10000) { throw new ValidateException("内容太长请检查是否含有乱码"); }
            DataTable dt_BTZBuyss = dao.GetList("select * from BTZBuyss where Buyid="+mb.Buyid);
            if (dt_BTZBuyss.Rows.Count > 0) { throw new ValidateException("买单" + mb.Buyid+"已存在申诉记录不能重复申诉"); }
            mb.phone = current.phone;
            DataTable dt_BTZBuy= dao.GetList("select * from BTZBuy where id=" + mb.Buyid.Value);
            mb.BuyNo = dt_BTZBuy.Rows[0]["BuyNo"].ToString();
            mb.SaleNo = dao.GetList("select SaleNo from BTZSale where id=" + mb.saleId.Value).Rows[0]["SaleNo"].ToString();
            mb.uid = current.id;
            mb.userId = current.userId;
            mb.userName = current.userName;
            mb.flag = 1;
            mb.addTime = DateTime.Now;
            Member sale = memberBLL.GetModelByUserId(mb.saleuserId);
            if (sale!=null)
            {
            mb.salephone = sale.phone;
            //mb.saleuserId = sale.userId;
           // mb.saleuserName = sale.userName;
            }
            //投诉类型
            if(dt_BTZBuy.Rows[0]["uid"].ToString() == current.id.ToString())mb.ssType = "买家投诉卖家";
            if(dt_BTZBuy.Rows[0]["saleUid"].ToString() == current.id.ToString()) mb.ssType = "卖家投诉买家";
            if (mb.ssType == "买家投诉卖家") mb.isSsstate = 1;//锁
            if (mb.ssType == "卖家投诉买家") mb.isSsstate = 1;//锁
            if (mb.isSsstate == 1) dao.ExecuteBySql("update Member set isSsstate=1 where id in (" + mb.uid +","+ mb.saleUid+")");
            mb.isJcjy = 0;//默认没有解除交易
            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }
        public int Delete(int id)
        {
            string sql = "delete from BTZBuyss where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int qx(int id)
        {
            if (this.GetOne("select * from BTZBuyss where id = " + id).flag == 2) { throw new ValidateException("该申诉已取消"); }
            string sql = "update BTZBuyss set flag=2 where id=" + id;
            //取消包含解除
            jc(id);
            return dao.ExecuteBySql(sql);
        }
        public int jc(int id)
        {
            string sql = "update BTZBuyss set isSsstate=0,flag=2 where id=" + id;
            dao.ExecuteBySql(sql);
            BTZBuyss ca=this.GetOne("select * from BTZBuyss where id = " + id);
            //看下此两人如果没有申诉记录是申诉状态即取消各自的申诉状态
            int uid = ca.uid.Value;
            DataTable dt_BTZBuyss = dao.GetList("select * from BTZBuyss where isSsstate=1 and (uid=" + uid + " or saleUid=" + uid + ")");
            if (dt_BTZBuyss.Rows.Count == 0) dao.ExecuteBySql("update Member set isSsstate=0 where id=" + uid);
            uid = ca.saleUid.Value;
            dt_BTZBuyss = dao.GetList("select * from BTZBuyss where isSsstate=1 and (uid=" + uid + " or saleUid=" + uid + ")");
            if (dt_BTZBuyss.Rows.Count == 0) dao.ExecuteBySql("update Member set isSsstate=0 where id=" + uid);
            return 1;
        }
        public int jcjy(int id)
        {
            string sql = "update BTZBuyss set isJcjy=1 where id=" + id;
            BTZBuyss ca = this.GetOne("select * from BTZBuyss where id = " + id);
            DataTable dt_BTZBuy = dao.GetList("select * from BTZBuy where id =" + ca.Buyid.Value);
            if (dt_BTZBuy.Rows[0]["flag"].ToString() == "6" || dt_BTZBuy.Rows[0]["flag"].ToString() == "8") { throw new ValidateException("交易已完成不能撤单"); }
            //付款记录状态更新为9解除交易
            dao.ExecuteBySql("update BTZBuy set flag=9,saleId=0,saleUid=0,saleuserId='',saleuserName='' where id =" + ca.Buyid.Value);
            dao.ExecuteBySql("update BTZSale set flag=1,Ppzz=0,ppBuyid=0 where id=" + ca.saleId.Value);
            //取消包含解除
            jc(id);
            return dao.ExecuteBySql(sql);
        }
        public void hh(int id, string hhcont)
        {
            string sql = "update BTZBuyss set flag=3,hhcont='" + hhcont + "' where id=" + id;
            dao.ExecuteBySql(sql);
        }
       
    }
}
