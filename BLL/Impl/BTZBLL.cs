﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BTZBLL : BaseBLL<BTZ>, IBTZBLL
    {

        private System.Type type = typeof(BTZ);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }
        //public IBTZBuyBLL btzbuyBLL { get; set; }
        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new BTZ GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            BTZ mb = (BTZ)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new BTZ GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BTZ mb = (BTZ)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public DataTable GetTable(string sql)
        {
          
            return dao.GetList(sql);
        }

        public new List<BTZ> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<BTZ> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZ>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZ)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<BTZ> GetList(string sql)
        {
            List<BTZ> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZ>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZ model = (BTZ)ReflectionUtil.GetModel(type, row);
                    //是否是当前日期的时间不是的话更新成当前日期
                    if (model.Ksqysj.Value < DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString()))
                    {
                        model.Ksqysj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + model.Ksqysj.Value.ToString("HH:mm"));
                        model.Jsqysj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + model.Jsqysj.Value.ToString("HH:mm"));
                        model.Yyjzsj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + model.Yyjzsj.Value.ToString("HH:mm"));

                        BTZ updateModel = new BTZ();
                        updateModel.id = model.id;
                        updateModel.Ksqysj = model.Ksqysj;
                        updateModel.Jsqysj = model.Jsqysj;
                        updateModel.Yyjzsj = model.Yyjzsj;
                        dao.Update(updateModel);
                    }
                    model.Qydjs = (model.Ksqysj.Value - DateTime.Now).TotalSeconds;
                    model.Qyjsdjs = (model.Jsqysj.Value - DateTime.Now).TotalSeconds;
                    model.Yydjs = (model.Yyjzsj.Value - DateTime.Now).TotalSeconds;
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<BTZ> GetListPage(BTZ model, string fields)
        {
            dao.ExecuteBySql("update BTZ set Ppkssj='2000-01-01',Ppjssj='2000-01-01',Lybili=0 where DateDiff(dd,Ppkssj,GETDATE())>0");//第二天络绎阁匹配时间归默认值
            string order = "row_number() over(order by m.addTime desc) rownumber ";
            if (!ValidateUtils.CheckIntZero(model.isShelve)) order = "row_number() over(order by m.Ksqysj asc) rownumber ";
            PageResult<BTZ> page = new PageResult<BTZ>();
            string sql = "select " + fields + "," + order + " from BTZ m where 1=1 ";
            string countSql = "select count(1) from BTZ m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

             
                if (!ValidateUtils.CheckIntZero(model.isShelve))
                {
                    param.Add(new DbParameterItem("m.isShelve", ConstUtil.EQ, model.isShelve));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
               
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
           
            //int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            //int endnum = model.page.Value * model.rows.Value;          //截至条数

            model.rows = page.total;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = page.total;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZ> list = new List<BTZ>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZ modelrow = (BTZ)ReflectionUtil.GetModel(type, row);
                    //是否是当前日期的时间不是的话更新成当前日期
                    if (modelrow.Ksqysj.Value < DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString()))
                    {
                        modelrow.Ksqysj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + modelrow.Ksqysj.Value.ToString("HH:mm"));
                        modelrow.Jsqysj = modelrow.Ksqysj.Value.AddSeconds(modelrow.Qycxsj.Value);
                        modelrow.Yyjzsj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + modelrow.Yyjzsj.Value.ToString("HH:mm"));

                        BTZ updateModel = new BTZ();
                        updateModel.id = modelrow.id;
                        updateModel.Ksqysj = modelrow.Ksqysj;
                        updateModel.Jsqysj = modelrow.Jsqysj;
                        updateModel.Yyjzsj = modelrow.Yyjzsj;
                        dao.Update(updateModel);
                    }
                    modelrow.Qydjs = (modelrow.Ksqysj.Value - DateTime.Now).TotalSeconds;
                    modelrow.Qyjsdjs = (modelrow.Jsqysj.Value - DateTime.Now).TotalSeconds;
                    modelrow.Yydjs = (modelrow.Yyjzsj.Value - DateTime.Now).TotalSeconds;
                    modelrow.Ppdjs = (modelrow.Ppkssj.Value - DateTime.Now).TotalSeconds;
                    modelrow.Ppjsdjs = modelrow.Ppdjs + modelrow.Qycxsj.Value;
                    list.Add(modelrow);
                }
            }
            page.rows = list;
            return page;
        }


        public PageResult<BTZ> GetListPageAll(BTZ model, string fields)
        {
            PageResult<BTZ> page = new PageResult<BTZ>();
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from BTZ m  where 1=1 ";
            string countSql = "select count(1) from BTZ m  where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

              
                if (!ValidateUtils.CheckIntZero(model.isShelve))
                {
                    param.Add(new DbParameterItem("m.isShelve", ConstUtil.EQ, model.isShelve));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
              
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            model.rows = page.total;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = page.total;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZ> list = new List<BTZ>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZ)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }


        public BTZ GetModel(int id)
        {
            return this.GetOne("select * from BTZ where id = " + id);
        }

        public BTZ SaveProduct(BTZ mb, Member current)
        {

            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            else if (ValidateUtils.CheckNull(mb.BTZName)) { throw new ValidateException("名称不能为空"); }
            else if (mb.Qycxsj == null || mb.Qycxsj < 0) { throw new ValidateException("抢养持续时间-不能为空或负数"); }
            else if (mb.Yysxlz == null || mb.Yysxlz < 0) { throw new ValidateException("预约 领养所需靈氣-不能为空或负数"); }
            else if (mb.Jqsxlz == null || mb.Jqsxlz < 0) { throw new ValidateException("即抢 领养所需靈氣-不能为空或负数"); }
            else if (mb.Hyts == null || mb.Hyts < 0) { throw new ValidateException("合约天数-不能为空或负数"); }
            else if (mb.RsyBili == null || mb.RsyBili < 0) { throw new ValidateException("日收益比例-不能为空或负数"); }
            else if (mb.ZcszjBili == null || mb.ZcszjBili < 0) { throw new ValidateException("再出售增加利息比例-不能为空或负数"); }
            else if (mb.ZsBTT == null || mb.ZsBTT < 0) { throw new ValidateException("赠送DOGE数量-不能为空或负数"); }
            else if (mb.ZsBTD == null || mb.ZsBTD < 0) { throw new ValidateException("赠送BTD数量-不能为空或负数"); }
            else if (mb.Rfxesx == null || mb.Rfxesx < 0) { throw new ValidateException("平台每日发行价值额度上限-不能为空或负数"); }
            else if (mb.Gzzzxx == null || mb.Gzzzxx < 0) { throw new ValidateException("规则价值下限-不能为空或负数"); }
            else if (mb.Gzzzsx == null || mb.Gzzzsx < 0) { throw new ValidateException("规则价值上限-不能为空或负数"); }
            else if (mb.Zszzxx == null || mb.Zszzxx < 0) { throw new ValidateException("展示价值下限-不能为空或负数"); }
            else if (mb.Zszzsx == null || mb.Zszzsx < 0) { throw new ValidateException("展示价值上限-不能为空或负数"); }
            else if (mb.Cffzbs == null || mb.Cffzbs < 0) { throw new ValidateException("拆份阀值倍数-不能为空或负数"); }
            else if (mb.Cffzfs == null || mb.Cffzfs < 0) { throw new ValidateException("拆份阀值份数-不能为空或负数"); }
            else if (mb.Mtxyrs == null || mb.Mtxyrs < 0) { throw new ValidateException("每天续养人数-不能为空或负数"); }
            else if (mb.jLevel == null || mb.jLevel <=0) { throw new ValidateException("级别-不能为空或负数"); }
            else if (mb.bLevel == null || mb.bLevel <= 0) { throw new ValidateException("班级-不能为空或负数"); }
            else if (mb.ZdcsBili == null || mb.ZdcsBili < 0) { throw new ValidateException("自动出售比例-不能为空或负数"); }
            else if (ValidateUtils.CheckNull(mb.imgUrl)) { throw new ValidateException("请上传图片"); }
            if (mb.Gzzzxx == mb.Gzzzsx) { throw new ValidateException("上限与下限不能相等"); }
            //if (dao.GetList("select * from BTZ where id not in (0) and Gzzzxx=" + mb.Gzzzxx).Rows.Count > 0) { throw new ValidateException("此规则下限-" + mb.Gzzzxx + "-已存在"); }
            if (dao.GetList("select * from BTZ where id not in (0) and jLevel=" + mb.jLevel + " and bLevel=" + mb.bLevel).Rows.Count > 0) { throw new ValidateException("此级别-" + mb.jLevel + "-此班级-"+ mb.bLevel+"-已存在"); }
            int maxjLevel=0;
            DataTable dt_Max_jLevel=dao.GetList("select isnull(MAX(jLevel),0) as maxjLevel from BTZ");
            if(dt_Max_jLevel.Rows.Count>0)
            maxjLevel = int.Parse(dt_Max_jLevel.Rows[0]["maxjLevel"].ToString());
            if (mb.jLevel.Value > (maxjLevel + 1)) { throw new ValidateException("最高级别只能为-" + (maxjLevel + 1) + "-"); }
            //同一级别的：不同班级下所有猪品种的规则价值范围必须相等
            DataTable BTZBj = dao.GetList("select * from BTZ where  jLevel=" + mb.jLevel + " and id not in (0)");
            if (BTZBj.Rows.Count > 0)
            {
                if (mb.Gzzzxx!= double.Parse(BTZBj.Rows[0]["Gzzzxx"].ToString())) { throw new ValidateException("此级别此班级规则下限必须为-" + BTZBj.Rows[0]["Gzzzxx"].ToString()); }
                if (mb.Gzzzsx!= double.Parse(BTZBj.Rows[0]["Gzzzsx"].ToString())) { throw new ValidateException("此级别此班级规则上限必须为-" + BTZBj.Rows[0]["Gzzzsx"].ToString()); }
            }
            //高级别猪必须大于低级别的
            DataTable BTZJb = dao.GetList("select * from BTZ where  jLevel<" + mb.jLevel + " order by Gzzzsx desc");
            if (BTZJb.Rows.Count > 0)
            {
                //下级的上限如果大于本级别下限则不行
                if (mb.Gzzzxx.Value <= double.Parse(BTZJb.Rows[0]["Gzzzsx"].ToString())) { throw new ValidateException("此级别下限必须大于-" + BTZJb.Rows[0]["Gzzzsx"].ToString()); }
            }
            //字符串换成时间
            mb.Ksqysj = DateTime.Parse(DateTime.Now.Year.ToString()+"-"+DateTime.Now.Month.ToString()+"-"+DateTime.Now.Day.ToString()+" "+mb.KsqysjStr);
            mb.Yyjzsj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + mb.YyjzsjStr);
            //算出结束时间
            mb.Jsqysj = mb.Ksqysj.Value.AddSeconds(mb.Qycxsj.Value);
            mb.JsqysjStr = mb.Jsqysj.Value.ToString("HH:mm:ss");
            //设置默认值
            if (current.isAdmin == 1)
            {
                mb.uid = 1;
                mb.userId = "system";
                mb.flag = 2;
                mb.isShelve = 2;
            }
            else
            {
                mb.uid = current.id;
                mb.userId = current.userId;
                mb.flag = 1;
                mb.isShelve = 1;
            }
         
            mb.addTime = DateTime.Now;
            mb.Ppjssj = DateTime.Parse("2000-01-01");
            mb.Ppkssj = DateTime.Parse("2000-01-01");
            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }

        public BTZ UpdateProduct(BTZ mb, Member current)
        {
            //非空校验
            if (mb == null) { throw new ValidateException("更新内容为空"); }
            else if (ValidateUtils.CheckNull(mb.BTZName)) { throw new ValidateException("名称不能为空"); }
            else if (mb.Qycxsj == null || mb.Qycxsj < 0) { throw new ValidateException("抢养持续时间-不能为空或负数"); }
            else if (mb.Yysxlz == null || mb.Yysxlz < 0) { throw new ValidateException("预约 领养所需靈氣-不能为空或负数"); }
            else if (mb.Jqsxlz == null || mb.Jqsxlz < 0) { throw new ValidateException("即抢 领养所需靈氣-不能为空或负数"); }
            else if (mb.Hyts == null || mb.Hyts < 0) { throw new ValidateException("合约天数-不能为空或负数"); }
            else if (mb.RsyBili == null || mb.RsyBili < 0) { throw new ValidateException("日收益比例-不能为空或负数"); }
            else if (mb.ZcszjBili == null || mb.ZcszjBili < 0) { throw new ValidateException("再出售增加利息比例-不能为空或负数"); }
            else if (mb.ZsBTT == null || mb.ZsBTT < 0) { throw new ValidateException("赠送DOGE数量-不能为空或负数"); }
            else if (mb.ZsBTD == null || mb.ZsBTD < 0) { throw new ValidateException("赠送BTD数量-不能为空或负数"); }
            else if (mb.Rfxesx == null || mb.Rfxesx < 0) { throw new ValidateException("平台每日发行价值额度上限-不能为空或负数"); }
            else if (mb.Gzzzxx == null || mb.Gzzzxx < 0) { throw new ValidateException("规则价值下限-不能为空或负数"); }
            else if (mb.Gzzzsx == null || mb.Gzzzsx < 0) { throw new ValidateException("规则价值上限-不能为空或负数"); }
            else if (mb.Zszzxx == null || mb.Zszzxx < 0) { throw new ValidateException("展示价值下限-不能为空或负数"); }
            else if (mb.Zszzsx == null || mb.Zszzsx < 0) { throw new ValidateException("展示价值上限-不能为空或负数"); }
            else if (mb.Cffzbs == null || mb.Cffzbs < 0) { throw new ValidateException("拆份阀值倍数-不能为空或负数"); }
            else if (mb.Cffzfs == null || mb.Cffzfs < 0) { throw new ValidateException("拆份阀值份数-不能为空或负数"); }
            else if (mb.Mtxyrs == null || mb.Mtxyrs < 0) { throw new ValidateException("每天续养人数-不能为空或负数"); }
            else if (mb.jLevel == null || mb.jLevel <= 0) { throw new ValidateException("级别-不能为空或负数"); }
            else if (mb.bLevel == null || mb.bLevel <= 0) { throw new ValidateException("班级-不能为空或负数"); }
            else if (mb.ZdcsBili == null || mb.ZdcsBili < 0) { throw new ValidateException("自动出售比例-不能为空或负数"); }
            if (mb.Gzzzxx == mb.Gzzzsx) { throw new ValidateException("上限与下限不能相等"); }
            //if (dao.GetList("select * from BTZ where id not in (" + mb.id + ") and Gzzzxx=" + mb.Gzzzxx).Rows.Count > 0) { throw new ValidateException("此规则下限-" + mb.Gzzzxx + "-已存在"); }
            if (dao.GetList("select * from BTZ where id not in (" + mb.id + ") and jLevel=" + mb.jLevel + " and bLevel=" + mb.bLevel).Rows.Count > 0) { throw new ValidateException("此级别-" + mb.jLevel + "-此班级-" + mb.bLevel + "-已存在"); }
            int maxjLevel = int.Parse(dao.GetList("select MAX(jLevel) as maxjLevel from BTZ").Rows[0]["maxjLevel"].ToString());
            if (mb.jLevel.Value > (maxjLevel + 1)) { throw new ValidateException("最高级别只能为-" + (maxjLevel + 1) + "-"); }
            //同一级别的：不同班级下所有猪品种的规则价值范围必须相等
            DataTable BTZBj = dao.GetList("select * from BTZ where  jLevel=" + mb.jLevel + " and id not in (" + mb.id + ")");
            if (BTZBj.Rows.Count>0)
            {
                if (mb.Gzzzxx != double.Parse(BTZBj.Rows[0]["Gzzzxx"].ToString())) { throw new ValidateException("此级别此班级规则下限必须为-" + BTZBj.Rows[0]["Gzzzxx"].ToString()); }
                if (mb.Gzzzsx != double.Parse(BTZBj.Rows[0]["Gzzzsx"].ToString())) { throw new ValidateException("此级别此班级规则上限必须为-" + BTZBj.Rows[0]["Gzzzsx"].ToString()); }
            }
            //高级别猪必须大于低级别的
            DataTable BTZJb = dao.GetList("select * from BTZ where  jLevel<" + mb.jLevel + " order by Gzzzsx desc");
            if (BTZJb.Rows.Count > 0)
            {
                //下级的上限如果大于本级别下限则不行
                if (mb.Gzzzxx.Value <= double.Parse(BTZJb.Rows[0]["Gzzzsx"].ToString())) { throw new ValidateException("此级别下限必须大于-" + BTZJb.Rows[0]["Gzzzsx"].ToString()); }
            }
            //字符串换成时间
            mb.Ksqysj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + mb.KsqysjStr);
            mb.Yyjzsj = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + " " + mb.YyjzsjStr);
            //算出结束时间
            mb.Jsqysj = mb.Ksqysj.Value.AddSeconds(mb.Qycxsj.Value);
            mb.JsqysjStr = mb.Jsqysj.Value.ToString("HH:mm:ss");
            //更新
            int c = dao.Update(mb);
            return mb;
        }

        public int SaveShelve(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到上架的商品"); }
            string sql = "update BTZ set isShelve = 2 where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int IsCanJoin(Member current)
        {
            current=memberBLL.GetModelById(current.id.Value);
            if (current.isSksh == null) current.isSksh = 0;
            if (current.isSsstate == 1) { throw new ValidateException("您目前处于申诉状态，不能参与交易！"); }
            if (current.isSksh!=1) { throw new ValidateException("您的收款信息尚未审核通过，暂时不能操作！"); }
            if (current.isSmrz==null||current.isSmrz.Value == 0) { throw new ValidateException("当前会员没有实名认证-不能参加！"); }
            if (current.skIswx == 1 && (current.imgUrlwx == "" || current.imgUrlwx==null)) { throw new ValidateException("微信没有上传二维码-不能参加！"); }
            if (current.skIszfb == 1 && (current.imgUrlzfb == "" || current.imgUrlzfb == null)) { throw new ValidateException("支付宝没有上传二维码-不能参加！"); }
            //if (dao.GetList("select * from BTZjscity where province='" + current.province + "' and city='" + current.city + "'").Rows.Count > 0) { throw new ValidateException("禁售城市" + current.province + current.city + "！"); }
            //是否设置2个收款方式以上
            int count_skfs = 0;
            if (current.skIsbank!=null&&current.skIsbank == 1) count_skfs++;
            if (current.skIswx != null & current.skIswx == 1) count_skfs++;
            if (current.skIszfb != null & current.skIszfb == 1) count_skfs++;
            if (current.skIsszhb != null & current.skIsszhb == 1) count_skfs++;
            if (count_skfs < 2) { throw new ValidateException("必须设置2个收款方式以上！"); }

            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("Yxgzzslz");
            double Yxgzzslz = Convert.ToDouble(param["Yxgzzslz"].paramValue);

            //if (double.Parse(dao.GetList("select isnull(SUM(epoints),0) as allEpoints from Charge where ispay=2 and uid=" + current.id.Value).Rows[0]["allEpoints"].ToString()) < Yxgzzslz)
            //{ throw new ValidateException("至少充值" + Yxgzzslz + "靈氣后才能参与游戏！"); }
            MemberAccount act = accountBLL.GetModel(current.id.Value);
            DataTable dt_BTZBuy = dao.GetList("select * from BTZBuy where uid=" + current.id.Value);
            if (act.agentLz < Yxgzzslz && dt_BTZBuy.Rows.Count==0)//会员首次参与领养时，才受这个条件限制
            { throw new ValidateException("至少余额要有" + Yxgzzslz + "靈氣后才能参与游戏！"); }
            //是否有虚拟会员已供交易
            if (dao.GetList("select * from Member where isVirtual=1").Rows.Count==0)
            { throw new ValidateException("请系统先设置虚拟会员才可进行交易！"); }
            return 0;
        }


        public int BTZJqr(int BTZid, Member current)
        {
            IsCanJoin(current);
            
            //此会员有没有有效机器人
            DataTable dt_Jqr=dao.GetList("select * from Jqr where isStop=0 and dqTime>getdate() and uid=" + current.id.Value);
            if (dt_Jqr.Rows.Count == 0) { throw new ValidateException("未激活，购买机器人自动抢养！"); }
            string BTZid_in = dt_Jqr.Rows[0]["BTZid_in"].ToString();
            //此猪是否在机器人预约设置中
            bool isIn=false;
            string[] sArray = BTZid_in.Split(',');
            foreach (string BTZidStr in sArray)
            {
                if (BTZidStr == BTZid.ToString()) isIn = true;
            }

            if (isIn==true)
            {
                //重新算出络绎阁列表
                DataTable dt_BTZ = dao.GetList("select * from BTZ where id in (0" + BTZid_in + ") and id not in (" + BTZid + ")");
                string new_BTZid_in = "";
                for (int i = 0; i < dt_BTZ.Rows.Count; i++) new_BTZid_in += "," + dt_BTZ.Rows[i]["id"].ToString();
                    //关闭机器人
                dao.ExecuteBySql("update Jqr set BTZid_in='" + new_BTZid_in + "' where id=" + dt_Jqr.Rows[0]["id"].ToString());
                return -1;
            }
            else
            {
            BTZid_in += "," + BTZid;
            string sql = "update Jqr set BTZid_in='" + BTZid_in + "',kqTime=getdate() where id=" + dt_Jqr.Rows[0]["id"].ToString();
             dao.ExecuteBySql(sql);
             return 1;
            }
        }

        public int BTZyy(int BTZid, Member current)
        {
            IsCanJoin(current);
            DataTable dt_BTZBuy = dao.GetList("select * from BTZBuy where BTZid=" + BTZid + " and  (DateDiff(dd,Yysj,getdate())=0 or DateDiff(dd,Qysj,getdate())=0) and uid=" + current.id.Value);
            if (dt_BTZBuy.Rows.Count > 0) { throw new ValidateException("此络绎阁今天你已预约或抢养！"); }
            //过期同一络绎阁预约记录删除掉
            //dao.ExecuteBySql("delete from BTZBuy  where BTZid=" + BTZid + " and DateDiff(dd,Yysj,getdate())>0 and  flag in (1) and uid=" + current.id.Value);

            //dt_BTZBuy = dao.GetList("select * from BTZBuy where BTZid=" + BTZid + " and  flag in (1,2,3,5,6) and uid=" + current.id.Value);
            //if (dt_BTZBuy.Rows.Count > 0) { throw new ValidateException("你抢养的此络绎阁正在匹配或未到期不能重复抢养同一品种！"); }
            BTZ model=GetModel(BTZid);
            if (model.Ksqysj.Value < DateTime.Now) { throw new ValidateException("预约时间已结束！"); }
            Member memVirtual = memberBLL.GetOne("select top 1 * from Member where isVirtual=1 and isLock=0 order by loginLockTime asc");
            if (memVirtual == null) { throw new ValidateException("系统必须存在虚拟会员以供自动出售卖单！"); }
            
            BTZBuy bbuy = new BTZBuy();
            bbuy.uid = current.id;
            bbuy.userId = current.userId;
            bbuy.userName = current.userName;
            bbuy.phone = current.phone;
            bbuy.BTZCode = model.BTZCode;
            bbuy.BTZid = BTZid;
            bbuy.BTZName = model.BTZName;
            bbuy.ZsBTD = model.ZsBTD;
            bbuy.ZsBTT = model.ZsBTT;
            bbuy.flag = 1;
            if (model.Yyjzsj.Value < DateTime.Now)
                bbuy.sxf = model.Jqsxlz;
            else
            bbuy.sxf = model.Yysxlz;
            MemberAccount act = accountBLL.GetModel(current.id.Value);
            if (act.agentLz < bbuy.sxf) { throw new ValidateException("你的靈氣余额不足！"); }


            bbuy.Czr = current.userId;
            bbuy.Yysj = DateTime.Now;
            //bbuy.Qysj = DateTime.Now;
            bbuy.addTime = DateTime.Now;
            bbuy.isVirtual = 1;
            bbuy.Hyts = model.Hyts;
            bbuy.RsyBili = model.RsyBili;
            bbuy.ZcszjBili = model.ZcszjBili;
            bbuy.isZcs = 0;
            bbuy.saleId = 0;
            if (memVirtual==null)
            {
                bbuy.saleUid = 0;
                bbuy.saleuserId = "";
                bbuy.saleuserName = "";
            }
            else
            {
                bbuy.saleUid = memVirtual.id;
                bbuy.saleuserId = memVirtual.userId;
                bbuy.saleuserName = memVirtual.userName;
                dao.ExecuteBySql("update Member set loginLockTime=getdate() where id=" + memVirtual.id);//以这个时间作为卖出时间。下次取别人，以达到轮流卖的目的
            }
            //所有会员的买入时间统一：当天日期+该猪品的：抢养开始时间
            bbuy.Kssj = model.Ksqysj;
            bbuy.Jssj = bbuy.Kssj.Value.AddDays(bbuy.Hyts.Value);
            dao.SaveByIdentity(bbuy);

            MemberAccount sub = new MemberAccount();
            sub.id = current.id;
            sub.agentLz = bbuy.sxf;
            accountBLL.UpdateSub(sub);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.income = 0;
            fromLs.outlay = bbuy.sxf;
            fromLs.addtime = DateTime.Now;
            fromLs.uid = current.id;
            fromLs.userId = current.userId;
            fromLs.tableName = "BTZBuy";
            fromLs.addUid = current.id;
            fromLs.addUser = current.userId;
            fromLs.accountId = ConstUtil.JOURNAL_LZ;
            fromLs.abst = "预约络绎阁扣除-" + model.BTZName;
            fromLs.last = act.agentLz.Value - bbuy.sxf;
            liushuiBLL.Save(fromLs);

            return 1;
        }
        public int BTZyyvip(int BTZid, Member current)
        {
            BTZBuy model = (BTZBuy)ReflectionUtil.GetModel(typeof(BTZBuy), dao.GetOne("select * from  BTZBuy where BTZid=" + BTZid + " and  flag in (1) and uid=" + current.id.Value));//预约中的
            if (model == null) { throw new ValidateException("你还没有预约记录不能开启VIP！"); }
            model.YyVipsj = DateTime.Now;
            dao.Update(model);
            return 1;
        }

        public string isCg(int BTZid, Member current)
        {
            if (current.id != null && current.id.Value != 0)
            dao.ExecuteBySql("exec BTZSalePP;exec BTZBuyPP;exec BTZSalePP;exec BTZSaleHG;");//跑下匹配,卖出记录优先，买单匹配（产生一些状态为4的失败记录，但这些记录要求有VIP标识的要再次参与卖单匹配。所以要跑多一次BTZSalePP）,剩下没卖出的回购
            string result = "";
            BTZBuy model = (BTZBuy)ReflectionUtil.GetModel(typeof(BTZBuy), dao.GetOne("select * from  BTZBuy where (DateDiff(dd,Qysj,GETDATE())=0 or DateDiff(dd,Yysj,GETDATE())=0)  and BTZid=" + BTZid + "  and uid=" + current.id.Value));//当天预约结果
            if (model == null) result = "noBuy";
            else
            {
                if (model.flag == 3) result = "success";
                else if (model.flag == 1 || model.flag == 2) result = "nostartpp";
                else
                    result = "lose";
            }
            return result;
        }

        public int ly(int BTZid, Member current)
        {
            IsCanJoin(current);
            DataTable dt_BTZBuy = dao.GetList("select * from BTZBuy where BTZid=" + BTZid + " and   DateDiff(dd,Qysj,getdate())=0 and uid=" + current.id.Value);
            if (dt_BTZBuy.Rows.Count > 0) { throw new ValidateException("此络绎阁今天你已抢养！"); }
            dt_BTZBuy = dao.GetList("select * from BTZBuy where BTZid=" + BTZid + " and   DateDiff(dd,Yysj,getdate())=0 and flag>=3 and uid=" + current.id.Value);
            if (dt_BTZBuy.Rows.Count > 0) { throw new ValidateException("此络绎阁今天你已VIP预约并匹配！"); }
            //DataTable dt_BTZBuy = dao.GetList("select * from BTZBuy where BTZid=" + BTZid + " and  flag in (2,3,5,6) and uid=" + current.id.Value);
            //if (dt_BTZBuy.Rows.Count > 0) { throw new ValidateException("你抢养的此络绎阁正在匹配或未到期不能重复抢养同一品种！"); }
            BTZBuy searchbbuy = (BTZBuy)ReflectionUtil.GetModel(typeof(BTZBuy), dao.GetOne("select * from  BTZBuy where BTZid=" + BTZid + " and  flag in (1) and uid=" + current.id.Value));//预约中的

            BTZ model = GetModel(BTZid);
            if (model.Ppkssj.Value > DateTime.Now.AddDays(-1)) throw new ValidateException("已抢完 ");//{ throw new ValidateException("匹配时间已开始。抢养时间已结束当前时间-" + DateTime.Now.ToString() + "！"); }
            if(searchbbuy==null)
            {
                Member memVirtual = memberBLL.GetOne("select top 1 * from Member where isVirtual=1 and isLock=0 order by loginLockTime asc");
                if (memVirtual == null) { throw new ValidateException("系统必须存在虚拟会员以供自动出售卖单！"); }
        
            BTZBuy bbuy = new BTZBuy();
            bbuy.uid = current.id;
            bbuy.userId = current.userId;
            bbuy.userName = current.userName;
            bbuy.phone = current.phone;
            bbuy.BTZCode = model.BTZCode;
            bbuy.BTZid = BTZid;
            bbuy.BTZName = model.BTZName;
            bbuy.ZsBTD = model.ZsBTD;
            bbuy.ZsBTT = model.ZsBTT;
            bbuy.flag = 2;
            if (model.Yyjzsj.Value < DateTime.Now)
                bbuy.sxf = model.Jqsxlz;
            else
                bbuy.sxf = model.Yysxlz;
            MemberAccount act = accountBLL.GetModel(current.id.Value);
            if (act.agentLz < bbuy.sxf) { throw new ValidateException("你的靈氣余额不足！"); }

            bbuy.Czr = current.userId;
            bbuy.Qysj = DateTime.Now;
            bbuy.addTime = DateTime.Now;
            bbuy.isVirtual = 1;
            bbuy.Hyts = model.Hyts;
            bbuy.RsyBili = model.RsyBili;
            bbuy.ZcszjBili = model.ZcszjBili;
            bbuy.isZcs = 0;
            bbuy.saleId = 0;
            if (memVirtual == null)
            {
                bbuy.saleUid = 0;
                bbuy.saleuserId = "";
                bbuy.saleuserName = "";
            }
            else
            {
                bbuy.saleUid = memVirtual.id;
                bbuy.saleuserId = memVirtual.userId;
                bbuy.saleuserName = memVirtual.userName;
                dao.ExecuteBySql("update Member set loginLockTime=getdate() where id=" + memVirtual.id);//以这个时间作为卖出时间。下次取别人，以达到轮流卖的目的
            }
            //所有会员的买入时间统一：当天日期+该猪品的：抢养开始时间
            bbuy.Kssj = model.Ksqysj;
            bbuy.Jssj = bbuy.Kssj.Value.AddDays(bbuy.Hyts.Value);
            dao.SaveByIdentity(bbuy);
            //更新猪领养比例--抢养时间结束后
            dao.ExecuteBySql("exec calBTZLyBili;");
            MemberAccount sub = new MemberAccount();
            sub.id = current.id;
            sub.agentLz = bbuy.sxf;
            accountBLL.UpdateSub(sub);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.income = 0;
            fromLs.outlay = bbuy.sxf;
            fromLs.addtime = DateTime.Now;
            fromLs.uid = current.id;
            fromLs.userId = current.userId;
            fromLs.tableName = "BTZBuy";
            fromLs.addUid = current.id;
            fromLs.addUser = current.userId;
            fromLs.accountId = ConstUtil.JOURNAL_LZ;
            fromLs.abst = "抢养络绎阁扣除-" + model.BTZName;
            fromLs.last = act.agentLz.Value - bbuy.sxf;
            liushuiBLL.Save(fromLs);
            }
            else
            {
                searchbbuy.Qysj = DateTime.Now;
                searchbbuy.flag = 2;
                dao.Update(searchbbuy);
            }
            return 1;
        }
        

        

        public string GetJqrBTZid(Member current)
        {
            string BTZid_in = "";
            DataTable dt_Jqr = dao.GetList("select BTZid_in from Jqr where isStop=0 and dqTime>getdate() and uid=" + current.id.Value);
            if (dt_Jqr.Rows.Count > 0)
            {
                if (dt_Jqr.Rows[0]["BTZid_in"].ToString() != "") BTZid_in =dt_Jqr.Rows[0]["BTZid_in"].ToString();
            }

            return BTZid_in;
        }
        public int IsVidyy(int BTZid,Member current)
        {
            return dao.GetList("select * from BTZBuy where  DateDiff(dd,YyVipsj,getdate())=0 and BTZid=" + BTZid + " and uid=" + current.id.Value).Rows.Count;
            
        }
        public int Isyy(int BTZid, Member current)
        {
            return dao.GetList("select * from BTZBuy where  DateDiff(dd,Yysj,getdate())=0 and BTZid=" + BTZid + " and uid=" + current.id.Value).Rows.Count;
            
        }
        public int Isly(int BTZid, Member current)
        {
            return dao.GetList("select * from BTZBuy where  DateDiff(dd,Qysj,getdate())=0 and BTZid=" + BTZid + " and uid=" + current.id.Value).Rows.Count;
            
        }
        
        
        public int SaveCancelShelve(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到下架的商品"); }
            string sql = "update BTZ set isShelve = 1 where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int Delete(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到删除的络绎阁"); }
            string sql = "select isShelve from BTZ where id = " + id;
            object o = dao.ExecuteScalar(sql);
            if (o == null) { throw new ValidateException("找不到删除的络绎阁"); }
            int isShelve = Convert.ToInt32(o);
            if (isShelve == 2) { throw new ValidateException("络绎阁还未下架，请下架后再删除"); }
            if (dao.GetList("select * from BTZBuy where BTZid=" + id).Rows.Count > 0) { throw new ValidateException("此络绎阁有交易记录不能删除"); }
            sql = "delete from BTZ where id=" + id;
            return dao.ExecuteBySql(sql);
        }
        public int startPp(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到络绎阁"); }
            BTZ model = GetModel(id);
            if (DateTime.Now < model.Ppkssj.Value) { throw new ValidateException("当前时间<这只猪的：倒计时开始时间"); }
            model.Ppkssj = DateTime.Now;
            model.Ppjssj = model.Ppkssj.Value.AddSeconds(model.Qycxsj.Value);
            dao.Update(model);
            return 1;
        }
        

       
    }
}
