﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    class ProductTypeBLL : BaseBLL<ProductType>, IProductTypeBLL
    {

        private System.Type type = typeof(ProductType);
        public IBaseDao dao { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new ProductType GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            ProductType mb = (ProductType)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ProductType GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ProductType mb = (ProductType)ReflectionUtil.GetModel(type, row);
            return mb;
        }



        public new List<ProductType> GetList(string sql)
        {
            List<ProductType> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ProductType>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ProductType)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }


        public Dictionary<string, List<TreeModel>> GetTreeModelList()
        {
            string sql = "select * from ProductType ";
            DataTable dt = dao.GetList(sql);
            Dictionary<string, List<TreeModel>> di1 = new Dictionary<string, List<TreeModel>>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    DataRow row = dt.Rows[i];
                    TreeModel model = new TreeModel();
                    model.id = Convert.ToInt32(row["id"]);
                    model.value = row["name"].ToString();
                    string parentId = Convert.ToInt32(row["parentId"]).ToString();

                    if (!di1.ContainsKey(parentId))
                    {
                        List<TreeModel> chlids = new List<TreeModel>();
                        chlids.Add(model);
                        di1.Add(parentId, chlids);
                    }
                    else
                    {
                        List<TreeModel> chlids = di1[parentId];
                        chlids.Add(model);
                        di1[parentId] = chlids;
                    }
                }
            }
            return di1;
        }

        public PageResult<ProductType> GetListPage(ProductType model, string fields)
        {
            PageResult<ProductType> page = new PageResult<ProductType>();
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from ProductType m where 1=1 ";
            string countSql = "select count(1) from ProductType m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (model.id != null && model.id != 0)
                {
                    param.Add(new DbParameterItem("m.id", ConstUtil.EQ, model.id));
                }
                if (!ValidateUtils.CheckIntZero(model.grade))
                {
                    param.Add(new DbParameterItem("m.grade", ConstUtil.EQ, model.grade));
                }
                if (!ValidateUtils.CheckIntZero(model.parentId))
                {
                    param.Add(new DbParameterItem("m.parentId", ConstUtil.EQ, model.parentId));
                }
                if (!ValidateUtils.CheckNull(model.name))
                {
                    param.Add(new DbParameterItem("m.name", ConstUtil.LIKE, model.name));
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<ProductType> list = new List<ProductType>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ProductType)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

        public ProductType GetModel(int id)
        {
            return this.GetOne("select * from ProductType where id = " + id);
        }

        public ProductType SaveProductType(ProductType mb)
        {

            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            //else if (ValidateUtils.CheckNull(mb.name)) { throw new ValidateException("分类名称不能为空"); }
            //else if (ValidateUtils.CheckIntZero(mb.grade)) { throw new ValidateException("分类级别不能为空"); }
            
            //1级无父类
            //if(mb.grade == 1 && mb.parentId > 0) { throw new ValidateException("一级类目无需父类"); }
            //2级必须有父类
            //if (mb.grade == 2 && mb.parentId==0) { throw new ValidateException("二级类目需填父类"); }

            if(mb.no=="BTZJb")//级别
            {
                if (dao.GetList("select * from ProductType where no='BTZJb' and BTZLevel=" + mb.BTZLevel).Rows.Count > 0) { throw new ValidateException("级别-" + mb.BTZLevel + "-已存在"); }
            }
            if (mb.no == "BTZBj")//班级
            {
                if (mb.parentId == 0 || mb.parentId == null) { throw new ValidateException("级别必须选择"); }
                if (dao.GetList("select * from ProductType where no='BTZBj' and parentId=" + mb.parentId + " and BTZLevel=" + mb.BTZLevel).Rows.Count > 0) { throw new ValidateException("级别-" + mb.parentId+"-班级-" + mb.BTZLevel + "-已存在"); }
            }

            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }

        public ProductType UpdateProductType(ProductType mb)
        {
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            else if (ValidateUtils.CheckNull(mb.name)) { throw new ValidateException("分类名称不能为空"); }
            else if (ValidateUtils.CheckIntZero(mb.grade)) { throw new ValidateException("分类级别不能为空"); }
            //1级无父类
            if (mb.grade == 1 && mb.parentId > 0) { throw new ValidateException("一级类目无需父类"); }
            //2级必须有父类
            if (mb.grade == 2 && mb.parentId == 0) { throw new ValidateException("二级类目需填父类"); }

            int c = dao.Update(mb);
            return mb;
        }


        public int Delete(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到删除的分类"); }
            ProductType mb = GetModel(id);
            if (mb.no == "BTZJb")//级别
            {
                if (dao.GetList("select * from BTZ where jLevel=" + mb.BTZLevel).Rows.Count > 0) { throw new ValidateException("此级别下有络绎阁"); }
                if (dao.GetList("select * from ProductType where no='BTZBj' and parentId=" + mb.BTZLevel).Rows.Count > 0) { throw new ValidateException("级别-" + mb.BTZLevel + "-下有班级"); }
                string maxBTZLevel = dao.GetList("select MAX(BTZLevel) as maxBTZLevel from ProductType where no='BTZJb'").Rows[0]["maxBTZLevel"].ToString();
                if (mb.BTZLevel.ToString() != maxBTZLevel) { throw new ValidateException("只能从最高级别-" + maxBTZLevel + "-开始删除"); }
            }
            if (mb.no == "BTZBj")//班级
            {
                if (dao.GetList("select * from BTZ where jLevel=" + mb.parentId+" and bLevel=" + mb.BTZLevel).Rows.Count > 0) { throw new ValidateException("此级别下有络绎阁"); }
              
            }
            return 1;
        }

       

    }
}
