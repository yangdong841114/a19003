﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class JqrBLL : BaseBLL<Jqr>, IJqrBLL
    {

        private System.Type type = typeof(Jqr);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Jqr GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            Jqr mb = (Jqr)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new Jqr GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Jqr mb = (Jqr)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<Jqr> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Jqr> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Jqr>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Jqr)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public DataTable  GetDataTable(string sql)
        {
            return dao.GetList(sql);
        }

        public new List<Jqr> GetList(string sql)
        {
            List<Jqr> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Jqr>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Jqr model = (Jqr)ReflectionUtil.GetModel(type, row);
                   
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<Jqr> GetListPage(Jqr model, string fields)
        {
            PageResult<Jqr> page = new PageResult<Jqr>();
            string sql = "select " + fields + ",row_number() over(order by m.gmTime desc) rownumber from Jqr m where 1=1 ";
            string countSql = "select count(1) from Jqr m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

             
                if (model.isStop!=null)
                {
                    param.Add(new DbParameterItem("m.isStop", ConstUtil.EQ, model.isStop));
                }
                if (model.uid != null)
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.gmTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.gmTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Jqr> list = new List<Jqr>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Jqr modelrow = (Jqr)ReflectionUtil.GetModel(type, row);
                    list.Add(modelrow);
                }
            }
            page.rows = list;


            //汇总
            dt = dao.GetList(sql, param_nopage, true);
            int all_ljcszs = 0;//累计出售机器人总数
            int all_yxzs = 0;//有效机器伙总数
            double all_ljcszjz = 0;//累计出售机器人总价值
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Jqr model_row = (Jqr)ReflectionUtil.GetModel(type, row);
                    if (model_row.ly == "认购")
                    {
                        all_ljcszs += 1;
                        all_ljcszjz += model_row.Lzsl.Value;
                    }
                    if (model_row.isStop == 0)
                        all_yxzs += 1;
                }
            }
            Jqr foot_model = new Jqr();
            list = new List<Jqr>();
            foot_model.ljcszs = all_ljcszs;
            foot_model.yxzs = all_yxzs;
            foot_model.ljcszjz = all_ljcszjz;
            list.Add(foot_model);
            page.footer = list;

            return page;
        }



        public Jqr GetModel(int id)
        {
            return this.GetOne("select * from Jqr where id = " + id);
        }

        public Jqr Save(Jqr mb, Member current)
        {
            if (GetDataTable("select * from Jqr where isStop=0 and uid=" + current.id.Value).Rows.Count > 0) { throw new ValidateException("每人只能购买一个机器人"); }
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("Jqrlz", "Jqryxq");
            double Jqrlz = Convert.ToDouble(param["Jqrlz"].paramValue);
            int Jqryxq = Convert.ToInt32(param["Jqryxq"].paramValue);
            MemberAccount act = accountBLL.GetModel(mb.uid.Value);
            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            else if (mb.Lzsl == null || mb.Lzsl < 0) { throw new ValidateException("购买靈氣数量-不能为空或负数"); }
            mb.BTZid = 0;
            if (Jqrlz> act.agentLz.Value) { throw new ValidateException("靈氣-余额不足"); }

            //设置默认值
            if (current.isAdmin == 1)
            {
                mb.uid = 1;
                mb.userId = "system";
                mb.isStop =0;
            }
            else
            {
                mb.uid = current.id;
                mb.userId = current.userId;
                mb.isStop = 0;
            }
         
            //保存
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //减少自身靈氣
            MemberAccount sub = new MemberAccount();
            sub.id = mb.uid;
            sub.agentLz = mb.Lzsl;
            accountBLL.UpdateSub(sub);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.outlay = mb.Lzsl;
            fromLs.addtime = DateTime.Now;
            fromLs.uid = mb.uid;
            fromLs.userId = mb.userId;
            fromLs.tableName = "Jqr";
            fromLs.addUid = current.id;
            fromLs.addUser = current.userId;
            fromLs.accountId = ConstUtil.JOURNAL_LZ;   //靈氣
            fromLs.abst = "购买机器人";
            fromLs.last = act.agentLz.Value - mb.Lzsl.Value;
            fromLs.income = 0;
            fromLs.sourceId = newId;
            liushuiBLL.Save(fromLs);
            return mb;
        }

        public Jqr Update(Jqr mb, Member current)
        {
            
            int c = dao.Update(mb);
            return mb;
        }

       

        public int Delete(int id)
        {
            if (ValidateUtils.CheckIntZero(id)) { throw new ValidateException("找不到删除的记录"); }
            string sql = "select isShelve from Jqr where id = " + id;
            object o = dao.ExecuteScalar(sql);
            if (o == null) { throw new ValidateException("找不到删除的记录"); }
            sql = "delete from Jqr where id=" + id;
            return dao.ExecuteBySql(sql);
        }

       
    }
}
