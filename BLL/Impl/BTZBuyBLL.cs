﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BTZBuyBLL : BaseBLL<BTZBuy>, IBTZBuyBLL
    {

        private System.Type type = typeof(BTZBuy);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }
        public IBTZBLL btzBLL { get; set; }
        public IBTZSaleBLL btzsaleBLL { get; set; }
        

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new BTZBuy GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            BTZBuy mb = (BTZBuy)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new BTZBuy GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BTZBuy mb = (BTZBuy)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<BTZBuy> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<BTZBuy> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZBuy>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZBuy)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<BTZBuy> GetList(string sql)
        {
            List<BTZBuy> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZBuy>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuy model = (BTZBuy)ReflectionUtil.GetModel(type, row);
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<BTZBuy> GetListPage(BTZBuy model, string fields)
        {
            dao.ExecuteBySql(@"update BTZBuy
set BTZBuy.userName=Member.userName
from Member
where BTZBuy.uid=Member.id and BTZBuy.userName is null");
            dao.ExecuteBySql(@"update BTZBuy
set BTZBuy.province=Member.province,BTZBuy.city=Member.city 
from Member
where BTZBuy.uid=Member.id and BTZBuy.province is null");
            dao.ExecuteBySql(@"update BTZBuy
set BTZBuy.phone=Member.phone
from Member
where BTZBuy.uid=Member.id and BTZBuy.phone is null");
            dao.ExecuteBySql(@"update BTZSale
set BTZSale.salephone=Member.phone
from Member
where BTZSale.uid=Member.id and BTZSale.salephone is null");
            //更新订单分类普通订单还是虚拟会员的订单
            dao.ExecuteBySql(@"update BTZBuy set BuymemType=2
where uid in (select id from Member where isVirtual=1) and BuymemType is null;
update BTZBuy set BuymemType=1 where BuymemType is null;");
            PageResult<BTZBuy> page = new PageResult<BTZBuy>();
            string sql = "select " + fields + @",row_number() over(order by m.id desc) rownumber from BTZBuy m left join Member mem
on m.saleUid=mem.id
left join BTZSale bsale
on m.id=bsale.ppBuyid where 1=1 ";
            string countSql = "select count(1) from BTZBuy m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    if(model.flag==-35)
                    { 
                        sql += " and m.flag in (3,5)";
                        countSql += " and m.flag in (3,5)";
                    }
                    else
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckIntZero(model.isXy))
                {
                    sql += " and m.id in (select Buyid from BTZBuyxy)";
                    countSql += " and m.id in (select Buyid from BTZBuyxy)";
                }
                if (!ValidateUtils.CheckIntZero(model.BTZid))
                {
                    param.Add(new DbParameterItem("m.BTZid", ConstUtil.EQ, model.BTZid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (!ValidateUtils.CheckNull(model.province))
                {
                    param.Add(new DbParameterItem("m.province", ConstUtil.LIKE, model.province));
                }
                if (!ValidateUtils.CheckNull(model.city))
                {
                    param.Add(new DbParameterItem("m.city", ConstUtil.LIKE, model.city));
                }
                if (!ValidateUtils.CheckIntZero(model.BuymemType))
                {
                    param.Add(new DbParameterItem("m.BuymemType", ConstUtil.EQ, model.BuymemType));
                }
                if (!ValidateUtils.CheckNull(model.userName))
                {
                    param.Add(new DbParameterItem("m.userName", ConstUtil.LIKE, model.userName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (model.startTimeYysj != null)
                {
                    string date_str = model.startTimeYysj.Value.ToString("yyyy-MM-dd");
                    param.Add(new DbParameterItem("m.Yysj", ConstUtil.DATESRT_LGT_DAY, DateTime.Parse(date_str)));
                }
                if (model.endTimeYysj != null)
                {
                    param.Add(new DbParameterItem("m.Yysj", ConstUtil.DATESRT_EGT_DAY, DateTime.Parse(model.endTimeYysj.Value.ToString("yyyy-MM-dd"))));
                }
                if (model.startTimeQysj != null)
                {
                    param.Add(new DbParameterItem("m.Qysj", ConstUtil.DATESRT_LGT_DAY, model.startTimeQysj));
                }
                if (model.endTimeQysj != null)
                {
                    param.Add(new DbParameterItem("m.Qysj", ConstUtil.DATESRT_EGT_DAY, model.endTimeQysj));
                }
                if (model.startTimeJssj != null)
                {
                    param.Add(new DbParameterItem("m.Jssj", ConstUtil.DATESRT_LGT_DAY, model.startTimeJssj));
                }
                if (model.endTimeJssj != null)
                {
                    param.Add(new DbParameterItem("m.Jssj", ConstUtil.DATESRT_EGT_DAY, model.endTimeJssj));
                }
                if(model.isVirtual!=null)
                {
                    sql += " and m.uid in (select id from Member where isVirtual=" + model.isVirtual + ")";
                    countSql += " and m.uid in (select id from Member where isVirtual=" + model.isVirtual + ")";
                }
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZBuy> list = new List<BTZBuy>();
            Dictionary<string, ParameterSet> param_system = paramBLL.GetDictionaryByCodes("BTZcffksj");//买家会员成功抢养后，须在1小时内给卖家进行
            double BTZcffksj = Convert.ToDouble(param_system["BTZcffksj"].paramValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuy modelrow = (BTZBuy)ReflectionUtil.GetModel(type, row);
                    modelrow.syfksj = "";
                    if (modelrow.Ppsj != null && modelrow.flag==3)
                    {
                        //算出剩余付款时间
                        TimeSpan ts=(modelrow.Ppsj.Value.AddHours(BTZcffksj)-DateTime.Now);
                        modelrow.syfksj = ts.TotalSeconds.ToString();
                        //modelrow.syfksj = ts.Hours.ToString() + "时" + ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒";
                    }
                    if (modelrow.payTime != null)
                    {
                        //算出剩余收款时间
                        TimeSpan ts = (modelrow.payTime.Value.AddHours(BTZcffksj) - DateTime.Now);
                        modelrow.sysksj = ts.TotalSeconds.ToString();
                    }
                    if (modelrow.BuyNo == null || modelrow.BuyNo == "")
                    {
                        string Generate_No = modelrow.addTime.Value.ToString("yyyyMMddHHmm") + GetRandom() + (1001 + modelrow.id).ToString().Substring(0, 4);
                        //生成编号
                        dao.ExecuteBySql("update BTZBuy set BuyNo='" + Generate_No + "' where id=" + modelrow.id.Value);
                        modelrow.BuyNo = Generate_No;
                    }
                    if (modelrow.saleId != null && modelrow.saleId.Value!=0)
                    {
                        BTZSale model_sale = btzsaleBLL.GetModel(modelrow.saleId.Value);
                        if (model_sale.SaleNo == null || model_sale.SaleNo == "")
                        {
                            string Generate_No = model_sale.addTime.Value.ToString("yyyyMMddHHmm") + GetRandom() + (1000 + model_sale.id).ToString().Substring(0, 4);
                            //生成编号
                            dao.ExecuteBySql("update BTZSale set SaleNo='" + Generate_No + "' where id=" + modelrow.saleId.Value);
                            modelrow.SaleNo = Generate_No;
                        }
                        else
                        modelrow.SaleNo = model_sale.SaleNo;
                    }
                  
                    //是否是续养过的
                    DataTable dt_BTZBuyxy = dao.GetList("select * from BTZBuyxy");
                    if (Common.SqlChecker.GetNewDataTable(dt_BTZBuyxy, "Buyid=" + modelrow.id.Value).Rows.Count > 0)
                        modelrow.isXy = 1;
                    else
                        modelrow.isXy = 0;
                    list.Add(modelrow);
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double totalZjz = 0;
            double totaldqZjz = 0;
            double totalSxf = 0;
            int totalJlzs = 0;
            int totalCsrs = 0;
            int totalLyrs = 0;
            int totalPprs = 0;
            string sale_uid_in = "";
            string buy_uid_in = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuy model_row = (BTZBuy)ReflectionUtil.GetModel(type, row);
                    totalJlzs++;
                    try
                    { 
                    totalZjz += model_row.priceOldzz.Value;
                    totaldqZjz += model_row.priceOldzz.Value * (1 + (model_row.RsyBili.Value + model_row.ZcszjBili.Value * model_row.isZcs.Value) * model_row.Hyts.Value / 100);
                    }
                    catch { }
                        totalSxf += model_row.sxf.Value;
                    if (sale_uid_in.IndexOf("," + model_row.saleUid.ToString()) == -1)
                    {
                        totalCsrs++;
                        sale_uid_in += "," + model_row.saleUid.ToString();
                    }
                    if (buy_uid_in.IndexOf("," + model_row.uid.ToString()) == -1)
                    {
                        if (model_row.flag == 3 || model_row.flag == 5 || model_row.flag == 6 || model_row.flag == 8) totalPprs++;
                        totalLyrs++;
                        buy_uid_in += "," + model_row.uid.ToString();
                    }
                }
            }
            BTZBuy foot_model = new BTZBuy();
            list = new List<BTZBuy>();
            foot_model.totalJlzs = totalJlzs;
            foot_model.totalZjz = totalZjz;
            foot_model.totalSxf = totalSxf;
            foot_model.totalCsrs = totalCsrs;
            foot_model.totalLyrs = totalLyrs;
            foot_model.totalPprs = totalPprs;
            foot_model.totaldqZjz = totaldqZjz;
            list.Add(foot_model);
            page.footer = list;

            return page;
        }

        private string GetRandom()
        {
            Random ro = new Random(10);
            long tick = DateTime.Now.Ticks;
            Random ran = new Random((int)(tick & 0xffffffffL) | (int)(tick >> 32));
            int iResult = ran.Next(999999);
            string s = iResult.ToString().PadLeft(4, '0');
            return s;
        }

        public PageResult<BTZBuy> GetListPageJymx(BTZBuy model, string fields)
        {
            dao.ExecuteBySql(@"update BTZBuy
set BTZBuy.phone=Member.phone
from Member
where BTZBuy.uid=Member.id and BTZBuy.phone is null");
            //更新订单分类普通订单还是虚拟会员的订单
            dao.ExecuteBySql(@"update BTZBuy set BuymemType=2
where uid in (select id from Member where isVirtual=1) and BuymemType is null;
update BTZBuy set BuymemType=1 where BuymemType is null;");
            PageResult<BTZBuy> page = new PageResult<BTZBuy>();
            string sql = "select " + fields + @",bsale.SaleNo,row_number() over(order by bsale.flag asc,m.Ppsj desc) rownumber from BTZBuy m
inner join Member mem
on m.saleUid=mem.id
inner join BTZSale bsale
on m.id=bsale.ppBuyid
where 1=1";
            string countSql = @"select count(1) from BTZBuy m inner join Member mem
on m.saleUid=mem.id
inner join BTZSale bsale
on m.id=bsale.ppBuyid
where 1=1";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.id))
                {
                    param.Add(new DbParameterItem("m.id", ConstUtil.EQ, model.id));
                }
                if (!ValidateUtils.CheckIntZero(model.saleId))
                {
                    param.Add(new DbParameterItem("m.saleId", ConstUtil.EQ, model.saleId));
                }
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.BTZid))
                {
                    param.Add(new DbParameterItem("m.BTZid", ConstUtil.EQ, model.BTZid));
                }
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (!ValidateUtils.CheckIntZero(model.BuymemType))
                {
                    param.Add(new DbParameterItem("m.BuymemType", ConstUtil.EQ, model.BuymemType));
                }
                if (!ValidateUtils.CheckIntZero(model.SalememType))
                {
                   if(model.SalememType==1)
                   {
                        sql += " and bsale.uid in (select id from Member where isVirtual=0) ";
                        countSql += " and bsale.uid in (select id from Member where isVirtual=0) ";
                   }
                   if (model.SalememType == 2)
                   {
                       sql += " and bsale.uid in (select id from Member where isVirtual=1) ";
                       countSql += " and bsale.uid in (select id from Member where isVirtual=1) ";
                   }
                }
                if (model.flagNotin != null && model.flagNotin != "")
                {
                    sql += " and m.flag not in (" + model.flagNotin + ")";
                    countSql += " and m.flag not in (" + model.flagNotin + ")";
                }
                if (!ValidateUtils.CheckNull(model.saleuserId))
                {
                    param.Add(new DbParameterItem("m.saleuserId", ConstUtil.LIKE, model.saleuserId));
                }
                if (!ValidateUtils.CheckNull(model.saleuserName))
                {
                    param.Add(new DbParameterItem("m.saleuserName", ConstUtil.LIKE, model.saleuserName));
                }
                if (!ValidateUtils.CheckIntZero(model.saleflag))
                {
                    param.Add(new DbParameterItem("bsale.flag", ConstUtil.EQ, model.saleflag));
                }
               
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("bsale.Cssj", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("bsale.Cssj", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
                if (model.startTimePpsj != null)
                {
                    param.Add(new DbParameterItem("m.Ppsj", ConstUtil.DATESRT_LGT_DAY, model.startTimePpsj));
                }
                if (model.endTimePpsj != null)
                {
                    param.Add(new DbParameterItem("m.Ppsj", ConstUtil.DATESRT_EGT_DAY, model.endTimePpsj));
                }
                
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);
            List<DbParameterItem> param_nopage = param;
            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZBuy> list = new List<BTZBuy>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuy modelrow = (BTZBuy)ReflectionUtil.GetModel(type, row);
                    if (modelrow.saleId != null && modelrow.saleId != 0 && (modelrow.SaleNo==null||modelrow.SaleNo==""))
                    {
                        string Generate_No = modelrow.addTime.Value.ToString("yyyyMMddHHmm") + GetRandom() + (1000+modelrow.id).ToString().Substring(0,4);
                        //生成编号
                        dao.ExecuteBySql("update BTZSale set SaleNo='" + Generate_No + "' where id=" + modelrow.saleId.Value);
                        modelrow.SaleNo = Generate_No;
                    }
                    if (modelrow.BuyNo == null || modelrow.BuyNo == "")
                    {
                        string Generate_No = modelrow.addTime.Value.ToString("yyyyMMddHHmm") + GetRandom() + (1001 + modelrow.id).ToString().Substring(0, 4);
                        //生成编号
                        dao.ExecuteBySql("update BTZBuy set BuyNo='" + Generate_No + "' where id=" + modelrow.id.Value);
                        modelrow.BuyNo = Generate_No;
                    }
                    list.Add(modelrow);
                }
            }
            page.rows = list;

            //汇总已审未审
            dt = dao.GetList(sql, param_nopage, true);
            double totalZjz = 0;
            double totalSxf = 0;
            int totalJlzs = 0;
            int totalCsrs = 0;
            int totalLyrs = 0;
            string sale_uid_in = "";
            string buy_uid_in = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuy model_row = (BTZBuy)ReflectionUtil.GetModel(type, row);
                    totalJlzs++;
                    totalZjz += model_row.priceOldzz.Value;
                    totalSxf += model_row.sxf.Value;
                    if (sale_uid_in.IndexOf("," + model_row.saleUid.ToString()) == -1)
                    {
                        totalCsrs++;
                        sale_uid_in += "," + model_row.saleUid.ToString();
                    }
                    if (buy_uid_in.IndexOf("," + model_row.uid.ToString()) == -1)
                    {
                        totalLyrs++;
                        buy_uid_in += "," + model_row.uid.ToString();
                    }
                }
            }
            BTZBuy foot_model = new BTZBuy();
            list = new List<BTZBuy>();
            foot_model.totalJlzs = totalJlzs;
            foot_model.totalZjz = totalZjz;
            foot_model.totalSxf = totalSxf;
            foot_model.totalCsrs = totalCsrs;
            foot_model.totalLyrs = totalLyrs;
            list.Add(foot_model);
            page.footer = list;

            return page;
        }
        




        public BTZBuy GetModel(int id)
        {
            return this.GetOne("select * from BTZBuy where id = " + id);
        }

        public BTZBuy SaveProduct(BTZBuy mb, Member current)
        {

            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
           
         

            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }
        public int Delete(int id)
        {
            string sql = "delete from BTZBuy where id=" + id;
            return dao.ExecuteBySql(sql);
        }

        public int fk(int id)
        {
            BTZBuy ca=this.GetOne("select * from BTZBuy where id = " + id);
            if (ca.flag != 3) { throw new ValidateException("状态必须为領養成功的才能付款"); }
            //付款记录状态更新
            BTZBuy update = new BTZBuy();
            update.id = id;
            update.flag = 5;
            update.payTime = DateTime.Now;
            dao.Update(update);
            //匹配成功后，买家会员在30分钟内进行：确认付款 操作，将会获得10个靈氣作为奖励
            Dictionary<string, ParameterSet> param_system = paramBLL.GetDictionaryByCodes("qrfkJlfz", "qrfkJllz");
            double qrfkJlfz = Convert.ToDouble(param_system["qrfkJlfz"].paramValue);
            double qrfkJllz = Convert.ToDouble(param_system["qrfkJllz"].paramValue);
            TimeSpan ts=DateTime.Now - ca.Ppsj.Value;
            if (ts.TotalMinutes < qrfkJlfz)
            {
                MemberAccount act = accountBLL.GetModel(ca.uid.Value);
                //10个靈氣作为奖励
                MemberAccount sub = new MemberAccount();
                sub.id = ca.uid;
                sub.agentLz = qrfkJllz;
                accountBLL.UpdateAdd(sub);

                //流水帐
                LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
                fromLs.income = qrfkJllz;
                fromLs.outlay = 0;
                fromLs.addtime = DateTime.Now;
                fromLs.uid = ca.uid;
                fromLs.userId = ca.userId;
                fromLs.tableName = "BTZBuyfk";
                fromLs.addUid = ca.uid;
                fromLs.addUser = ca.userId;
                fromLs.accountId = ConstUtil.JOURNAL_LZ;
                fromLs.abst =qrfkJlfz+ "分钟内确认付款奖励-";
                fromLs.last = act.agentLz.Value + qrfkJllz;
                liushuiBLL.Save(fromLs);
            }
           return 1;
        }

        public int fk(BTZBuy model)
        {
            BTZBuy ca = this.GetOne("select * from BTZBuy where id = " + model.id);
            if (ca.flag != 3) { throw new ValidateException("状态必须为領養成功的才能付款"); }
            //付款记录状态更新
            dao.Update(model);
            //匹配成功后，买家会员在30分钟内进行：确认付款 操作，将会获得10个靈氣作为奖励
            Dictionary<string, ParameterSet> param_system = paramBLL.GetDictionaryByCodes("qrfkJlfz", "qrfkJllz");
            double qrfkJlfz = Convert.ToDouble(param_system["qrfkJlfz"].paramValue);
            double qrfkJllz = Convert.ToDouble(param_system["qrfkJllz"].paramValue);
            TimeSpan ts = DateTime.Now - ca.Ppsj.Value;
            if (ts.TotalMinutes < qrfkJlfz)
            {
                MemberAccount act = accountBLL.GetModel(ca.uid.Value);
                //10个靈氣作为奖励
                MemberAccount sub = new MemberAccount();
                sub.id = ca.uid;
                sub.agentLz = qrfkJllz;
                accountBLL.UpdateAdd(sub);

                //流水帐
                LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
                fromLs.income = qrfkJllz;
                fromLs.outlay = 0;
                fromLs.addtime = DateTime.Now;
                fromLs.uid = ca.uid;
                fromLs.userId = ca.userId;
                fromLs.tableName = "BTZBuyfk";
                fromLs.addUid = ca.uid;
                fromLs.addUser = ca.userId;
                fromLs.accountId = ConstUtil.JOURNAL_LZ;
                fromLs.abst = qrfkJlfz + "分钟内确认付款奖励-";
                fromLs.last = act.agentLz.Value + qrfkJllz;
                liushuiBLL.Save(fromLs);
            }
            return 1;
        }

        public int cd(int id)
        {
            BTZBuy ca = this.GetOne("select * from BTZBuy where id = " + id);
            if (ca.flag == 6 || ca.flag == 8) { throw new ValidateException("交易已完成不能撤单"); }
            //付款记录状态更新为9解除交易
            BTZBuy update = new BTZBuy();
            update.id = id;
            update.flag = 2;
            update.Qysj = DateTime.Now;
            update.saleId = 0;
            update.saleUid = 0;
            update.saleuserId = "";
            update.saleuserName = "";
            dao.Update(update);
            
            //出售记录待转让
            BTZSale updatesale = btzsaleBLL.GetModel(ca.saleId.Value);
            //本次撤单人员如果是虚拟会员，则卖单明天再匹配
            if (memberBLL.GetModelById(ca.uid.Value).isVirtual.Value == 1) updatesale.canPptime = updatesale.canPptime.Value.AddDays(1);
            updatesale.flag = 1;
            updatesale.Ppzz = 0;
            updatesale.ppBuyid = 0;
            btzsaleBLL.Update(updatesale);

            return 1;
        }

        public int sk(int id)
        {
            if (id == null || id == 0) { throw new ValidateException("购买记录不能为空"); }
            BTZBuy ca = this.GetOne("select * from BTZBuy where id = " + id);
            if (ca.flag== 3) { throw new ValidateException("买家还未付款"); }
            if (ca.flag == 6) { throw new ValidateException("此单已确认收款了请不要重复确认"); }
            if (ca.flag != 5) { throw new ValidateException("状态必须为已付款的才能收款"); }
            //赠送DOGE数量,赠送BTD数量
            BTZ modelbtz = btzBLL.GetModel(ca.BTZid.Value);
            //付款记录状态更新
            BTZBuy update = new BTZBuy();
            update.id = id;
            update.flag = 6;
            update.confirmPayTime = DateTime.Now;
            //所有会员的买入时间统一：当天日期+该猪品的：抢养开始时间
            if(ca.Kssj==null)
            {
            update.Kssj = modelbtz.Ksqysj;
            update.Jssj = update.Kssj.Value.AddDays(ca.Hyts.Value);
            }
            dao.Update(update);
            //出售记录
            BTZSale updatesale = btzsaleBLL.GetModel(ca.saleId.Value);
            updatesale.flag = 3;
            btzsaleBLL.Update(updatesale);

            //其它出现别的卖单也匹配给这个买单的更新一下
            dao.ExecuteBySql("update BTZSale set flag=1,ppBuyid=0 where ppBuyid=" + id + " and id!=" + ca.saleId);
           
            MemberAccount act = accountBLL.GetModel(ca.uid.Value);
            MemberAccount add = new MemberAccount();
            add.id = ca.uid.Value;
            add.agentBTT = modelbtz.ZsBTT;
            accountBLL.UpdateAdd(add);
            LiuShuiZhang toLs = new LiuShuiZhang();   //转入流水
            toLs.outlay = 0;
            toLs.income = modelbtz.ZsBTT;
            toLs.addtime = DateTime.Now;
            toLs.tableName = "BTZBuy";
            toLs.addUid = ca.uid;
            toLs.addUser = ca.userId;
            toLs.accountId = ConstUtil.JOURNAL_BTT;     //BTT
            toLs.abst = "领养络绎阁成功完成支付赠送-" + ca.BTZName;
            toLs.last = act.agentBTT.Value + modelbtz.ZsBTT.Value;
            toLs.sourceId = ca.id;
            toLs.uid = ca.uid;
            toLs.userId = ca.userId;
            liushuiBLL.Save(toLs);

            add = new MemberAccount();
            add.id = ca.uid.Value;
            add.agentBTD = modelbtz.ZsBTD;
            accountBLL.UpdateAdd(add);
            toLs = new LiuShuiZhang();   //转入流水
            toLs.outlay = 0;
            toLs.income = modelbtz.ZsBTD;
            toLs.addtime = DateTime.Now;
            toLs.tableName = "BTZBuy";
            toLs.addUid = ca.uid;
            toLs.addUser = ca.userId;
            toLs.accountId = ConstUtil.JOURNAL_BTD;     //BTD
            toLs.abst = "领养络绎阁成功完成支付赠送-" + ca.BTZName;
            toLs.last = act.agentBTD.Value + modelbtz.ZsBTD.Value;
            toLs.sourceId = ca.id;
            toLs.uid = ca.uid;
            toLs.userId = ca.userId;
            liushuiBLL.Save(toLs);
            //更新会员络绎阁持有数量
            dao.ExecuteBySql("update Member set BTZsl=(select COUNT(1) from BTZBuy where uid=" + ca.uid.Value + " and flag=6) where id="+ca.uid.Value);
            //领养成功城市奖
            dao.ExecuteBySql("exec csjBonus " + ca.uid.Value);
            return 1;
        }

        public int xy(int id)
        {
            BTZBuy ca = this.GetOne("select * from BTZBuy where id = " + id);
            BTZ btz_model = btzBLL.GetModel(ca.BTZid.Value);
            if (ca.flag != 6) { throw new ValidateException("状态必须为已确认付款的才能续养"); }
            if (ca.Jssj < DateTime.Now) { throw new ValidateException("当前日期必须小于区块猪的结束日期"); }
            BTZ modelbtz = btzBLL.GetModel(ca.BTZid.Value);
            //当天该区块猪品种续养记录的数量 < 区块猪发布时所填写的：每天续养人数；
            if (dao.GetList("SELECT * FROM BTZBuyxy where DateDiff(dd,addTime,getdate())=0 and BTZid=" + ca.BTZid.Value).Rows.Count >= modelbtz.Mtxyrs.Value) 
            {
                string msg = "续养名额已满";
                if(modelbtz.Mtxyrs.Value>0) msg = "当天续养人数不能超过" + modelbtz.Mtxyrs.Value;
                throw new ValidateException(msg); 
            }
            //到期后的总价值（含领养收益）< 该区块猪价值 在平台中第1笔出售记录所出售的价值 时
            double firstZzSale = 0;
            DataTable dt_BTZSale = dao.GetList("select top 1 * from BTZSale where BTZid=" + ca.BTZid.Value + " order by id asc");
            if (dt_BTZSale.Rows.Count > 0) firstZzSale = double.Parse(dt_BTZSale.Rows[0]["Cszz"].ToString());
            double dqzz=ca.priceOldzz.Value*(100+(ca.RsyBili.Value+ca.isZcs.Value*ca.ZcszjBili.Value)*ca.Hyts.Value)/100;
           
            //同一络绎阁买入记录只能续养1次，续养相当于重新领养了一次该络绎阁，该扣的靈氣手续费，该得的BTT、BTD均要获得；只是要为该络绎阁写入一条：续养记录
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("BTZtymrjlxycs", "BTZxyzjzBei");
            int BTZtymrjlxycs = Convert.ToInt32(param["BTZtymrjlxycs"].paramValue);
            double BTZxyzjzBei = Convert.ToDouble(param["BTZxyzjzBei"].paramValue);
            if (dqzz >= firstZzSale * BTZxyzjzBei) { throw new ValidateException("到期区块猪价值必须小于平台中第1笔出售记录所出售的价值" + firstZzSale + "-倍数-" + BTZxyzjzBei); }



            DataTable dt_BTZBuyxy = dao.GetList("select  * from BTZBuyxy where Buyid=" + id);
            if (dt_BTZBuyxy.Rows.Count >= BTZtymrjlxycs) { throw new ValidateException("同一络绎阁买入记录只能续养次数：" + BTZtymrjlxycs); }
            MemberAccount act = accountBLL.GetModel(ca.uid.Value);
            if (act.agentLz.Value < btz_model.Jqsxlz) { throw new ValidateException("靈氣余额不够，需要" + btz_model.Jqsxlz); }
            BTZBuyxy bxy = new BTZBuyxy();
            bxy.addTime = DateTime.Now;
            bxy.BTZid = ca.BTZid;
            bxy.Buyid = id;
            bxy.Jssj = ca.Jssj.Value.AddDays(btz_model.Hyts.Value);
            bxy.Kssj = ca.Jssj;
            bxy.sxf = btz_model.Jqsxlz;
            bxy.uid = ca.uid;
            bxy.ZsBTD = btz_model.ZsBTD;
            bxy.ZsBTT = btz_model.ZsBTT;
            bxy.Hyts = btz_model.Hyts;
            bxy.RsyBili = btz_model.RsyBili;
            bxy.ZcszjBili = btz_model.ZcszjBili;
            dao.SaveByIdentity(bxy);

            //付款记录状态更新
            BTZBuy update = new BTZBuy();
            update.id = id;
            update.Hyts = ca.Hyts.Value + btz_model.Hyts.Value;
            update.RsyBili = btz_model.RsyBili;
            update.ZcszjBili = btz_model.ZcszjBili;
            update.Jssj = ca.Jssj.Value.AddDays(btz_model.Hyts.Value);
            dao.Update(update);
            
            //扣手续费
            MemberAccount sub = new MemberAccount();
            sub.id = ca.uid;
            sub.agentLz = btz_model.Jqsxlz;
            accountBLL.UpdateSub(sub);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.income = 0;
            fromLs.outlay = btz_model.Jqsxlz;
            fromLs.addtime = DateTime.Now;
            fromLs.uid = ca.uid;
            fromLs.userId = ca.userId;
            fromLs.tableName = "BTZBuyxy";
            fromLs.addUid = ca.uid;
            fromLs.addUser = ca.userId;
            fromLs.accountId = ConstUtil.JOURNAL_LZ;
            fromLs.abst = "续养络绎阁扣除-"+ca.BTZName;
            fromLs.last = act.agentLz.Value - btz_model.Jqsxlz;
            liushuiBLL.Save(fromLs);
          
            //赠送DOGE数量,赠送BTD数量
           
          
            MemberAccount add = new MemberAccount();
            add.id = ca.uid.Value;
            add.agentBTT = modelbtz.ZsBTT;
            accountBLL.UpdateAdd(add);
            LiuShuiZhang toLs = new LiuShuiZhang();   //转入流水
            toLs.outlay = 0;
            toLs.income = modelbtz.ZsBTT;
            toLs.addtime = DateTime.Now;
            toLs.tableName = "BTZBuy";
            toLs.addUid = ca.uid;
            toLs.addUser = ca.userId;
            toLs.accountId = ConstUtil.JOURNAL_BTT;     //BTT
            toLs.abst = "续养络绎阁赠送-" + ca.BTZName;
            toLs.last = act.agentBTT.Value + modelbtz.ZsBTT.Value;
            toLs.sourceId = ca.id;
            toLs.uid = ca.uid;
            toLs.userId = ca.userId;
            liushuiBLL.Save(toLs);

            add = new MemberAccount();
            add.id = ca.uid.Value;
            add.agentBTD = modelbtz.ZsBTD;
            accountBLL.UpdateAdd(add);
            toLs = new LiuShuiZhang();   //转入流水
            toLs.outlay = 0;
            toLs.income = modelbtz.ZsBTD;
            toLs.addtime = DateTime.Now;
            toLs.tableName = "BTZBuy";
            toLs.addUid = ca.uid;
            toLs.addUser = ca.userId;
            toLs.accountId = ConstUtil.JOURNAL_BTD;     //BTD
            toLs.abst = "续养络绎阁赠送-" + ca.BTZName;
            toLs.last = act.agentBTD.Value + modelbtz.ZsBTD.Value;
            toLs.sourceId = ca.id;
            toLs.uid = ca.uid;
            toLs.userId = ca.userId;
            liushuiBLL.Save(toLs);

            return 1;
        }

       
    }
}
