﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class LoginHistoryBLL : ILoginHistoryBLL
    {

        private System.Type type = typeof(LoginHistory);
        public IBaseDao dao { get; set; }

        public int Save(LoginHistory lg)
        {
            if (lg == null) { throw new ValidateException("登录日志,保存内容为空"); }
            if (ValidateUtils.CheckNull(lg.userId) || ValidateUtils.CheckIntZero(lg.uid)) { throw new ValidateException("登录日志,用户为空"); }
            object o = dao.SaveByIdentity(lg);
            return Convert.ToInt32(o);
        }


        public PageResult<LoginHistory> GetListPage(LoginHistory model, Member current)
        {
            PageResult<LoginHistory> page = new PageResult<LoginHistory>();
            string sql = "select m.*,b.userName,row_number() over(order by m.id desc) rownumber from LoginHistory m inner join Member b on m.uid = b.id where 1=1 ";
            string countSql = "select count(1) from LoginHistory m inner join Member b on m.uid = b.id where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //如果不是超级管理员不能查询超级管理员的登录日志
            if (ConstUtil.SUPER_ADMIN != current.userId)
            {
                param.Add(new DbParameterItem("m.userId", ConstUtil.NEQ, ConstUtil.SUPER_ADMIN));
            }

            //查询条件
            if (model != null)
            {
                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
                if (model.flag != null && model.flag>=0)
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.loginTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.loginTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
            }
            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<LoginHistory> list = new List<LoginHistory>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((LoginHistory)ReflectionUtil.GetModel(type, row));
                }
            }
            page.rows = list;
            return page;
        }

    }
}
