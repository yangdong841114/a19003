﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class JqrzrBLL : BaseBLL<Jqrzr>, IJqrzrBLL
    {

        private System.Type type = typeof(Jqrzr);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }

        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }
        public IJqrBLL jqrBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }
        public new Jqrzr GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Jqrzr mb = (Jqrzr)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public PageResult<Jqrzr> GetListPage(Jqrzr model, string fields)
        {
            PageResult<Jqrzr> page = new PageResult<Jqrzr>();
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from Jqrzr m where 1=1 ";
            string countSql = "select count(1) from Jqrzr m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {
                if (model.uid!=null)
                {
                    sql += " and (m.fromUid=" + model.uid + " or m.toUid="+ model.uid + ")";
                    countSql += " and (m.fromUid=" + model.uid + " or m.toUid=" + model.uid + ")";
                }
                if (!ValidateUtils.CheckNull(model.fromUserId))
                {
                    param.Add(new DbParameterItem("m.fromUserId", ConstUtil.LIKE, model.fromUserId));
                }
                if (!ValidateUtils.CheckNull(model.fromUserName))
                {
                    param.Add(new DbParameterItem("m.fromUserName", ConstUtil.LIKE, model.fromUserName));
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Jqrzr> list = new List<Jqrzr>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Jqrzr modelrow = (Jqrzr)ReflectionUtil.GetModel(type, row);
                    modelrow.fromPhone = memberBLL.GetModelById(modelrow.fromUid.Value).phone;
                    modelrow.toPhone = memberBLL.GetModelById(modelrow.toUid.Value).phone;
                    list.Add(modelrow);
                }
            }
            page.rows = list;
            return page;
        }




        public Jqrzr Save(Jqrzr mb, Member current)
        {
            DateTime gmTime = DateTime.Now;
            DataTable dt_Jqr=dao.GetList("select * from Jqr where isStop=0 and dqTime>getdate() and id=" + mb.Jqrid.Value);
            if (dt_Jqr.Rows.Count == 0) { throw new ValidateException("当前机器人已失效或过期"); }
            if (dt_Jqr.Rows.Count > 0) gmTime = DateTime.Parse(dt_Jqr.Rows[0]["gmTime"].ToString());//机器人购买时间

            Member memTouser = memberBLL.GetModelByphone(mb.toUserId);
            if (memTouser == null) { throw new ValidateException("转入账户不存在"); }
            if (memTouser.id.Value == current.id.Value) { throw new ValidateException("自己不能转给自己"); }
            //转入者不能有机器人
            if (dao.GetList("select * from Jqr where isStop=0 and dqTime>getdate() and uid=" + memTouser.id.Value).Rows.Count > 0) { throw new ValidateException("转入人已有一个有效机器人"); }

            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("Jqrzrts");//10天才能转一次
            int Jqrzrts = int.Parse(param["Jqrzrts"].paramValue);
            Jqrzr last_zr = this.GetOne("select top 1 * from Jqrzr where fromUid=" + current.id.Value + " order by addTime desc");
            if (last_zr!=null)
            {
                TimeSpan ts=DateTime.Now-last_zr.addTime.Value;
                if (ts.TotalDays < Jqrzrts) throw new ValidateException(Jqrzrts+"天才能转一次"); 
            }
            if (gmTime!= null)
            {
                TimeSpan ts = DateTime.Now - gmTime;
                if (ts.TotalDays < Jqrzrts) throw new ValidateException(Jqrzrts + "天才能转一次");
            }
            
            mb.addTime = DateTime.Now;
            mb.fromUid = current.id;
            mb.fromUserId = current.userId;
            mb.fromUserName = current.userName;
            mb.toUid = memTouser.id;
            mb.toUserId = memTouser.userId;
            mb.toUserName = memTouser.userName;
            //保存
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //转出者机器人变为失效
             Jqr jqrUpdate=new Jqr();
            jqrUpdate.id= mb.Jqrid.Value;
            jqrUpdate.isStop=1;
            jqrBLL.Update(jqrUpdate);
            //转入者收到一个一样的机器人
            Jqr jqrAdd = jqrBLL.GetModel(mb.Jqrid.Value);
            jqrAdd.id = null;
            jqrAdd.uid = memTouser.id;
            jqrAdd.userId = memTouser.userId;
            jqrAdd.userName = memTouser.userName;
            jqrAdd.isStop = 0;
            jqrAdd.ly = "转让";
            dao.SaveByIdentity(jqrAdd);
            return mb;
        }

       

       
    }
}
