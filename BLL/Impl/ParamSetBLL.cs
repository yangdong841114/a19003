﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class ParamSetBLL : BaseBLL<ParameterSet>, IParamSetBLL
    {
        private System.Type type = typeof(ParameterSet);
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }


        public DataTable GetDataTable(string sql)
        {
            return dao.GetList(sql);
        }

        public int ExecSql(string sql)
        {
            return dao.ExecuteBySql(sql);
        }

        public new ParameterSet GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param,true);
            if (row == null) return null;
            ParameterSet mb = (ParameterSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new ParameterSet GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            ParameterSet mb = (ParameterSet)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<ParameterSet> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<ParameterSet> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count>0)
            {
                list = new List<ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ParameterSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<ParameterSet> GetList(string sql)
        {
            List<ParameterSet> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((ParameterSet)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public Dictionary<string, ParameterSet> GetAllToDictionary()
        {
            Dictionary<string, ParameterSet> rr = null;
            DataTable dt = dao.GetList("select * from parameterSet");
            if (dt != null && dt.Rows.Count > 0)
            {
                rr = new Dictionary<string, ParameterSet>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    ParameterSet pr = (ParameterSet)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.paramCode, pr);
                }
            }
            return rr;
        }

        public int UpdateList(List<ParameterSet> list)
        {
            DataTable dt = dao.GetList("select * from parameterSet");
            if(list==null || list.Count==0){
                throw new ValidateException("要保存的内容为空");
            }
            int cfnew_cffs = 0;
            int cfnew_BTZid = 0;
            foreach(ParameterSet ps in list){
                if (ps.paramCode == "cfnew_cffs") cfnew_cffs = int.Parse(ps.paramValue);
                if (ps.paramCode == "cfnew_BTZid") cfnew_BTZid = int.Parse(ps.paramValue);

                if(ValidateUtils.CheckNull(ps.paramValue))
                {
                 
                    if (ps.paramCode == "userIdPrefix")
                    {
                        ps.paramValue = "";
                    }
                    else
                    {
                        throw new ValidateException(ps.paramCode + "：的值为空");
                    }
                }
                DataRow[] rows = dt.Select("paramCode='"+ps.paramCode+"'");
                if(rows==null || rows.Length==0){
                    throw new ValidateException(ps.paramCode+"没有找到数据");
                }
                rows[0]["paramValue"] = ps.paramValue;
            }
            //设置的拆分份数须<=最高级别猪的合约天数，否则给出提示：拆分份数须<=最高级别猪的合约天数
            DataTable dt_BTZ=dao.GetList("select * from BTZ where 1=1 order by jLevel desc");
            int Hyts_Zgjb = int.Parse(dt_BTZ.Rows[0]["Hyts"].ToString());//最高级别猪的合约天数
            if (cfnew_cffs>Hyts_Zgjb) throw new ValidateException("拆分份数须<=最高级别猪的合约天数" + Hyts_Zgjb);
            double Gzzzsx = double.Parse(dt_BTZ.Rows[0]["Gzzzsx"].ToString());//最高级别猪的价值上限
            double cfzz = Gzzzsx/cfnew_cffs;//参数拆分后的价值
            //须在：拆分后猪的级别 的规则价值上、下限范围内
            dt_BTZ = dao.GetList("select * from BTZ where id=" + cfnew_BTZid + " and Gzzzxx<=" + cfzz + " and Gzzzsx>=" + cfzz);
            if (dt_BTZ.Rows.Count == 0) throw new ValidateException("拆分后猪的级别 的规则价值" + cfzz+" 不在指定级别猪的上下限中");

            List<DbParameterItem> param = ParamUtil.Get()
                .Add(new DbParameterItem("paramValue", DbType.String, null))
                .Add(new DbParameterItem("paramCode",DbType.String,null))
                .Result();
            string sql = "update ParameterSet set paramValue=@paramValue where paramCode=@paramCode";
            DataSet set = new DataSet();
            set.Tables.Add(dt);
            dt.TableName = "ParameterSet";
            return dao.UpdateBatchByDataSet(set, "ParameterSet", sql, param);
        }

        public Dictionary<string, ParameterSet> GetDictionaryByCodes(params string[] paramCode)
        {
            List<string> list = new List<string>();
            for (int i = 0; i < paramCode.Length; i++)
            {
                list.Add(paramCode[i]);
            }
            return GetToDictionary(list);
        }

        public Dictionary<string, ParameterSet> GetToDictionary(List<string> paramCodeList)
        {
            Dictionary<string, ParameterSet> rr = new Dictionary<string, ParameterSet>();
            string sql = "select * from ParameterSet ";
            if (paramCodeList != null && paramCodeList.Count>0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (string s in paramCodeList)
                {
                    sb.Append("'").Append(s).Append("',");
                }
                sql += " where paramCode in (" + sb.ToString().Substring(0, sb.Length - 1) + ")";
            }
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    ParameterSet pr = (ParameterSet)ReflectionUtil.GetModel(type, row);
                    rr.Add(pr.paramCode, pr);
                }
            }
            return rr;
        }
    }
}
