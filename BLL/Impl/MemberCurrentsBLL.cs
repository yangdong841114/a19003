﻿using DAO;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Impl
{
    public class MemberCurrentsBLL : BaseBLL<MemberCurrents>, IMemberCurrentsBLL
    {
        public IBaseDao dao { get; set; }
        public override IBaseDao GetDao()
        {
            return dao;
        }
        public int SaveMemberCurrents(MemberCurrents model)
        {
            object o = this.SaveByIdentity(model);
            int id = Convert.ToInt32(o);
            return id;
        }
    }
}
