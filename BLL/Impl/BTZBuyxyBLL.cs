﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class BTZBuyxyBLL : BaseBLL<BTZBuyxy>, IBTZBuyxyBLL
    {

        private System.Type type = typeof(BTZBuyxy);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new BTZBuyxy GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            BTZBuyxy mb = (BTZBuyxy)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new BTZBuyxy GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            BTZBuyxy mb = (BTZBuyxy)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<BTZBuyxy> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<BTZBuyxy> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZBuyxy>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((BTZBuyxy)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<BTZBuyxy> GetList(string sql)
        {
            List<BTZBuyxy> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<BTZBuyxy>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuyxy model = (BTZBuyxy)ReflectionUtil.GetModel(type, row);
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<BTZBuyxy> GetListPage(BTZBuyxy model, string fields)
        {
            PageResult<BTZBuyxy> page = new PageResult<BTZBuyxy>();
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from BTZBuyxy m where 1=1 ";
            string countSql = "select count(1) from BTZBuyxy m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }
                if (!ValidateUtils.CheckIntZero(model.Buyid))
                {
                    param.Add(new DbParameterItem("m.Buyid", ConstUtil.EQ, model.Buyid));
                }
               
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<BTZBuyxy> list = new List<BTZBuyxy>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    BTZBuyxy modelrow = (BTZBuyxy)ReflectionUtil.GetModel(type, row);
                   
                    list.Add(modelrow);
                }
            }
            page.rows = list;
            return page;
        }




        public BTZBuyxy GetModel(int id)
        {
            return this.GetOne("select * from BTZBuyxy where id = " + id);
        }

        public BTZBuyxy SaveProduct(BTZBuyxy mb, Member current)
        {
            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
           
            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            return mb;
        }
        public int Delete(int id)
        {
            string sql = "delete from BTZBuyxy where id=" + id;
            return dao.ExecuteBySql(sql);
        }

       
    }
}
