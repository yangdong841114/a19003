﻿using Common;
using DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Model;

namespace BLL.Impl
{
    public class AxdbjlBLL : BaseBLL<Axdbjl>, IAxdbjlBLL
    {

        private System.Type type = typeof(Axdbjl);
        public IBaseDao dao { get; set; }
        public IParamSetBLL paramBLL { get; set; }
        public IMemberBLL memberBLL { get; set; }

        public ILiuShuiZhangBLL liushuiBLL { get; set; }

        public IMemberAccountBLL accountBLL { get; set; }

        public override IBaseDao GetDao()
        {
            return dao;
        }

        public new Axdbjl GetOne(string sql, List<Common.DbParameterItem> param)
        {
            DataRow row = dao.GetOne(sql, param, true);
            if (row == null) return null;
            Axdbjl mb = (Axdbjl)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new Axdbjl GetOne(string sql)
        {
            DataRow row = dao.GetOne(sql);
            if (row == null) return null;
            Axdbjl mb = (Axdbjl)ReflectionUtil.GetModel(type, row);
            return mb;
        }

        public new List<Axdbjl> GetList(string sql, List<Common.DbParameterItem> param)
        {
            List<Axdbjl> list = null;
            DataTable dt = dao.GetList(sql, param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Axdbjl>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    list.Add((Axdbjl)ReflectionUtil.GetModel(type, row));
                }
            }
            return list;
        }

        public new List<Axdbjl> GetList(string sql)
        {
            List<Axdbjl> list = null;
            DataTable dt = dao.GetList(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = new List<Axdbjl>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Axdbjl model = (Axdbjl)ReflectionUtil.GetModel(type, row);
                    list.Add(model);
                }
            }
            return list;
        }



        public PageResult<Axdbjl> GetListPage(Axdbjl model, string fields)
        {
            PageResult<Axdbjl> page = new PageResult<Axdbjl>();
            string sql = "select " + fields + ",row_number() over(order by m.id desc) rownumber from Axdbjl m where 1=1 ";
            string countSql = "select count(1) from Axdbjl m where 1=1 ";
            List<DbParameterItem> param = new List<DbParameterItem>();
            //查询条件
            if (model != null)
            {

                if (!ValidateUtils.CheckIntZero(model.uid))
                {
                    param.Add(new DbParameterItem("m.uid", ConstUtil.EQ, model.uid));
                }

                if (model.dbxqs!=null&&!ValidateUtils.CheckIntZero(model.dbxqs))
                {
                    param.Add(new DbParameterItem("m.dbxqs", ConstUtil.EQ, model.dbxqs));
                }
               
                if (!ValidateUtils.CheckNull(model.userId))
                {
                    param.Add(new DbParameterItem("m.userId", ConstUtil.LIKE, model.userId));
                }
              
                if (!ValidateUtils.CheckIntZero(model.flag))
                {
                    param.Add(new DbParameterItem("m.flag", ConstUtil.EQ, model.flag));
                }
                if(model.flagIn!=null&&model.flagIn!="")
                {
                    sql += " and m.flag in (" + model.flagIn + ")";
                    countSql += " and m.flag in (" + model.flagIn + ")";
                }
                if (model.startTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_LGT_DAY, model.startTime));
                }
                if (model.endTime != null)
                {
                    param.Add(new DbParameterItem("m.addTime", ConstUtil.DATESRT_EGT_DAY, model.endTime));
                }
              
            }

            //查询记录条数
            page.total = dao.GetCount(countSql, param, true);

            int strnum = (model.page.Value - 1) * model.rows.Value + 1; //开始条数
            int endnum = model.page.Value * model.rows.Value;          //截至条数

            param.Add(new DbParameterItem("strnum", null, strnum));
            param.Add(new DbParameterItem("endnum", null, endnum));

            DataTable dt = dao.GetPageList(sql, param);
            List<Axdbjl> list = new List<Axdbjl>();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    Axdbjl modelrow = (Axdbjl)ReflectionUtil.GetModel(type, row);
                   
                    list.Add(modelrow);
                }
            }
            page.rows = list;
            return page;
        }

        public string GetSqKjInfo(int dbxqs)
        {
            //最近获奖期数
            DataTable dt_Axdbjl = dao.GetList(@"select * from Axdbjl where flag=3 order by dbxqs desc");
            if (dt_Axdbjl.Rows.Count > 0) dbxqs = int.Parse(dt_Axdbjl.Rows[0]["dbxqs"].ToString());
            string Info = @" <ul class='winlistcont'>";
            dt_Axdbjl = dao.GetList(@"select distinct Member.userId,Member.userName,Member.phone,sum(getLz) as zjje from Axdbjl,Member
where Axdbjl.uid=Member.id
and Axdbjl.flag=3 and Axdbjl.dbxqs="+dbxqs+@"
group by Member.userId,Member.userName,Member.phone");

            for (int i = 0; i < dt_Axdbjl.Rows.Count; i++)
            {
            string phone= dt_Axdbjl.Rows[i]["phone"].ToString();
            phone=phone.Substring(0,3)+"****"+phone.Substring(7,4);
            Info +=@" <li>
                    <div class='winlcont'>
                        <ul class='tga3'>
                            <li>"+ dt_Axdbjl.Rows[i]["userId"].ToString()+"</li>"
                            +"<li>"+phone+"</li>"
                            +"<li><span>"+dt_Axdbjl.Rows[i]["zjje"].ToString()+@"</span></li>
                        </ul>
                        <div class='clear'></div>
                    </div>
                </li>";
            }
            Info += " </ul>";
            return Info;
        }


        public Axdbjl GetModel(int id)
        {
            return this.GetOne("select * from Axdbjl where id = " + id);
        }

        public Axdbjl SaveProductAdmin(Axdbjl mb, Member current)
        {
            BaseSet modelBaseSet = (BaseSet)ReflectionUtil.GetModel(typeof(BaseSet), dao.GetOne("select * from BaseSet where id = 1"));
            mb.uid = current.id;
            mb.userId = current.userId;
            mb.userName = current.userName;
            mb.flag = 4;
            mb.addTime = DateTime.Now;
            mb.getLz = 0;
            mb.dbxqs = modelBaseSet.dbxqs;
            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;
            return mb;
        }

        public Axdbjl SaveProduct(Axdbjl mb, Member current)
        {
            BaseSet modelBaseSet = (BaseSet)ReflectionUtil.GetModel(typeof(BaseSet), dao.GetOne("select * from BaseSet where id = 1"));
            if (modelBaseSet.axdbJssz.Value < DateTime.Now) { throw new ValidateException("夺宝已结束结算中"); }
            if (modelBaseSet.dbkg == 0) { throw new ValidateException("夺宝未开启"); }
            //非空校验
            if (mb == null) { throw new ValidateException("保存内容为空"); }
            mb.uid = current.id;
            mb.userId = current.userId;
            mb.userName = current.userName;
            mb.flag = 1;
            //管理员后台注资不参与开奖
            if(current.isAdmin == 1)
            mb.flag = 2;
            mb.addTime = DateTime.Now;
            Dictionary<string, ParameterSet> param = paramBLL.GetDictionaryByCodes("Axdbxmclz", "Axdbxmcms");
            double Axdbxmclz = Convert.ToDouble(param["Axdbxmclz"].paramValue); //每参与一次压宝会员需支付1靈氣进入平台
            int Axdbxmcms = Convert.ToInt32(param["Axdbxmcms"].paramValue); //会员每参与一次夺宝，倒计时增加60秒
            //管理员后台注资自由金额
            if (current.isAdmin!=1)
            mb.trLz = Axdbxmclz;
            mb.getLz = 0;
            mb.dbxqs = modelBaseSet.dbxqs;

            MemberAccount act = accountBLL.GetModel(current.id.Value);
            if (act.agentLz.Value < Axdbxmclz) { throw new ValidateException("靈氣余额不够，需要" + Axdbxmclz); }

            //保存商品
            object o = dao.SaveByIdentity(mb);
            int newId = Convert.ToInt32(o);
            mb.id = newId;

            //扣手续费
            MemberAccount sub = new MemberAccount();
            sub.id = current.id;
            sub.agentLz = Axdbxmclz;
            accountBLL.UpdateSub(sub);

            //流水帐
            LiuShuiZhang fromLs = new LiuShuiZhang(); //转出流水
            fromLs.income = 0;
            fromLs.outlay = Axdbxmclz;
            fromLs.addtime = DateTime.Now;
            fromLs.uid = current.id;
            fromLs.userId = current.userId;
            fromLs.tableName = "Axdbjl";
            fromLs.addUid = current.id;
            fromLs.addUser = current.userId;
            fromLs.accountId = ConstUtil.JOURNAL_LZ;
            fromLs.abst = "爱心夺宝";
            fromLs.last = act.agentLz.Value - Axdbxmclz;
            liushuiBLL.Save(fromLs);
            dao.ExecuteBySql("update BaseSet set bqtzrs=bqtzrs+1,  axdbAccount=axdbAccount+" + Axdbxmclz + " ,jclz=jclz+" + Axdbxmclz + " ,axdbJssz=DATEADD(ss," + Axdbxmcms.ToString() + ",axdbJssz)");
            return mb;
        }
        public int Delete(int id)
        {
            string sql = "delete from Axdbjl where id=" + id;
            return dao.ExecuteBySql(sql);
        }

      
       
    }
}
