﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    /// <summary>
    /// 业务逻辑接口
    /// </summary>
    public interface IBTZjscityBLL : IBaseBLL<BTZjscity>
    {
       
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<BTZjscity> GetListPage(BTZjscity model, string fields);
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        BTZjscity Save(BTZjscity model, Member current);

        int Delete(int id);
    }
}
