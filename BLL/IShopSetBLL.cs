﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 设置报单中心业务逻辑接口
    /// </summary>
    public interface IShopSetBLL : IBaseBLL<ShopSet>
    {
        /// <summary>
        /// 保存设置报单中心记录
        /// </summary>
        /// <param name="model">实体对象</param>
        /// <param name="current">当前会员</param>
        /// <returns></returns>
        ShopSet SaveShopSet(ShopSet model, Member current);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <param name="fields">要查询的列</param>
        /// <returns></returns>
        PageResult<ShopSet> GetListPage(ShopSet model);

        /// <summary>
        /// 开通报单中心
        /// </summary>
        /// <param name="memberId">要开通的会员ID</param>
        /// <param name="current">当前操作会员</param>
        /// <returns></returns>
        int OpenAgent(int memberId, Member current);
        int Lock(int id);
        /// <summary>
        /// 删除报单中心申请
        /// </summary>
        /// <param name="memberId">会员ID</param>
        /// <returns></returns>
        int DeleteAgent(int memberId);
        int CancelAgent(int memberId);
        /// <summary>
        /// 批量启用冻结报单中心
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        int UpdateLock(List<int> list, int isLock);

        /// <summary>
        /// 申请报单中心
        /// </summary>
        /// <param name="uid">用户ID</param>
        /// <returns></returns>
        int InsertApplyShop(int uid, string provinceAgent, string cityAgent);
    }
}
