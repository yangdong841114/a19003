﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    /// <summary>
    /// 机器人业务逻辑接口
    /// </summary>
    public interface IJqrzrBLL : IBaseBLL<Jqrzr>
    {
       
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<Jqrzr> GetListPage(Jqrzr model, string fields);
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        Jqrzr Save(Jqrzr model, Member current);

       

    }
}
