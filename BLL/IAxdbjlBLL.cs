﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 络绎阁业务逻辑接口
    /// </summary>
    public interface IAxdbjlBLL : IBaseBLL<Axdbjl>
    {
       
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<Axdbjl> GetListPage(Axdbjl model, string fields);
        //获取某期开奖信息
        string GetSqKjInfo(int dbxqs);
      
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        Axdbjl SaveProduct(Axdbjl model, Member current);
        Axdbjl SaveProductAdmin(Axdbjl model, Member current);
       
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);
        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Axdbjl GetModel(int id);
    }
}
