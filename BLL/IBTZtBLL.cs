﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace BLL
{
    /// <summary>
    /// 络绎阁业务逻辑接口
    /// </summary>
    public interface IBTZBLL : IBaseBLL<BTZ>
    {
       
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<BTZ> GetListPage(BTZ model, string fields);
        PageResult<BTZ> GetListPageAll(BTZ model, string fields);
        DataTable GetTable(string sql);
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        BTZ SaveProduct(BTZ model, Member current);

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        BTZ UpdateProduct(BTZ model, Member current);

        int BTZJqr(int BTZid, Member current);

        string isCg(int BTZid, Member current);

        int BTZyy(int BTZid, Member current);
        int BTZyyvip(int BTZid, Member current);
        int ly(int BTZid, Member current);

        string GetJqrBTZid(Member current);
        int IsVidyy(int BTZid, Member current);
        int Isyy(int BTZid, Member current);
        int Isly(int BTZid, Member current);
        /// <summary>
        /// 上架
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveShelve(int id);

        /// <summary>
        /// 商品下架
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int SaveCancelShelve(int id);

        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);
        int startPp(int id);
        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BTZ GetModel(int id);
    }
}
