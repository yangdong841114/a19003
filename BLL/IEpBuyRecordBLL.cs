﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// EP购买记录业务逻辑接口
    /// </summary>
    public interface IEpBuyRecordBLL : IBaseBLL<EpBuyRecord>
    {

        /// <summary>
        /// 分页查询记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<EpBuyRecord> GetListPage(EpBuyRecord model);

        /// <summary>
        /// 导出EXCEL
        /// </summary>
        /// <param name="model">查询条件</param>
        /// <returns></returns>
        DataTable GetExportList(EpBuyRecord model);

        /// <summary>
        /// 分页查询状态记录
        /// </summary>
        /// <param name="model">查询条件对象</param>
        /// <returns></returns>
        PageResult<EpBuyStatus> GetSatusListPage(EpBuyStatus model);

        /// <summary>
        /// 保存购买记录
        /// </summary>
        /// <param name="model">保存的记录</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveRecord(EpBuyRecord model, Member current);

        /// <summary>
        /// 保存批量购买记录
        /// </summary>
        /// <param name="list">记录列表</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveRecordList(List<EpBuyRecord> list, Member current);

        /// <summary>
        /// 取消购买
        /// </summary>
        /// <param name="id">取消的记录ID</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveCancel(int id,Member current);

        /// <summary>
        /// 买家确认付款
        /// </summary>
        /// <param name="model">付款信息</param>
        /// <param name="current">当前操作人</param>
        /// <param name="isAdmin">是否管理员，管理员不需要上传凭证</param>
        /// <returns></returns>
        int SavePay(EpBuyRecord model, Member current,bool isAdmin);

        /// <summary>
        /// 批量付款
        /// </summary>
        /// <param name="model">付款记录列表</param>
        /// <param name="current">当前操作人</param>
        /// <param name="isAdmin">是否管理员，管理员不需要上传凭证</param>
        /// <returns></returns>
        int SaveBatchPay(List<EpBuyRecord> list, Member current, bool isAdmin);

        /// <summary>
        /// 卖家确认收款
        /// </summary>
        /// <param name="model">确认信息</param>
        /// <param name="current">当前操作人</param>
        /// <returns></returns>
        int SaveSurePay(EpBuyRecord model, Member current);

    }
}
