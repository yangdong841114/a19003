﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 络绎阁业务逻辑接口
    /// </summary>
    public interface IBTZBuyBLL : IBaseBLL<BTZBuy>
    {
       
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<BTZBuy> GetListPage(BTZBuy model, string fields);
        PageResult<BTZBuy> GetListPageJymx(BTZBuy model, string fields);
        
      
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        BTZBuy SaveProduct(BTZBuy model, Member current);

       
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);
        int fk(BTZBuy model);
        int fk(int id);
        int sk(int id);
        int cd(int id);
        int xy(int id);
        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BTZBuy GetModel(int id);
    }
}
