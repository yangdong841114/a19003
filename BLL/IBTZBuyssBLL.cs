﻿using Common;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    /// <summary>
    /// 络绎阁业务逻辑接口
    /// </summary>
    public interface IBTZBuyssBLL : IBaseBLL<BTZBuyss>
    {
       
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="model">查询参数对象</param>
        /// <param name="fields">因该表cont字段较大，分页查询时不应查询出返回到前端</param>
        /// <returns></returns>
        PageResult<BTZBuyss> GetListPage(BTZBuyss model, string fields);
        
      
        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <returns></returns>
        BTZBuyss SaveProduct(BTZBuyss model, Member current);

       
        /// <summary>
        /// 删除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        int Delete(int id);
        int qx(int id);
        int jc(int id);
        int jcjy(int id);
        void hh(int id,string hhcont);
        /// <summary>
        /// 根据ID查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BTZBuyss GetModel(int id);
    }
}
