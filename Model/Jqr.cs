﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 机器人表实体
    /// </summary>
    [Serializable]
    public class Jqr : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }
        //购买靈氣
        public virtual double? Lzsl { get; set; }
        //购买时间
        public virtual DateTime? gmTime { get; set; }
        //到期时间
        public virtual DateTime? dqTime { get; set; }
        //机器人开启时间
        public virtual DateTime? kqTime { get; set; }
        //有效期
        public virtual int? yxq { get; set; }
        //是否停用
        public virtual int? isStop { get; set; }
        //来源
        public virtual string ly { get; set; }
        public virtual int? BTZid { get; set; }
        public virtual string BTZid_in { get; set; }
       
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        //累计出售机器人总数
        public virtual int? ljcszs { get; set; }
        //有效机器伙总数
        public virtual int? yxzs { get; set; }
        //累计出售机器人总价值
        public virtual double? ljcszjz { get; set; }

    }
}
