﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 络绎阁表实体
    /// </summary>
    [Serializable]
    public class BTZ : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //编号
        public virtual string BTZCode { get; set; }
        //名称
        public virtual string BTZName { get; set; }
        //开始抢养时间
        public virtual DateTime? Ksqysj { get; set; }
        public virtual string KsqysjStr { get; set; }
        public virtual DateTime? Jsqysj { get; set; }
        public virtual string JsqysjStr { get; set; }
        //抢养时间倒计时
        public virtual double? Qydjs { get; set; }
        public virtual double? Qyjsdjs { get; set; }
        //预约时间倒计时
        public virtual double? Yydjs { get; set; }
        //抢养持续时间
        public virtual double? Qycxsj { get; set; }
        //预约截止时间
        public virtual DateTime? Yyjzsj { get; set; }
        public virtual string  YyjzsjStr { get; set; }
        //预约 领养所需靈氣
        public virtual double? Yysxlz { get; set; }
        //即抢 领养所需靈氣
        public virtual double? Jqsxlz { get; set; }
        //合约天数
        public virtual int? Hyts { get; set; }
        //日收益比例
        public virtual double? RsyBili { get; set; }
        //再出售增加利息比例
        public virtual double? ZcszjBili { get; set; }
        //赠送DOGE数量
        public virtual double? ZsBTT { get; set; }
        //赠送BTD数量
        public virtual double? ZsBTD { get; set; }
        //图片url
        public virtual string imgUrl { get; set; }
        //平台每日发行价值额度上限
        public virtual double? Rfxesx { get; set; }
        //规则价值下限
        public virtual double? Gzzzxx { get; set; }
        //规则价值上限
        public virtual double? Gzzzsx { get; set; }
        //展示价值下限
        public virtual double? Zszzxx { get; set; }
        //展示价值上限
        public virtual double? Zszzsx { get; set; }
        //拆份阀值倍数
        public virtual double? Cffzbs { get; set; }
        //拆份阀值份数
        public virtual int? Cffzfs { get; set; }
        //每天续养人数
        public virtual int? Mtxyrs { get; set; }
        //级别
        public virtual int? jLevel { get; set; }
        //班级
        public virtual int? bLevel { get; set; }
        //自动出售比例
        public virtual double? ZdcsBili { get; set; }
        //当天领养比例
        public virtual double? Lybili { get; set; }
        //添加时间
        public virtual DateTime? addTime { get; set; }
        //匹配开始时间
        public virtual DateTime? Ppkssj { get; set; }
        public virtual DateTime? Ppjssj { get; set; }
        public virtual double? Ppdjs { get; set; }
        public virtual double? Ppjsdjs { get; set; }

        public virtual string isHavejqr { get; set; }
        public virtual string isVipyy { get; set; }
        public virtual string isyy { get; set; }
        public virtual string isly { get; set; }
       
        //是否上架  1：否，2：是
        public virtual int? isShelve { get; set; }
        //是否审核1：否，2：是
        public virtual int? flag { get; set; }
      
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

    }
}
