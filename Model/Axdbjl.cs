﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 络绎阁表实体
    /// </summary>
    [Serializable]
    public class Axdbjl : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户姓名
        public virtual string userName { get; set; }
        //状态1抢夺中2错失宝藏3夺得宝藏
        public virtual int? flag { get; set; }
        //增加时间
        public virtual DateTime? addTime { get; set; }
        //投入靈氣
        public virtual double? trLz { get; set; }
        //夺得靈氣
        public virtual double? getLz { get; set; }
        public virtual int? dbxqs { get; set; }



        public virtual string flagIn { get; set; }
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

    }
}
