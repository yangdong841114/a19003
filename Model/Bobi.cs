﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 拨比查询实体
    /// </summary>
    [Serializable]
    public class Bobi : Page, DtoData
    {
        public virtual DateTime? jstime { get; set; }

        public virtual double? income { get; set; }

        public virtual double? outlay { get; set; }

        public virtual double? profit { get; set; }

        public virtual double? rate { get; set; }

        public virtual string bili { get; set; }
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
       
    }
}
