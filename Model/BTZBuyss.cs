﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 络绎阁表实体
    /// </summary>
    [Serializable]
    public class BTZBuyss : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //卖单ID
        public virtual int? saleId { get; set; }
        //买单ID
        public virtual int? Buyid { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户姓名
        public virtual string userName { get; set; }
        //手机
        public virtual string phone { get; set; }
        //状态1已申诉2已取消
        public virtual int? flag { get; set; }
        //增加时间
        public virtual DateTime? addTime { get; set; }
        //络绎阁原始价值
        public virtual double? priceOldzz { get; set; }
        //求购单编号
        public virtual string BuyNo { get; set; }
        public virtual string SaleNo { get; set; } 
        //卖方用户ID
        public virtual int? saleUid { get; set; }
        //卖方帐号
        public virtual string saleuserId { get; set; }
        //卖方姓名
        public virtual string saleuserName { get; set; }
        //卖方手机
        public virtual string salephone { get; set; }

        public virtual string cont { get; set; }
        public virtual string hhcont { get; set; }
        public virtual string imgUrlss { get; set; }
        public virtual string ssType { get; set; }
        //是否申诉状态
        public virtual int? isSsstate { get; set; }
        //是否解除交易
        public virtual int? isJcjy { get; set; }  

        public virtual string isCanqx { get; set; }
       

        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

    }
}
