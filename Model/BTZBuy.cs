﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 络绎阁表实体
    /// </summary>
    [Serializable]
    public class BTZBuy : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户姓名
        public virtual string userName { get; set; }
        public virtual string phone { get; set; }
        //编号
        public virtual string BTZCode { get; set; }
        //名称
        public virtual string BTZName { get; set; }
        //状态1已预约2已申请领养3領養成功4匹配失败5已付款6已确认付款7付款超时领养失败8已出售9解除交易
        public virtual int? flag { get; set; }
        public virtual string flagNotin { get; set; }
        //络绎阁ID
        public virtual int? BTZid { get; set; }
        //手续费
        public virtual double? sxf { get; set; }
        //操作人
        public virtual string Czr { get; set; }
        //预约时间
        public virtual DateTime? Yysj { get; set; }
        //抢养时间
        public virtual DateTime? Qysj { get; set; }
        //匹配时间
        public virtual DateTime? Ppsj { get; set; }
        //增加时间
        public virtual DateTime? addTime { get; set; }
        //络绎阁原始价值
        public virtual double? priceOldzz { get; set; }
        //络绎阁当前价值
        public virtual double? priceCurrentzz { get; set; }
        //是否虚拟会员做为卖家
        public virtual int? isVirtual { get; set; }
        //合约天数
        public virtual int? Hyts { get; set; }
        //开始计算价值时间
        public virtual DateTime? Kssj { get; set; }
        //结束计算价值时间
        public virtual DateTime? Jssj { get; set; }
        public virtual DateTime? Cfsj { get; set; }
        //日收益比例
        public virtual double? RsyBili { get; set; }
        //再出售增加利息比例
        public virtual double? ZcszjBili { get; set; }
        //是否再出售
        public virtual int? isZcs { get; set; }
        //预约VIP时间
        public virtual DateTime? YyVipsj { get; set; }
        //机器人开启时间
        public virtual DateTime? Jqrkqsj { get; set; }
        //卖单ID
        public virtual int? saleId { get; set; }
        //卖方用户ID
        public virtual int? saleUid { get; set; }
        //卖方帐号
        public virtual string saleuserId { get; set; }
        //卖方姓名
        public virtual string saleuserName { get; set; }
        //支付时间
        public virtual DateTime? payTime { get; set; }
        //支付凭证图片
        public virtual string imgUrl { get; set; }
        //确认支付时间
        public virtual DateTime? confirmPayTime { get; set; }
        //支付类型
        public virtual string payType { get; set; }
        //是否已发短信通知
        public virtual int? isSendsms { get; set; }
        //赠送DOGE数量
        public virtual double? ZsBTT { get; set; }
        //赠送BTD数量
        public virtual double? ZsBTD { get; set; }
        //求购单编号
        public virtual string BuyNo { get; set; }
        public virtual string SaleNo { get; set; }
        //订单分类：1普通会员订单2虚拟会员订单
        public virtual int? BuymemType { get; set; }
        public virtual int? SalememType { get; set; }
        public virtual string ppLog { get; set; }
      
        //冗余字段
        //是否续养
        public virtual int? isXy { get; set; }
        
        //手机号码
        public virtual string salephone { get; set; }
        //开户行
        public string bankName { get; set; }
        //银行卡号
        public string bankCard { get; set; }
        //开户支行（地址） 
        public string bankAddress { get; set; }
        //开户名
        public string bankUser { get; set; }
        //省
        public virtual string province { get; set; }
        //市
        public virtual string city { get; set; }
        //支付方式
        public virtual int? skIsbank { get; set; }
        public virtual int? skIszfb { get; set; }
        public virtual int? skIswx { get; set; }
        public virtual int? skIsszhb { get; set; }
        public string szhbmc { get; set; }
        public string szhbmc1 { get; set; }
        public string imgUrlzfb { get; set; }
        public string imgUrlwx { get; set; }
        public string imgUrlszhb { get; set; }
        public string imgUrlszhb1 { get; set; }
        public string BTTaddress { get; set; }
        public string ETHaddress { get; set; }
        public virtual DateTime? Cssj { get; set; }
        //状态1待转让2已转让3交易完成
        public virtual int? saleflag { get; set; }
        //出售总价值
        public virtual double? Cszz { get; set; }
        //已转让价值
        public virtual double? Ppzz { get; set; }

        //匹配次数
        public virtual int? Ppcs { get; set; }

        //汇总字段
        //出售人数
        public virtual int? totalCsrs { get; set; }
        //领养人数
        public virtual int? totalLyrs { get; set; }
        //匹配人数
        public virtual int? totalPprs { get; set; }
        //记录总数
        public virtual int? totalJlzs { get; set; }
        //总价值
        public virtual double? totalZjz { get; set; }
        //到期总价值
        public virtual double? totaldqZjz { get; set; }
        //手续费总额
        public virtual double? totalSxf { get; set; }
        //二级密码
        public virtual string passOpen { get; set; }
        //剩余付款时间
        public virtual string syfksj { get; set; }
        //剩余收款时间
        public virtual string sysksj { get; set; }
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
         public virtual DateTime? startTimeYysj { get; set; }
        public virtual DateTime? endTimeYysj { get; set; }
        public virtual DateTime? startTimeQysj { get; set; }
        public virtual DateTime? endTimeQysj { get; set; }
        public virtual DateTime? startTimeJssj { get; set; }
        public virtual DateTime? endTimeJssj { get; set; }
        public virtual DateTime? startTimePpsj { get; set; }
        public virtual DateTime? endTimePpsj { get; set; }
        

        

        

    }
}
