﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 禁售城市实体
    /// </summary>
    [Serializable]
    public class BTZjscity : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //省
        public virtual string province { get; set; }
        //市
        public virtual string city { get; set; }
        //区
        public virtual string area { get; set; }

    }
}
