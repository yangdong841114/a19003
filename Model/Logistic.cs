﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Model
{
    /// <summary>
    /// 物流商
    /// </summary>
    [Serializable]
    public class Logistic : Page, DtoData
    {
        //物流商编号
        public string logNo { get; set; }

        //物流商名称
        public string logName { get; set; }

        //同步时间
        public DateTime? addTime { get; set; }

    }
}
