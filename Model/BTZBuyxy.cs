﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 络绎阁表实体
    /// </summary>
    [Serializable]
    public class BTZBuyxy : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        public virtual int? Buyid { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
      
        //络绎阁ID
        public virtual int? BTZid { get; set; }
        //手续费
        public virtual double? sxf { get; set; }
        //合约天数
        public virtual int? Hyts { get; set; }
        //日收益比例
        public virtual double? RsyBili { get; set; }
        //再出售增加利息比例
        public virtual double? ZcszjBili { get; set; }
       
        //增加时间
        public virtual DateTime? addTime { get; set; }
        
        //开始计算价值时间
        public virtual DateTime? Kssj { get; set; }
        //结束计算价值时间
        public virtual DateTime? Jssj { get; set; }
        
        //赠送DOGE数量
        public virtual double? ZsBTT { get; set; }
        //赠送BTD数量
        public virtual double? ZsBTD { get; set; }
       
      
      
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

    }
}
