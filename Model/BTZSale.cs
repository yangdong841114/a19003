﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 络绎阁表实体
    /// </summary>
    [Serializable]
    public class BTZSale : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户姓名
        public virtual string userName { get; set; }
        //编号
        public virtual string BTZCode { get; set; }
        //名称
        public virtual string BTZName { get; set; }
        //状态1待转让2已转让3交易完成
        public virtual int? flag { get; set; }
        //络绎阁ID
        public virtual int? BTZid { get; set; }
        //源自那个求购记录转化而来ID
        public virtual int? fromBuyid { get; set; }
        //匹配求购记录ID
        public virtual int? ppBuyid { get; set; }
        public virtual string ppBuyNo { get; set; }
        public virtual string SaleNo { get; set; }
        //出售时间
        public virtual DateTime? Cssj { get; set; }
        //增加时间
        public virtual DateTime? addTime { get; set; }

        //出售总价值
        public virtual double? Cszz { get; set; }
        //已转让价值
        public virtual double? Ppzz { get; set; }
        
        //匹配次数
        public virtual int? Ppcs { get; set; }

        public virtual string salephone { get; set; }
        public virtual string Czr { get; set; }
        //账户类型
        public virtual int? accountId { get; set; }

        //冗余字段
        public virtual int? buyuid { get; set; }
        //用户编码
        public virtual string buyuserId { get; set; }
        //用户姓名
        public virtual string buyuserName { get; set; }
        public virtual string buyuserNameHide { get; set; }
        public virtual string buyphone { get; set; }
        //支付类型
        public virtual string payType { get; set; }
        public virtual int? isVirtual { get; set; }
        public virtual int? issyzBTZ { get; set; }
        //支付时间
        public virtual DateTime? payTime { get; set; }
        //支付凭证图片
        public virtual string imgUrl { get; set; }
        //确认支付时间
        public virtual DateTime? confirmPayTime { get; set; }
        //来源：领养或拆分
        public virtual string lyStatus { get; set; }
        public virtual string lyBTZCode { get; set; }
        //名称
        public virtual string lyBTZName { get; set; }
        //络绎阁ID
        public virtual int? lyBTZid { get; set; }
        //可以匹配时间
        public virtual DateTime? canPptime { get; set; }

        public virtual string BuyflagNotIn { get; set; }

        //汇总字段
        //出售人数
        public virtual int? totalCsrs { get; set; }
        //领养人数
        public virtual int? totalLyrs { get; set; }
        //记录总数
        public virtual int? totalJlzs { get; set; }
        //总价值
        public virtual double? totalZjz { get; set; }
        //手续费总额
        public virtual double? totalSxf { get; set; }

        //剩余收款时间
        public virtual string sysksj { get; set; }
        //剩余付款时间
        public virtual string syfksj { get; set; }
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }
        //匹配时间
        public virtual DateTime? Ppsj { get; set; }
    }
}
