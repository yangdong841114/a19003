﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 账户金额修改表
    /// </summary>
    [Serializable]
    public class AccountUpdate : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }
        //修改金额
        public virtual double? epoints { get; set; }
        //账户类型
        public virtual int? accountType { get; set; }
        //增加或减少，0：增加，1：减少
        public virtual int? subType { get; set; }
        //写入时间
        public virtual DateTime? addTime { get; set; }
        //操作人ID
        public virtual int? addUid { get; set; }
        //操作人名称
        public virtual string addUser { get; set; }  
        //备注
        public virtual string remark { get; set; }
        //是否减为负数
        public virtual string isFs { get; set; }  

        //查询冗余
        public virtual DateTime? startTime { get; set; }

        public virtual DateTime? endTime { get; set; }
    }
}
