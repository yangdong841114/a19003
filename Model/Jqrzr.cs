﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 转账表实体
    /// </summary>
    [Serializable]
    public class Jqrzr : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        public virtual int? uid { get; set; }
        //转出用户ID
        public virtual int? fromUid { get; set; }
        //转出用户编码
        public virtual string fromUserId { get; set; }
        //转出用户名称
        public virtual string fromUserName { get; set; }
        public virtual string fromPhone { get; set; }
        //转入用户ID
        public virtual int? toUid { get; set; }
        //转入用户编码
        public virtual string toUserId { get; set; }
        //传入用户名称
        public virtual string toUserName { get; set; }
        public virtual string toPhone { get; set; }
        
        //转账时间
        public virtual DateTime? addTime { get; set; }
        //状态：机器人ID
        public virtual int? Jqrid { get; set; }
        

        //查询 条件冗余字段start--------------------------------------------------------
        public virtual DateTime? startTime { get; set; }
        public virtual DateTime? endTime { get; set; }

        //查询 条件冗余字段end--------------------------------------------------------
       
    }
}
