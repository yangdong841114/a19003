﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 设置报单中心表实体
    /// </summary>
    [Serializable]
    public class ShopSet : Page, DtoData
    {
        //主键
        public virtual int? id { get; set; }
        //用户ID
        public virtual int? uid { get; set; }
        //用户编码
        public virtual string userId { get; set; }
        //用户名称
        public virtual string userName { get; set; }
        //设置时间
        public virtual DateTime? addTime { get; set; }
        //创建人ID
        public virtual int? createId { get; set; }
        //创建人编码
        public virtual string createUser { get; set; }
        //省
        public virtual string provinceAgent { get; set; }
        //市
        public virtual string cityAgent { get; set; }
        //城市奖金额
        public virtual double? Csjje { get; set; }
        //累计城市奖封顶
        public virtual double? Csjfd { get; set; }

    }
}
