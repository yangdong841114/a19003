﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    /// <summary>
    /// 用户账户表实体
    /// </summary>
    [Serializable]
    public class MemberAccount : Page,DtoData
    {
        //用户ID
        public virtual int? id { get; set; }
        //电子币
        public virtual double? agentDz { get; set; }
        //奖金币
        public virtual double? agentJj { get; set; }
        //购物币
        public virtual double? agentGw { get; set; }
        //复投
        public virtual double? agentFt { get; set; }
        //累计奖金
        public virtual double? agentTotal { get; set; }
        //靈氣
        public virtual double? agentLz { get; set; }
        //BTT
        public virtual double? agentBTT { get; set; }
        //BTD
        public virtual double? agentBTD { get; set; }
        //推荐收益
        public virtual double? agentTjsy { get; set; }
        //团队奖
        public virtual double? agentTdj { get; set; }
        //城市奖
        public virtual double? agentCsj { get; set; }
        //络绎阁
        public virtual double? agentBtz { get; set; }
        public virtual double? agentLysy { get; set; } 
        public virtual double? agentLockBouns { get; set; }

        //冗余
        public virtual string userId { get; set; }
        public virtual string userName { get; set; }
       
    }
}
