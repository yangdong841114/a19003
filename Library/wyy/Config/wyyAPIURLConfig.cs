﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public  class wyyAPIURLConfig
{
    /// <summary>
    /// 设置频道为录制状态
    /// </summary>
    public static string setAlwaysRecord = "https://vcloud.163.com/app/channel/setAlwaysRecord";
    /// <summary>
    /// 设置视频录制回调地址
    /// </summary>
    public static string setcallback = "https://vcloud.163.com/app/record/setcallback";
    /// <summary>
    /// 视频文件信息编辑
    /// </summary>
    public static string editVideo = "https://vcloud.163.com/app/vod/video/edit";
    /// <summary>
    /// 删除视频文件
    /// </summary>
    public static string videoDelete = "https://vcloud.163.com/app/vod/video/videoDelete";
    /// <summary>
    /// 获取视频文件信息
    /// </summary>
    public static string getVideo = "https://vcloud.163.com/app/vod/video/get";
}
