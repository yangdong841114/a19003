﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

public class URLData
{

    public URLData()
    {

    }
    /// <summary>
    /// 根据Key生成value字符串
    /// </summary>
    /// <param name="info">信息数据</param>
    /// <param name="paramsRequest">生成字符串的key值</param>
    /// <param name="paramsDecode">需要专门转码的值</param>
    /// <returns></returns>
    public static string getUrlData(Dictionary<string,string> info, string[] paramsRequest, string[] paramsDecode)
    {
        //返回结果
        string result = "";

        //循环生成信息
        foreach (string param in paramsRequest)
        {

            if (paramsDecode.Contains(param))
            {
                result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + HttpUtility.UrlEncode(info[param], Encoding.GetEncoding("gb2312")));
            }
            else
            {
                result += result.Equals("") ? (param + "=" + info[param]) : ("&" + param + "=" + info[param]);
            }



        }

        return result;

    }

    /// <summary>
    /// 将NameValueCollection  转成   Dictionary<string,string>
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public static Dictionary<string,string> changDictionary(NameValueCollection info)
    {
        //返回结果
        Dictionary<string, string> result = new Dictionary<string, string>();

        foreach (string key in info.Keys)
        {
             result.Add(key, info[key]);
        }

        return result;
    }


    /// <summary>
    /// 将string类型的字符串转化成Dictionary(网银)
    /// </summary>
    /// <param name="str"></param>
    /// <param name="paramNames"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    public static Dictionary<string, string> changDictionary(string str, string[] paramNames,Encoding type)
    {
        //返回结果
        Dictionary<string, string> result = new Dictionary<string, string>();
        foreach (string param in paramNames)
        {
            result.Add(param, GetQueryString(param,str,'=','&', type));
        }
       
        return result;
    }

    /// <summary>
    /// 将string类型的字符串转化成Dictionary
    /// </summary>
    /// <param name="str">含有数据信息的字符串</param>
    /// <param name="paramNames">需要的key的数组</param>
    /// <param name="type">编码格式</param>
    /// <param name="strSplitChar">键值对之间的分隔符</param>
    /// <param name="valueSplitChar">键值之间的分割符</param>
    /// <returns></returns>
    public static Dictionary<string, string> changDictionary(string str, string[] paramNames, Encoding type, char valueSplitChar, char strSplitChar)
    {
        //返回结果
        Dictionary<string, string> result = new Dictionary<string, string>();
        foreach (string param in paramNames)
        {
            result.Add(param, GetQueryString(param, str, valueSplitChar, strSplitChar, type));
        }

        return result;
    }

    /// <summary>
    /// 将Dictionary<string, string>转化成string
    /// </summary>
    /// <param name="info"></param>
    /// <returns></returns>
    public static string toStringDictionary(Dictionary<string, string> info)
    {
        //返回结果
        StringBuilder sb = new StringBuilder();
       
        //循环生成
        foreach (string key in info.Keys)
        {
            sb.Append("["+key+","+info[key]+"]  ");
        }


        return sb.ToString();

    }


    /// <summary>
    /// 转化成前台信息输出
    /// </summary>
    /// <param name="info">输出的信息</param>
    /// <returns></returns>
    public static string toFormatDictionary(Dictionary<string, string> info)
    {
        //返回结果
        StringBuilder sb = new StringBuilder();

        //循环生成
        foreach (string key in info.Keys)
        {
            sb.Append("  [" + key + "]" + "："+info[key]+"\n");
        }


        return sb.ToString();

    }

    /// <summary>
    /// 将类似于：param1=value1&param2=value2的信息串，进行分割，获取。
    /// </summary>
    /// <param name="getParaName">变量名</param>
    /// <param name="requestUrl">数据信息</param>
    /// <param name="valueSplitChar">变量与值得分割符</param>
    /// <param name="strSplitChar">数据间分隔符</param>
    /// <param name="type">原编码格式</param>
    /// <returns></returns>
    public static string GetQueryString(string getParaName, string requestUrl, char valueSplitChar, char strSplitChar, Encoding type)
    {

        string result = "";

        string[] strUrlArg = requestUrl.Split(strSplitChar);

        for (int i = 0; i < strUrlArg.Length; i++)
        {
            if (strUrlArg[i].IndexOf(getParaName + valueSplitChar) >= 0)
            {
                result = System.Web.HttpUtility.UrlDecode(strUrlArg[i].Split(valueSplitChar)[1],type);
                break;
            }
        }
        return result;
    }

    /// <summary>
    /// 一键支付请求
    /// </summary>
    /// <param name="requestURL">地址</param>
    /// <param name="requestdata">数据</param>
    /// <param name="post">是否为post</param>
    /// <returns></returns>
    public static string instantPayRequest(string requestURL, string requestdata, bool post)
    {
        string responseStr = "";

        if (post)
        {
            responseStr = HttpUtil.HttpPost(requestURL, requestdata);
        }
        else
        {
            responseStr = HttpUtil.HttpGet(requestURL, requestdata);
        }
        return responseStr;
    }


}
